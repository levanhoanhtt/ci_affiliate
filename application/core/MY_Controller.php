<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(function_exists('date_default_timezone_set')) date_default_timezone_set('Asia/Bangkok');
        $user = $this->Musers->get(1);$this->session->set_userdata('user', $user);
    }

    protected function commonData($user, $title, $data = array()){
        $data['user'] = $user;
        $data['title'] = $title;
        $data['listActions'] = $this->Mactions->getByUserId($user['UserId']);
        $data['appTypeId'] = $this->Mconstants->getAppTypeId();
        return $data;
    }

    protected function checkUserLogin($isApi = false){
        $user = $this->session->userdata('user');
        if($user){
            $appTypeId = ADMIN_TYPE_ID;// $this->Mconstants->getAppTypeId();
            $flag = false;
            if($appTypeId == ADMIN_TYPE_ID){
                $statusId = $this->Musers->getFieldValue(array('UserId' => $user['UserId']), 'StatusId', 0);
                if($statusId == STATUS_ACTIVED) return $user;
                else $flag = true;
            }
            else{
                $this->load->model('Mcustomers');
                $statusId = $this->Mcustomers->getFieldValue(array('CustomerId' => $user['CustomerId']), 'StatusId', 0);
                if($statusId == STATUS_ACTIVED) return $user;
                else $flag = true;
            }
            if($flag){
                $fields = array('user', 'configs');
                foreach($fields as $field) $this->session->unset_userdata($field);
                if($isApi) echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                else redirect('admin?redirectUrl='.current_url());
                die();
            }
        }
        else{
            if($isApi) echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            else redirect('admin?redirectUrl='.current_url());
            die();
        }
    }

    protected function loadModel($models = array()){
        foreach($models as $model) $this->load->model($model);
    }

    protected function arrayFromPost($fields) {
        $data = array();
        foreach ($fields as $field) $data[$field] = trim($this->input->post($field));
        return $data;
    }

    protected function arrayFromGet($fields) {
        $data = array();
        foreach ($fields as $field) $data[$field] = trim($this->input->get($field));
        return $data;
    }

    protected function sendMail($emailFrom, $nameFrom, $emailTo, $subject, $message){
        $this->load->library('email');
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from($emailFrom, $nameFrom);
        $this->email->to($emailTo);
        $this->email->subject($subject);
        $this->email->message($message);
        if($this->email->send()) return true;
        return false;
    }
}