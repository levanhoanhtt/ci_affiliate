<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'cookie';
$route['404_override'] = '';//'user/permission';
$route['translate_uri_dashes'] = FALSE;
//===============================================================
$route['A(:any)'] = 'cookie/A$1';
$route['admin'] = 'user';
$route['roleaction/(:num)/(:num)'] = 'roleaction/index/$1/$2';
$route['groupaction/(:num)'] = 'groupaction/index/$1';
$route['category/(:num)'] = 'category/index/$1';
$route['article/(:num)'] = 'article/index/$1';
$route['order/(:num)'] = 'order/index/$1';
$route['transaction/(:num)'] = 'transaction/index/$1';
$route['transactioninternal/(:num)'] = 'transactioninternal/index/$1';
$route['remind/(:num)/(:num)'] = 'remind/index/$1/$2';
$route['remind/(:num)'] = 'remind/index/$1';
//$route['sentence/(:num)'] = 'sentence/index/$1';

//========== Ci_ricky Route =========
/*$route['san-pham.html'] = 'site/allProduct';
$route['san-pham-trang-(:num).html'] = 'site/allProduct/$1';
$route['tim-kiem.html'] = 'site/search';
$route['gio-hang.html'] = 'site/cart';
//$route['(:any)-p(:num).html'] = 'site/detailProduct/$1/$2';
$route['products/(:any)'] = 'site/detailProduct/$1';
$route['pages/(:any)'] = 'site/page/$1';
$route['(:any)-c(:num).html'] = 'site/categoryProduct/$1/$2';
$route['(:any)-c(:num)-trang-(:num).html'] = 'site/categoryProduct/$1/$2/$3';*/