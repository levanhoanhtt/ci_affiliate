<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconstants extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //USER
    public $roles = array(
        1 => 'Quản lý',
        2 => 'CTV'
    );
    /*public $workTypes = array(
        1 => 'PART TIME',
        2 => 'FULL TIME'
    );*/
    //CUSTOMER
    public $customerTypes = array(
        1 => 'Cá nhân',
        2 => 'Công ty'
    );

    public $customerKinds = array(
        1 => 'Khách lẻ',
        2 => 'Khách buôn',
        3 => 'Khách CTV'
    );

    public $discountType = array(
        1 => '5%',
        2 => '8%',
        3 => '10%',
        4 => '15%',
        5 => '20%',
        6 => '30%',
    );

    public $paymentTime = array(
        1 => '1 tháng',
        2 => '2 tháng'
    );

    //STORE
    public $storeTypes = array(
        1 => 'Kho',
        2 => 'Cửa hàng',
        3 => 'Kho + Cửa hàng'
    );

    //SUPPLIER
    /*public $supplierTypes = array(
        1 => 'Công ty',
        2 => 'Cá nhân',
    );*/

    //product
    public $productStatus = array(
        2 => 'Đang Kinh doanh',
        1 => 'Tạm dừng Kinh doanh',
        3 => 'Không còn kinh doanh'
    );

    public $productDisplayTypes = array(
        1 => 'Hiển thị trêm web',
        2 => 'Ẩn nhưng search mới ra',
        3 => 'Ẩn hoàn toàn'
    );

    public $productKinds = array(
        1 => 'Sản phẩm đơn',
        2 => 'Sản phẩm nhiều phiên bản',
        3 => 'Combo'
    );

    /*public $productLevels = array(
        2 => 'Sản phẩm chính',
        1 => 'Sản phẩm phụ'
    );

    public $productKinds = array(
        1 => 'Sản phẩm đơn',
        2 => 'Sản phẩm nhiều phiên bản',
        3 => 'Combo'
    );

    public $productDisplayTypes = array(
        1 => 'Hiển thị trêm web',
        2 => 'Ẩn nhưng search mới ra',
        3 => 'Ẩn hoàn toàn'
    );

    public $fileTypes = array(
        1 => 'Ảnh',
        2 => 'PDF',
        3 => 'Text',
        4 => 'Audio',
        5 => 'Video'
    );

    public $scanTypes = array(
        1 => 'Nhập kho',
        2 => 'Kiểm kho',
        3 => 'Xuất Lưu chuyển kho',
        4 => 'Nhập Lưu chuyển kho',
        5 => 'Hoàn đơn hàng'
    );*/

    //ORDER
    public $orderStatus = array(
        1 => 'Chờ xử lý',
        2 => 'Đang xử lý', //'Đã chốt',
        //3 => 'Đã hủy bỏ',
        //4 => 'Chờ báo hủy',
        5 => 'Thất bại',
        6 => 'Thành công'
    );

    public $orderChannels = array(
        1 => 'WEB',
        2 => 'FB',
        3 => 'Tự tạo',
        4 => 'POS' //Trực tiếp
        //5 => 'Phone'
    );

    public $verifyStatus = array(
        1 => 'Chưa xác minh',
        2 => 'Đã xác minh'
    );

    /*public $deliveryTypes = array(
        1 => 'POS',
        2 => 'Từ xa'
    );*/

    public $paymentStatus = array(
        1 => 'Không thanh toán trước', //Chưa thanh toán
        2 => 'Đã thanh toán 1 phần',
        3 => 'Đã thanh toán tất cả',
        4 => 'Hoàn trả 1 phần',
        5 => 'Hoàn trả toàn bộ',
        6 => 'Có cho nợ lại'
    );

    public $CODStatus = array(
        1 => 'Không có COD',
        3 => 'Chưa thu khách',
        2 => 'Đã thu khách',
        4 => 'Đã nhận về đủ',
        5 => 'Sai sót'
    );

    //TRANSACTION
    public $transactionTypes = array(
        1 => 'Phiếu thu',
        2 => 'Phiếu chi',
        //3 => 'Công nợ'
    );

    public $transactionStatus = array(
        1 => 'Chờ xử lý',
        2 => 'Đã áp dụng',
        3 => 'Đã hủy'
    );

    public $verifyLevels = array(
        1 => 'Chưa xác thực',
        2 => 'Chờ duyệt',
        3 => 'Đã duyệt',
        4 => 'Check lại',
        5 => 'Chờ duyệt lại',
        6 => 'Không duyệt'
    );

    public $moneySources = array(
        1 => 'Tiền mặt',
        2 => 'Tiền chuyển khoản',
        3 => 'Thẻ điện thoại',
        4 => 'Quẹt thẻ'
    );

    public $discountStatus = array(
        1 => 'Chờ duyệt',
        2 => 'Thất bại',
        3 => 'Thành công'
    );
    /*public $bankTypes = array(
        1 => 'Cá nhân',
        2 => 'Doanh nghiệp'
    );*/

    /*public $canEditLevels = array(
        1 => 'Nhân viên duyệt',
        2 => 'Quản lý duyệt'
    );*/

    /*public $articleTypes = array(
        1 => 'Post',
        2 => 'Page'
    );*/

    //TRANSPORT
    public $transportStatus = array(
        1 => 'Chờ xử lý',
        2 => 'Chờ giao hàng', //Đã đóng hàng
        3 => 'Đang giao hàng',
        4 => 'Đã giao hàng',
        5 => 'Hủy giao hàng',
        6 => 'Đang chuyển hoàn',
        7 => 'Đã chuyển hoàn',
        8 => 'Đang treo',
        9 => 'Đang thất lạc'
    );

    //Khuyen mai
    public $promotionTypes = array(
        1 => 'Mã khuyến mãi (Coupon)',
        2 => 'Chương trình khuyến mãi'
    );

    public $promotionStatus = array(
        1 => 'Đang khuyến mãi',
        2 => 'Đã kích hoạt',
        3 => 'Chưa kích hoạt',
        4 => 'Ngừng khuyến mãi'
    );

    public $reduceTypes = array(
        1 => 'Giá trị xác định',
        2 => 'Theo phần trăm',
        3 => 'Miễn phí vận chuyển'
    );

    public $discountTypes = array(
        1 => 'Một lần trên một đơn hàng',
        2 => 'Cho từng mặt hàng trong giỏ hàng'
    );

    //hoan hang ve
    public $returnGoodTypes = array(
        1 => 'Hoàn đơn bưu điện',
        2 => 'Hoàn đơn ngoại thành'
    );

    //remind
    public $remindStatus = array(
        1 => 'Chờ xử lý',
        //2 => 'Đang xử lý',
        //3 => 'Đã xử lý',
        4 => 'Đã hủy bỏ',
        5 => 'Hoàn thành',
        6 => 'Thất bại'
    );
    /*public $remindTypes = array(
        1 => 'Nhắc nhở',
        2 => 'Thanh toán đơn hàng',
        3 => 'Tư vấn lại'
    );*/

    //import
    public $importStatus = array(
        5 => 'Chờ xử lý',
        1 => 'Chờ duyệt',
        2 => 'Đã duyệt',
        3 => 'Đã hủy'
        //4 => 'Không duyệt'
    );

    //store circulation
    public $storeCirculationStatus = array(
        1 => 'Chờ xử lý',
        2 => 'Duyệt giao hàng',
        3 => 'Duyệt nhận hàng',
        4 => 'Đã hủy bỏ',
        5 => 'Thất bại',
    );

    //
    public $itemStatus = array(
        2 => 'Đang hoạt động',
        1 => 'Tạm dừng',
        3 => 'Dừng hoạt động'
    );

    public $status = array(
        2 => 'Đã duyệt',
        1 => 'Chưa duyệt',
        3 => 'Không được duyệt',
        4 => 'Xem xét thêm'
    );

    /*public $itemTypes = array(
        1 => 'Nhóm Sản phẩm', //chuyen muc sp
        2 => 'Loại hàng hóa', //loai sp
        3 => 'Sản phẩm',
        4 => 'Bài viết',
        5 => 'Khách hàng',
        6 => 'Đơn hàng',
        7 => 'Lưu chuyển kho',
        8 => 'Nhập kho',
        9 => 'Vận chuyển',
        10 => 'Tài chính',
        11 => 'Nhóm Khách hàng',
        12 => 'Khuyến mại',
        13 => 'Sản phẩm con',
        14 => 'Hoàn hàng về',
        15 => 'Tồn kho',
        16 => "Combo Sản phẩm",
        17 => 'Phiếu thu',
        18 => 'Phiếu chi',
        19 => 'Lịch sử thay đổi tồn kho',
        20 => 'Phiếu thu nội bộ',
        21 => 'Phiếu chi nội bộ',
        22 => 'Phiếu in BarCode',
        23 => 'Lịch nhắc nhở',
        24 => 'Giá vốn',
        25 => 'Vận chuyển LCK',
        26 => 'Sổ quỹ',
    );

    public $inventoryTypes = array(
        1 => 'Cộng thêm',
        2 => 'Trừ đi'
    );*/

    public $genders = array(
        1 => 'Nam',
        2 => 'Nữ'
    );

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-danger',
        5 => 'label label-default',
        6 => 'label label-success',
        7 => 'label label-warning',
        8 => 'label label-danger',
        9 => 'label label-default',
        10 => 'label label-success',
        11 => 'label label-warning',
        12 => 'label label-danger'
    );

    public function selectConstants($key, $selectName, $itemId = 0, $isAll = false, $txtAll = 'Tất cả', $selectClass = '', $attrSelect = ''){
        $obj = $this->$key;
        if($obj) {
            echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
            if($isAll) echo '<option value="0">'.$txtAll.'</option>';
            foreach($obj as $i => $v){
                if($itemId == $i) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$v.'</option>';
            }
            echo "</select>";
        }
    }

    public function selectObject($listObj, $objKey, $objValue, $selectName, $objId = 0, $isAll = false, $txtAll = "Tất cả", $selectClass = '', $attrSelect = ''){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.$id.'"'.$attrSelect.'>';
        if($isAll) echo '<option value="0">'.$txtAll.'</option>';
        /*if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }*/
        $isSelectMutiple = is_array($objId);
        foreach($listObj as $obj){
            $selected = '';
            if(!$isSelectMutiple) {
                if ($obj[$objKey] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj[$objKey], $objId)) $selected = ' selected="selected"';
            echo '<option value="'.$obj[$objKey].'"'.$selected.'>'.$obj[$objValue].'</option>';
        }
        echo '</select>';
    }

    public function selectNumber($start, $end, $selectName, $itemId = 0, $asc = false, $attrSelect = ''){
        echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
        if($asc){
            for($i = $start; $i <= $end; $i++){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        else{
            for($i = $end; $i >= $start; $i--){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        echo '</select>';
    }

    public function getObjectValue($listObj, $objKey, $objValue, $objKeyReturn){
        foreach($listObj as $obj){
            if($obj[$objKey] == $objValue) return $obj[$objKeyReturn];
        }
        return '';
    }

    public function getUrl($itemSlug, $itemId, $itemTypeId, $siteId = 1){
        $retVal = 'javascript:void(0)';
        if($siteId == 1){ //ricky
            if ($itemTypeId == 1) $retVal = base_url($itemSlug . '-c' . $itemId . '.html');
            elseif ($itemTypeId == 3) $retVal = base_url('products/' . $itemSlug);
            elseif ($itemTypeId == 4) $retVal = base_url('pages/' . $itemSlug);
        }
        elseif($siteId == 2){ //rulya
            if ($itemTypeId == 1) $retVal = base_url('danh-muc/'.$itemSlug);
            elseif ($itemTypeId == 3) $retVal = base_url('san-pham/' . $itemSlug);
            elseif ($itemTypeId == 4) $retVal = base_url('blog/' . $itemSlug);
        }
        return $retVal;
    }

    public function getAppTypeId(){
        $appTypeId = 1;//CTV
        if($_SERVER['HTTP_HOST'] == 'aff.ricky.vn') $appTypeId = ADMIN_TYPE_ID;
        return $appTypeId;
    }
}