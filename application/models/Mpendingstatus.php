<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpendingstatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "pendingstatus";
        $this->_primary_key = "PendingStatusId";
    }

    public function getList($itemTypeId){
        return $this->getBy(array('StatusId' => STATUS_ACTIVED, 'ItemTypeId' => $itemTypeId));
    }
}