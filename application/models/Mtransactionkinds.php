<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactionkinds extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactionkinds";
        $this->_primary_key = "TransactionKindId";
    }
}
