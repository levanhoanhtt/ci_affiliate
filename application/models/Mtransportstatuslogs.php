<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransportstatuslogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportstatuslogs";
        $this->_primary_key = "TransportStatusLogId";
    }

    public function getListStatusIds($transportId, $transportStatusId = 0){
        $retVal = $this->getListFieldValue(array('TransportId' => $transportId), 'TransportStatusId');
        if(!in_array(1, $retVal)) $retVal[] = 1;
        if($transportStatusId > 0 && !in_array($transportStatusId, $retVal)) $retVal[] = $transportStatusId;
        return $retVal;
    }

    public function getListStatusIdsBatch($transportIds, $transportStatusIds){
        $retVal = array();
        $statusLogs = $this->getByQuery('SELECT TransportId, TransportStatusId FROM transportstatuslogs WHERE TransportId IN ?', array($transportIds));
        foreach($statusLogs as $statusLog){
            if(isset($retVal[$statusLog['TransportId']])){
                $retVal[$statusLog['TransportId']][] = $statusLog['TransportStatusId'];
                if(!in_array(1, $retVal[$statusLog['TransportId']])) $retVal[$statusLog['TransportId']][] = 1;
            }
            else $retVal[$statusLog['TransportId']] = array(1, $statusLog['TransportStatusId']);
        }
        foreach($transportStatusIds as $transportId => $transportStatusId){
            if(isset($retVal[$transportId])){
                if(!in_array($transportStatusId, $retVal[$transportId])) $retVal[$transportId][] = $transportStatusId;
                if(!in_array(1, $retVal[$transportId])) $retVal[$transportId][] = 1;
            }
            else $retVal[$transportId] = array(1, $transportStatusId);
        }
        return $retVal;
    }
}