<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlinkaffs extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "linkaffs";
        $this->_primary_key = "LinkAffId";
    }

    public function getCount($postData){
        $query = "1 = 1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM linkaffs WHERE 1= 1" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND linkaffs.CustomerId=".$postData['CustomerId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND linkaffs.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND linkaffs.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function checkShortLink($url) {
        $query = "SELECT * FROM linkaffs WHERE ShortLink=?";
        $param = array($url);
        $menus = $this->getByQuery($query, $param);
        return $menus;
    }

    public function checkOldLink($url) {
        $query = "SELECT * FROM linkaffs WHERE OriginalLink=?";
        $param = array($url);
        $menus = $this->getByQuery($query, $param);
        return $menus;
    }

    public function getList(){
        return $this->get();
    }

    /*public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM linkaffs";
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $user){
        $query = '';
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " linkaffs.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND linkaffs.CrDateTime <= '{$postData['EndDate']}'";
        if(isset($user['CustomerId'])) $query .= " AND linkaffs.CustomerId = '{$user['CustomerId']}'";
        return $query;
    }*/

    public function countRow() {
        $this->db->from('linkaffs');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getLinkAffByShortLink($link) {
        $query = "SELECT * FROM linkaffs WHERE ShortLink=? LIMIT 1";
        $param = array($link);
        $linkAff = $this->db->query($query, $param)->row_array();
        return $linkAff;
    }

     public function searchDataEffctual($postData = array()){
        $query = "select {selects} from linkaffs {joins} where 1=1". $this->buildQuery($postData)." ORDER BY orders.CrDateTime DESC";
        $queryProduct = "select COUNT( linkaffs.ProductId) AS Count, products.ProductName AS ProductName  from linkaffs {joins} where 1=1". $this->buildQuery($postData,$user)." GROUP BY linkaffs.ProductId ORDER BY Count DESC LIMIT 3";
        $selects = [
            'orders.*',
            'linkaffs.*',

        ];
        $joins = [
            'orderproducts' => "LEFT JOIN orderproducts ON linkaffs.ProductId = orderproducts.ProductId",
            'orders' => "LEFT JOIN orders ON orderproducts.OrderId = orders.OrderId",
            'products' =>'LEFT JOIN products ON products.ProductId = orderproducts.ProductId'
        ];
        $wheres = array('orders.OrderStatusId > 0');

        $joins_string = implode(' ', $joins);
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);

        // $queryProduct = str_replace('{selects}', $selects_string, $queryProduct);
        $queryProduct = str_replace('{joins}', $joins_string, $queryProduct);
        $queryProduct = str_replace('{wheres}', $wheres_string, $queryProduct);

        $productAll = $this->getByQuery($queryProduct);
        $dataAll = $this->getByQuery($query);

        $dataOrders = array();
        foreach ($dataAll as $key1 => $all) {
            $dataOrders[$all['OrderId']] = $all;
        }
        $orderSuccess = 0;
        $orderWaittingProcessing = 0;
        $orderProcessing = 0;
        $orderFailure = 0;
        $discountSuccess = 0;
        $discountWaittingProcessing = 0;
        $discountFailure = 0;
        $totalCommission = 0;
        if(count($dataOrders) > 0){
            foreach ($dataOrders as $key => $order) {
                if($order['OrderStatusId'] == 1)$orderWaittingProcessing += count($order['OrderStatusId']);
                if($order['OrderStatusId'] == 2) $orderProcessing += count($order['OrderStatusId']);
                if($order['OrderStatusId'] == 5) $orderFailure += count($order['OrderStatusId']);
                if($order['OrderStatusId'] == 6) $orderSuccess += count($order['OrderStatusId']);

                if($order['DiscountStatusId'] == 1)$discountWaittingProcessing += $order['DiscountCost'];
                if($order['DiscountStatusId'] == 2)$discountFailure += $order['DiscountCost'];
                if($order['DiscountStatusId'] == 3)$discountSuccess += $order['DiscountCost'];

                $totalCommission += $order['DiscountCost'];



            }
            $totalOrders =  intVal(count($dataOrders));
            // chi cho 4 vì có 4 trạng thái
            $percentOrderSuccess    = ($orderSuccess * 100) / $totalOrders;
            $percentOrderProcessing = ($orderProcessing * 100) / $totalOrders;
            $percentOrderFailure    = ($orderFailure * 100) / $totalOrders;
            // phần trăm tiền hoa hồng
            $percentCommissionSuccess               = ($discountSuccess * 100) / $totalCommission;
            $percentCommissionWaittingProcessing    = ($discountWaittingProcessing * 100) / $totalCommission;
            $percentCommissionFailure               = ($discountFailure * 100) / $totalCommission;



            $dataOrders = array(
                'order' => array(
                            'totalOrders'               => $totalOrders,
                            'orderSuccess'              => $orderSuccess,
                            'orderProcessing'           => $orderProcessing,
                            'orderFailure'              => $orderFailure,
                            'percentOrderSuccess'       => number_format($percentOrderSuccess,2),
                            'percentOrderProcessing'    => number_format($percentOrderProcessing,2),
                            'percentOrderFailure'       => number_format($percentOrderFailure,2),
                        ),
                'commission' => array(
                            'totalCommission'               => $totalCommission,
                            'discountWaittingProcessing'    => $discountWaittingProcessing,
                            'discountFailure'               => $discountFailure,
                            'discountSuccess'               => $discountSuccess,
                            'percentCommissionSuccess'      => $percentCommissionSuccess,
                            'percentCommissionWaittingProcessing' => $percentCommissionWaittingProcessing,
                            'percentCommissionFailure'      => $percentCommissionFailure,
                        ),
                'product' => array(
                    'p1' => array(
                        'Count' => $productAll[0]["Count"],
                        'ProductName' => $productAll[0]["ProductName"]
                    ),
                    'p2' => array(
                        'Count' => $productAll[1]["Count"],
                        'ProductName' => $productAll[1]["ProductName"]
                    ),
                    'p3' => array(
                        'Count' => $productAll[2]["Count"],
                        'ProductName' => $productAll[2]["ProductName"]
                    ),
                ),
            );
        }else{
            $dataOrders = array(
                'order' => array(
                            'totalOrders'               => 0,
                            'orderSuccess'              => 0,
                            'orderProcessing'           => 0,
                            'orderFailure'              => 0,
                            'percentOrderSuccess'       => 0,
                            'percentOrderProcessing'    => 0,
                            'percentOrderFailure'       => 0,
                        ),
                'commission' => array(
                            'totalCommission'               => 0,
                            'discountWaittingProcessing'    => 0,
                            'discountFailure'               => 0,
                            'discountSuccess'               => 0,
                            'percentCommissionSuccess'      => 0,
                            'percentCommissionWaittingProcessing' => 0,
                            'percentCommissionFailure'      => 0,
                        ),
                'product' => array(
                    'p1' => array(
                        'Count' => 0,
                        'ProductName' => " "
                    ),
                    'p2' => array(
                        'Count' => 0,
                        'ProductName' => " "
                    ),
                    'p3' => array(
                        'Count' => 0,
                        'ProductName' => ""
                    ),
                ),
            );
        }



        return $dataOrders;
    }

}