<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductprices extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productprices";
        $this->_primary_key = "ProductPriceId";
    }

    public function getByProductId($productId){
        return $this->getBy(array('ProductId' => $productId), false, 'Quantity', 'ProductPriceId,ProductId,ProductChildId,Quantity,Price', 0, 0, 'asc');
    }
}