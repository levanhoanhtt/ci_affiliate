<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactions extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "transactions";
        $this->_primary_key = "TransactionId";
    }

    public $labelCss = array(
        'TransactionStatusCss' => array(
            1 => 'label lable-grey',
            2 => 'label lable-green',
            3 => 'label lable-red'
        ),
        'VerifyLevelCss' => array(
            1 => 'label lable-grey',
            2 => 'label lable-yellow',
            3 => 'label lable-green',
            4 => 'label lable-grey',
            5 => 'label lable-yellow',
            6 => 'label lable-red'
        )
    );

    public function update($postData, $transactionId = 0, $tagNames = array(), $actionLogs = array()){
        $this->load->model('Mtags');
        $transactionTypeId = $postData['TransactionTypeId'];
        $itemTypeId = $transactionTypeId == 1 ? 17 : 18;
        $isUpdate = $transactionId > 0;
        $this->db->trans_begin();
        $transactionId = $this->save($postData, $transactionId);
        if ($transactionId > 0) {
            if (!empty($actionLogs)){
                $actionLogs['ItemId'] = $transactionId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate) $this->db->delete('itemtags', array('ItemId' => $transactionId, 'ItemTypeId' => $itemTypeId));
            else{
                $transactionCode =  ($postData['TransactionTypeId'] == 1 ? 'PT-' : 'PC-') . ($transactionId + 10000);
                $this->db->update('transactions', array('TransactionCode' => $transactionCode), array('TransactionId' => $transactionId));
            }
            //cong tien cho khach
            if($postData['CustomerId'] > 0 && $postData['PaidCost'] > 0 && $postData['TransactionStatusId'] == 2){
                if($transactionTypeId == 1) $this->db->query('UPDATE customers SET Balance = Balance + ? WHERE CustomerId = ?', array($postData['PaidCost'], $postData['CustomerId'])); //phieu thu
                elseif($transactionTypeId == 2) $this->db->query('UPDATE customers SET Balance = Balance - ? WHERE CustomerId = ?', array($postData['PaidCost'], $postData['CustomerId'])); //phieu chi
            }
            if(!empty($tagNames)) {
                $itemTags = array();
                foreach ($tagNames as $tagName) {
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if ($tagId > 0) {
                        $itemTags[] = array(
                            'ItemId' => $transactionId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if (!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $transactionId;
        }
    }

    public function updateVerifyLevel($transactionId, $verifyLevelId, $transaction){
        $crDateTime = getCurentDateTime();
        $transactionTypeId = $transaction['TransactionTypeId'];
        $flag = $verifyLevelId == 6 && $transaction['TransactionStatusId'] == 2;
        $actionLogs = array();
        $actionLogs[] = array(
            'ItemId' => $transactionId,
            'ItemTypeId' => $transactionTypeId == 1 ? 17 : 18,
            'ActionTypeId' => 2,
            'Comment' => $transaction['Comment'],
            'CrUserId' => $transaction['CrUserId'],
            'CrDateTime' => $crDateTime
        );
        $this->db->trans_begin();
        if($flag) $this->db->query('UPDATE transactions SET TransactionStatusId = 3, VerifyLevelId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE TransactionId = ?', array($verifyLevelId, $transaction['CrUserId'], $crDateTime, $transactionId));
        else $this->db->query('UPDATE transactions SET VerifyLevelId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE TransactionId = ?', array($verifyLevelId, $transaction['CrUserId'], $crDateTime, $transactionId));
        if($transaction['VerifyLevelId'] == 1 && $verifyLevelId == 3 && $transaction['CustomerId'] > 0){ //thay doi tien cua khach
            if($transactionTypeId == 1) $this->db->query('UPDATE customers SET Balance = Balance + ? WHERE CustomerId = ?', array($transaction['PaidCost'], $transaction['CustomerId'])); //phieu thu
            elseif($transactionTypeId == 2) $this->db->query('UPDATE customers SET Balance = Balance - ? WHERE CustomerId = ?', array($transaction['PaidCost'], $transaction['CustomerId'])); //phieu chi
        }
        if($flag && $transaction['CustomerId'] > 0){ //tao phieu hoan tien cho khach
            //$transactionTypeId == 1 => tao phieu chi moi de bu lai phieu thu
            //$transactionTypeId == 2 => tao phieu thu moi de bu lai phieu chi
            $postData = array(
                'TransactionTypeId' => $transactionTypeId == 1 ? 2 : 1,
                'TransactionStatusId' => 2,
                'MoneySourceId' => $transaction['MoneySourceId'],
                'MoneyPhoneId' => $transaction['MoneyPhoneId'],
                'VerifyLevelId' => 3,
                'StoreId' => $transaction['StoreId'],
                'TransactionReasonId' => $transaction['TransactionReasonId'],
                'CustomerId' => $transaction['CustomerId'],
                'BankId' => $transaction['BankId'],
                'PaidCost' => $transaction['PaidCost'],
                'Comment' => 'Hoàn tiền phiếu '.($transactionTypeId == 1 ? 'thu' : 'chi').' '.$transaction['TransactionCode'],
                'PrintStatusId' => 1,
                'HasDebt' => 1,
                'DebtComment' => '',
                'TransportId' => 0,
                'CrUserId' => $transaction['CrUserId'],
                'CrDateTime' => $crDateTime
            );
            $transactionNewId = $this->update($postData);
            $actionLogs[] = array(
                'ItemId' => $transactionNewId,
                'ItemTypeId' => $transactionTypeId == 1 ? 18 : 17,
                'ActionTypeId' => 1,
                'Comment' => $postData['Comment'],
                'CrUserId' => $transaction['CrUserId'],
                'CrDateTime' => $crDateTime
            );
        }
        $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateField($postData, $transactionId, $actionLogs = array()){
        $this->db->trans_begin();
        $transactionId = $this->save($postData, $transactionId);
        if($transactionId > 0 && !empty($actionLogs)){
            $this->load->model('Mactionlogs');
            $this->Mactionlogs->save($actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getTotalPaidCost($transportId, $transactionTypeId){
        $retVal = 0;
        $ts = $this->getByQuery('SELECT SUM(PaidCost) AS SumPaidCost FROM transactions WHERE TransportId = ? AND TransactionTypeId = ? AND TransactionStatusId = ?', array($transportId, $transactionTypeId, STATUS_ACTIVED));
        if(!empty($ts)) $retVal = $ts[0]['SumPaidCost'];
        return $retVal;
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "SELECT transactions.TransactionId AS totalRow FROM transactions {joins} WHERE {wheres}";
        $query = "SELECT {selects} FROM transactions {joins} WHERE {wheres} ORDER BY transactions.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'transactions.*',
            'customers.CustomerId',
            'customers.FullName ',
            'customers.Email',
            'customers.PhoneNumber',
            'customers.PhoneNumber2',
            'stores.StoreName',
            'transactionreasons.TransactionReasonName'
        ];
        $joins = [
            'customers' => "inner join customers on customers.CustomerId = transactions.CustomerId",
            'stores' => "left join stores on stores.StoreId = transactions.StoreId",
            'transactionreasons' => 'left join transactionreasons on transactionreasons.TransactionReasonId = transactions.TransactionReasonId'
        ];
        $wheres = array('TransactionStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(isset($postData['TransactionTypeId']) && $postData['TransactionTypeId'] > 0){
            $wheres[] = "transactions.TransactionTypeId = ?";
            $dataBind[] = $postData['TransactionTypeId'];
        }
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0){
            $wheres[] = 'transactions.CustomerId = ?';
            $dataBind[] = $postData['CustomerId'];
        }
        //search theo text
        if(!empty($searchText)){
            if(filter_var($searchText, FILTER_VALIDATE_EMAIL)){
                $whereSearch = 'customers.Email like ?';
                $dataBind[] = "%$searchText%";
            }
            else if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'transactions.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*else if(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'transactions.TransactionCode like ? or transactions.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            /*else if(strpos("da duyet",$searchText) > -1 )$whereSearch = 'transactions.TransactionStatusId > 1';
            else if(strpos('chua duyet',$searchText) > -1) $whereSearch = 'transactions.TransactionStatusId = 1';
            //else if(strpos('ghi no',$searchText) > -1 ||  strpos('no',$searchText) > -1  ||  strpos('ghi',$searchText) > -1 ) $whereSearch = 'transactions.TransactionTypeId = 3';
            else if(strpos('phieu',$searchText) > -1) $whereSearch = 'transactions.TransactionTypeId = 2 or transactions.TransactionTypeId = 1';
            else if(strpos('phieu thu',$searchText) > -1 || strpos('thu',$searchText) > -1) $whereSearch = 'transactions.TransactionTypeId = 1';
            else if(strpos('phieu chi',$searchText) > -1 || strpos('chi',$searchText) > -1) $whereSearch = 'transactions.TransactionTypeId = 2';*/
            else{
                $whereSearch = 'transactions.TransactionCode like ? or customers.FullName like ? or customers.Email like ? or customers.PhoneNumber like ? or customers.PhoneNumber2 like ? or stores.StoreName like ?';
                for( $i = 0; $i < 6; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'group_money':
                        $wheres[] = "transactions.PaidCost $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'group_phone':
                        $wheres[] = "(customers.PhoneNumber $conds[0] ? or customers.PhoneNumber2 $conds[0] ?)";
                        $dataBind[] = $conds[1];
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_email':
                        $wheres[] = "customers.Email $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_date':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'transactions.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "transactions.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "transactions.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(transactions.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'group_status_trans':
                        $wheres[] = "transactions.TransactionStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_status_verify':
                        $wheres[] = "transactions.VerifyLevelId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    /*case 'group_type_trans':
                        $wheres[] = "transactions.TransactionTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;*/
                    case 'group_store':
                        $wheres[] = "stores.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_moneysource':
                        $wheres[] = "transactions.MoneySourceId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_reason':
                        $wheres[] = "transactions.TransactionReasonId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_bank':
                        $wheres[] = "transactions.BankId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'transaction_tag':
                        $wheres[] = "transactions.TransactionId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId IN(17,18) AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataTransactions = $this->getByQuery($query, $dataBind);
        $transactionStatus =  $this->Mconstants->transactionStatus;
        $verifyLevels =  $this->Mconstants->verifyLevels;
        $moneySources =  $this->Mconstants->moneySources;
        for ($i = 0; $i < count($dataTransactions); $i++) {
            $dataTransactions[$i]['TransactionStatusName'] = $dataTransactions[$i]['TransactionStatusId'] > 0 ? $transactionStatus[$dataTransactions[$i]['TransactionStatusId']] : '';
            $dataTransactions[$i]['VerifyLevelName'] = $dataTransactions[$i]['VerifyLevelId'] > 0 ? $verifyLevels[$dataTransactions[$i]['VerifyLevelId']] : '';
            $dataTransactions[$i]['MoneySourceName'] = $dataTransactions[$i]['MoneySourceId'] > 0 ? $moneySources[$dataTransactions[$i]['MoneySourceId']] : '';
            $dayDiff = getDayDiff($dataTransactions[$i]['CrDateTime'], $now);
            $dataTransactions[$i]['CrDateTime'] = ddMMyyyy($dataTransactions[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataTransactions[$i]['DayDiff'] = $dayDiff;
            $dataTransactions[$i]['labelCss'] = $this->labelCss;
        }

        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data = array();
        $data['dataTables'] = $dataTransactions;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentTransactions';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }

    public function searchMixByFilter($searchText, $itemFilters, $limit, $page){
        $selectFrom = "(SELECT t.TransactionId, 0 AS TrannsactionInternalId, t.TransactionCode, t.CrUserId, t.CrDateTime, t.PaidCost, t.OrderId, t.MoneySourceId, t.TransactionReasonId, t.VerifyLevelId, t.TransactionTypeId, t.BankId, 1 AS TypeId
                        FROM transactions t WHERE t.TransactionStatusId = 2 AND (t.TransactionTypeId = 1 OR (t.TransactionTypeId = 2 AND t.OrderId = 0))
                        UNION ALL
                        SELECT 0 AS TransactionId, t1.TransactionInternalId, t1.TransactionCode, t1.CrUserId, t1.CrDateTime, t1.PaidCost, 0 AS OrderId, t1.MoneySourceId, 0 AS TransactionReasonId, t1.VerifyLevelId, t1.TransactionTypeId, t1.BankId, 2 AS TypeId
                        FROM transactioninternals t1 WHERE t1.TransactionStatusId = 2
                        ) a";
        $queryCount = "SELECT a.* FROM {$selectFrom} WHERE {wheres}";
        $query = "SELECT {selects} FROM {$selectFrom}  {joins} WHERE {wheres} ORDER BY a.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'a.*',
            'transactionreasons.TransactionReasonName',
            'users.FullName'
        ];
        $joins = [
            'transactionreasons' => 'left join transactionreasons on transactionreasons.TransactionReasonId = a.TransactionReasonId',
            'users' => 'left join users on users.UserId = a.CrUserId'
        ];
        $wheres = array('1 = 1');
        $whereSearch= '';
        $dataBind = [];
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'a.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*else if(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'transactions.TransactionCode like ? or transactions.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            /*else if(strpos("da duyet",$searchText) > -1 )$whereSearch = 'transactions.TransactionStatusId > 1';
            else if(strpos('chua duyet',$searchText) > -1) $whereSearch = 'transactions.TransactionStatusId = 1';
            //else if(strpos('ghi no',$searchText) > -1 ||  strpos('no',$searchText) > -1  ||  strpos('ghi',$searchText) > -1 ) $whereSearch = 'transactions.TransactionTypeId = 3';
            else if(strpos('phieu',$searchText) > -1) $whereSearch = 'transactions.TransactionTypeId = 2 or transactions.TransactionTypeId = 1';
            else if(strpos('phieu thu',$searchText) > -1 || strpos('thu',$searchText) > -1) $whereSearch = 'transactions.TransactionTypeId = 1';
            else if(strpos('phieu chi',$searchText) > -1 || strpos('chi',$searchText) > -1) $whereSearch = 'transactions.TransactionTypeId = 2';*/
            else{
                $whereSearch = 'a.TransactionCode like ?';
                $dataBind[] = "$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'group_money':
                        $wheres[] = "a.PaidCost $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'group_date':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'a.CrDateTime >= ? and a.CrDateTime <= ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "a.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "a.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(a.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'group_status_verify':
                        $wheres[] = "a.VerifyLevelId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_type':
                        $parts = explode('-', $conds[1]);
                        if(count($parts) == 2){
                            if($parts[0] > 0 && $parts[1] > 0){
                                $wheres[] = "a.TypeId = ?";
                                $wheres[] = "a.TransactionTypeId = ?";
                                $dataBind[] = $parts[0];
                                $dataBind[] = $parts[1];
                            }
                        }
                        break;
                    case 'group_moneysource':
                        $wheres[] = "a.MoneySourceId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_reason':
                        $wheres[] = "a.TransactionReasonId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_bank':
                        $wheres[] = "a.BankId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataTransactions = $this->getByQuery($query, $dataBind);
        //$transactionStatus =  $this->Mconstants->transactionStatus;
        $verifyLevels =  $this->Mconstants->verifyLevels;
        $moneySources =  $this->Mconstants->moneySources;
        $typeNames = array(
            '1-1' => 'Thu bán hàng',
            '1-2' => 'Chi bán hàng',
            '2-1' => 'Thu nội bộ',
            '2-2' => 'Chi nội bộ',
        );
        $labelCss = $this->labelCss;;
        $labelCss['TypeNameCss'] = array(
            '1-1' => 'label lable-green',
            '1-2' => 'label lable-yellow',
            '2-1' => 'label lable-grey',
            '2-2' => 'label lable-red',
        );
        for ($i = 0; $i < count($dataTransactions); $i++) {
            //$dataTransactions[$i]['TransactionStatusName'] = $dataTransactions[$i]['TransactionStatusId'] > 0 ? $transactionStatus[$dataTransactions[$i]['TransactionStatusId']] : '';
            $dataTransactions[$i]['VerifyLevelName'] = $dataTransactions[$i]['VerifyLevelId'] > 0 ? $verifyLevels[$dataTransactions[$i]['VerifyLevelId']] : '';
            $dataTransactions[$i]['MoneySourceName'] = $dataTransactions[$i]['MoneySourceId'] > 0 ? $moneySources[$dataTransactions[$i]['MoneySourceId']] : '';
            $typeId = $dataTransactions[$i]['TypeId'].'-'.$dataTransactions[$i]['TransactionTypeId'];
            $dataTransactions[$i]['TypeId1'] = $typeId;
            $dataTransactions[$i]['TypeName'] = $typeNames[$typeId];
            $dayDiff = getDayDiff($dataTransactions[$i]['CrDateTime'], $now);
            $dataTransactions[$i]['CrDateTime'] = ddMMyyyy($dataTransactions[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataTransactions[$i]['DayDiff'] = $dayDiff;
            $dataTransactions[$i]['labelCss'] = $labelCss;
        }
        //$totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $listTranssactionAll = $this->getByQuery($queryCount, $dataBind);
        $totalReceive = 0; // tổng thu
        $totalPayment = 0; // tổng chi
        $totalFund = 0;
        foreach ($listTranssactionAll as $t) {
            if($t["TransactionTypeId"] == 1) $totalReceive += $t['PaidCost'];
            else if($t["TransactionTypeId"] == 2) $totalPayment += $t['PaidCost'];
        }
        $totalSurvival = $totalFund + $totalReceive - $totalPayment;
        $totalRow = count($listTranssactionAll);
        $pageSize = ceil($totalRow / $limit);
        $data = array();
        $data['dataTables'] = array('data' => $dataTransactions, 'total' => array('totalReceive' => $totalReceive, 'totalPayment' => $totalPayment, 'totalFund' => $totalFund, 'totalSurvival' => $totalSurvival));
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentTransactions';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}