<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcategories extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "categories";
        $this->_primary_key = "CategoryId";
    }

    public function changeStatusBatch($categoryIds, $statusId, $userId){
        $this->db->query('UPDATE categories SET StatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE CategoryId IN ?', array($statusId, $userId, getCurentDateTime(), $categoryIds));
        return true;
    }

    public function update($postData, $categoryId = 0){
        $this->db->trans_begin();
        if($postData['DisplayOrder'] > 0) {
            $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
            $this->db->where(array('ItemTypeId' => $postData['ItemTypeId'], 'ProductTypeId' => $postData['ProductTypeId'], 'CategoryTypeId' => $postData['CategoryTypeId'], 'ParentCategoryId' => $postData['ParentCategoryId'], 'DisplayOrder>=' => $postData['DisplayOrder']));
            $this->db->update('categories');
        }
        $categoryId = $this->save($postData, $categoryId);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $categoryId;
        }
    }

    public function getListByItemType($itemTypeIds, $isAllItemType = false, $isAllStatus = false, $isMerge = false){
        $retVal = array();
        $query = 'SELECT * FROM categories WHERE 1=1 AND StatusId';
        if($isAllStatus) $query .= '> 0';
        else $query .= '='.STATUS_ACTIVED;
        if($isAllItemType) $cates = $this->getByQuery($query . ' ORDER BY DisplayOrder ASC');
        else{
            if(is_numeric($itemTypeIds) && $itemTypeIds > 0) $cates = $this->getByQuery($query. ' AND ItemTypeId = ? ORDER BY DisplayOrder ASC', array($itemTypeIds));
            elseif(is_array($itemTypeIds)) $cates = $this->getByQuery($query . ' AND ItemTypeId IN ? ORDER BY DisplayOrder ASC', array($itemTypeIds));
        }
        if($isMerge){
            foreach ($cates as $c) {
                if ($c['ParentCategoryId'] == 0) {
                    $c['Childs'] = array();
                    foreach ($cates as $c1) {
                        if ($c1['ParentCategoryId'] == $c['CategoryId']) $c['Childs'][] = $c1;
                    }
                    $retVal[] = $c;
                }
            }
        }
        else {
            foreach ($cates as $c) {
                if ($c['ParentCategoryId'] == 0) {
                    $retVal[] = $c;
                    foreach ($cates as $c1) {
                        if ($c1['ParentCategoryId'] == $c['CategoryId']) $retVal[] = $c1;
                    }
                }
            }
        }
        return $retVal;
    }

    public function getListByItem($itemId, $itemTypeId, $categoryTypeId = 1){
        return $this->getByQuery('SELECT CategoryId, CategoryName, CategorySlug FROM categories WHERE ItemTypeId = ? AND CategoryId IN(SELECT CategoryId FROM categoryitems WHERE ItemId = ? AND ItemTypeId = ?)', array($categoryTypeId, $itemId, $itemTypeId));
    }
    
    public function getProduct() {
        return $this->getBy(['ItemTypeId'=>1]);
    }
    
    public function getNews() {
        return $this->getBy(['ItemTypeId'=>4]);
    }
}
