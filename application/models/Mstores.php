<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstores extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "stores";
        $this->_primary_key = "StoreId";
    }

    public function update($postData, $storeId = 0, $userIds = array()){
        $isUpdate = $storeId > 0;
        $this->db->trans_begin();
        $storeId = $this->save($postData, $storeId);
        if($storeId > 0){
            if($isUpdate) $this->db->delete('storeusers', array('StoreId' => $storeId));
            $storeUsers = array();
            foreach($userIds as $userId) $storeUsers[] = array('StoreId' => $storeId, 'UserId' => $userId);
            if(!empty($storeUsers)) $this->db->insert_batch('storeusers', $storeUsers);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $storeId;
        }
    }

    public function getByUserId($userId, $isAll = false, $isGetStoreId = false){
        if($isAll) $retVal = $this->getBy(array('ItemStatusId' => STATUS_ACTIVED), false, '', 'StoreId, StoreName');
        else $retVal = $this->getByQuery('SELECT s.StoreId, s.StoreName FROM stores s INNER JOIN storeusers su ON s.StoreId = su.StoreId WHERE s.ItemStatusId = ? AND su.UserId = ?', array(STATUS_ACTIVED, $userId));
        if($isGetStoreId){
            $ids = array();
            foreach($retVal as $s) $ids[] = $s['StoreId'];
            if(empty($ids)) $ids[] = -1;
            return $ids;
        }
        return $retVal;
    }

    public function getWithLocation(){
        $retVal = array();
        $stores = $this->getByQuery('SELECT stores.StoreName,provinces.ProvinceName,districts.DistrictName,stores.StoreId FROM stores
                   INNER JOIN provinces on provinces.ProvinceId = stores.ProvinceId
                   INNER JOIN districts on districts.DistrictId = stores.DistrictId');
        foreach ($stores as $store) {
            $retVal[$store['StoreId']] = $store;
        }
        return $retVal;
    }
}
