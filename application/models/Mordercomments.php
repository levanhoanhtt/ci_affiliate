<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mordercomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "ordercomments";
        $this->_primary_key = "OrderCommentId";
    }

    public function getListByOrderId($orderId){
        return $this->getByQuery('SELECT ordercomments.*, users.FullName FROM ordercomments INNER JOIN users ON ordercomments.UserId = users.UserId WHERE ordercomments.OrderId = ? ORDER BY ordercomments.CrDateTime DESC', array($orderId));
    }
}