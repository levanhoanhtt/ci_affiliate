<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mremindcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "remindcomments";
        $this->_primary_key = "RemindCommentId";
    }

    public function getListByRemindId($remindId){
        return $this->getByQuery('SELECT remindcomments.*, users.FullName FROM remindcomments INNER JOIN users ON remindcomments.UserId = users.UserId WHERE remindcomments.RemindId = ? ORDER BY remindcomments.CrDateTime DESC', array($remindId));
    }
}