<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muserparts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "userparts";
        $this->_primary_key = "UserPartId";
    }

    public function getListCurrent($userId){
		return $this->getByQuery("SELECT PartId, RoleId FROM userparts WHERE UserId = ? AND (EndDate IS NULL OR EndDate >= NOW())", array($userId));
    }
}