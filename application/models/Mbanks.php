<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbanks extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "banks";
        $this->_primary_key = "BankId";
    }

    public function update($postData, $bankId = 0){
        $this->db->trans_begin();
        if($postData['DisplayOrder'] > 0) {
            $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
            $this->db->where(array('DisplayOrder >=' => $postData['DisplayOrder']));
            $this->db->update('banks');
        }
        $bankId = $this->save($postData, $bankId);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $bankId;
        }
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        if (isset($postData['BankNumber']) && !empty($postData['BankNumber'])) $query .= " AND BankNumber LIKE '%{$postData['BankNumber']}%'";
        if (isset($postData['BankName']) && !empty($postData['BankName'])) $query .= " AND BankName LIKE '%{$postData['BankName']}%'";
        if (isset($postData['BankHolder']) && !empty($postData['BankHolder'])) $query .= " AND BankHolder LIKE '%{$postData['BankHolder']}%'";
        if (isset($postData['BankTypeId']) && $postData['BankTypeId'] > 0) $query .= " AND BankTypeId=".$postData['BankTypeId'];
        if (isset($postData['StatusId']) && $postData['StatusId'] > 0) $query .= " AND StatusId=".$postData['StatusId'];
        if (isset($postData['StoreId']) && $postData['StoreId'] > 0) $query .= " AND StoreId=".$postData['StoreId'];
        if (isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query .= " AND CustomerId=".$postData['CustomerId'];
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM banks WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY DisplayOrder ASC';
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }
}