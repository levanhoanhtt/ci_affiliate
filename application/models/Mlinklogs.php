<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlinklogs extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "linklogs";
        $this->_primary_key = "LinkLogId";
    }

    public function searchDataEffctual($postData = array()){
    	$query = "select {selects} from linklogs {joins} where 1=1".$this->buildQuery($postData)." ORDER BY linklogs.CrDateTime DESC";
        $queryOrder = "select DISTINCT(orderproducts.OrderId) from linklogs {joinsOrder} where 1=1".$this->buildQuery($postData)." ORDER BY linklogs.CrDateTime DESC";
    	$selects = [
            'linklogs.*',
        ];

        $joins = [
            'linkaffs' => "LEFT JOIN linkaffs ON linkaffs.LinkAffId = linklogs.LinkAffId",
        ];

        $joinsOrder = [
            'linkaffs' => "LEFT JOIN linkaffs ON linkaffs.LinkAffId = linklogs.LinkAffId",
            'orderproducts' => 'LEFT JOIN orderproducts ON linkaffs.ProductId = orderproducts.ProductId',
        ];


        $joins_string = implode(' ', $joins);
        $joins_string_order = implode(' ', $joinsOrder);

        $selects_string = implode(',', $selects);
        $query = str_replace('{selects}', $selects_string, $query);

        $query = str_replace('{joins}', $joins_string, $query);
        $queryOrder = str_replace('{joinsOrder}', $joins_string_order, $queryOrder);

        $dataLinklogs = $this->getByQuery($query);
        $dataOrders = $this->getByQuery($queryOrder);
        if(count($dataLinklogs) > 0 && count($dataOrders) > 0){
            $distinctIpAddress = array();
            foreach ($dataLinklogs as $log) {
                $distinctIpAddress[$log['IpAddress']] = $log;
            }
            $totalClick = count($dataLinklogs);

            
            $totalOrders = intVal(count($dataOrders));

            $arrLinklogs = array(
                'totalClick' => $totalClick,
                'frequency'  => number_format(intVal($totalClick) / intVal(count($distinctIpAddress)),2),
                'percentTotalOrder' => ($totalOrders / intVal($totalClick)) * 100,
            );
        }else{
            $arrLinklogs = array(
                'totalClick' => 0,
                'frequency'  => 0,
                'percentTotalOrder' => 0,
            );
        }
        return $arrLinklogs;
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND linkaffs.CustomerId=".$postData['CustomerId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND linklogs.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND linklogs.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }
}