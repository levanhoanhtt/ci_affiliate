<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "users";
        $this->_primary_key = "UserId";
    }

    public function login($userName, $userPass){
        if(!empty($userName) && !empty($userPass)){
            $appTypeId = $this->Mconstants->getAppTypeId();
            if($appTypeId == ADMIN_TYPE_ID){
                $users = $this->getByQuery("SELECT * FROM users WHERE UserPass=? AND StatusId=? AND (UserName=? OR PhoneNumber=?) LIMIT 1", array(md5($userPass), STATUS_ACTIVED, $userName, $userName));
                if(!empty($users)){
                    $user = $users[0];
                    $user['AppTypeId'] = $appTypeId;
                    $user['CustomerId'] = 0;
                    /*$this->load->model('Mlogins');
                    $this->Mlogins->save(array('UserId' => $user['UserId'], 'IpAddress' => $this->input->ip_address(), 'UserAgent' => $this->input->user_agent(), 'LoginDateTime' => getCurentDateTime()));*/
                    return $user;
                }
            }
            else{
                $this->load->model('Mcustomers');
                $users = $this->Mcustomers->login($userName, $userPass);
                if(!empty($users)){
                    $user = $users[0];
                    $user['AppTypeId'] = $appTypeId;
                    $user['UserId'] = 0;
                    return $user;
                }
            }
        }
        return false;
    }

    public function checkExist($userId, $userName, $email, $phoneNumber){
        $query = "SELECT UserId FROM users WHERE UserId!=? AND StatusId=?";
        if(!empty($email) && !empty($phoneNumber)){
            $query .= " AND (UserName=? OR Email=? OR PhoneNumber=?) LIMIT 1";
            $users = $this->getByQuery($query, array($userId, STATUS_ACTIVED, $userName, $email, $phoneNumber));
        }
        elseif(!empty($email)){
            $query .= " AND (UserName=? OR Email=?) LIMIT 1";
            $users = $this->getByQuery($query, array($userId, STATUS_ACTIVED, $userName, $email));
        }
        elseif(!empty($phoneNumber)){
            $query .= " AND (UserName=? OR PhoneNumber=?) LIMIT 1";
            $users = $this->getByQuery($query, array($userId, STATUS_ACTIVED, $userName, $phoneNumber));
        }
        if (!empty($users)) return true;
        return false;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM users WHERE StatusId > 0" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserName']) && !empty($postData['UserName'])) $query.=" AND UserName LIKE '%{$postData['UserName']}%'";
        if(isset($postData['FullName']) && !empty($postData['FullName'])) $query.=" AND FullName LIKE '%{$postData['FullName']}%'";
        if(isset($postData['Email']) && !empty($postData['Email'])) $query.=" AND Email LIKE '%{$postData['Email']}%'";
        if(isset($postData['PhoneNumber']) && !empty($postData['PhoneNumber'])) $query.=" AND PhoneNumber LIKE '%{$postData['PhoneNumber']}%'";
        if(isset($postData['DegreeName']) && !empty($postData['DegreeName'])) $query.=" AND DegreeName LIKE '%{$postData['DegreeName']}%'";
        if(isset($postData['GenderId']) && $postData['GenderId'] > 0) $query.=" AND GenderId=".$postData['GenderId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['GroupId']) && $postData['GroupId'] > 0) $query.=" AND UserId IN(SELECT UserId FROM usergroups WHERE GroupId={$postData['GroupId']})";
        return $query;
    }

    public function getListForSelect($userIdFirst = 0, $fullNameFist = '') {
        $retVal = array();
        if($userIdFirst > 0){
            $users = $this->getByQuery('SELECT UserId,FullName,PhoneNumber,Email FROM users WHERE StatusId = ? ORDER BY (CASE UserId WHEN ? THEN 1 ELSE 2 END) ASC, UserId DESC', array(STATUS_ACTIVED, $userIdFirst));
            $i = 0;
            foreach($users as $u){
                $i++;
                if($i == 1 && !empty($fullNameFist)) $u['FullName'] = $fullNameFist;
                $retVal[] = $u;
            }
        }
        else $retVal = $this->getBy(array('StatusId' => STATUS_ACTIVED), false, '', 'UserId,FullName,PhoneNumber,Email');
        return $retVal;
    }

    public function update($postData, $userId = 0, $groupIds = array()){
        $isUpdate = $userId > 0;
        $this->db->trans_begin();
        $userId = $this->save($postData, $userId);
        if($userId > 0){
            if($isUpdate) $this->db->delete('usergroups', array('UserId' => $userId));
            if(!empty($groupIds)){
                $userGroups = array();
                foreach ($groupIds as $groupId) $userGroups[] = array('UserId' => $userId, 'GroupId' => $groupId);
                if(!empty($userGroups)) $this->db->insert_batch('usergroups', $userGroups);
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $userId;
        }
    }
}