<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mreminds extends MY_Model {
	function __construct() {
        parent::__construct();
        $this->_table_name = "reminds";
        $this->_primary_key = "RemindId";
    }

    public function getCount($postData){
        $query = "RemindStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM reminds WHERE RemindStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY RemindDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['RemindTitle']) && !empty($postData['RemindTitle'])) $query.=" AND RemindTitle LIKE '%{$postData['RemindTitle']}%'";
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND UserId=".$postData['UserId'];
        if(isset($postData['PartId']) && $postData['PartId'] > 0) $query.=" AND PartId=".$postData['PartId'];
        if(isset($postData['RemindTypeId']) && $postData['RemindTypeId'] > 0) $query.=" AND RemindTypeId=".$postData['RemindTypeId'];
        if(isset($postData['RemindStatusId'])){
            if($postData['RemindStatusId'] > 0) $query.=" AND RemindStatusId=".$postData['RemindStatusId'];
            elseif($postData['RemindStatusId'] == 0) $query.=" AND RemindStatusId = 1 AND DATE(RemindDate) = '".date('Y-m-d')."'";
        }
        if(isset($postData['OutOfDate']) && $postData['OutOfDate'] > 0) $query.=" AND OutOfDate=".$postData['OutOfDate'];
        return $query;
    }
    
    public function update($postData, $remindId = 0, $comments = array(), $actionLogs = array()){
        $isUpdate = $remindId > 0 ? true : false;
        $this->db->trans_begin();
        $remindId = $this->save($postData, $remindId);
        if ($remindId > 0) {
            if (!empty($actionLogs)) {
                $actionLogs['ItemId'] = $remindId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if(!$isUpdate){
                $remindCode = 'NN-' . ($remindId + 10000);
                $this->db->update('reminds', array('RemindCode' => $remindCode), array('RemindId' => $remindId));
                if(!empty($comments)){
                    $remindComments = array();
                    foreach($comments as $comment){
                        $remindComments[] = array(
                            'RemindId' => $remindId,
                            'UserId' => $postData['CrUserId'],
                            'Comment' => $comment,
                            'CrDateTime' => $postData['CrDateTime']
                        );
                    }
                    if (!empty($remindComments)) $this->db->insert_batch('remindcomments', $remindComments);
                }
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $remindId;
        }
    }

    public function updateRemindInCustomerConsult($postData, $remindId = 0, $actionLogs = array(),$customerConsultId = 0){
        $isUpdate = $remindId > 0 ? true : false;
        $this->db->trans_begin();
        $remindId = $this->save($postData, $remindId);
        if ($remindId > 0) {
            if (!empty($actionLogs)) {
                $actionLogs['ItemId'] = $remindId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if(!$isUpdate){
                $remindCode = 'NN-' . ($remindId + 10000);
                if($customerConsultId > 0) $remindTitle = 'Nhắc nhở Tư vấn lại #TVL-' . ($remindId + 10000);
                $this->db->update('reminds', array('RemindCode' => $remindCode, 'RemindTitle' => $remindTitle), array('RemindId' => $remindId));
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $remindId;
        }
    }

    public function insertOwnOrder($postData, $comments){
        $this->db->trans_begin();
        $this->db->update('reminds', array('RemindStatusId' => 6, 'UpdateUserId' => $postData['CrUserId'], 'UpdateDateTime' => $postData['CrDateTime']), array('OrderId' => $postData['OrderId']));
        $remindId = $this->save($postData);
        if($remindId > 0 && !empty($comments)){
            $remindComments = array();
            foreach($comments as $comment){
                $remindComments[] = array(
                    'RemindId' => $remindId,
                    'UserId' => $postData['CrUserId'],
                    'Comment' => $comment,
                    'CrDateTime' => $postData['CrDateTime']
                );
            }
            if (!empty($remindComments)) $this->db->insert_batch('remindcomments', $remindComments);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getReminds($timeNow, $timeEnd, $userId){
        //return array();
        return $this->getByQuery("SELECT RemindId,RemindTitle, DATE_FORMAT(RemindDate, '%H:%i %d/%m/%Y') AS RemindTime FROM reminds WHERE RemindStatusId = 1 AND UserId = ? AND ((RemindDate BETWEEN ? AND ?) OR  RemindDate <= NOW())", array($userId, $timeNow, $timeEnd));
    }
}