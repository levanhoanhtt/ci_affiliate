<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mgroups extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "groups";
        $this->_primary_key = "GroupId";
    }

    public function cloneAction($groupId, $postData){
        $this->db->trans_begin();
        $groupIdNew = $this->save($postData);
        if($groupIdNew > 0) $this->db->query('INSERT INTO groupactions(GroupId, ActionId) SELECT ?, ActionId FROM groupactions WHERE GroupId = ?', array($groupIdNew, $groupId));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $groupIdNew;
        }
    }
}