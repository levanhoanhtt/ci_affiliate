<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customers";
        $this->_primary_key = "CustomerId";
    }

    public function login($userName, $userPass){
        return $this->getByQuery("SELECT * FROM customers WHERE Password=? AND StatusId=? AND (Email=? OR PhoneNumber=?) LIMIT 1", array(md5($userPass), STATUS_ACTIVED, $userName, $userName));
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM customers WHERE StatusId > 0" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['CustomerKindId']) && $postData['CustomerKindId'] > 0) $query.=" AND CustomerKindId=".$postData['CustomerKindId'];
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (FullName LIKE '%{$postData['SearchText']}%' OR PhoneNumber LIKE '%{$postData['SearchText']}%' OR PhoneNumber2 LIKE '%{$postData['SearchText']}%' OR Email LIKE '%{$postData['SearchText']}%')";
        return $query;
    }

    //public function checkExist($customerId, $email, $phoneNumber){
    public function checkExist($customerId, $postData){
        $query = "SELECT CustomerId FROM customers WHERE CustomerId!=? AND StatusId=?";
        $param = array($customerId, STATUS_ACTIVED);
        $conds = '';
        $phones = array();
        if(!empty($postData['PhoneNumber'])) $phones[] = $postData['PhoneNumber'];
        if(!empty($postData['PhoneNumber1'])) $phones[] = $postData['PhoneNumber1'];
        if(!empty($postData['PhoneNumber2'])) $phones[] = $postData['PhoneNumber2'];
        if(!empty($postData['Email'])){
            $conds = "Email = ?";
            $param[] = $postData['Email'];
        }
        if(!empty($phones)){
            if(!empty($conds)) $conds .= ' OR ';
            $in = '';
            foreach($phones as $phone) $in .= "'{$phone}',";
            $in = substr($in, 0, strlen($in) - 1);
            $conds .= "PhoneNumber IN({$in}) OR PhoneNumber2 IN({$in})";
        }
        if(!empty($conds)) $query .= " AND ({$conds})";
        $users = $this->getByQuery($query, $param);
        if (!empty($users)) return $users[0]['CustomerId'];
        return 0;
    }

    // dung chung cho reset password và update cmnd ctv
    public function updateCollaborator($postData, $customerId = 0, $actionLogs = array()){
        $itemTypeId = 5;
        $isUpdate = $customerId > 0 ? true : false;
        $this->db->trans_begin();
        $customerId = $this->save($postData, $customerId);
        if($customerId > 0){
            if(!empty($actionLogs)){
                $actionLogs['ItemId'] = $customerId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $customerId;
        }
    }

    public function update($postData, $customerId = 0, $tagNames = array(), $actionLogs = array()){
        $this->load->model(array('Mtags','Mfiles'));
        $itemTypeId = 5;
        $isUpdate = $customerId > 0 ? true : false;
        $this->db->trans_begin();
        $customerId = $this->save($postData, $customerId, array('BirthDay', 'UpdateUserId', 'UpdateDateTime'));
        if($customerId > 0){
            $this->db->update('customers', array('CustomerCode' => $this->genCustomerCode($customerId)), array('CustomerId' => $customerId));
            if(!empty($actionLogs)){
                $actionLogs['ItemId'] = $customerId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate) $this->db->delete('itemtags', array('ItemId' => $customerId, 'ItemTypeId' => $itemTypeId));
            if(!empty($tagNames)){
                $itemTags = array();
                foreach($tagNames as $tagName){
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if($tagId > 0){
                        $itemTags[] = array(
                            'ItemId' => $customerId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if(!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $customerId;
        }
    }

    public function genCustomerCode($customerId){
        return 'CTV-'.($customerId + 10000);
    }

    public function updateGroup($customerIds, $customerGroupId, $metaData){
        $actionLogs = array();
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $this->db->query('UPDATE customers SET CustomerGroupId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE CustomerId IN ?', array($customerGroupId, $metaData['UserId'], $crDateTime, $customerIds));
        foreach($customerIds as $customerId){
            $actionLogs = array(
                'ItemId' => $customerId,
                'ItemTypeId' => 5,
                'ActionTypeId' => 2,
                'Comment' => $metaData['FullName'] . ': Cập nhật nhóm khách hàng thành '.$metaData['CustomerGroupName'],
                'CrUserId' => $metaData['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($itemTags)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    /*public function deleteBatch($customerIds, $user){
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $this->db->query('UPDATE customers SET StatusId = 0, UpdateUserId = ?, UpdateDateTime = ? WHERE CustomerId IN ?', array($user['UserId'], $crDateTime, $customerIds));
        $actionLogs = array();
        foreach($customerIds as $customerId){
            $actionLogs[] = array(
                'ItemId' => $customerId,
                'ItemTypeId' => 5,
                'ActionTypeId' => 3,
                'Comment' => $user['FullName'].' xóa khách hàng',
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }*/

    public function getBalance($customerId){
        return $this->getFieldValue(array('CustomerId' => $customerId), 'Balance', 0);
    }
    
    public function getInfoWithOrder($orderId){
        return $this->getByQuery('SELECT orders.OrderCode,customeraddress.Address,provinces.ProvinceName,districts.DistrictName,wards.WardName,COUNT(orderproducts.ProductId) AS count_product FROM customeraddress
                INNER JOIN orders on orders.CustomerAddressId = customeraddress.CustomerAddressId
                INNER JOIN orderproducts on orderproducts.OrderId = orders.OrderId 
                INNER JOIN provinces on provinces.ProvinceId = customeraddress.ProvinceId 
                INNER JOIN districts on districts.DistrictId = customeraddress.DistrictId 
                LEFT JOIN wards on wards.WardId = customeraddress.WardId
                WHERE orders.OrderId = ?', array($orderId));
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select {selects} from customers {joins} where {wheres} GROUP BY customers.CustomerId having {havings}";
        $query = "select {selects} from customers {joins} where {wheres} GROUP BY customers.CustomerId having {havings} ORDER BY customers.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'customers.*',
            'COUNT(orders.OrderId) AS TotalOrder',
            'SUM(orders.TotalCost) AS TotalPrice'
        ];
        $selectCounts = [
            'customers.CustomerId AS totalRow'
        ];
        $joins = [
            'orders' => "LEFT JOIN orders ON (orders.CustomerId = customers.CustomerId AND orders.OrderStatusId > 0)"
        ];
        $havings = array();
        $wheres = array('customers.StatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(isset($postData['CustomerKindId']) && $postData['CustomerKindId'] > 0){
            $wheres[] = "customers.CustomerKindId = ?";
            $dataBind[] = $postData['CustomerKindId'];
        }
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'customers.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'customers.FullName like ? or customers.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'customers.FullName like ? or customers.PhoneNumber like ? or customers.PhoneNumber2 like ? or customers.Email like ?';
                for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
            }
        }
         if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                 $conds = $item['conds'];
                 // var_dump($conds);
                switch ($filed_name) {
                    case 'total_price':
                        $selectCounts[] = 'SUM(orders.TotalCost) AS TotalPrice';
                        $havings[] = "TotalPrice $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'total_order':
                        $selectCounts[] = 'COUNT(orders.OrderId) AS TotalOrder';
                        $havings[] = "TotalOrder $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'customer_balance':
                        $wheres[] = "customers.Balance $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'order_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'orders.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "orders.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "orders.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(orders.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'receive_ad':
                        $wheres[] = "customers.IsReceiveAd $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'customer_kind':
                        $wheres[] = "customers.CustomerKindId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'customer_status':
                        $wheres[] = "customers.StatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'customer_address':
                        $wheres[] = "customers.Address like ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'customer_group':
                        $wheres[] = "customers.CustomerGroupId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'month_birth':
                        $wheres[] = "MONTH(customers.BirthDay) $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'customer_tag':
                        $wheres[] = "customers.CustomerId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 5 AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $selects_count_string = implode(',', $selectCounts);
        $wheres_string = implode(' and ', $wheres);
        $havings_string = implode(' and ', $havings);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{havings}', $havings_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{selects}', $selects_count_string, $queryCount);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        $queryCount = str_replace('{havings}', $havings_string, $queryCount);
        if(empty($wheres)){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        if(empty($havings)){
            $query = str_replace('having', '', $query);
            $queryCount = str_replace('having', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataCustomers = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataCustomers); $i++) {
            $dayDiff = getDayDiff($dataCustomers[$i]['CrDateTime'], $now);
            $dataCustomers[$i]['CrDateTime'] = ddMMyyyy($dataCustomers[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataCustomers[$i]['DayDiff'] = $dayDiff;
            $dataCustomers[$i]['StatusName'] = $dataCustomers[$i]['StatusId'] > 0 ? $this->Mconstants->status[$dataCustomers[$i]['StatusId']] : '';
            $dataCustomers[$i]['labelCss'] = $this->Mconstants->labelCss;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataCustomers;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentCustomers';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}