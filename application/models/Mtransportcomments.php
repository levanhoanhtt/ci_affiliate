<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mtransportcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportcomments";
        $this->_primary_key = "TransportCommentId";
    }

    public function getList($transportId){
        return $this->getByQuery('SELECT transportcomments.*, users.FullName FROM transportcomments INNER JOIN users ON transportcomments.UserId = users.UserId WHERE transportcomments.TransportId = ? ORDER BY transportcomments.CrDateTime DESC', array($transportId));
    }

    /*public function getList($transportId, $limits){
        $count = $this->db->query("SELECT * FROM transportcomments WHERE TransportId = $transportId ");
      
        $limit = "";
      
        if($limits == 0){
            $limit = 2;
        }else{
            $limit = $count->num_rows();
        }
        $sql = "SELECT transportcomments.`Comment` as `Comment`,
			transportcomments.CrDateTime as CrDateTime,
			users.UserName as UserName,
			users.Avatar as Avatar
			FROM transportcomments
			LEFT JOIN users ON users.UserId = transportcomments.UserId
            LEFT JOIN transports ON transports.TransportId = transportcomments.TransportId
            WHERE transports.TransportId = $transportId
			ORDER BY transportcomments.TransportCommentId DESC LIMIT $limit";
            
		return $this->getByQuery($sql);
    }*/
}