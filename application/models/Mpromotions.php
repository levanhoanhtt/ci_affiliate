<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpromotions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "promotions";
        $this->_primary_key = "PromotionId";
    }

    public function changeStatusBatch($promotionIds, $statusId, $userId){
        $this->db->query('UPDATE promotions SET PromotionStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE PromotionId IN ?', array($statusId, $userId, getCurentDateTime(), $promotionIds));
        return true;
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page){
        $queryCount = "select promotions.PromotionId AS totalRow from promotions {joins} where {wheres} GROUP BY promotions.PromotionId";
        $query = "select {selects} from promotions {joins} where {wheres} GROUP BY promotions.PromotionId ORDER BY promotions.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'promotions.*',
            //'provinces.ProvinceName',
            'COUNT(orderpromotions.OrderPromotionId) AS UseTime',
        ];
        $joins = [
            //'provinces' => "LEFT JOIN provinces ON provinces.ProvinceId = promotions.ProvinceId"
            'orderpromotions' => "LEFT JOIN orderpromotions ON (orderpromotions.PromotionId = promotions.PromotionId AND orderpromotions.StatusId = 2)"
        ];
        $wheres = array('promotions.PromotionStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'promotions.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'promotions.PromotionName like ? or promotions.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'promotions.PromotionName like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                 $conds = $item['conds'];
                switch ($filed_name) {
                    case 'promotion_types':
                        $wheres[] = "promotions.PromotionTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'promotion_status':
                        $wheres[] = "promotions.PromotionStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'promotion_province':
                        $wheres[] = "promotions.ProvinceId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'promotion_customer_group':
                        $wheres[] = "promotions.PromotionItemId $conds[0] ?";
                        $wheres[] = "promotions.PromotionItemTypeId = 11";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }

        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $dataPromotions = $this->getByQuery($query, $dataBind);
        $listCategories = $this->Mcategories->getListByItemType(1);
        $listCustomerGroups = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
        $listProvinces = $this->Mprovinces->getList();
        for ($i = 0; $i < count($dataPromotions); $i++) {
            $dataPromotions[$i]['BeginDate'] = ddMMyyyy($dataPromotions[$i]['BeginDate'], 'd/m/Y');
            $dataPromotions[$i]['EndDate'] = empty($dataPromotions[$i]['EndDate']) ? '--' : ddMMyyyy($dataPromotions[$i]['EndDate'], 'd/m/Y');
            $dataPromotions[$i]['PromotionStatus'] = $dataPromotions[$i]['PromotionStatusId'] > 0 ? $this->Mconstants->promotionStatus[$dataPromotions[$i]['PromotionStatusId']] : '';
            $dataPromotions[$i]['PromotionType'] = $dataPromotions[$i]['PromotionTypeId'] > 0 ? $this->Mconstants->promotionTypes[$dataPromotions[$i]['PromotionTypeId']] : '';
            $dataPromotions[$i]['DiscountType'] = $dataPromotions[$i]['DiscountTypeId'] > 0 ? $this->Mconstants->discountTypes[$dataPromotions[$i]['DiscountTypeId']] : '';
            $dataPromotions[$i]['ProvinceName'] = $dataPromotions[$i]['ProvinceId'] > 0 ? $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $dataPromotions[$i]['ProvinceId'], 'ProvinceName') : '';
            $dataPromotions[$i]['CustomerName'] = $dataPromotions[$i]['PromotionItemId'] > 0 && $dataPromotions[$i]['PromotionItemTypeId'] == 5 ? $this->Mcustomers->getFieldValue(array('CustomerId' => $dataPromotions[$i]['PromotionItemId']), 'FullName') : '';
            $dataPromotions[$i]['ProductName'] = $dataPromotions[$i]['PromotionItemId'] > 0 && $dataPromotions[$i]['PromotionItemTypeId'] == 3 ? $this->Mproducts->getProductName($dataPromotions[$i]['PromotionItemId']) : '';
            $dataPromotions[$i]['ProductChildName'] = $dataPromotions[$i]['PromotionItemId'] > 0 && $dataPromotions[$i]['PromotionItemTypeId'] == 13 ? $this->Mproducts->getProductName(0, $dataPromotions[$i]['PromotionItemId']) : '';
            $dataPromotions[$i]['CategoryName'] = $dataPromotions[$i]['PromotionItemId'] > 0 && $dataPromotions[$i]['PromotionItemTypeId'] == 1 ? $this->Mconstants->getObjectValue($listCategories, 'CategoryId', $dataPromotions[$i]['PromotionItemId'], 'CategoryName') : '';
            $dataPromotions[$i]['CustomerGroupName'] = $dataPromotions[$i]['PromotionItemId'] > 0 && $dataPromotions[$i]['PromotionItemTypeId'] == 11 ? $this->Mconstants->getObjectValue($listCustomerGroups, 'CustomerGroupId', $dataPromotions[$i]['PromotionItemId'], 'CustomerGroupName') : '';
            $dataPromotions[$i]['labelCss'] = $this->Mconstants->labelCss;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataPromotions;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentPromotions';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}