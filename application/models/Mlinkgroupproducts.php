<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlinkgroupproducts extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "linkgroupproducts";
        $this->_primary_key = "LinkGroupProductId";
    }

    /*public function groupLinkGroup()
    {
        $listGroupProducts = $this->get();
        $linkGroup = array_reduce($listGroupProducts, function($grouped, $linkGroupProduct) {
            if (!isset($grouped[$linkGroupProduct['LinkAffId']])) {
                $grouped[$linkGroupProduct['LinkAffId']] = "";
            }
            $grouped[$linkGroupProduct['LinkAffId']] = $grouped[$linkGroupProduct['LinkAffId']].','.$linkGroupProduct['LinkGroupId'];
            return $grouped;
        });
        return $linkGroup;
    }
    
    public function getListProductByArrId($linkGroupId)
    {
        $query = "SELECT LinkAffId FROM linkgroupproducts WHERE LinkGroupId = ?";
        $param = array($linkGroupId);
        $linkAffIds =  $this->getByQuery($query, $param);
        
        $linkAffIdArr = formatArray($linkAffIds, 'LinkAffId');
        
        if ($linkAffIdArr) {
            $this->db->select("ProductId");
            $this->db->from("linkaffs");
            $this->db->where_in("LinkAffId", $linkAffIdArr);
            $productIdArr = formatArray($this->db->get()->result_array(), 'ProductId');
            
            if ($productIdArr) {
                $this->db->select("*");
                $this->db->from("products");
                $this->db->where_in("ProductId", $productIdArr);
                $products = $this->db->get()->result_array();
            }
        } else {
            return null;
        }
        
        return $products;
    }*/
}