<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomercomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customercomments";
        $this->_primary_key = "CustomerCommentId";
    }

    public function getListByCustomerId($customerId){
        return $this->getByQuery('SELECT customercomments.*, users.FullName FROM customercomments INNER JOIN users ON customercomments.UserId = users.UserId WHERE customercomments.CustomerId = ? ORDER BY customercomments.CrDateTime DESC', array($customerId));
    }
}