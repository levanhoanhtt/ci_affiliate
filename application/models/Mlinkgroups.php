<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlinkgroups extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "linkgroups";
        $this->_primary_key = "LinkGroupId";
    }

    /*public function getListPluck(){
        $query = "SELECT * FROM linkgroups";
        $listGroup =  $this->getByQuery($query);
        if (!$listGroup) {
            return null;
        }
        $arr = pluckData('LinkGroupId', 'LinkGroupName', $listGroup);
        return $arr;
    }*/

    public function update($products, $data, $linkGroupIdPost = 0)
    {
        $this->load->model('Mlinkgroups', 'Mlinkaffs', 'Mlinkgroupproducts');
        $isUpdate = $linkGroupIdPost > 0 ? true : false;
        $this->db->trans_begin();
        if ($isUpdate) {
            $linkGroupId = $this->Mlinkgroups->save($data, $linkGroupIdPost);
            $this->db->delete('linkgroupproducts', array('LinkGroupId' => $linkGroupIdPost));
        } else {
            $linkGroupId = $this->Mlinkgroups->save($data);

        }
        foreach ($products as $p) {
            $productId = $p['ProductId'];
            $linkAffs = $this->getLinkAffByProductId($productId);
            foreach ($linkAffs as $linkAff) {
                $datas[] = ['LinkGroupId'=>$linkGroupId, 'LinkAffId'=>$linkAff['LinkAffId']];
            }
        }
        if (isset($datas)) {
            $this->db->insert_batch('linkgroupproducts', $datas);
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $linkGroupId;
        }

    }

    public function getLinkAffByProductId($productId) {
        $query = "SELECT * FROM linkaffs WHERE ProductId=?";
        $param = array($productId);
        $linkAff = $this->db->query($query, $param)->result_array();
        return $linkAff;
    }
}