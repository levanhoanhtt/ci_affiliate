<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpartners extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "partners";
        $this->_primary_key = "PartnerId";
    }
}
