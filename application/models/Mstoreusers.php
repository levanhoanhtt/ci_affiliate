<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstoreusers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "storeusers";
        $this->_primary_key = "StoreUserId";
    }
}
