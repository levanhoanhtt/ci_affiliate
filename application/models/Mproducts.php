<?php

class Mproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "products";
        $this->_primary_key = "ProductId";
    }

    public function getCount($postData){
        $query = "ProductStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $orderBy = 'ProductId DESC'){
        $query = "SELECT * FROM products WHERE ProductStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY '.$orderBy;
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function searchLinkAff($postData, $perPage = 0, $page = 1, $orderBy = 'products.ProductId DESC'){
        $query = "SELECT * FROM products
                INNER JOIN linkaffs ON products.ProductId = linkaffs.ProductId
                WHERE products.ProductStatusId > 0" . $this->buildQueryJoin($postData) . ' ORDER BY '.$orderBy;
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ProductStatusId']) && $postData['ProductStatusId'] > 0) $query.=" AND ProductStatusId=".$postData['ProductStatusId'];
        if(isset($postData['ProductDisplayTypeId']) && $postData['ProductDisplayTypeId'] > 0) $query.=" AND ProductDisplayTypeId=".$postData['ProductDisplayTypeId'];
        if(isset($postData['ProductIdDifferent']) && $postData['ProductIdDifferent'] > 0) $query.=" AND ProductId!=".$postData['ProductIdDifferent'];
        if(isset($postData['CategoryId']) && $postData['CategoryId'] > 0) $query.=" AND ProductId IN(SELECT ItemId FROM categoryitems WHERE ItemTypeId=3 AND CategoryId = {$postData['CategoryId']})";
        if(isset($postData['CategoryIds']) && !empty($postData['CategoryIds'])) $query.=" AND ProductId IN(SELECT ItemId FROM categoryitems WHERE ItemTypeId=3 AND CategoryId IN({$postData['CategoryIds']}))";
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])){
            if(isset($postData['IsSearchExactly']) && $postData['IsSearchExactly'] == 1) $query.=" AND (ProductName LIKE '{$postData['SearchText']} %' OR ProductName LIKE '% {$postData['SearchText']}')";
            //else $query.=" AND (ProductName LIKE '%{$postData['SearchText']}%' OR ProductId IN(SELECT ProductId FROM productchilds WHERE ProductName LIKE '%{$postData['SearchText']}%'))";
            else $query.=" AND ProductName LIKE '%{$postData['SearchText']}%'";
        }
        return $query;
    }

    private function buildQueryJoin($postData){
        $query = '';
        if(isset($postData['ProductStatusId']) && $postData['ProductStatusId'] > 0) $query.=" AND products.ProductStatusId=".$postData['ProductStatusId'];
        if(isset($postData['ProductDisplayTypeId']) && $postData['ProductDisplayTypeId'] > 0) $query.=" AND products.ProductDisplayTypeId=".$postData['ProductDisplayTypeId'];
        if(isset($postData['ProductIdDifferent']) && $postData['ProductIdDifferent'] > 0) $query.=" AND products.ProductId!=".$postData['ProductIdDifferent'];
        if(isset($postData['CategoryId']) && $postData['CategoryId'] > 0) $query.=" AND products.ProductId IN(SELECT ItemId FROM categoryitems WHERE ItemTypeId=3 AND CategoryId = {$postData['CategoryId']})";
        if(isset($postData['CategoryIds']) && !empty($postData['CategoryIds'])) $query.=" AND products.ProductId IN(SELECT ItemId FROM categoryitems WHERE ItemTypeId=3 AND CategoryId IN({$postData['CategoryIds']}))";
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])){
            if(isset($postData['IsSearchExactly']) && $postData['IsSearchExactly'] == 1) $query.=" AND (products.ProductName LIKE '{$postData['SearchText']} %' OR products.ProductName LIKE '% {$postData['SearchText']}')";
            else $query.=" AND products.ProductName LIKE '%{$postData['SearchText']}%'";
        }
        return $query;
    }

    public function checkExist($barCode, $productId = 0){
        return $this->getFieldValue(array('BarCode' => $barCode, 'ProductStatusId' => STATUS_ACTIVED, 'ProductId !=' => $productId), 'ProductId', 0);
    }

    public function getInfoByOrder($orderId){
        $retVal = array();
        $products = $this->getByQuery('SELECT p.ProductId AS ProductId, p.ProductName AS ProductName, p.ProductImage AS ProductImage, p.BarCode AS BarCode, p.ProductKindId AS ProductKindId, pc.ProductName AS ProductName1, pc.ProductImage AS ProductImage1, pc.BarCode AS BarCode1, op.Quantity
                                        FROM products p, productchilds pc RIGHT JOIN orderproducts op  ON op.ProductChildId = pc.ProductChildId
                                        WHERE op.OrderId = ?  AND op.ProductId = p.ProductId', array($orderId));
        foreach($products as $p){
            if($p['ProductKindId'] == 3){
                $this->load->model('Mproductchilds');
                $productParts = $this->Mproductchilds->getBy(array('ProductId' => $p['ProductId'], 'StatusId' => STATUS_ACTIVED), false, '', 'Quantity, ProductPartId, ProductPartChildId');
                $products1 = array();
                $productChilds1 = array();
                foreach($productParts as $productPart){
                    if(!isset($products1[$productPart['ProductPartId']])) $products1[$productPart['ProductPartId']] = $this->get($productPart['ProductPartId'], true, '', 'ProductName, ProductImage, BarCode');
                    $pp = $products1[$productPart['ProductPartId']];
                    $productName = $pp['ProductName'];
                    $productImage = empty($pp['ProductImage']) ? NO_IMAGE : $pp['ProductImage'];
                    $barCode = $pp['BarCode'];
                    if($productPart['ProductPartChildId'] > 0) {
                        if (!isset($productChilds1[$productPart['ProductPartChildId']])) $productChilds1[$productPart['ProductPartChildId']] = $this->Mproductchilds->get($productPart['ProductPartChildId'], true, '', 'ProductName, ProductImage, BarCode');
                        $pp = $productChilds1[$productPart['ProductPartChildId']];
                        $productName .= ' (' . $pp['ProductName'] . ')';
                        if(!empty($pp['ProductImage'])) $productImage = $pp['ProductImage'];
                        $barCode = $pp['BarCode'];
                    }
                    $retVal[] = array(
                        'ProductName' => $productName,
                        'ProductImage' => base_url(PRODUCT_PATH.$productImage),
                        'BarCode' => $barCode,
                        'Quantity' => $p['Quantity'] * $productPart['Quantity']
                    );
                }
            }
            else {
                if(!empty($p['ProductImage1'])) $productImage = $p['ProductImage1'];
                elseif(!empty($p['ProductImage'])) $productImage = $p['ProductImage'];
                else $productImage = NO_IMAGE;
                $retVal[] = array(
                    'ProductName' => $p['ProductName'] . (empty($p['ProductName1']) ? '' : " ({$p['ProductName1']})"),
                    'ProductImage' => base_url(PRODUCT_PATH.$productImage),
                    'BarCode' => empty($p['BarCode1']) ? $p['BarCode'] : $p['BarCode1'],
                    'Quantity' => $p['Quantity']
                );
            }
        }
        return $retVal;
    }

    public function getInfoByStoreCirculation($storeCirculationId){
        $retVal = array();
        $products = $this->getByQuery('SELECT p.ProductName AS ProductName , p.BarCode AS BarCode, pc.ProductName AS ProductName1, pc.BarCode AS BarCode1, sp.Quantity
                                        FROM products p, productchilds pc RIGHT JOIN storecirculationproducts sp  ON sp.ProductChildId = pc.ProductChildId
                                        WHERE sp.StoreCirculationId = ?  AND sp.ProductId = p.ProductId', array($storeCirculationId));
        foreach($products as $p){
            $retVal[] = array(
                'ProductName' => $p['ProductName'] . (empty($p['ProductName1']) ? '' : " ({$p['ProductName1']})"),
                'BarCode' => empty($p['BarCode1']) ? $p['BarCode'] : $p['BarCode1'],
                'Quantity' => $p['Quantity']
            );
        }
        return $retVal;
    }

    public function getInfoByReturnGood($returnGoodId){
        $retVal = array();
        $products = $this->getByQuery('SELECT p.ProductName AS ProductName , p.BarCode AS BarCode, pc.ProductName AS ProductName1, pc.BarCode AS BarCode1, rgp.Quantity
                                        FROM products p, productchilds pc RIGHT JOIN returngoodproducts rgp  ON rgp.ProductChildId = pc.ProductChildId
                                        WHERE rgp.ReturnGoodId = ?  AND rgp.ProductId = p.ProductId', array($returnGoodId));
        foreach($products as $p){
            $retVal[] = array(
                'ProductName' => $p['ProductName'] . (empty($p['ProductName1']) ? '' : " ({$p['ProductName1']})"),
                'BarCode' => empty($p['BarCode1']) ? $p['BarCode'] : $p['BarCode1'],
                'Quantity' => $p['Quantity']
            );
        }
        return $retVal;
    }

    public function update($postData, $productId = 0, $images = array(), $productSEO  = array(), $cateIds1 = array(), $cateIds2 = array(), $tagNames = array(), $productChilds = array(), $prices = array(), $actionLogs = array()){
        $this->load->model('Mfiles');
        $this->load->model('Mitemmetadatas');
        $this->load->model('Mtags');
        $this->load->model('Mproductchilds');
        $itemTypeId = 3;
        $isUpdate = $productId > 0;
        $this->db->trans_begin();
        $productId = $this->save($postData, $productId, array('UpdateUserId', 'UpdateDateTime'));
        if($productId > 0){
            if(!empty($actionLogs)){
                $actionLogs['ItemId'] = $productId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate){
                $this->db->delete('itemfiles', array('ItemId' => $productId, 'ItemTypeId' => $itemTypeId));
                $this->db->delete('categoryitems', array('ItemId' => $productId, 'ItemTypeId' => $itemTypeId));
                $this->db->delete('itemtags', array('ItemId' => $productId, 'ItemTypeId' => $itemTypeId));
                $this->db->update('productchilds', array('StatusId' => 0), array('ProductId' => $productId));
            }
            if(!empty($images)){
                $itemFiles = array();
                foreach($images as $img){
                    $fileId = $this->Mfiles->getFileId($postData['ProductName'], $img, 1);
                    if($fileId > 0) {
                        $itemFiles[] = array(
                            'ItemId' => $productId,
                            'ItemTypeId' => $itemTypeId,
                            'FileId' => $fileId
                        );
                    }
                }
                if(!empty($itemFiles)) $this->db->insert_batch('itemfiles', $itemFiles);
            }
            if(!empty($productSEO)){
                $productSEO['ItemId'] = $productId;
                $itemMetadataId = 0;
                if($isUpdate) $itemMetadataId = $this->Mitemmetadatas->getFieldValue(array('ItemId' => $productId, 'ItemTypeId' => $itemTypeId), 'ItemMetadataId', 0);
                $this->Mitemmetadatas->save($productSEO, $itemMetadataId);
            }
            $categoryItems = array();
            if(!empty($cateIds1)){
                foreach($cateIds1 as $cateId){
                    $categoryItems[] = array(
                        'CategoryId' => $cateId,
                        'ItemId' => $productId,
                        'ItemTypeId' => $itemTypeId
                    );
                }
            }
            if(!empty($cateIds2)){
                foreach($cateIds2 as $cateId){
                    $categoryItems[] = array(
                        'CategoryId' => $cateId,
                        'ItemId' => $productId,
                        'ItemTypeId' => $itemTypeId
                    );
                }
            }
            if(!empty($categoryItems)) $this->db->insert_batch('categoryitems', $categoryItems);
            if(!empty($tagNames)){
                $itemTags = array();
                foreach($tagNames as $tagName){
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if($tagId > 0){
                        $itemTags[] = array(
                            'ItemId' => $productId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if(!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
            $productPrices = array();
            if(!empty($productChilds)){
                $productChildInserts = array();
                $productChildUpdates = array();
                foreach($productChilds as $pc){
                    $pc['ProductId'] = $productId;
                    if($pc['ProductChildId'] > 0){
                        unset($pc['ProductPrices']);
                        $productChildUpdates[] = $pc;
                    }
                    else{
                        if(!empty($pc['ProductPrices'])){
                            $pps = $pc['ProductPrices'];
                            unset($pc['ProductPrices']);
                            $productChildId = $this->Mproductchilds->save($pc);
                            if($productChildId > 0){
                                foreach($pps as $pp){
                                    $pp['ProductId'] = $productId;
                                    $pp['ProductChildId'] = $productChildId;
                                    $productPrices[] = $pp;
                                }
                            }
                        }
                        else{
                            unset($pc['ProductPrices']);
                            $productChildInserts[] = $pc;
                        }
                    }
                }
                if(!empty($productChildInserts)) $this->db->insert_batch('productchilds', $productChildInserts);
                if(!empty($productChildUpdates)) $this->db->update_batch('productchilds', $productChildUpdates, 'ProductChildId');
            }
            if(!empty($prices)){
                foreach($prices as $p){
                    $p['ProductId'] = $productId;
                    $p['ProductChildId'] = 0;
                    $p['CrUserId'] = $postData['CrUserId'];
                    $p['CrDateTime'] = $postData['CrDateTime'];
                    $productPrices[] = $p;
                }
            }
            if(!empty($productPrices)) $this->db->insert_batch('productprices', $productPrices);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $productId;
        }
    }

    public function changeStatusBatch($productIds, $statusId, $user){
        $crDateTime = getCurentDateTime();
        $comment = $statusId > 0 ? ($user['FullName'].' thay đổi trạng thái sản phẩm về '.$this->Mconstants->productStatus[$statusId]) : ($user['FullName'].' xóa sản phẩm');
        $this->db->trans_begin();
        $this->db->query('UPDATE products SET ProductStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ProductId IN ?', array($statusId, $user['UserId'], $crDateTime, $productIds));
        $actionLogs = array();
        foreach($productIds as $productId){
            $actionLogs[] = array(
                'ItemId' => $productId,
                'ItemTypeId' => 3,
                'ActionTypeId' => $statusId > 0 ? 2 : 3,
                'Comment' => $comment,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    //khi check don, lay truong hop du tieu chuan trong kho
    /*public function getListPerfectByOrder($orderId, $where = ''){
        return $this->getByQuery('SELECT products.ProductName,products.ProductId,productchilds.ProductChildId,productchilds.ProductName AS ProductNameChild,productquantity.Quantity AS product_quantity,orderproducts.Quantity AS order_quantity,orderproducts.Price,orderproducts.OriginalPrice,orderproducts.DiscountReason,productquantity.StoreId FROM products
                 INNER JOIN productquantity ON productquantity.ProductId = products.ProductId
                 INNER JOIN orderproducts ON orderproducts.ProductId = productquantity.ProductId LEFT JOIN productchilds ON (productchilds.ProductId = products.ProductId AND orderproducts.ProductChildId = productquantity.ProductChildId AND (productquantity.ProductChildId = productchilds.ProductChildId or productchilds.ProductChildId = 0)) WHERE orderproducts.OrderId = ? AND productquantity.Quantity > 0'.$where, array($orderId));
    }

    public function getListMissingByOrder($orderId){
        return $this->getByQuery('SELECT products.ProductName,products.ProductId,productchilds.ProductChildId,productchilds.ProductName as ProductNameChild,orderproducts.Quantity as order_quantity,orderproducts.Price,orderproducts.OriginalPrice,orderproducts.DiscountReason FROM orderproducts
                 LEFT JOIN products ON products.ProductId = orderproducts.ProductId
                 LEFT JOIN productchilds ON productchilds.ProductChildId = orderproducts.ProductChildId AND productchilds.ProductId = orderproducts.ProductId WHERE OrderId = ?', array($orderId));
    }*/

    public function getProductName($productId, $productChildId = 0, $barCode = ''){
        if($productId > 0) return $this->getFieldValue(array('ProductId' => $productId), 'ProductName');
        else{
            $this->load->model('Mproductchilds');
            if($productChildId > 0) {
                $pc = $this->Mproductchilds->get($productChildId, true, '', 'ProductId, ProductName');
                if($pc) return $this->getFieldValue(array('ProductId' => $pc['ProductId']), 'ProductName') . ' (' . $pc['ProductName'] . ')';
            }
            elseif(!empty($barCode)){
                $productName = $this->getFieldValue(array('BarCode' => $barCode), 'ProductName');
                if(!empty($productName)) return $productName;
                $pc = $this->Mproductchilds->getBy(array('BarCode' => $barCode, 'StatusId' => STATUS_ACTIVED), true, '', 'ProductId, ProductName');
                if($pc) return $this->getFieldValue(array('ProductId' => $pc['ProductId']), 'ProductName') . ' (' . $pc['ProductName'] . ')';
            }
        }
        return '';
    }

    public function getOtherProducts($productId, $limit = 10){
        $retVal = array();
        $this->load->model('Mcategoryitems');
        $cateIds = $this->Mcategoryitems->getCateIds($productId, 3);
        if(!empty($cateIds)) $retVal = $this->search(array('ProductStatusId' => STATUS_ACTIVED, 'ProductDisplayTypeId' => 1, 'ProductIdDifferent' => $productId, 'CategoryIds' => implode(',', $cateIds)), $limit, 1);
        return $retVal;
    }

    /*
     * $searchTypeId = 1: co sl ton kho
     * $searchTypeId = 2: co sl ton kho + gia von
     * $searchTypeId = 3: thong ke gia von
     */
    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        //$queryCount = "select products.ProductId AS totalRow from products {joins} where {wheres} GROUP BY products.ProductId";
        //$query = "select {selects} from products {joins} where {wheres} GROUP BY products.ProductId ORDER BY products.CrDateTime DESC LIMIT {limits}";
        $queryCount = "select products.ProductId AS totalRow from products {joins} where {wheres} GROUP BY products.ProductId{havings}";
        $query = "select {selects} from products {joins} where {wheres} GROUP BY products.ProductId{havings} ORDER BY products.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'products.ProductId',
            'products.ProductName',
            'products.ProductTypeId',
            'products.ProductStatusId',
            'products.ProductKindId',
            'products.BarCode',
            'products.Price',
            'products.DiscountPercent',
            'products.CookieTimeout',

            'categories.CategoryName',
            'producttypes.ProductTypeName',
            'suppliers.SupplierName',
        ];
        $joins = [
            'producttypes' => "left join producttypes on producttypes.ProductTypeId = products.ProductTypeId",
            'categoryitems' => "left join categoryitems on categoryitems.ItemId = products.ProductId",
            'categories' => "left join categories on categories.CategoryId = categoryitems.CategoryId",
            'suppliers' => "left join suppliers on suppliers.SupplierId = products.SupplierId"
        ];
        $havings = '';
        $searchTypeId = $postData['SearchTypeId'];
        if($searchTypeId == 1 || $searchTypeId == 2){
            $selects[] = '(SELECT SUM(Quantity) FROM productquantity pq WHERE pq.ProductId = products.ProductId{whereStore}) AS ProductQuantity';
            $queryCount = "select products.ProductId AS totalRow, (SELECT SUM(Quantity) FROM productquantity pq WHERE pq.ProductId = products.ProductId{whereStore}) AS ProductQuantity from products {joins} where {wheres} GROUP BY products.ProductId{havings}";
            if($searchTypeId == 2) {
                $selects[] = 'pricechanges.Price AS PriceCapital';
                $joins['pricechanges'] = 'left join pricechanges on (pricechanges.ProductId = products.ProductId AND pricechanges.ProductChildId = 0)';
            }
            if(isset($postData['InventoryStatusId'])){
                if($postData['InventoryStatusId'] == 1) $havings = " HAVING(ProductQuantity <= 0 OR ProductQuantity IS NULL)";//het hang
                elseif($postData['InventoryStatusId'] == 2) $havings = ' HAVING ProductQuantity > 0';//con hang
            }
        }
        $wheres = array('ProductStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            /*if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'products.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'products.BarCode like ? or products.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            if(strpos("dang kinh doanh",$searchText) > -1 )$whereSearch = 'products.ProductStatusId = 2';
            elseif(strpos('tam dung kinh doanh',$searchText) > -1) $whereSearch = 'products.ProductStatusId = 1';
            elseif(strpos('khong con kinh doanh',$searchText) > -1) $whereSearch = 'products.ProductStatusId = 3';
            else{
                //$whereSearch = 'products.ProductName like ? or categories.CategoryName like ? or producttypes.ProductTypeName like ? or suppliers.SupplierName like ?';
                //for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
                $whereSearch = 'products.ProductName like ? or producttypes.ProductTypeName like ? or suppliers.SupplierName like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        $storeId = isset($postData['StoreId']) ? $postData['StoreId'] : 0;
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'product_status_trade':
                        $wheres[] = "products.ProductStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_status_display':
                        $wheres[] = "products.ProductDisplayTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_type' :
                        $wheres[] = "products.ProductTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_kind':
                        $wheres[] = "products.ProductKindId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_suppliers':
                        $wheres[] = "products.SupplierId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_manufacturer':
                        $wheres[] = "products.ManufacturerId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_unit':
                        $wheres[] = "products.ProductUnitId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_group_1':
                    case 'product_group_2':
                        $wheres[] = "categoryitems.CategoryId $conds[0] ? and categoryitems.ItemTypeId = 3";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_store':
                        $storeId = $conds[1];
                        break;
                    case 'product_image':
                        if($conds[1] == 1) $wheres[] = "products.ProductImage = ''";
                        elseif($conds[1] == 1) $wheres[] = "products.ProductImage != ''";
                        break;
                    case 'product_tag':
                        $wheres[] = "products.ProductId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 3 AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_inventory_status':
                        if($searchTypeId == 1 || $searchTypeId == 2){
                            if ($conds[1] == 1) $havings = " HAVING(ProductQuantity <= 0 OR ProductQuantity IS NULL)";//het hang
                            elseif ($conds[1] == 2) $havings = ' HAVING ProductQuantity > 0';//con hang
                        }
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $selects_string = str_replace('{whereStore}', $storeId > 0 ? ' AND StoreId='.$storeId : '', $selects_string);
        $queryCount = str_replace('{whereStore}', $storeId > 0 ? ' AND StoreId='.$storeId : '', $queryCount);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{havings}', $havings, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        $queryCount = str_replace('{havings}', $havings, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        if($searchTypeId > 0){
            $this->load->model('Mproductchilds');
            $this->load->model('Mproductquantity');
            $this->load->model('Mpricechanges');
            $this->load->model('Mpricechangelogs');
        }
        $listProducts = array();
        $dataProducts = $this->getByQuery($query, $dataBind);
        $importIds = array();
        foreach($dataProducts as $p){
            $p['ProductKind'] = $p['ProductKindId'] > 0 ? $this->Mconstants->productKinds[$p['ProductKindId']] : '';
            $p['ProductStatus'] = $p['ProductStatusId'] > 0 ? $this->Mconstants->productStatus[$p['ProductStatusId']] : '';
            $p['ProductChildId'] = 0;
            $p['labelCss'] = $this->Mconstants->labelCss;
            if($searchTypeId == 2){
                if($p['ProductKindId'] == 1 && $p['PriceCapital'] == null) $p['PriceCapital'] = $p['Price'];
            }
            else if($searchTypeId == 3){
                if($p['ProductKindId'] == 1){
                    $listPrice = $this->Mpricechangelogs->getList($p['ProductId'], 0);
                    $p['ListPrices'] = $listPrice;
                    foreach($listPrice as $ip){
                        if(!in_array($ip['ImportId'], $importIds)) $importIds[] = $ip['ImportId'];
                    }
                }
                else $p['ListPrices'] = array();
            }
            $listProducts[] = $p;
            if($p['ProductKindId'] == 2 && $searchTypeId > 0){
                $listProductChilds = $this->Mproductchilds->getByProductId($p['ProductId']);
                foreach($listProductChilds as $pc){
                    $pc['ProductName'] = $p['ProductName'].' ('.$pc['ProductName'].')';
                    $pc['ProductKindId'] = 1;
                    $pc['ProductKind'] = 'Sản phẩm con';
                    $pc['ProductStatusId'] = $p['ProductStatusId'];
                    $pc['ProductStatus'] = $p['ProductStatus'];
                    if($searchTypeId == 1 || $searchTypeId == 2) {
                        $pc['ProductQuantity'] = $this->Mproductquantity->getQuantity($pc['ProductId'], $pc['ProductChildId'], $storeId);
                        if ($searchTypeId == 2) $pc['PriceCapital'] = $this->Mpricechanges->getPrice($pc['ProductId'], $pc['ProductChildId'], $pc['Price']);
                    }
                    elseif($searchTypeId == 3){
                        $listPrice =  $this->Mpricechangelogs->getList($pc['ProductId'], $pc['ProductChildId'], '', '');
                        $pc['ListPrices'] = $listPrice;
                        foreach($listPrice as $ip){
                            if(!in_array($ip['ImportId'], $importIds)) $importIds[] = $ip['ImportId'];
                        }
                    }
                    $listProducts[] = $pc;
                }
            }
        }
        if($searchTypeId == 3) {
            sort($importIds);
            if (!empty($listProducts)) $listProducts[0]['ImportIds'] = $importIds;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $listProducts;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentProducts';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }

    public function getListInventory(){
        $sql = "SELECT `products`.ProductId as products_id,products.ProductCode as products_code, products.ProductName as products_name,productquantity.Quantity as quantity,products.Sku as sku,products.ProductImage as images,products.ProductStatusId as product_status_id,stores.StoreCode as stores_code,productquantity.StoreId as store_id,
            productchilds.ProductName as color_product,stores.StoreName as store_name FROM `products`
            LEFT JOIN productquantity ON products.ProductId = productquantity.ProductId
            LEFT JOIN stores ON stores.StoreId = productquantity.StoreId
            LEFT JOIN productchilds ON products.ProductId = productchilds.ProductId
            WHERE products.ProductStatusId = 2";
        return $this->getByQuery($sql);
    }
}