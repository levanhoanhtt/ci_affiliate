<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomerbanks extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customerbanks";
        $this->_primary_key = "CustomerBankId";
    }

    public function checkBank($customerBankId, $bankNumber, $bankHolder){
        $query = "SELECT CustomerBankId FROM customerbanks WHERE CustomerBankId!=? AND BankNumber=? AND StatusId =?";
        $param = array($customerBankId, $bankNumber, STATUS_ACTIVED);
        
        $data = $this->getByQuery($query, $param);
        return count($data);
    }

    public function update($postData, $customerBankId){
        $this->db->trans_begin();
        $isUpdate = $customerBankId > 0;
        $customerBankId = $this->save($postData, $customerBankId, array('UpdateUserId', 'UpdateDateTime'));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $customerBankId;
        }
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        if (isset($postData['BankNumber']) && !empty($postData['BankNumber'])) $query .= " AND BankNumber LIKE '%{$postData['BankNumber']}%'";
        if (isset($postData['BankName']) && !empty($postData['BankName'])) $query .= " AND BankName LIKE '%{$postData['BankName']}%'";
        if (isset($postData['BankHolder']) && !empty($postData['BankHolder'])) $query .= " AND BankHolder LIKE '%{$postData['BankHolder']}%'";
        if (isset($postData['BranchName']) && !empty($postData['BranchName'])) $query .= " AND BranchName LIKE '%{$postData['BranchName']}%'";
        if (isset($postData['StatusId']) && $postData['StatusId'] > 0) $query .= " AND StatusId=".$postData['StatusId'];
        if (isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query .= " AND CustomerId=".$postData['CustomerId'];
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM customerbanks WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY CustomerBankId ASC';
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }
}