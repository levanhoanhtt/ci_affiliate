<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactionreasons extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactionreasons";
        $this->_primary_key = "TransactionReasonId";
    }
}
