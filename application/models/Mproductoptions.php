<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductoptions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productoptions";
        $this->_primary_key = "ProductOptionId";
    }
}
