<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title;?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Cập nhật</button></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('api/config/update/1', array('id' => 'configForm')); ?>
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin cửa hàng</h3>
                        <p>Tên cửa hàng xuất hiện trên cửa hàng của bạn. Thẻ tiêu đề và thẻ mô tả giúp xác định cửa hàng của bạn xuất hiện trên công cụ tìm kiếm.</p>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Tên cửa hàng <span class="required">*</span></label>
                            <input type="text" class="form-control hmdrequired" name="COMPANY_NAME" value="<?php echo $listConfigs['COMPANY_NAME']; ?>" data-field="Tên cửa hàng">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email liên hệ <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="EMAIL_CONTACT" value="<?php echo $listConfigs['EMAIL_CONTACT']; ?>" data-field="Email liên hệ">
                                    <p><i>Email được sử dụng để hệ thống liên lạc với bạn</i></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email khách hàng <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="EMAIL_COMPANY" value="<?php echo $listConfigs['EMAIL_COMPANY']; ?>" data-field="Email khách hàng">
                                    <p><i>Email được sử dụng cho hỗ trợ khách hàng</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mô tả cửa hàng <span class="required">*</span></label>
                            <textarea rows="2" class="form-control hmdrequired" name="META_DESC" data-field="Mô tả cửa hàng"><?php echo $listConfigs['META_DESC']; ?></textarea>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" style="width: 100%;">Ảnh Logo <button type="button" class="btn btn-box-tool" id="btnUpLogo"><i class="fa fa-upload"></i> Chọn hình</button></label>
                                    <img src="<?php echo IMAGE_PATH.$listConfigs['LOGO_IMAGE']; ?>" id="imgLogo" style="width: 100%;">
                                    <input type="text" hidden="hidden" id="logoImage" name="LOGO_IMAGE" value="<?php echo $listConfigs['LOGO_IMAGE']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" style="width: 100%;">Ảnh địa chỉ <button type="button" class="btn btn-box-tool" id="btnUpAddress"><i class="fa fa-upload"></i> Chọn hình</button></label>
                                    <img src="<?php echo IMAGE_PATH.$listConfigs['ADDRESS_IMAGE']; ?>" id="imgAddress" style="width: 100%;">
                                    <input type="text" hidden="hidden" id="addressImage" name="ADDRESS_IMAGE" value="<?php echo $listConfigs['ADDRESS_IMAGE']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Địa chỉ cửa hàng</h3>
                        <p>Địa chỉ này sẽ xuất hiện trên hoá đơn của bạn và sẽ được sử dụng để tính toán mức giá vận chuyển của bạn.</p>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label">Tên đăng ký giấy phép kinh doanh (GPKD) của doanh nghiệp <span class="required">*</span></label>
                            <input type="text" class="form-control hmdrequired" name="COMPANY_NAME_REG" value="<?php echo $listConfigs['COMPANY_NAME_REG']; ?>" data-field="Tên đăng ký giấy phép kinh doanh (GPKD) của doanh nghiệp">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Điện thoại <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="PHONE_NUMBER" value="<?php echo $listConfigs['PHONE_NUMBER']; ?>" data-field="Điện thoại">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Fax</label>
                                    <input type="text" class="form-control" name="FAX" value="<?php echo $listConfigs['FAX']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Tỉnh/ Thành phố</label>
                                    <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $listConfigs['ProvinceId'], false, '', ' select2'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Quận huyện</label>
                                    <?php echo $this->Mdistricts->selectHtml($listConfigs['DistrictId']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Phường xã</label>
                                    <?php echo $this->Mwards->selectHtml($listConfigs['WardId']); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="ADDRESS" value="<?php echo $listConfigs['ADDRESS']; ?>" data-field="Địa chỉ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cấu hình text</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Chữ logo đầu trang <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="TEXT_LOGO_HEADER" value="<?php echo $listConfigs['TEXT_LOGO_HEADER']; ?>" data-field="Chữ logo đầu trang">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Chữ logo menu <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="TEXT_LOGO_MENU" value="<?php echo $listConfigs['TEXT_LOGO_MENU']; ?>" data-field="Chữ logo menu">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default padding15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cấu hình In</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Điện thoại Kỹ thuật <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="PHONE_TECH" value="<?php echo $listConfigs['PHONE_TECH']; ?>" data-field="Điện thoại Kỹ thuật">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Điện thoại Tư vấn <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="PHONE_CONSULTANT" value="<?php echo $listConfigs['PHONE_CONSULTANT']; ?>" data-field="Điện thoại Tư vấn">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề in đơn hàng <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="ORDER_PRINT_TEXT"  value="<?php echo $listConfigs['ORDER_PRINT_TEXT']; ?>" data-field="Tiêu đề in đơn hàng">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề in giao hàng <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="TRANSPORT_PRINT_TEXT"  value="<?php echo $listConfigs['TRANSPORT_PRINT_TEXT']; ?>" data-field="Tiêu đề in giao hàng">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Cập nhật"></li>
                    <input type="text" hidden="hidden" id="autoLoad" value="1">
                </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>