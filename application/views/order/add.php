<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('api/order/update', array('id' => 'orderForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding20">
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered-bottom">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th class="text-right" style="width: 100px;">Thành tiền</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct"></tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch updated" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm (F3)">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">SKU</th>
                                                                    <th style="width: 100px;">Giá</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="light-blue">Ghi chú</label>
                                        <div class="box-transprt clearfix mb10">
                                            <button type="button" class="btn-updaten save" id="btnInsertComment">
                                                Lưu
                                            </button>
                                            <input type="text" class="add-text" id="comment" value="">
                                        </div>
                                        <div class="listComment" id="listComment"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row tb-sead">
                                            <div class="col-sm-6"><a href="javascript:void(0)" id="aTotalPrice">Tổng</a></div>
                                            <div class="col-sm-6 text-right"><span id="totalPrice">0</span> đ</div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" id="aPromotion">Khuyến mại</a>
                                                <div class="promotion" style="display: none;" id="boxPromotion">
                                                    <div class="content-promotion">
                                                        <div class="form-group">
                                                            <label>Nhập giá trị giảm giá cho đơn hàng</label>
                                                            <div class="input-group promotion-input">
                                                                <input id="reduceNumber" type="text" class="form-control" value="0">
                                                                <span class="input-group-addon active" id="spanPromotionCost">đ</span>
                                                                <span class="input-group-addon" id="spanPromotionPercent">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Hoặc sử dụng mã khuyến mại</label>
                                                            <input type="text" class="form-control" id="promotionCode" placeholder="Mã khuyến mại">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Lý do</label>
                                                            <input type="text" class="form-control" placeholder="Lý do giảm giá cho đơn hàng" id="promotionComment">
                                                        </div>
                                                    </div>
                                                    <div class="footer-promotion">
                                                        <button type="button" class="btn btn-default">Đóng</button>
                                                        <button type="button" class="btn btn-primary" id="btnApplyPromotion">Áp dụng</button>
                                                        <input type="text" hidden="hidden" id="promotionId" value="0">
                                                        <input type="text" hidden="hidden" id="reduceTypeId" value="1">
                                                        <input type="text" hidden="hidden" id="discountPercent" value="0">
                                                        <input type="text" hidden="hidden" id="checkPromotionUrl" value="<?php echo base_url('promotion/checkPromotion'); ?>">
                                                        <input type="text" hidden="hidden" id="updateOrderPromotionUrl" value="<?php echo base_url('promotion/updateOrderPromotion'); ?>">
                                                    </div>
                                                </div>
                                                <br/><i id="iPromotionText"></i>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <span id="promotionCost">-</span>
                                                <span id="vnd1" style="display: none;">đ</span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- <div class="col-sm-6">
                                                <a href="javascript:void(0)" id="aTransport">Cấu hình vận chuyển</a>
                                                <div class="transport" style="display: none;" id="boxTransport">
                                                    <div class="content-transport">
                                                        <div class="radio-group">
                                                            <p class="item"><input type="radio" name="TransportType" class="iCheck transportType" value="1"> Miễn phí vận chuyển</p>
                                                            <p class="item"><input type="radio" name="TransportType" class="iCheck transportType" value="2" checked> Vận chuyển tùy chọn</p>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="footer-transport">
                                                        <button type="button" class="btn btn-default">Đóng</button>
                                                        <button type="button" class="btn btn-primary" id="btnApplyConfigTransport">Áp dụng</button>
                                                    </div>
                                                </div>
                                                <br/><i id="iTransportText"></i>
                                            </div> -->
                                            <!-- <div class="col-sm-6 text-right">
                                                <span id="transportCost">-</span>
                                                <span id="vnd2" style="display: none;">đ</span>
                                            </div> -->
                                            <div class="divConfigExpand" id="divService"></div>
                                        </div>
                                        <hr class="hr-ths">
                                        <div class="row mb10">
                                            <div class="col-sm-6">Tổng tiền</div>
                                            <div class="col-sm-6 text-right"><span id="orderCost"></span> đ</div>
                                        </div>
                                        <div class="row mb10">
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" id="aPayment" style="line-height: 34px;">Thanh toán trước</a><br/>
                                                <i id="iPaymentText"></i>
                                            </div>
                                            <!-- <div class="col-sm-6 text-right">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="paymentCost" value="0">
                                                    <span class="input-group-addon"> đ</span>
                                                </div>
                                            </div> -->
                                        </div>
                                        <div class="divConfigExpand mb10" id="divOrderOwn" style="display: none;">
                                            <div class="row item" data-id="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                                                <div class="col-sm-6"><a href="javascript:void(0)" class="aExpand">Ghi nợ</a></div>
                                                <div class="col-sm-6 text-right"><span id="spanOtherCost_<?php echo DEBIT_OTHER_TYPE_ID; ?>" class="spanOtherCost">0</span> đ</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">Tổng cần thanh toán (COD)</div>
                                            <div class="col-sm-6 text-right bold light-dark2 fs-16">
                                                <span id="totalCost">0</span> đ
                                            </div>
                                            <div class="col-sm-12" style="margin-top: 10px;">
                                                <p class="pull-right"><a href="javascript:void(0)" class="aExpand">Mở rộng &gt;&gt;</a></p>
                                            </div>
                                        </div>
                                        <p class="text-right list-btnn">
                                            <a href="javascript:void(0)" class="btn btn-default check-order" id="aCheckOrder"><span>Check đơn</span></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default" id="boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng (F4)">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span> ₫</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;<a href="javascript:void(0)" id="aCustomerAddress"><i class="fa fa-pencil"></i></a></h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div>
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label light-blue">Đối tác</label>
                                <?php $this->Mconstants->selectObject($listPartners, 'PartnerId', 'PartnerName', 'PartnerId', 0, true, '--Đối tác--'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Loại đơn hàng</label>
                                <?php $this->Mconstants->selectObject($listOrderTypes, 'OrderTypeId', 'OrderTypeName', 'OrderTypeId', 0, true, '--Loại đơn hàng--'); ?>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label light-blue">Lý do biết đến mua hàng</label>
                                <?php// $this->Mconstants->selectObject($listOrderReasons, 'OrderReasonId', 'OrderReasonName', 'OrderReasonId', 0, true, '--Chọn lý do--'); ?>
                            </div> -->
                            <div class="form-group" style="display: none;">
                                <label class="control-label light-blue">Kênh bán hàng</label>
                                <?php $this->Mconstants->selectConstants('orderChannels', 'OrderChanelId', 3); ?>
                            </div>
                            <!--<div class="form-group">
                                <label class="control-label light-blue">Cách thức giao hàng</label>
                                <div class="radio-group">
                                    <span class="item"><input type="radio" name="DeliveryTypeId" class="iCheck" value="1"> POS</span>
                                    <span class="item"><input type="radio" name="DeliveryTypeId" class="iCheck" value="2" checked> Từ xa</span>
                                </div>
                            </div>-->
                        </div>
                        <div class="box box-default more-task">
                            <!--<a href="javascript:void(0)" class="task1 aRemind"><img src="assets/vendor/dist/img/icon07.png">Thêm task chăm sóc khách hàng</a>-->
                            <a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                        </div>
                        <div class="box box-default more-tabs padding20">
                            <div class="form-group">
                                <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                <input type="text" class="form-control" id="tags">
                            </div>
                            <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                            <div class="clearfix">
                                <?php foreach ($listTags as $t) { ?>
                                    <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="orderEditUrl" value="<?php echo base_url('order/edit'); ?>">
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getProductChildComboUrl" value="<?php echo base_url('api/product/getProductChildCombo'); ?>">
                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="checkOrderUrl" value="<?php echo base_url('api/order/checkQuantity'); ?>">
                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                    <input type="text" hidden="hidden" id="orderId" value="0">
                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                    <input type="text" hidden="hidden" id="orderStatusId" value="1">
                    <!-- <input type="text" hidden="hidden" id="pendingStatusId" value="0"> -->
                    <!-- <input type="text" hidden="hidden" id="paymentStatusId" value="0"> -->
                    <!-- <input type="text" hidden="hidden" id="verifyStatusId" value="1"> -->
                    <!-- <input type="text" hidden="hidden" id="orderStoreId" value="<?php echo count($listStores) == 1 ? $listStores[0]['StoreId'] : 0; ?>"> -->
                    <!-- <input type="text" hidden="hidden" id="deliveryTypeId" value="2"> -->
                    <!-- <input type="text" hidden="hidden" id="isShare" value="1"> -->
                    <input type="text" hidden="hidden" id="offsetOrderTypeId" value="<?php echo OFFSET_ORDER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="debitOtherTypeId" value="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="canEdit" value="1">
                    <input type="text" hidden="hidden" id="transportId" value="0">
                    <input type="text" hidden="hidden" id="customerGroupId1" value="0">
                    <input type="text" hidden="hidden" id="customerKindId1" value="<?php echo $customerKindId; ?>">
                    <input type="text" hidden="hidden" id="debtCost1" value="0">
                    <input type="text" hidden="hidden" id="remindOwnCost" value="0">
                    <input type="text" hidden="hidden" id="bankIdTmp" value="0">
                    <input type="text" hidden="hidden" id="moneySourceId" value="0">
                    <input type="text" hidden="hidden" id="realPaymentCost" value="0">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                </ul>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('includes/modal/customer_address', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('order/modal'); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>