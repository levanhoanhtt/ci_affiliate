<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div style="display: none;">
                    <?php echo form_open('user/staff'); ?>
                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                    <?php echo form_close(); ?>
                </div>
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
            		<div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Mã đơn</th>
                                <th>Ngày lên sàn</th>
                                <th>Lý do</th>
                                <th>Người đưa lên</th>
                                <th>Nhận về</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            	<?php $fullNames = array();
                                foreach($listOrders as $o){
                                    if(!isset($fullNames[$o['ShareUserId']])) $fullNames[$o['ShareUserId']] = $this->Musers->getFieldValue(array('UserId' => $o['ShareUserId']), 'FullName'); ?>
                            		<tr id="trItem_<?php echo $o['OrderId']; ?>">
                            			<td><a href="<?php echo base_url('order/edit/'.$o['OrderId']); ?>"><?php echo $o['OrderCode']; ?></a></td>
                            			<td><?php echo ddMMyyyy($o['ShareDateTime']); ?></td>
                            			<td><?php echo $o['ShareComment'] ?></td>
                            			<td><?php echo $fullNames[$o['ShareUserId']]; ?></td>
                            			<td class="actions"><a href="javascript:void(0)" class="aShare" data-id="<?php echo $o['OrderId']; ?>" title="Nhận về"><i class="fa fa-download"></i></a></td>
                            		</tr>
                            	<?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('api/order/updateField'); ?>">
            	</div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>