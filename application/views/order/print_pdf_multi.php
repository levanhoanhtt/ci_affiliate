
<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url(); ?>">
	<link rel="stylesheet" type="text/css" href="assets/css/orderprint.css?5">
	<title>In đơn hàng</title>
</head>
<body >
<?php $products = array();
$productChilds = array();
foreach ($printData as $data): ?>
	<div class="content_border">
	<div class="top">
		<div class="wrap-left-t">
			<h1><?php echo $configs["COMPANY_NAME_REG"]; ?></h1>
			<p>Địa chỉ: <?php echo $configs["ADDRESS"]; ?></p>
			<p>Hotline :  Kĩ thuật : <?php echo $configs["PHONE_TECH"]; ?> - Tư vấn : <?php echo $configs["PHONE_CONSULTANT"]; ?></p>
			<p>Website: <?php echo base_url(); ?> </p>
			<p>CS xử lý : <?php echo $data['storeName']; ?></p>
		</div>
		<div class="wrap-right-t">
			<img src="<?php echo $data["barcodeSrc"]; ?>" width="120px">
			<p class="text-center"><b><?php echo $data['order']['OrderCode']; ?></b></p>
		</div>
	</div>
	<div class="mid">
		<h1 class="text-center mt-15"><?php echo $configs["ORDER_PRINT_TEXT"]; ?></h1>
		<div class="wrap-content">
			<div class="content-l">
				<div class="title-line mgbt-0">Thông tin đơn hàng</div>
				<div class="order-info">
					<div class="item">
						<p><b><?php echo $data['customerAddress'] ? $data['customerAddress']['CustomerName'] : ''; ?></b></p>
						<p>SĐT: <?php echo $data['customerAddress'] ? $data['customerAddress']['PhoneNumber'] : ''; ?></p>
						<p><?php echo $data['customerAddress'] ? $data['customerAddress']['Address'] : ''; ?></p>
					</div>
					<div class="item">
						<p><b>Ngày đặt hàng</b></p>
						<p><?php echo ddMMyyyy($data['order']['CrDateTime']); ?></p>
					</div>
					<div class="item">
						<?php $paymentStatus = "";
						if($data['order']['PaymentStatusId'] > 0) $paymentStatus = $this->Mconstants->paymentStatus[$data['order']['PaymentStatusId']]; ?>
						<p><b>Phương thức thanh toán</b></p>
						<p><?php echo $paymentStatus; ?></p>
					</div>
					
					<div class="item">
						<p><b>Phương thức nhận hàng</b></p>
						<p><?php echo $data['transportTypeName'] ?></p>
					</div>

					<div class="item">
						<p><b>Tạo đơn bởi</b></p>
						<p><?php echo $data['crFullName']; ?></p>
					</div>
				</div>
			</div>
			<div class="content-r">
				<div class="title-line">Chi tiết đơn hàng</div>
				<table width="100%">
					<thead>
						<tr>
							<th class="text-left">Tên sản phẩm</th>
							<th>BH</th>
							<th>Số lượng</th>
							<th>Giá</th>
						</tr>
					</thead>
					<tbody>
						<?php $totalPrice = 0;
                            foreach($data['listOrderProducts'] as $op){
                                if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, Price, BarCode, Weight, GuaranteeMonth');
                                $productName = $products[$op['ProductId']]['ProductName'];
                                $price = $products[$op['ProductId']]['Price'];
                                $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                                $productChildName = '';
                                if($op['ProductChildId'] > 0){
                                    if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, Price, GuaranteeMonth');
                                    $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                    $price = $productChilds[$op['ProductChildId']]['Price'];
                                    $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                                }
                                $totalPrice += $price * $op['Quantity']; ?>
							<tr>
								<td>
									<p><?php echo $productName; ?> </p>
									<p><?php if(!empty($productChildName)) echo $productChildName; ?> </p>
								</td>
								<td class="text-center">
									<p><?php echo $guaranteeMonth; ?> tháng</p>
									<?php if($guaranteeMonth > 0) echo '<p>1 đổi 1</p>'; ?>
								</td>
								<td class="text-center"><?php echo priceFormat($op['Quantity']); ?></td>
								<td class="text-center"><?php echo priceFormat($price); ?>đ</td>
							</tr>
						<?php } ?>
						
						
					</tbody>
				</table>
				<div class="title-line mt-10">Thông tin thanh toán</div>
				<div class="payment-info">
					<p>Tổng sản phẩm: <span class="pull-right"><?php echo priceFormat($totalPrice) ?>đ</span></p>
					<p>Khuyến mãi: <span class="pull-right"><?php echo priceFormat($data['order']['Discount']); ?>đ</span></p>
					<p>Phí vận chuyển: <span class="pull-right"><?php echo priceFormat($data['order']['TransportCost']); ?>đ</span></p>
				</div>
				<div class="clear-b mt-22">
					<?php $totalCost = ($totalPrice - $data['order']['Discount']) + $data['order']['TransportCost']; ?>
					<p><b>Tổng tiền</b>: <span class="pull-right"><b><?php echo priceFormat($totalCost); ?>đ</b></span></p>
					<p><b>Đã thanh toán</b>: <span class="pull-right"><b><?php echo priceFormat($data['order']['PaymentCost']); ?>đ</b></span></p>
					<p><b>Trả sau</b>: <span class="pull-right"><b> <?php echo priceFormat($totalCost - $data['order']['PaymentCost']); ?>đ</b></span></p>
				</div>
			</div>
		</div>

	</div>
	<div class="bottom">
		<div class="text-center">
			<div class="w-33">
				<p><b>Người bán hàng</b></p>
				<p class="c-ddd">( Kí, họ tên )</p>
			</div>
			<div class="w-33">
				<p><b>Người mua hàng</b></p>
				<p class="c-ddd">( Kí, họ tên )</p>
			</div>
			<div class="w-33">
				<p><b>Quản lý</b></p>
				<p class="c-ddd">( Kí, họ tên )</p>
			</div>
		</div>
	</div>
	<div class="background"></div>
	</div>
	
<div class="clearfix" style="page-break-after: always;position: absolute;"></div>
<?php endforeach; ?>
</body>

<script type="text/javascript">
	window.onload = function() { window.print(); }
</script>
</html>
