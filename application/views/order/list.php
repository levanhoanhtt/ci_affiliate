<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <!--<ul class="list-inline new-stl">
                    <li><a href="<?php //echo base_url('order/add'); ?>" class="btn btn-primary">Thêm</a></li>
                </ul>-->
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php $orderStatus = $this->Mconstants->orderStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả đơn hàng theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="order_status">Trạng thái đơn hàng</option>
                                        <?php if($customerId == 0) echo '<option value="customer_id">CTV</option>'; ?>
                                        <option value="order_status_payment">Trạng thái thanh toán</option>
                                        <option value="order_status_transport">Trạng thái giao hàng</option>
                                        <option value="order_create">Thời điểm đặt hàng</option>
                                        <option value="order_chanels">Kênh bán hàng</option>
                                        <option value="order_type">Loại đơn hàng</option>
                                        <!--<option value="customer_kind">Loại khách hàng</option>-->
                                        <option value="order_status_cod">Trạng thái COD</option>
                                        <option value="order_transporter">Nhà vận chuyển</option>
                                        <option value="order_status_authen">Trạng thái xác thực</option>
                                        <option value="order_method_payment">Phương thức thanh toán</option>
                                        <option value="order_method_transaction">Phương thức vận chuyển</option>
                                        <!-- <option value="order_store">Cơ sở xử lý</option> -->
                                        <option value="order_cost">Tổng tiền đơn hàng</option>
                                        <option value="order_user_create">Người tạo</option>
                                        <option value="order_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 order_cost none-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 order_status order_status_payment order_status_transport order_status_cod order_transporter order_transporter order_chanels order_status_authen order_method_payment order_method_transaction order_store order_user_create order_type customer_id">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control order_status block-display">
                                        <?php foreach($orderStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if($customerId == 0){ ?>
                                        <select class="form-control customer_id none-display">
                                            <?php foreach($listCustomers as $c){ ?>
                                                <option value="<?php echo $c['CustomerId']; ?>"><?php echo $c['CustomerCode'].'-'.$c['FullName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>
                                    <select class="form-control order_status_payment none-display">
                                        <?php foreach($this->Mconstants->paymentStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_status_transport none-display">
                                        <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_chanels none-display">
                                        <?php foreach($this->Mconstants->orderChannels as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_type none-display">
                                        <?php foreach($listOrderTypes as $ot) :?>
                                            <option value="<?php echo $ot['OrderTypeId']; ?>"><?php echo $ot['OrderTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <!--<select class="form-control customer_kind none-display">
                                        <option value="1">Khách lẻ</option>
                                        <option value="2">Khách buôn</option>
                                        <option value="3">Khách CTV</option>
                                    </select>-->
                                    <select class="form-control order_create none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control order_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                    <select class="form-control order_status_cod none-display">
                                        <?php foreach($this->Mconstants->CODStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_transporter none-display">
                                        <?php foreach($listTransporters as $t) :?>
                                            <option value="<?php echo $t['TransporterId']; ?>"><?php echo $t['TransporterName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control order_status_authen none-display">
                                        <?php foreach($this->Mconstants->verifyStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_method_payment none-display">
                                        <?php foreach($this->Mconstants->paymentStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_method_transaction none-display">
                                        <?php foreach($listTransportTypes as $tt) :?>
                                            <option value="<?php echo $tt['TransportTypeId']; ?>"><?php echo $tt['TransportTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <!-- <select class="form-control order_store none-display">
                                        <?php // foreach($listStores as $s) :?>
                                            <option value="<?php // echo $s['StoreId']; ?>"><?php // echo $s['StoreName']; ?></option>
                                        <?php // endforeach;?>
                                    </select> -->
                                    <select class="form-control order_user_create none-display">
                                        <?php foreach($listUsers as $u) :?>
                                            <option value="<?php echo $u['UserId']; ?>"><?php echo $u['FullName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control order_cost input-number none-display" type="text">
                                    <input class="form-control order_tag none-display" type="text">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('api/order/searchByFilter/'.$customerId); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="print_order">In phiếu bán hàng</option>
                            <!--<option value="change_status-0">Xóa đơn hàng đã chọn</option>
                            <option value="">----------</option>
                            <?php foreach($orderStatus as $i => $v){ ?>
                                <option value="change_status-<?php echo $i; ?>"><?php echo $v; ?></option>
                            <?php } ?>
                            <option value="">----------</option>-->
                            <option value="verify_order-2">Xác thực đơn hàng</option>
                            <option value="verify_order-1">Bỏ xác thực đơn hàng</option>
                            <option value="">----------</option>
                            <option value="add_tags">Thêm nhãn</option>
                            <option value="delete_tags">Bỏ nhãn</option>
                        </select>
                    </div>
                    <style>
                        #tbodyOrder i{
                            cursor: pointer;
                            margin-left: 3px;
                        }
                        #tbodyOrder tr.orderStatus_4 td, #tbodyOrder tr.orderStatus_4 td a{
                            color: red;
                        }
                    </style>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã đơn</th>
                                <th>Thời gian</th>
                                <th class="text-right">Giá trị ĐH</th>
                                <th  class="text-right">Hoa hồng</th>
                                <!-- <th>Khách hàng</th> -->
                                <th class="text-center">TTĐH</th>
                                <th class="text-center">TTHH</th>
                                <th>Đối tác</th>
                                <!-- <th class="text-right">Kênh</th> -->
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeVerifyStatusBatchUrl" value="<?php echo base_url('api/order/changeVerifyStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="itemTypeId" value="6">
                    <input type="hidden" value="<?php echo base_url('customer/edit')?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('order/edit')?>" id="urlEditOrder">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <form action="<?php echo base_url('order/printPdfMultiple'); ?>" method="POST" target="_blank" id="printForm" style="display: none;">
                        <input type="hidden" name="OrderIds" value="" id="orderIds"/>
                        <input type="submit" value="Submit">
                    </form>
                    <input type="text" hidden="hidden" id="getProductByOrderUrl" value="<?php echo base_url('api/order/getProductByOrder') ?>">
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade" id="modalDetailOrder" tabindex="-1" role="dialog" aria-labelledby="modalDetailOrder">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i>Chi tiết đơn hàng</h4>
                </div>
                <div class="modal-body">
                     <div class="table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered-bottom">
                            <thead class="theadNormal">
                            <tr>
                                <th>Sản phẩm</th>
                                <th class="text-center" style="width: 60px;">Đơn vị</th>
                                <th class="text-center" style="width: 130px;">SKU</th>
                                <th class="text-center" style="width: 120px;">Bảo hành</th>
                                <th class="text-center" style="width: 150px;">Giá</th>
                                <th style="width: 85px;">Số lượng</th>
                                <th class="text-right" style="width: 100px;">Thành tiền</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct"></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>