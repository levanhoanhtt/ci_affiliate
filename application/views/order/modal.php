<div class="modal fade" id="modalConfigExpand" tabindex="-1" role="dialog" aria-labelledby="modalConfigExpand">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thêm thuộc tính mở rộng</h4>
            </div>
            <div class="modal-body">
                <div class="listAdvancedItems">
                    <?php foreach($listOtherServices as $os){ ?>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="mg-0 text-bold" id="pOtherServiceName_<?php echo $os['OtherServiceId']; ?>"><?php echo $os['OtherServiceName']; ?></p>
                                    <p class="small"><?php echo $os['OtherServiceDesc']; ?></p>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control cost" value="<?php echo priceFormat($os['ServiceCost']); ?>" id="otherCost_<?php echo $os['OtherServiceId']; ?>">
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn btn-primary btnApplyConfigExpand" data-id="<?php echo $os['OtherServiceId']; ?>">Thêm</div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSendOffset" tabindex="-1" role="dialog" aria-labelledby="modalSendOffset">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Sửa tổng tiền hàng</h4>
            </div>
            <div class="modal-body">
                <p class="text-center light-blue">Đơn hàng là gửi bù, có thể sửa được tổng tiền hàng nếu muốn.</p>
                <div class="form-group">
                    <label class="control-label">Tổng tiền hàng:</label>
                    <input class="form-control cost" type="text" id="offsetTotalPrice" value="0">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnSendOffset">Xác nhận</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalWaitPayment" tabindex="-1" role="dialog" aria-labelledby="modalWaitPayment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tạo phiếu chờ thanh toán</h4>
            </div>
            <div class="modal-body">
                <p class="text-center light-blue">Số dư tài khoản của khách không đủ.<br/>Bạn cần tạo phiếu nhắc nhở khách hàng</p>
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">Khách hàng :</label>
                        <div class="mh-info-customer no-padding" style="border: none !important;">
                            <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                            <div class="name-info">
                                <h4 class="i-name"></h4>
                                <div class="phones i-phone"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Số tiền chờ thanh toán:</label>
                            <div class="input-group">
                                <input type="text" class="form-control cost" id="ownCost" value="0">
                                <div class="input-group-addon"><i>đ</i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datetimepicker" id="remindDate" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="text" id="ownComment" value="" placeholder="Ghi chú">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btnNoSaveOwnOrder">Không cần</button>
                <button class="btn btn-primary" type="button" id="btnSaveOwnOrder">Tạo nhắc nhở</button>
                <input type="text" hidden="hidden" id="isUpdateOrder" value="0">
                <input type="text" hidden="hidden" id="insertOwnOrderUrl" value="<?php echo base_url('remind/insertOwnOrder'); ?>">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalRemind" tabindex="-1" role="dialog" aria-labelledby="modalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-clock-o"></i> Thêm nhắc nhở</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label normal">Tiêu đề nhắc nhở <span class="required">*</span></label>
                    <input class="form-control hmdrequired" type="text" value="" id="remindTitle" data-field="Tiêu đề nhắc nhở">
                </div>
                <div class="form-group">
                    <label class="control-label normal">Nội dung nhắc nhở </label>
                    <input class="form-control" type="text" value="" id="remindComment" placeholder="Thêm nội dung ghi chú">
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label normal">Thời điểm cần xử lý <span class="required">*</span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datetimepicker hmdrequired" id="remindDate1" value="" data-field="Thời điểm cần xử lý">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label normal">Nhắc nhở cho nhân viên</label>
                            <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', $user['UserId'], true, '--Chọn--', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label normal">Hoặc phòng ban</label>
                            <?php $this->Mconstants->selectObject($listParts, 'PartId', 'PartName', 'PartId', 0, true, '--Chọn--', ' select2'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnAddRemind">Hoàn thành</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="text" hidden="hidden" id="insertRemindUrl" value="<?php echo base_url('remind/update'); ?>">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalCheckQuantity" tabindex="-1" role="dialog" aria-labelledby="modalCheckQuantity">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tình trạng sản phẩm trong kho</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalProductCombo" tabindex="-1" role="dialog" aria-labelledby="modalProductCombo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Chi tiết sản phẩm Combo</h4>
            </div>
            <div class="modal-body">
                <div class="no-padding">
                    <label class="text-success"><i class="fa fa-check-circle"></i> <span id="spanProductComboName"></span></label>
                    <div class="table-responsive divTable">
                        <table class="table table-hover">
                            <thead class="theadNormal">
                            <tr>
                                <th>Sản phẩm</th>
                                <th class="text-center" style="width: 130px;">SKU</th>
                                <th class="text-center" style="width: 120px;">Bảo hành</th>
                                <th class="text-center" style="width: 150px;">Giá</th>
                                <th class="text-center" style="width: 85px;">Số lượng</th>
                                <th class="text-center" style="width: 85px;">VAT</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductCombo"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalChooseStore" tabindex="-1" role="dialog" aria-labelledby="modalChooseStore">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"><i class="fa fa-wrench"></i> Lựa chọn cơ sở xử lý thủ công</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="radio-group">
                        <?php $storeId = isset($order) ? $order['StoreId'] : 0;
                        if($storeId == 0 && count($listStores) == 1) $storeId = $listStores[0]['StoreId'];
                        foreach($listStores as $s){ ?>
                            <p class="item"><input type="radio" name="OrderStoreId" class="iCheck" value="<?php echo $s['StoreId']; ?>"<?php if($storeId == $s['StoreId']) echo ' checked'; ?>> <span id="spanStoreName_<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></span></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnUpdateStore">Cập nhật</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalVerifyComplete" tabindex="-1" role="dialog" aria-labelledby="modalVerifyComplete">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Xác nhận hoàn thành đơn hàng POS</h4>
            </div>
            <div class="modal-body">
                <div class="divTable">
                    <table class="table table-hover">
                        <tbody id="tbobyVerifyPayment"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnSubmitPos">Xác nhận</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<?php foreach($listConfigs as $configCode => $configValue){ ?>
    <input type="text" hidden="hidden" id="config_<?php echo $configCode; ?>" value="<?php echo $configValue; ?>">
<?php } ?>