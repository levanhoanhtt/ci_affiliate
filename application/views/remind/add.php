<?php $this->load->view('includes/header'); ?>
	<div class="content-wrapper">
		<div class="container-fluid">
			<section class="content-header no-pd-lr">
				<h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
				<ul class="list-inline">
					<li><button class="btn btn-primary submit">Hoàn thành</button></li>
					<li><a href="<?php echo base_url('remind'); ?>" id="aRemindList" class="btn btn-default">Đóng</a></li>
				</ul>
			</section>
			<section class="content">
				<?php echo form_open('remind/update', array('id' => 'remindForm')); ?>
				<div class="row">
					<div class="col-sm-8 no-padding">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Chi tiết nhắc nhở</h3>
							</div>
							<div class="box-body">
								<div class="form-group">
									<label class="control-label">Tiêu đề nhắc nhở <span class="required">*</span></label>
									<input class="form-control hmdrequired" type="text" value="" id="remindTitle" data-field="Tiêu đề nhắc nhở">
								</div>
								<div class="form-group">
									<label class="control-label">Cập nhật tình hình hiện tại <span class="required">*</span></label>
									<div class="box-transprt clearfix mb10">
										<button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
										<input type="text" class="add-text" id="comment" value="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label normal">Thời điểm cần xử lý <span class="required">*</span></label>
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control hmdrequired" id="remindDate" value="" data-field="Thời điểm cần xử lý">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label normal">Trạng thái</label>
											<?php $this->Mconstants->selectConstants('remindStatus', 'RemindStatusId'); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label normal">Nhắc nhở cho nhân viên</label>
											<?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', $user['UserId'], true, '--Chọn--', ' select2'); ?>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label normal">Hoặc phòng ban</label>
											<?php $this->Mconstants->selectObject($listParts, 'PartId', 'PartName', 'PartId', 0, true, '--Chọn--', ' select2'); ?>
										</div>
									</div>
								</div>
								<ul class="list-inline pull-right margin-right-10">
									<li><button class="btn btn-primary submit" type="button">Hoàn thành</button></li>
									<li><a href="<?php echo base_url('remind'); ?>" class="btn btn-default">Đóng</a></li>
									<input type="text" hidden="hidden" id="orderId" value="0">
									<input type="text" hidden="hidden" id="customerId" value="0">
									<input type="text" hidden="hidden" id="ownCost" value="0">
									<input type="text" hidden="hidden" id="remindTypeId" value="1">
									<input type="text" hidden="hidden" id="customerConsultId" value="0">
									<input type="text" hidden="hidden" id="remindId" value="0">
									<input type="text" hidden="hidden" id="timeProcessed" value="0">
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="box box-default" id="boxChooseCustomer">
							<div class="box-header with-border">
								<h3 class="box-title">Lịch sử xử lý</h3>
							</div>
							<div class="box-body">
								<div class="listComment" id="listComment"></div>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</section>
		</div>
	</div>
<?php $this->load->view('includes/footer'); ?>