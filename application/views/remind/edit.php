<?php $this->load->view('includes/header'); ?>
	<div class="content-wrapper">
		<div class="container-fluid">
			<section class="content-header no-pd-lr">
				<h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
				<ul class="list-inline">
					<?php if($canEdit){ ?><li><button class="btn btn-primary submit">Cập nhật</button></li><?php } ?>
					<li><a href="<?php echo base_url('remind/'.($remindId > 0 ? $remind['RemindTypeId'] : '')); ?>" id="aRemindList" class="btn btn-default">Đóng</a></li>
				</ul>
			</section>
			<section class="content">
				<?php $this->load->view('includes/notice'); ?>
				<?php if($remindId > 0){ ?>
				<?php echo form_open('remind/update', array('id' => 'remindForm')); ?>
				<div class="row">
					<div class="col-sm-8 no-padding">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Chi tiết nhắc nhở<?php if($remind['TimeProcessed'] > 0) echo ' ('.$remind['TimeProcessed'].' lần)'; ?></h3>
								<div class="box-tools pull-right" style="line-height: 35px;">
									<?php echo $objName; ?>
								</div>
							</div>
							<div class="box-body">
								<?php if($remind['OutOfDate'] == 2 && $remind['RemindStatusId'] == 1){ ?>
									<div class="alert alert-danger">
										<p><i class="fa fa-exclamation"></i> Nhắc nhỡ đã quá hạn</p>
									</div>
								<?php } elseif($remind['RemindStatusId'] == 4){ ?>
									<div class="alert alert-danger">
										<p><i class="fa fa-exclamation"></i> Nhắc nhỡ đã hủy bỏ</p>
									</div>
								<?php } elseif($remind['RemindStatusId'] == 5){ ?>
									<div class="alert alert-success">
										<p><i class="fa fa-check"></i> Nhắc nhỡ đã hoàn thành</p>
									</div>
								<?php } ?>
								<div class="form-group">
									<label class="control-label">
										Tiêu đề nhắc nhở <span class="required">*</span>
										<?php if($remind['RemindTypeId'] == 2){ ?>
											<a href="<?php echo base_url('order/edit/'.$remind['OrderId']); ?>" target="_blank"><i class="fa fa-link"></i></a>
										<?php } elseif($remind['RemindTypeId'] == 3){ ?>
											<a href="<?php echo base_url('customerconsult/edit/'.$remind['CustomerConsultId']); ?>" target="_blank"><i class="fa fa-link"></i></a>
										<?php } ?>
									</label>
									<input class="form-control hmdrequired" type="text" value="<?php echo $remind['RemindTitle']; ?>" id="remindTitle" data-field="Tiêu đề nhắc nhở">
								</div>
								<div class="form-group">
									<label class="control-label">Cập nhật tình hình hiện tại <span class="required">*</span></label>
									<div class="box-transprt clearfix mb10">
										<button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
										<input type="text" class="add-text" id="comment" value="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label normal">Thời điểm cần xử lý <span class="required">*</span></label>
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input type="text" class="form-control hmdrequired" id="remindDate" value="<?php echo ddMMyyyy($remind['RemindDate'], 'd/m/Y H:i'); ?>" data-field="Thời điểm cần xử lý">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label normal">Trạng thái</label>
											<?php $this->Mconstants->selectConstants('remindStatus', 'RemindStatusId', $remind['RemindStatusId']); ?>
										</div>
									</div>
								</div>
								<ul class="list-inline pull-right margin-right-10">
									<?php if($canEdit){ ?><li><button class="btn btn-primary submit">Cập nhật</button></li><?php } ?>
									<li><a href="<?php echo base_url('remind'); ?>" class="btn btn-default">Đóng</a></li>
									<input type="text" hidden="hidden" id="orderId" value="<?php echo $remind['OrderId']; ?>">
									<input type="text" hidden="hidden" id="customerId" value="<?php echo $remind['CustomerId']; ?>">
									<input type="text" hidden="hidden" id="ownCost" value="<?php echo $remind['OwnCost']; ?>">
									<input type="text" hidden="hidden" id="userId" value="<?php echo $remind['UserId']; ?>">
									<input type="text" hidden="hidden" id="partId" value="<?php echo $remind['PartId']; ?>">
									<input type="text" hidden="hidden" id="remindTypeId" value="<?php echo $remind['RemindTypeId']; ?>">
									<input type="text" hidden="hidden" id="customerConsultId" value="<?php echo $remind['CustomerConsultId']; ?>">
									<input type="text" hidden="hidden" id="remindId" value="<?php echo $remindId; ?>">
									<input type="text" hidden="hidden" id="insertRemindCommentUrl" value="<?php echo base_url('remind/insertComment'); ?>">
									<input type="text" hidden="hidden" id="remindDateOld" value="<?php echo ddMMyyyy($remind['RemindDate'], 'd/m/Y H:i'); ?>">
									<input type="text" hidden="hidden" id="timeProcessed" value="<?php echo $remind['TimeProcessed']; ?>">
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="box box-default" id="boxChooseCustomer">
							<div class="box-header with-border">
								<h3 class="box-title">Lịch sử xử lý</h3>
							</div>
							<div class="box-body">
								<div class="listComment" id="listComment">
									<?php $i = 0;
									foreach($listRemindComments as $oc){
										$i++;
										if($i < 3){ ?>
											<div class="box-customer mb10">
												<table>
													<tbody>
													<tr>
														<th rowspan="2" valign="top"><img src="assets/vendor/dist/img/users2.png" alt=""></th>
														<th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
														<th class="time"><?php echo ddMMyyyy($oc['CrDateTime'], 'H:i d/m/Y'); ?></th>
													</tr>
													<tr>
														<td colspan="2">
															<p class="pComment"><?php echo $oc['Comment']; ?></p>
														</td>
													</tr>
													</tbody>
												</table>
											</div>
										<?php }
									} ?>
								</div>
								<?php if(count($listRemindComments) > 2){ ?>
									<div class="text-right light-dark">
										<a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
				<div class="row">
					<?php $this->load->view('includes/action_logs_new'); ?>
				</div>
				<?php $this->load->view('includes/modal/comment', array('itemName' => 'nhắc nhở', 'listItemComments' => $listRemindComments)); ?>
				<?php } ?>
			</section>
		</div>
	</div>
<?php $this->load->view('includes/footer'); ?>