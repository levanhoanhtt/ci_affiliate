<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('transportcaretype/update', array('id' => 'transportCareTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Chế độ chăm sóc đặc biệt</th>
                                <th>Phương thức vận chuyển</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransportCareType">
                            <?php
                            foreach($listTransportCareTypes as $tct){ ?>
                                <tr id="transportCareType_<?php echo $tct['TransportCareTypeId']; ?>">
                                    <td id="transportCareTypeName_<?php echo $tct['TransportCareTypeId']; ?>"><?php echo $tct['TransportCareTypeName']; ?></td>
                                    <td id="transportTypeName_<?php echo $tct['TransportCareTypeId']; ?>"><?php echo $this->Mconstants->getObjectValue($listTransportTypes, 'TransportTypeId', $tct['TransportTypeId'], 'TransportTypeName'); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $tct['TransportCareTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $tct['TransportCareTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="transportTypeId_<?php echo $tct['TransportCareTypeId']; ?>" value="<?php echo $tct['TransportTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="transportCareTypeName" name="TransportCareTypeName" value="" data-field="Chế độ chăm sóc đặc biệt"></td>
                                <td><?php $this->Mconstants->selectObject($listTransportTypes, 'TransportTypeId', 'TransportTypeName', 'TransportTypeId'); ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransportCareTypeId" id="transportCareTypeId" value="0" hidden="hidden">
                                    <input type="text" hidden="hidden" id="deleteTransportCareTypeUrl" value="<?php echo base_url('transportcaretype/delete'); ?>">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>