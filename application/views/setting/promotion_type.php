<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('promotiontype/update', array('id' => 'promotionTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Loại khuyến mại</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPromotionType">
                            <?php
                            foreach($listPromotionTypes as $bt){ ?>
                                <tr id="promotionTypeId_<?php echo $bt['PromotionTypeId']; ?>">
                                    <td id="promotionTypeName_<?php echo $bt['PromotionTypeId']; ?>"><?php echo $bt['PromotionTypeName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['PromotionTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['PromotionTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="promotionTypeName" name="PromotionTypeName" value="" data-field="Loại khuyến mại"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="PromotionTypeId" id="promotionTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deletePromotionTypeUrl" value="<?php echo base_url('promotiontype/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>