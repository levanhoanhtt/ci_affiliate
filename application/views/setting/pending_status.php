<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('pendingstatus/update', array('id' => 'pendingStatusForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Trạng thái chờ xử lý</th>
                                <th>Loại</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPendingStatus">
                            <?php $itemTypes = array(
                                6 => 'Đơn hàng',
                                9 => 'Vận chuyển'
                            );
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listPendingStatus as $p){ ?>
                                <tr id="pendingStatus_<?php echo $p['PendingStatusId']; ?>">
                                    <td id="pendingStatusName_<?php echo $p['PendingStatusId']; ?>"><?php echo $p['PendingStatusName']; ?></td>
                                    <td id="itemTypeName_<?php echo $p['PendingStatusId']; ?>"><span class="<?php echo $labelCss[$p['ItemTypeId']]; ?>"><?php echo $itemTypes[$p['ItemTypeId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['PendingStatusId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['PendingStatusId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="itemTypeId_<?php echo $p['PendingStatusId']; ?>" value="<?php echo $p['ItemTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="pendingStatusName" name="PendingStatusName" value="" data-field="Trạng thái chờ xử lý"></td>
                                <td>
                                    <select class="form-control" name="ItemTypeId" id="itemTypeId">
                                        <option value="6">Đơn hàng</option>
                                        <option value="9">Vận chuyển</option>
                                    </select>
                                </td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="PendingStatusId" id="pendingStatusId" value="0" hidden="hidden">
                                    <input type="text" id="deletePendingStatusUrl" value="<?php echo base_url('pendingstatus/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>