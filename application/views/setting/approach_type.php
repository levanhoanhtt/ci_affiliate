<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('approachtype/update', array('id' => 'approachTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Cách tiếp cận</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyApproachType">
                            <?php
                            foreach($listApproachTypes as $bt){ ?>
                                <tr id="approachType_<?php echo $bt['ApproachTypeId']; ?>">
                                    <td id="approachTypeName_<?php echo $bt['ApproachTypeId']; ?>"><?php echo $bt['ApproachTypeName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['ApproachTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['ApproachTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="approachTypeName" name="ApproachTypeName" value="" data-field="Cách tiếp cận"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ApproachTypeId" id="approachTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteApproachTypeUrl" value="<?php echo base_url('approachtype/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>