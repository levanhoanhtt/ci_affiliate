<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('producttype/update', array('id' => 'productTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Mảng kinh doanh</th>
                                <th>Cổ phần</th>
                                <th>Ngày hoạt động</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductType">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            foreach($listProductTypes as $pt){ ?>
                                <tr id="productType_<?php echo $pt['ProductTypeId']; ?>">
                                    <td id="productTypeName_<?php echo $pt['ProductTypeId']; ?>"><?php echo $pt['ProductTypeName']; ?></td>
                                    <td id="isShareName_<?php echo $pt['ProductTypeId']; ?>"><label class="<?php echo $labelCss[$pt['IsShare']]; ?>"><?php echo $pt['IsShare'] == 1 ? 'Không' : 'Có'; ?></label></td>
                                    <td id="activeDate_<?php echo $pt['ProductTypeId']; ?>"><?php echo ddMMyyyy($pt['ActiveDate']); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $pt['ProductTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $pt['ProductTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="isShare_<?php echo $pt['ProductTypeId']; ?>" value="<?php echo $pt['IsShare']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="productTypeName" name="ProductTypeName" value="" data-field="Mảng kinh doanh"></td>
                                <td><input type="checkbox" id="iCheck"></td>
                                <td><input type="text" class="form-control datepicker hmdrequired" id="activeDate" name="ActiveDate" value="" autocomplete="off"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductTypeId" id="productTypeId" value="0" hidden="hidden">
                                    <input type="text" name="IsShare" id="isShare" value="1" hidden="hidden">
                                    <input type="text" id="deleteProductTypeUrl" value="<?php echo base_url('producttype/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>