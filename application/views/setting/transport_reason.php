<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('transportreason/update', array('id' => 'transportReasonForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Lý do miễn phí ship</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransportReason">
                            <?php
                            foreach($listTransportReasons as $or){ ?>
                                <tr id="transportReason_<?php echo $or['TransportReasonId']; ?>">
                                    <td id="transportReasonName_<?php echo $or['TransportReasonId']; ?>"><?php echo $or['TransportReasonName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $or['TransportReasonId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $or['TransportReasonId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="transportReasonName" name="TransportReasonName" value="" data-field="Lý do miễn phí ship"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransportReasonId" id="transportReasonId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTransportReasonUrl" value="<?php echo base_url('transportreason/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>