<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('transactionreason/update', array('id' => 'transactionReasonForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Lý do</th>
                                <th>Loại phiếu</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransactionReason">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $transactionTypes = $this->Mconstants->transactionTypes;
                            foreach($listTransactionReasons as $tr){ ?>
                                <tr id="transactionReason_<?php echo $tr['TransactionReasonId']; ?>">
                                    <td id="transactionReasonName_<?php echo $tr['TransactionReasonId']; ?>"><?php echo $tr['TransactionReasonName']; ?></td>
                                    <td id="transactionTypeName_<?php echo $tr['TransactionReasonId']; ?>"><span class="<?php echo $labelCss[$tr['TransactionTypeId']]; ?>"><?php echo $transactionTypes[$tr['TransactionTypeId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $tr['TransactionReasonId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $tr['TransactionReasonId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="transactionTypeId_<?php echo $tr['TransactionReasonId']; ?>" value="<?php echo $tr['TransactionTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="transactionReasonName" name="TransactionReasonName" value="" data-field="Lý do"></td>
                                <td><?php $this->Mconstants->selectConstants('transactionTypes', 'TransactionTypeId'); ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransactionReasonId" id="transactionReasonId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTransactionReasonUrl" value="<?php echo base_url('transactionreason/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>