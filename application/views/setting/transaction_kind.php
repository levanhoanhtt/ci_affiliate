<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('transactionkind/update', array('id' => 'transactionKindForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Loại thu chi</th>
                                <th>Loại phiếu</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransactionKind">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $transactionTypes = $this->Mconstants->transactionTypes;
                            foreach($listTransactionKinds as $tk){ ?>
                                <tr id="transactionKind_<?php echo $tk['TransactionKindId']; ?>">
                                    <td id="transactionKindName_<?php echo $tk['TransactionKindId']; ?>"><?php echo $tk['TransactionKindName']; ?></td>
                                    <td id="transactionTypeName_<?php echo $tk['TransactionKindId']; ?>"><span class="<?php echo $labelCss[$tk['TransactionTypeId']]; ?>"><?php echo $transactionTypes[$tk['TransactionTypeId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $tk['TransactionKindId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $tk['TransactionKindId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="transactionTypeId_<?php echo $tk['TransactionKindId']; ?>" value="<?php echo $tk['TransactionTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="transactionKindName" name="TransactionKindName" value="" data-field="Loại thu chi"></td>
                                <td><?php $this->Mconstants->selectConstants('transactionTypes', 'TransactionTypeId'); ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransactionKindId" id="transactionKindId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTransactionKindUrl" value="<?php echo base_url('transactionkind/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>