<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canEdit){ ?><li><button class="btn btn-primary submit">Lưu</button></li><?php } ?>
                    <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($userId > 0){ ?>
                    <?php echo form_open('api/user/saveUser', array('id' => 'userForm')); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Họ và tên <span class="required">*</span></label>
                                <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $userEdit['FullName']; ?>" data-field="Họ và tên">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Mã nhân viên <span class="required">*</span></label>
                                <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="<?php echo $userEdit['UserName']; ?>" data-field="Tên đăng nhập">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Email <span class="required">*</span></label>
                                <input type="text" name="Email" class="form-control hmdrequired" value="<?php echo $userEdit['Email']; ?>" data-field="Email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Di động <span class="required">*</span></label>
                                <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="<?php echo $userEdit['PhoneNumber']; ?>" data-field="Di động">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Ngày sinh <span class="required">*</span></label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control hmdrequired datepicker" name="BirthDay" value="<?php echo ddMMyyyy($userEdit['BirthDay']); ?>" autocomplete="off" data-field="Ngày sinh">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Trạng thái</label>
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', $userEdit['StatusId']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Facebook</label>
                                <input type="text" name="Facebook" class="form-control" value="<?php echo $userEdit['Facebook']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Giới tính</label>
                                <?php $this->Mconstants->selectConstants('genders', 'GenderId', $userEdit['GenderId']); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Bằng cấp</label>
                                <input type="text" name="DegreeName" class="form-control" value="<?php echo $userEdit['DegreeName']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Tỉnh/ Thành phố</label>
                                <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $userEdit['ProvinceId'], false, '', ' select2'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Quận huyện</label>
                                <?php echo $this->Mdistricts->selectHtml($userEdit['DistrictId']); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Phường xã</label>
                                <?php echo $this->Mwards->selectHtml($userEdit['WardId']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                <input type="text" name="Address" class="form-control hmdrequired" value="<?php echo $userEdit['Address']; ?>" data-field="Địa chỉ">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Nhóm quyền <span class="required">*</span></label>
                            <?php $this->Mconstants->selectObject($listGroups, 'GroupId', 'GroupName', 'GroupIds[]', $groupIds, true, '', ' select2', ' multiple'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="control-label">Số tiền giới hạn định mức (VNĐ)</label>
                                <div class="input-group">
                                    <input type="text" id="maxCost" name="MaxCost" class="form-control" value="<?php echo priceFormat($userEdit['MaxCost']); ?>" style="width: 90%;"<?php if($userEdit['MaxCost'] == 0) echo ' disabled'; ?>>
                                    <span class="input-group-addon" style="float: left;border: none;background: none;">
                                        <span class="item"><input type="checkbox" id="cbUnlimit" class="iCheck"<?php if($userEdit['MaxCost'] == 0) echo ' checked'; ?>> Không giới hạn</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Số tiền đang ứng trước (VNĐ)</label>
                                <input type="text" class="form-control" value="<?php echo priceFormat($userEdit['CostUse']); ?>" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php $avatar = (empty($userEdit['Avatar']) ? NO_IMAGE : $userEdit['Avatar']); ?>
                                <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 200px;height: 200px;display: block;max-width: 100%;">
                                <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="box box-default padding15">
                                <div class="box-body table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Phòng ban</th>
                                            <th>Chức vụ</th>
                                            <th>Từ ngày</th>
                                            <th>Đến ngày</th>
                                            <th style="width: 50px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyUserPart">
                                        <?php foreach($listUserParts as $up){ ?>
                                            <tr id="userPart_<?php echo $up['UserPartId']; ?>">
                                                <td id="partName_<?php echo $up['UserPartId']; ?>"><?php echo $up['PartId'] > 0 ? $this->Mconstants->getObjectValue($listParts, 'PartId', $up['PartId'], 'PartName') : 'Tất cả'; ?></td>
                                                <td id="roleName_<?php echo $up['UserPartId']; ?>"><?php echo $this->Mconstants->getObjectValue($listRoles, 'RoleId', $up['RoleId'], 'RoleName'); ?></td>
                                                <td id="beginDate_<?php echo $up['UserPartId']; ?>"><?php echo ddMMyyyy($up['BeginDate']); ?></td>
                                                <td id="endDate_<?php echo $up['UserPartId']; ?>"><?php echo ddMMyyyy($up['EndDate']); ?></td>
                                                <td class="actions">
                                                    <a href="javascript:void(0)" class="link_edit" data-role-id="<?php echo $up['RoleId']; ?>" data-id="<?php echo $up['UserPartId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                                    <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $up['UserPartId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                    <input type="text" hidden="hidden" id="partId_<?php echo $up['UserPartId']; ?>" value="<?php echo $up['PartId']; ?>">
                                                    <input type="text" hidden="hidden" id="roleId_<?php echo $up['UserPartId']; ?>" value="<?php echo $up['RoleId']; ?>">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><?php echo $this->Mconstants->selectObject($listParts, 'PartId', 'PartName', 'PartId', 0, true); ?></td>
                                            <td>
                                                <?php $this->Mconstants->selectObject($listRoles, 'RoleId', 'RoleName', 'RoleId', 0, true, 'Nhóm', ' select2'); ?>
                                               </td>
                                            <td><input type="text" class="form-control datepicker" id="beginDate" value="" autocomplete="off" data-field="Từ ngày"></td>
                                            <td><input type="text" class="form-control datepicker" id="endDate" value="" autocomplete="off"></td>
                                            <td class="actions">
                                                <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                                <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                                <input type="text" id="userPartId" value="0" hidden="hidden">
                                                <input type="text" id="updatePartUrl" value="<?php echo base_url('api/user/updatePart'); ?>" hidden="hidden">
                                                <input type="text" id="deletePartUrl" value="<?php echo base_url('api/user/deletePart'); ?>" hidden="hidden">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($canEdit){ ?>
                        <ul class="list-inline pull-right margin-right-10">
                            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                            <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Đóng</a></li>
                            <input type="text" name="UserId" id="userId" hidden="hidden" value="<?php echo $userId; ?>">
                            <input type="text" name="UserPass" hidden="hidden" value="<?php echo $userEdit['UserPass']; ?>">
                            <input type="text" hidden="hidden" value="<?php echo base_url('role/listRole'); ?>" id="listRoleUrl">
                            
                        </ul>
                    <?php } ?>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>