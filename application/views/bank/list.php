<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('bank/add'); ?>" class="btn btn-primary">Thên ngân hàng</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('bank'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="BankName" class="form-control" value="<?php echo set_value('BankName'); ?>" placeholder="Tên ngân hàng">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="BankHolder" class="form-control" value="<?php echo set_value('BankHolder'); ?>" placeholder="Chủ tài khoản">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="BankNumber" class="form-control" value="<?php echo set_value('BankNumber'); ?>" placeholder="Số tài khoản">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('customerTypes', 'BankTypeId', set_value('BankTypeId'), true, 'Loại tài khoản'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, 'Trạng thái'); ?>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                 <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Thứ tự</th>
                                <th>Tên ngân hàng</th>                                
                                <th>Chủ tài khoản</th>
                                <th>Số tài khoản</th>
                                <th>Loại tài khoản</th>
                                <th>Chi nhánh</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyBank">
                            <?php $customerTypes = $this->Mconstants->customerTypes;
                            $status = $this->Mconstants->status;
                            $labelCss = $this->Mconstants->labelCss; 
                            foreach($listBanks as $b){ ?>
                                <tr id="bank_<?php echo $b['BankId']; ?>">
                                    <td class="actions">
                                        <div class="btn-group" id="btnGroup_<?php echo $b['BankId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $b['BankId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                                <li><a href="javascript:void(0)" class="link_delete" data-id="<?php echo $b['BankId']; ?>">Xóa</a></li>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $b['BankId']; ?>" value="<?php echo $b['StatusId']; ?>">
                                    </td>
                                    <td>
                                        <?php $attrSelect = ' onchange="changeDisplayOrder(this, \'' . $b['BankId'] . '\')" data-id="'.$b['BankId'].'"';
                                        $this->Mconstants->selectNumber(1, 100, 'DisplayOrder_'.$b['BankId'], $b['DisplayOrder'], true, $attrSelect); ?>
                                    </td>
                                    <td><?php echo $b['BankName']; ?> (<?php echo $b['BankCode']; ?>)</td>
                                    <td><a href="<?php echo base_url('bank/edit/'.$b['BankId']); ?>"><?php echo $b['BankHolder']; ?></a></td>
                                    <td><?php echo $b['BankNumber']; ?></td>
                                    <td><span class="<?php echo $labelCss[$b['BankTypeId']]; ?>"><?php echo $customerTypes[$b['BankTypeId']]; ?></span></td>
                                    <td><?php echo $b['BranchName']; ?></td>
                                    <td id="statusName_<?php echo $b['BankId']; ?>"><span class="<?php echo $labelCss[$b['StatusId']]; ?>"><?php echo $status[$b['StatusId']]; ?></span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('bank/changeStatus'); ?>">
                <input type="text" hidden="hidden" id="changeDisplayOrderUrl" value="<?php echo base_url('bank/changeDisplayOrder'); ?>">
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>