<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
        <?php if ($customerId > 0) { ?>
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                
            </section>
            <section class="content">
                <div class="row">
                	<?php echo form_open('api/customer/updateCollaborator', array('id' => 'collaboratorForm')); ?>
                	<div class="col-sm-3 no-padding"></div>
                    <div class="col-sm-6 no-padding">
                    	<div class="nav-tabs-custom">
                    		<div class="tab-content">
                    			<div class="box-body table-responsive divTable">
                    				<ul class="list-inline">
					                	<li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
					                    <li><a href="<?php echo base_url('customer/collaborator'); ?>" class="btn btn-default">Đóng</a></li>
					                </ul>
                    				<div class="form-group">
                    					<label class="control-label">Số chứng minh nhân dân <span class="required">*</span></label>
                    					<input type="hidden" hidden="hidden" class="form-control" id="customerId" value="<?= $customerId ?>"  >
                    					<input type="text" class="form-control hmdrequired" id="iDCardNumber" value="<?= $customer['IDCardNumber'] ?>"  data-field="Số chứng minh nhân dân">
                    				</div>
                    				<div class="form-group">
                    					<label class="control-label">Họ và tên <span class="required">*</span></label>
                    					<input type="text" class="form-control hmdrequired" id="fullName" value="<?= $customer['FullName'] ?>"  data-field="Họ và tên">
                    				</div>
                    				<div class="form-group">
                    					<label class="control-label">Ngày sinh <span class="required">*</span></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker hmdrequired" id="birthDay" data-field="Ngày sinh" autocomplete="off" value="<?= ddMMyyyy($customer['BirthDay']) ?>">
                                        </div>
                    				</div>
                    				<div class="form-group">
                    					<label class="control-label">Nơi cấp <span class="required">*</span></label>

                    					<input type="text" class="form-control hmdrequired" id="iDCardAddress" value="<?= $customer['IDCardAddress'] ?>" data-field="Nơi cấp">
                    				</div>
                    				<div class="form-group">
                    					<label class="control-label">Ngày cấp <span class="required">*</span></label>
                    					<div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker hmdrequired" id="iDCardDate" name="IDCardDate" data-field="Ngày cấp" autocomplete="off" value="<?= ddMMyyyy($customer['IDCardDate']) ?>">
                                        </div>
                    				</div>
                    				<div class="row">
                    					<div class="col-sm-6 form-group">
                    						<?php $image = json_decode($customer['IDCardImages']); ?>
                    						<label class="control-label">Mặt trước</label>
                    						<div class="box-tools pull-right">
		                                        <button type="button" class="btn btn-primary" id="btnUpImage2"><i class="fa fa-upload"></i> Hình mặt trước</button>
		                                    </div>
		                                    <div class="box-body">
		                                    	<img src="<?= $image[0] ?>" style="width: 100%;" id="productImage1">
		                                    </div>
                    					</div>
                    					<div class="col-sm-6 form-group">
                    						<label class="control-label">Mặt sau</label>
                    						<div class="box-tools pull-right">
		                                        <button type="button" class="btn btn-primary" id="btnUpImage3"><i class="fa fa-upload"></i> Hình mặt sau</button>
		                                    </div>
		                                    <div class="box-body">
		                                    	<img src="<?= $image[1] ?>" style="width: 100%;" id="productImage2">
		                                    </div>
                    					</div>
                    				</div>
                    				<ul class="list-inline">
					                	<li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
					                    <li><a href="<?php echo base_url('customer/collaborator'); ?>" class="btn btn-default">Đóng</a></li>
					                </ul>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="col-sm-3 no-padding"></div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>