<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-3">
                        <div class="box box-solid">
                            <div class="box-body no-padding">
                                <ul class="nav nav-pills nav-stacked" id="ulLinks">
                                    <li class="active"><a href="#divCustomer_1" data-toggle="tab" data-id="1"><i class="fa fa-info"></i> Thông tin cá nhân</a></li>
                                    <li><a href="#divCustomer_2" data-toggle="tab" data-id="2"><i class="fa fa-bank"></i> Thông tin thanh toán</a></li>
                                    <li><a href="#divCustomer_3" data-toggle="tab" data-id="3"><i class="fa fa-lock"></i> Đổi mật khẩu</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                        <div class="box box-primary divCustomer tab-pane fade in active" id="divCustomer_1">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thông tin cá nhân</h3>
                            </div>
                            <div class="box-body">
                                <?php echo form_open('api/customer/updateCollaborator', array('id' => 'collaboratorForm')); ?>
                                <div class="form-group">
                                    <label class="control-label">Số chứng minh thư <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="IDCardNumber" id="iDCardNumber" value="<?php echo $customer['IDCardNumber']; ?>" data-field="Số chứng minh thư">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Họ và tên <span class="required">*</span></label>
                                    <input type="text" id="fullName" class="form-control hmdrequired" value="<?php echo $customer['IDCardNumber']; ?>" data-field="Họ và tên">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ngày sinh </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" id="birthDay" value="<?php echo ddMMyyyy($customer['BirthDay']); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nơi cấp <span class="required">*</span></label>
                                    <input type="text" class="form-control hmdrequired" name="IDCardAddress" id="iDCardAddress" value="<?php echo $customer['IDCardAddress']; ?>" data-field="Nơi cấp">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ngày cấp <span class="required">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker hmdrequired" id="iDCardDate" name="IDCardDate" data-field="Ngày cấp" autocomplete="off" value="<?= ddMMyyyy($customer['IDCardDate']) ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <?php $iDCardImages = $customer['IDCardImages'];
                                    if(!empty($iDCardImages)) $iDCardImages = json_decode($iDCardImages, true);
                                    else $iDCardImages = array();
                                    $image1 = $image2 = '';
                                    if(count($iDCardImages) == 2){
                                        $image1 = $iDCardImages[0]; //USER_PATH.
                                        $image2 = $iDCardImages[1]; //USER_PATH.
                                    }
                                    elseif(count($iDCardImages) == 1) $image1 = USER_PATH.$iDCardImages[0]; ?>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Mặt trước&nbsp;&nbsp; <button type="button" class="btn btn-info btn-sm pull-right" id="btnUpImage2"><i class="fa fa-upload"></i></button></label>
                                            <img src="<?php echo $image1; ?>" id="productImage1" style="width: 100%;<?php if(empty($image1)) echo 'display: none;'; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Mặt sau&nbsp;&nbsp; <button type="button" class="btn btn-info btn-sm pull-right" id="btnUpImage3"><i class="fa fa-upload"></i></button></label>
                                            <img src="<?php echo $image2; ?>" id="productImage2" style="width: 100%;<?php if(empty($image2)) echo 'display: none;'; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success update_collaborator">Cập nhật</button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="box box-primary divCustomer tab-pane fade in" id="divCustomer_2" >
                            <div class="box-header with-border">
                                <h3 class="box-title">Thông tin thanh toán</h3>
                            </div>
                            <div class="box-body">
                                <?php echo form_open('customerbank/update', array('id' => 'customerbankForm')); ?>

                                <div class="col-sm-12">
                                    <div class="row form-group">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="radio" name="radio_check" class="radio_check" value="1" checked> Có tài khoản ngân hàng
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="radio" name="radio_check" class="radio_check" value="2"> Không có
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account_bank">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Tên ngân hàng</label>
                                                <input type="text" class="form-control hmdrequired" id="bankName" name="BankName" data-field="Tên ngân hàng" value="<?= count($customerbank) > 0 ? $customerbank[0]['BankName']:'';?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Chi nhánh</label>
                                                <input type="text" class="form-control hmdrequired" id="branchName" name="BranchName" data-field="Chi nhánh" value="<?= count($customerbank) > 0 ? $customerbank[0]['BranchName']:'';?>">
                                                <label><i><span style="color: red">VD:</span> Ngân hàng BIDV chi nhánh Phù Ninh - tỉnh Phú Thọ</i></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Chủ tài khoản</label>
                                                <input type="text" class="form-control" id="bankHolder" name="BankHolder" data-field="Chủ tài khoản" value="<?= count($customerbank) > 0 ? $customerbank[0]['BankHolder']:'';?>">
                                                <label><i><span style="color: red">VD:</span> VU KIM LE</i></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Số tài khoản</label>
                                                <input type="text" class="form-control hmdrequired" id="bankNumber" name="BankNumber" data-field="Số tài khoản" value="<?= count($customerbank) > 0 ? $customerbank[0]['BankNumber']:'';?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success update_customerbank">Cập nhật</button>
                                        <input type="text" hidden="hidden" id="customerBankId" name="CustomerBankId" value="<?= count($customerbank) > 0 ? $customerbank[0]['CustomerBankId']:0;?>">
                                    </div>
                                </div>
                                <div class="not_account_bank" style="display: none">
                                    <div class="col-sm-12">
                                        <span class="text-center">Tiền sẽ được chuyển đến bạn qua CMND, hãy ra bưu điện Việt Nam gần nhà để nhận!</span>
                                    </div>
                                </div>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="box box-primary divCustomer tab-pane fade in" id="divCustomer_3" >
                            <div class="box-header with-border">
                                <h3 class="box-title">Đổi mật khẩu</h3>
                            </div>
                            <div class="box-body">
                                <?php echo form_open('api/customer/updatePassword', array('id' => 'updatePasswordForm')); ?>
                                    <div class="form-group">
                                        <label class="control-label">Mật khẩu cũ <span class="required">*</span></label>
                                        <input type="password" class="form-control hmdrequired" id="passwordOld" data-field="Mật khẩu cũ">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Mật khẩu mới <span class="required">*</span></label>
                                        <input type="password" class="form-control hmdrequired" id="passwordNew" data-field="Mật khẩu mới">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nhập lại mật khẩu mới <span class="required">*</span></label>
                                        <input type="password" class="form-control hmdrequired" id="repeatPassword" data-field="Nhập lại mật khẩu mới">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success updatePassword">Cập nhật</button>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
            <input type="text" hidden="hidden" id="customerId" name="CustomerId" value="<?= $customer['CustomerId']?>">
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>