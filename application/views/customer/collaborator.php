<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
        <div class="container-fluid">
            <style>
                .list-his{font-size: 14px !important;}
                .list-his li{line-height: 34px;}
            </style>
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl list-his mgt-10"></ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả CTV</a></li>
                        <?php foreach ($listFilters as $f) { ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group margin ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả vận chuyển theo</span>:</label>
                            <form class="form-inline">
                                 <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="total_price">Số tiền đã mua</option>
                                        <option value="total_order">Số đơn hàng</option>
                                        <option value="customer_balance">Số dư tài khoản</option>
                                        <!--<option value="customer_kind">Loại CTV</option>-->
                                        <option value="order_create">Thời điểm đặt hàng</option>
                                        <option value="receive_ad">Nhận email quảng cáo</option>
                                        <!--<option value="order_un_finish">Có đơn hàng chưa hoàn tất</option>-->
                                        <option value="customer_status">Tình trạng tài khoản</option>
                                        <<option value="customer_tag">Được tag với</option>
                                        <option value="customer_address">Địa chỉ</option>
                                        <option value="customer_group">Nhóm CTV</option>
                                        <option value="month_birth">Tháng sinh</option>
                                        <option value="customer_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 total_price total_order customer_balance block-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 receive_ad customer_status customer_tag customer_address customer_group month_birth none-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!--<select class="form-control customer_kind none-display">
                                        <option value="1">Khách lẻ</option>
                                        <option value="2">Khách buôn</option>
                                        <option value="3">Khách CTV</option>
                                    </select>-->
                                    <select class="form-control receive_ad none-display">
                                        <option value="2">Có</option>
                                        <option value="1">Không</option>
                                    </select>
                                    <select class="form-control customer_status none-display">
                                        <?php foreach($this->Mconstants->status as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control customer_group none-display">
                                        <?php foreach($listCustomerGroups as $cg){ ?>
                                            <option value="<?php echo $cg['CustomerGroupId']; ?>"><?php echo $cg['CustomerGroupName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control month_birth none-display">
                                        <?php for($i = 1; $i < 13; $i++){ ?>
                                            <option value="<?php echo $i; ?>">Tháng <?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_create none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control customer_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control total_price total_order customer_balance input-number block-display" type="text">
                                    <input class="form-control customer_address customer_tag none-display" type="text">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker order_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/customer/searchByFilter/3'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <input type="text" class="form-control" id="itemSearchName">
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div>
                    <div class="box-header with-border">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="add_tags">Thêm nhãn</option>
                            <option value="delete_tags">Bỏ nhãn</option>
                            <!-- <option value="create_group">Tạo nhóm CTV</option> -->
                        </select>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Tên CTV</th>
                                <th>Mã CTV</th>
                                <!-- <th>Ngày tạo</th>
                                <th>Số điện thoại</th>
                                <th>Địa chỉ</th> -->
                                <th class="text-right">Tổng ĐH</th>
                                <th class="text-right">HH đang xử lý</th>
                                <th class="text-right">HH thành công</th>
                                <th class="text-right">HH đã trả</th>
                                <th class="text-right">HH còn lại</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomer"></table>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="5">
                    <input type="text" hidden="hidden" id="searchProductUrl" value="<?php echo base_url('api/customer/searchByFilter'); ?>/">
                    <input type="hidden" value="<?php echo base_url('customer/editCollaborator')?>" id="urlEditCustomer">
                    <input type="text" hidden="hidden" id="customerIds" name="">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>