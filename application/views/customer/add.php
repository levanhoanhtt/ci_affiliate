<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Lưu</button></li>
                    <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('api/customer/update', array('id' => 'customerForm')); ?>
                <div class="flexbox-annotated-section">
                    <div class="flexbox-annotated-section-annotation">
                        <div class="annotated-section-title pd-all-20"><h2>Thông tin chung</h2></div>
                        <div class="annotated-section-description pd-all-20 p-none-t">
                            <p class="color-note tabcustomer in privateCus">Một số thông tin cơ bản của CTV.</p>
                            <p class="color-note tabcustomer corporateCus">Một số thông tin cơ bản của người đại diện doanh nghiệp.</p>
                        </div>
                    </div>
                    <div class="flexbox-annotated-section-content">
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 10px;">
                                        <ul class="nav nav-tabs" id="ulCustomerKindId">
                                            <li class="active">
                                                <a href="javascript:void(0)" data-toggle="tab" data-id="3">CTV</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio-group">
                                            <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="1" checked> Cá nhân</span>
                                            <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="2"> Công ty</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Họ <span class="required">*</span></label>
                                            <input type="text" id="firstName" class="form-control hmdrequired" value="" data-field="Họ">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Tên <span class="required">*</span></label>
                                            <input type="text" id="lastName" class="form-control hmdrequired" value="" data-field="Tên">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Số điện thoại <span class="required">*</span></label>
                                            <input type="text" id="phoneNumber" class="form-control hmdrequired" value="" data-field="Số điện thoại">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Email </label>
                                            <input type="text" id="email" class="form-control " value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Giới tính</label>
                                            <div class="radio-group">
                                                <span class="item"><input type="radio" name="GenderId" class="iCheck" value="1" checked> Nam</span>
                                                <span class="item"><input type="radio" name="GenderId" class="iCheck" value="2"> Nữ</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Ngày sinh </label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" class="form-control datepicker" id="birthDay" value="" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Facebook</label>
                                            <input type="text" id="facebook" class="form-control" name="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row tabcustomer corporateCus">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Chức vụ </label>
                                            <input type="text" id="positionName" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="corporateCus tabcustomer">
                    <hr class="hr-new">
                    <div class="flexbox-annotated-section">
                        <div class="flexbox-annotated-section-annotation">
                            <div class="annotated-section-title pd-all-20">
                                <h2>Thông tin công ty</h2>
                            </div>
                            <div class="annotated-section-description pd-all-20 p-none-t">
                                <p class="color-note">Nhập thông tin về công ty.</p>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section-content">
                            <div class="box box-default padding15">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Tên công ty </label>
                                                <input type="text" id="companyName" class="form-control" value="">
                                            </div>
                                        </div>
                                        <!-- <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Mã số thuế </label>
                                                <input type="text" id="taxCode" class="form-control" value="">
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="hr-new">
                <div class="flexbox-annotated-section">
                    <div class="flexbox-annotated-section-annotation">
                        <div class="annotated-section-title pd-all-20">
                            <h2>Địa chỉ</h2>
                        </div>
                        <div class="annotated-section-description pd-all-20 p-none-t">
                            <p class="color-note tabcustomer in privateCus">Địa chỉ chính của CTV này.</p>
                            <p class="color-note tabcustomer corporateCus">Địa chỉ của công ty.</p>
                        </div>
                    </div>
                    <div class="flexbox-annotated-section-content">
                        <div class="box box-default padding15">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Quốc gia </label>
                                            <?php $this->Mconstants->selectObject($listCountries, 'CountryId', 'CountryName', 'CountryId', 0, false, '', ' select2'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 VNon">
                                        <div class="form-group">
                                            <label class="control-label">Tỉnh / Thành </label>
                                            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', 0, true, '--Tỉnh / Thành--', ' select2'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 VNoff" style="display: none">
                                        <div class="form-group">
                                            <label class="control-label">Postal / Zip Code</label>
                                            <input type="text" id="zipCode" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row VNon">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Quận/ Huyện</label>
                                            <?php echo $this->Mdistricts->selectHtml(); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Phường / Xã </label>
                                            <?php echo $this->Mwards->selectHtml(); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Địa chỉ </label>
                                    <input type="text" id="address" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="hr-new">
                <div class="flexbox-annotated-section">
                    <div class="flexbox-annotated-section-annotation">
                        <div class="annotated-section-title pd-all-20">
                            <h2>Chứng minh thư</h2>
                        </div>
                    </div>
                    <div class="flexbox-annotated-section-content">
                        <div class="box box-default padding15">
                            <div class="box-body">
                                <div class="row">
                                   <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Số chứng minh thư <span class="required">*</span></label>
                                            <input type="text" class="form-control hmdrequired" id="iDCardNumber" data-field="Số chứng minh thư" name="IDCardNumber">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                            <input type="text" class="form-control hmdrequired" id="iDCardAddress" data-field="Địa chỉ" name="IDCardAddress">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Ngày cấp <span class="required">*</span></label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" class="form-control datepicker hmdrequired" id="iDCardDate" name="IDCardDate" value="" data-field="Ngày cấp" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Hình chứng mình thư <span class="required">*</span></label>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" id="btnUpImage1"><i class="fa fa-upload"></i> Chọn hình</button>
                                            </div>
                                            <div class="box-body">
                                                <ul class="list-inline" id="ulImages"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="privateCus tabcustomer in">
                    <hr class="hr-new">
                    <div class="flexbox-annotated-section">
                        <div class="flexbox-annotated-section-annotation">
                            <div class="annotated-section-title pd-all-20">
                                <h2>Ghi chú</h2>
                            </div>
                            <div class="annotated-section-description pd-all-20 p-none-t">
                                <p class="color-note">Nhập ghi chú về CTV.</p>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section-content">
                            <div class="box box-default padding15">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Nhóm CTV</label>
                                                <select class="form-control" name="CustomerGroupId" id="customerGroupId">
                                                    <option value="0">--Chọn--</option>
                                                    <?php foreach($listCustomerGroups as $cg){ ?>
                                                        <option value="<?php echo $cg['CustomerGroupId']; ?>" class="op op_<?php echo $cg['CustomerKindId']; ?>"<?php if($cg['CustomerKindId'] > 1) echo ' style="display: none;"'; ?>><?php echo $cg['CustomerGroupName']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ghi chú </label>
                                        <textarea class="form-control" id="customerComment" rows="2"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                        <input type="text" class="form-control" id="tags">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Thêm các nhãn đã có</label>
                                        <ul class="list-inline" id="ulTagExist">
                                            <?php foreach($listTags as $t) { ?>
                                                <li><a href="javascript:void(0)"><?php echo $t['TagName']; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="hr-new">
                    <div class="flexbox-annotated-section">
                        <div class="flexbox-annotated-section-annotation">
                            <div class="annotated-section-title pd-all-20">
                                <h2>Cài đặt nâng cao</h2>
                            </div>
                            <div class="annotated-section-description pd-all-20 p-none-t">
                                <p class="color-note">Cài đặt nâng cao</p>
                            </div>
                        </div>
                        <div class="flexbox-annotated-section-content">
                            <button class="advance-btn"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                            <div class="box box-default padding15 advance-tab" style="display: none">
                                <div class="box-body">
                                    <div class="row">
                                        <!-- <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Nhân viên chăm sóc chính</label>
                                                <?php // $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'CareStaffId', 0, true, '--Nhân viên chăm sóc chính--', ' select2'); ?>
                                            </div>
                                        </div> -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Chiết khấu giá</label>
                                                <?php $this->Mconstants->selectConstants('discountType', 'DiscountTypeId', 0, true, '--Chiết khấu giá--'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="divWholesale" style="display: none;">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Kì hạn thanh toán</label>
                                                <?php $this->Mconstants->selectConstants('paymentTime', 'PaymentTimeId', 0, true, '--Kì hạn thanh toán--'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Mật khẩu</label>
                                                 <input type="text" id="password" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Nhắc lại mật khẩu</label>
                                                <input type="text" id="rePass" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                    <li><a href="<?php echo base_url('customer'); ?>" id="customerListUrl" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="customerId" value="0">
                    <input type="text" hidden="hidden" id="statusId" value="2">
                    <input type="text" hidden="hidden" id="customerKindId" value="3">
                </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>