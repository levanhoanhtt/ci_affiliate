<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
        <?php if ($customerId > 0) { ?>
            <section class="content-header">
                <h1><?php echo $title.$customer['FullName']; ?></h1>
                
            </section>
            <section class="content">
                <div class="row">
                	<?php echo form_open('api/customer/updatePassword', array('id' => 'updatePasswordForm')); ?>
                	<div class="col-sm-3 no-padding"></div>
                    <div class="col-sm-6 no-padding">
                    	<div class="nav-tabs-custom">
                    		<div class="tab-content">
                    			<div class="box-body table-responsive divTable">
                    				<ul class="list-inline">
					                	<li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
					                    <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
					                </ul>
                    				<div class="row">
                    					<div class="form-group">
                    						<label class="control-label">Mật khẩu cũ <span class="required">*</span></label>
                    						<input type="password" class="form-control hmdrequired" id="passwordOld" data-field="Mật khẩu cũ">
                    					</div>
                    					<div class="form-group">
                    						<label class="control-label">Mật khẩu mới <span class="required">*</span></label>
                    						<input type="password" class="form-control hmdrequired" id="passwordNew" data-field="Mật khẩu mới">
                    					</div>
                    					<div class="form-group">
                    						<label class="control-label">Nhập lại mật khẩu mới <span class="required">*</span></label>
                    						<input type="password" class="form-control hmdrequired" id="repeatPassword" data-field="Nhập lại mật khẩu mới">
                    					</div>
                    				</div>
                    				<ul class="list-inline">
					                	<li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
					                    <li><a href="<?php echo base_url('customer'); ?>" class="btn btn-default">Đóng</a></li>
					                </ul>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="col-sm-3 no-padding"></div>
                    <?php echo form_close(); ?>
                </div>
            </section>
            <input type="hidden" hidden="hidden" value="<?= $customer['CustomerId'] ?>"  id="customerId">
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>