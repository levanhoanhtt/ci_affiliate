<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('promotion/add'); ?>" class="btn btn-primary">Tạo khuyến mại</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả khuyến mại</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả nhập kho theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="promotion_types">Loại khuyến mại</option>
                                        <option value="promotion_status">Trạng thái</option>
                                        <option value="promotion_province">Áp dụng cho tỉnh</option>
                                        <option value="promotion_customer_group">Nhóm khách hàng</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 promotion_types promotion_status promotion_province promotion_customer_group display-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control promotion_types block-display">
                                        <?php foreach($this->Mconstants->promotionTypes as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control promotion_status none-display">
                                        <?php foreach($this->Mconstants->promotionStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control promotion_province none-display">
                                        <?php foreach($listProvinces as $p){ ?>
                                            <option value="<?php echo $p['ProvinceId']; ?>"><?php echo $p['ProvinceName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control promotion_customer_group none-display">
                                        <?php foreach($listCustomerGroups as $cg){ ?>
                                            <option value="<?php echo $cg['CustomerGroupId']; ?>"><?php echo $cg['CustomerGroupName']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('promotion/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="change_status-2">Kích hoạt</option>
                            <option value="change_status-4">Ngừng kích hoạt</option>
                            <option value="change_status-4">Ngừng khuyến mại</option>
                            <option value="change_status-0">Xóa khuyến mại</option>
                        </select>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã/ Tên</th>
                                <th>Loại</th>
                                <th>Trạng thái</th>
                                <th>Chi tiết</th>
                                <th>Số lần sử dụng</th>
                                <th>Bắt đầu/ kết thúc</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPromotion">
                           
                           <!--  <?php // $labelCss = $this->Mconstants->labelCss;
                           // $promotionStatus = $this->Mconstants->promotionStatus;
                           // $promotionTypes = $this->Mconstants->promotionTypes;
                           // $discountTypes = $this->Mconstants->discountTypes;
                          //  $customerNames = array();
                           // $productNames = array();
                           // $productChildNames = array();
                          //  foreach($listPromotions as $p){ // ?>
                                <tr id="trItem_<?php// echo $p['PromotionId']; ?>">
                                    <td><input type="checkbox" class="iCheckTable iCheckItem" value="<?php //echo $p['PromotionId']; ?>"></td>
                                    <td><?php// echo $p['PromotionName']; ?></td>
                                    <td><span class="<?php // echo $labelCss[$p['PromotionTypeId']]; ?>"><?php // echo $promotionTypes[$p['PromotionTypeId']]; ?></span></td>
                                    <td><span class="<?php // echo $labelCss[$p['PromotionStatusId']]; ?>"><?php // echo $promotionStatus[$p['PromotionStatusId']]; ?></span></td>
                                    <td>
                                        <?php // if($p['ReduceTypeId'] == 3){
                                          //  echo 'Miễn phí vận chuyển đối với mức phí vận chuyển nhỏ hơn hoặc bằng '.$p['ReduceNumber'].' VNĐ áp dụng cho ';
                                         //   if($p['ProvinceId'] > 0) echo $this->Mconstants->getObjectValue($listProvinces, 'ProvinceId', $p['ProvinceId'], 'ProvinceName');
                                         //   else echo 'tất cả tỉnh thành';
                                       // }
                                      //  else{
                                      //      echo 'Giảm '.priceFormat($p['ReduceNumber']);
                                      //      if($p['ReduceTypeId'] == 1) echo ' VNĐ ';
                                      //      else echo ' % ';
                                      //      if($p['PromotionItemTypeId'] == 0) echo 'cho tất cả đơn hàng';
                                      //      elseif($p['PromotionItemTypeId'] == 6) echo 'cho đơn hàng có giá trị từ '.priceFormat($p['MinimumCost']).' VNĐ';
                                      //      elseif($p['PromotionItemId'] > 0){
                                      //          if($p['PromotionItemTypeId'] == 1){
                                       //             echo 'cho Nhóm sản phẩm '.$this->Mconstants->getObjectValue($listCategories, '//CategoryId', $p['PromotionItemId'], 'CategoryName');
                                       //             echo ' ('.$discountTypes[$p['DiscountTypeId']].')';
                                         //       }
                                        //        elseif($p['PromotionItemTypeId'] == 11) echo 'cho Nhóm khách hàng '.$this->Mconstants->getObjectValue($listCustomerGroups, 'CustomerGroupId', $p['PromotionItemId'], 'CustomerGroupName');
                                                //elseif($p['PromotionItemTypeId'] == 3){
                                                   // echo 'cho Sản phẩm ';
                                                    //if(!isset($productNames[$p['PromotionItemId']])) $productNames[$p['PromotionItemId']] = $this->Mproducts->getProductName($p['PromotionItemId']);
                                                    //echo $productNames[$p['PromotionItemId']].' ('.$discountTypes[$p['DiscountTypeId']].')';
                                              //  }
                                                //elseif($p['PromotionItemTypeId'] == 13){ //sp con
                                                    //echo 'cho Sản phẩm ';
                                                    //if(!isset($productChildNames[$p['PromotionItemId']])) $productChildNames[$p['PromotionItemId']] = $this->Mproducts->getProductName(0, $p['PromotionItemId']);
                                                    //echo $productChildNames[$p['PromotionItemId']];
                                              //  }
                                                //elseif($p['PromotionItemTypeId'] == 5){
                                                    //echo 'cho Khách hàng ';
                                                    //if(!isset($customerNames[$p['PromotionItemId']])) $customerNames[$p['PromotionItemId']] = $this->Mcustomers->getFieldValue(array('CustomerId' => $p['PromotionItemId']), 'FullName');
                                                    //echo $customerNames[$p['PromotionItemId']];
                                               // }
                                           // }
                                        //}
                                        //if($p['PromotionTypeId'] == 2 && $p['ProductNumber'] > 0) echo ' (số lượng tối thiểu là '.$p['ProductNumber'].')'; ?>
                                    </td>
                                    <td>0/ <?php //echo $p['IsUnLimit'] == 2 ? '∞' : $p['NumberUse'] ?></td>
                                    <td>
                                        <p>Bắt đâu: <span><?php //echo ddMMyyyy($p['BeginDate']); ?></span></p>
                                        <p>Kết thúc: <span><?php //echo empty($p['EndDate']) ? '--' : ddMMyyyy($p['EndDate']); ?></span></p>
                                    </td>
                                </tr>
                            <?php // } ?>                         
 -->
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeItemStatusUrl" value="<?php echo base_url('promotion/changeStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="itemTypeId" value="12">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>