<div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"><i class="fa fa-comments-o"></i> Ghi chú <?php echo isset($itemName) ? $itemName : ''; ?></h4>
            </div>
            <div class="modal-body">
                <div class="listComment">
                    <?php foreach($listItemComments as $ic){ ?>
                        <div class="box-customer mb10">
                            <table>
                                <tbody>
                                <tr>
                                    <th rowspan="2" valign="top"><img src="assets/vendor/dist/img/users2.png" alt=""></th>
                                    <th><a href="javascript:void(0)" class="name"><?php echo $ic['FullName']; ?></a></th>
                                    <th class="time"><?php echo ddMMyyyy($ic['CrDateTime'], 'H:i d/m/Y'); ?></th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p class="pComment"><?php echo $ic['Comment']; ?></p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>