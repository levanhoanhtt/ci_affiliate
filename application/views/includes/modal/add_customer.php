<div class="modal fade" id="modalAddCustomer" role="dialog" aria-labelledby="modalAddCustomer">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thêm khách hàng</h4>
            </div>
            <div class="modal-body">
                <section class="content">
                    <style>#customerForm .box-default{border: 1px solid #d2d6de;}</style>
                    <?php echo form_open('api/customer/update', array('id' => 'customerForm')); ?>
                    <div class="box box-default padding15">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 10px;">
                                    <ul class="nav nav-tabs" id="ulCustomerKindId">
                                        <li class="active">
                                            <a href="javascript:void(0)" data-toggle="tab" data-id="3">Khách CTV</a>
                                        </li>
                                    </ul>
                                    <input type="text" hidden="hidden" id="customerKindId" value="1">
                                </div>
                                <div class="col-sm-6">
                                    <div class="radio-group">
                                        <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="1" checked> Cá nhân</span>
                                        <span class="item"><input type="radio" name="CustomerTypeId" class="iCheck iCheckCustomerType" value="2"> Công ty</span>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6">
                                    <div class="form-group">
                                        <span class="item"><input type="checkbox" class="iCheck" id="isReceiveAd"> Có Nhận Quảng Cáo</span>
                                    </div>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Họ <span class="required">*</span></label>
                                        <input type="text" id="firstName" class="form-control hmdrequired" value="" data-field="Họ">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Tên <span class="required">*</span></label>
                                        <input type="text" id="lastName" class="form-control hmdrequired" value="" data-field="Tên">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Số điện thoại <span class="required">*</span></label>
                                        <input type="text" id="phoneNumber" class="form-control hmdrequired" value="" data-field="Số điện thoại">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Email </label>
                                        <input type="text" id="email" class="form-control " value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Giới tính</label>
                                        <div class="radio-group">
                                            <span class="item"><input type="radio" name="GenderId" class="iCheck" value="1" checked> Nam</span>
                                            <span class="item"><input type="radio" name="GenderId" class="iCheck" value="2"> Nữ</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Ngày sinh </label>
                                        <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            <input type="text" class="form-control datepicker" id="birthDay" value="" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Facebook</label>
                                        <input type="text" id="facebook" class="form-control" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú </label>
                                <textarea class="form-control" id="customerComment" rows="2"></textarea>
                            </div>
                            <div id="divCompany" class="row" style="display: none;">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Chức vụ </label>
                                        <input type="text" id="positionName" class="form-control" value="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tên công ty </label>
                                        <input type="text" id="companyName" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Mã số thuế </label>
                                        <input type="text" id="taxCode" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-default padding15">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Địa chỉ </label>
                                        <input type="text" id="address" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group ">
                                        <label class="control-label">Quốc gia </label>
                                        <?php $this->Mconstants->selectObject($listCountries, 'CountryId', 'CountryName', 'CountryId', 232, false, '', ' select2'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group VNon">
                                        <label class="control-label">Tỉnh / Thành </label>
                                        <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', 0, true, '', ' select2'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group VNon">
                                        <label class="control-label">Quận/ Huyện</label>
                                        <?php if (!isset($listDistricts)) $listDistricts = array();
                                        echo $this->Mdistricts->selectHtml(0, 'DistrictId', $listDistricts); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group VNon">
                                        <label class="control-label">Phường / Xã </label>
                                        <?php echo $this->Mwards->selectHtml(); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 VNoff" style="display: none">
                                    <div class="form-group">
                                        <label class="control-label">ZipCode</label>
                                        <input type="text" id="zipCode" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                <div class="box box-default padding15">
                    <div class="box-body">
                    <div class="row">
                       <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Số chứng minh thư <span class="required">*</span></label>
                                <input type="text" class="form-control hmdrequired" id="iDCardNumber" data-field="Số chứng minh thư" name="IDCardNumber">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                <input type="text" class="form-control hmdrequired" id="iDCardAddress" data-field="Địa chỉ" name="IDCardAddress">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Ngày cấp <span class="required">*</span></label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker hmdrequired" id="iDCardDate" name="IDCardDate" value="" data-field="Ngày cấp" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Hình chứng mình thư <span class="required">*</span></label>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnUpImage1"><i class="fa fa-upload"></i> Chọn hình</button>
                                </div>
                                <div class="box-body">
                                    <ul class="list-inline" id="ulImages"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>    
                </div>
                    <div class="box box-default padding15">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Nhóm khách hàng</label>
                                        <select class="form-control" name="CustomerGroupId" id="customerGroupId">
                                            <option value="0">--Chọn--</option>
                                            <?php foreach($listCustomerGroups as $cg){ ?>
                                                <option value="<?php echo $cg['CustomerGroupId']; ?>" class="op op_<?php echo $cg['CustomerKindId']; ?>"<?php if($cg['CustomerKindId'] > 1) echo ' style="display: none;"'; ?>><?php echo $cg['CustomerGroupName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Nhân viên chăm sóc chính</label>
                                        <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'CareStaffId', 0, true, '--Chọn--', ' select2'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Chiết khấu giá</label>
                                        <?php $this->Mconstants->selectConstants('discountType', 'DiscountTypeId', 0, true, '--Chọn--'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divWholesale" style="display: none;">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Cấu hình nợ </label>
                                        <input type="text" id="debtCost" class="form-control cost" value="0">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Kì hạn thanh toán</label>
                                        <?php $this->Mconstants->selectConstants('paymentTime', 'PaymentTimeId', 0, true, '--Chọn--'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Mật khẩu</label>
                                        <input type="text" id="password" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Nhắc lại mật khẩu</label>
                                        <input type="text" id="rePass" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnUpdateCustomer">Cập nhật</button>
            </div>
        </div>
    </div>
</div>
