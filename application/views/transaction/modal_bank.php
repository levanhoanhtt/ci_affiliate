<div class="modal fade" id="modalChooseBank" tabindex="-1" role="dialog" aria-labelledby="modalChooseBank">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Chọn tài khoản ngân hàng</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">Loại tài khoản</label>
                        <div class="radio-group">
                            <span class="item"><input type="radio" name="BankTypeId" class="iCheck iCheckBankType" value="1" checked> Cá nhân</span>
                            <span class="item"><input type="radio" name="BankTypeId" class="iCheck iCheckBankType" value="2"> Công ty</span>
                        </div>
                    </div>
                </div>
                <style>
                    #listBank li{cursor:pointer;margin:10px 0;border:1px solid #eee;padding:10px;}
                    #listBank li:hover{background-color: #ECEFF2;}
                    #listBank li.active{background-color: #5EADF1;color: #fff;}
                </style>
                <ul class="list-group" id="listBank">
                    <?php foreach($listBanks as $b){ ?>
                        <li class="liBank_<?php echo $b['BankTypeId']; ?>" data-id="<?php echo $b['BankId']; ?>"<?php if($b['BankTypeId'] == 2) echo ' style="display: none;"'; ?>>
                            <p class="pBankName"><?php echo $b['BankName']; ?> (<?php echo $b['BankCode']; ?>)</p>
                            <p>Chủ tài khoản: <?php echo $b['BankHolder']; ?></p>
                            <p>STK: <span class="spanBankName"><?php echo $b['BankNumber']; ?></span></p>
                            <p>Chi nhánh: <?php echo $b['BranchName']; ?></p>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnChooseBank">Hoàn thành</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="text" hidden="hidden" id="bankIdTmp" value="0">
            </div>
        </div>
    </div>
</div>