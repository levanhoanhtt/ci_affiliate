<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('transaction/add/'.$transactionTypeId); ?>" class="btn btn-primary">Tạo phiếu <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?></a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter" >
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả phiếu <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?></a></li>
                        <?php foreach ($listFilters as $f) { ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả đơn hàng theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="group_money">Số tiền</option>
                                        <option value="group_phone">Số điện thoại</option>
                                        <option value="group_email">Địa chỉ email</option>
                                        <option value="group_date">Thời điểm tạo phiếu</option>
                                        <option value="group_status_trans">Trạng thái phiếu</option>
                                        <option value="group_status_verify">Trạng thái duyệt</option>
                                        <option value="group_store">Cơ sở</option>
                                        <option value="group_moneysource">Loại tiền</option>
                                        <option value="group_reason">Lý do</option>
                                        <option value="group_bank">Ngân hàng</option>
                                        <option value="transaction_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 group_money block-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 group_email group_phone none-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="like" type="hidden"/>
                                </div>
                                <div class="form-group mb10 group_store none-display">
                                    <div class="text_opertor">ở</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group mb10 group_status_trans group_status_verify group_moneysource group_reason group_bank none-display ">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!-- group_money group_order field đây là các filter được sử dụng tùy chọn này-->
                                    <select class="form-control group_status_trans none-display">
                                        <?php foreach($this->Mconstants->transactionStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_status_verify none-display">
                                        <?php foreach($this->Mconstants->verifyLevels as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_store none-display">
                                        <?php foreach($listStores as $s){ ?>
                                            <option value="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_moneysource none-display">
                                        <?php foreach($this->Mconstants->moneySources as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_reason none-display">
                                        <?php foreach($listTransactionReasons as $tr){ ?>
                                            <option value="<?php echo $tr['TransactionReasonId']; ?>"><?php echo $tr['TransactionReasonName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_bank none-display">
                                        <?php foreach($listBanks as $b){ ?>
                                            <?php echo $b['BankNumber'].' ( '.$b['BankName'].' - '.$b['BankHolder'].' )'; ?>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_date none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control transaction_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!-- group_money group_order field đây là các filter được sử dụng input này-->
                                    <input class="form-control group_money input-number" type="text">
                                    <input class="form-control group_phone group_email transaction_tag none-display" type="text">
                                    <input class="form-control datepicker group_date none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker group_date none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('api/transaction/searchByFilter/'.$transactionTypeId); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName">
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header with-border">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="add_tags">Thêm nhãn</option>
                            <option value="delete_tags">Bỏ nhãn</option>
                        </select>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" value="0" class="iCheckTable" id="checkAll"></th>
                                <th>Mã phiếu</th>
                                <th>Ngày tạo</th>
                                <th><?php echo $transactionTypeId == 1 ? 'Từ' : 'Chi cho'; ?></th>
                                <th class="text-right">Số tiền</th>
                                <th>Loại tiền</th>
                                <th>Lý do</th>
                                <th class="text-center">Trạng thái phiếu</th>
                                <th class="text-center">Trạng thái duyệt</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransaction"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="<?php echo $itemTypeId; ?>">
                    <input type="hidden" value="<?=base_url('transaction/edit')?>" id="urlEditTransaction">
                    <input type="hidden" value="<?=base_url('customer/edit')?>" id="urlEditCustomer">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>