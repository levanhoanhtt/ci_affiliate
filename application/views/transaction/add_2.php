<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
                    <li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
                    <li><a href="<?php echo base_url('transaction/2'); ?>" id="aTransactionList" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('api/transaction/update', array('id' => 'transactionForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Chi tiết</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Số tiền chi <span class="required">*</span></label>
                                            <input class="form-control" type="text" value="0" id="paidCost"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Lý do</label>
                                            <?php $this->Mconstants->selectObject($listTransactionReasons, 'TransactionReasonId', 'TransactionReasonName', 'TransactionReasonId'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Loại tiền</label>
                                            <select class="form-control" name="MoneySourceId" id="moneySourceId">
                                                <option value="1">Tiền mặt</option>
                                                <option value="2">Tiền chuyển khoản</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_1 divMoneySource_4">
                                        <div class="form-group">
                                            <label class="control-label normal">Cơ sở</label>
                                            <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', $storeId, true, '--Chọn cơ sở--'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_3" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label normal">Thẻ điện thoại</label>
                                            <?php $this->Mconstants->selectObject($listMoneyPhones, 'MoneyPhoneId', 'MoneyPhoneName', 'MoneyPhoneId', 0, true, '--Thẻ điện thoại--'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_2" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label normal">Số tài khoản <i class="fa fa-pencil" id="iBank" title="Chọn ngân hàng" style="cursor: pointer;"></i></label>
                                            <input type="text" hidden="hidden" name="BankId" id="bankId" value="0">
                                            <div class="text-success" id="bankInfo"></div>
                                            <select class="form-control" id="selectBank"></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_1">
                                        <div class="form-group">
                                            <label class="control-label normal">Từ quỹ</label>
                                            <?php $this->Mconstants->selectObject($listFunds, 'FundId', 'FundName', 'FundId', $fundId, true, '--Từ quỹ--'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p><input type="checkbox" class="iCheck" id="cbPrintStatus"> Có in phiếu thu và giao cho khách</p>
                                            <p><input type="checkbox" class="iCheck" id="cbHasDebt"> Có công nợ tạm ứng tiền cho công ty</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" id="divDebtComment" style="display: none;">
                                            <input type="text" class="form-control" id="debtComment" value="" placeholder="Nhập ghi chú tạm ứng">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-header with-border" style="margin-top: -15px;"></div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label normal">Ghi chú</label>
                                    <textarea rows="2" class="form-control" id="comment"></textarea>
                                </div>
                            </div>
                            <div class="box-header with-border" style="margin-top: -15px;"></div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label normal lbIcon"><i class="glyphicon glyphicon-credit-card"></i> Hoàn thành dể tạo phiếu</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
                                            <li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default" id="boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span>₫</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin khách hàng</h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div>
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default more-tabs padding20">
                            <div class="form-group">
                                <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                <input type="text" class="form-control" id="tags">
                            </div>
                            <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                            <div class="clearfix">
                                <?php foreach ($listTags as $t) { ?>
                                    <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="transactionId" value="0">
                <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                <input type="text" hidden="hidden" id="transactionTypeId" value="<?php echo $transactionTypeId; ?>">
                <input type="text" hidden="hidden" id="orderId" value="0">
                <input type="text" hidden="hidden" id="canEdit" value="1">
                <input type="text" hidden="hidden" id="canEditLevel" value="<?php echo $canEditLevel; ?>">
                <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                <?php echo form_close(); ?>
                <?php $this->load->view('transaction/modal_bank'); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>