<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($transactionTypeId > 0 && $canEdit){ ?>
                        <li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
                        <li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
                        <li><button class="btn btn-danger btnCancel">Hủy</button></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('transaction/'.$transactionTypeId); ?>" id="aTransactionList" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php if($transactionTypeId > 0){ ?>
                <?php echo form_open('api/transaction/update', array('id' => 'transactionForm')); ?>
                <div class="row">
                    <?php $verifyLevelId = $transaction['VerifyLevelId']; ?>
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Chi tiết Phiếu thu</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Số tiền đã nhận <span class="required">*</span></label>
                                            <input class="form-control" type="text" value="<?php echo priceFormat($transaction['PaidCost']); ?>" id="paidCost"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Lý do</label>
                                            <?php $this->Mconstants->selectObject($listTransactionReasons, 'TransactionReasonId', 'TransactionReasonName', 'TransactionReasonId', $transaction['TransactionReasonId']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Loại tiền</label>
                                            <?php $this->Mconstants->selectConstants('moneySources', 'MoneySourceId', $transaction['MoneySourceId']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_1 divMoneySource_4">
                                        <div class="form-group">
                                            <label class="control-label normal">Cơ sở</label>
                                            <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', $transaction['StoreId'], true, '--Chọn cơ sở--'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_3" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label normal">Thẻ điện thoại</label>
                                            <?php $this->Mconstants->selectObject($listMoneyPhones, 'MoneyPhoneId', 'MoneyPhoneName', 'MoneyPhoneId', $transaction['MoneyPhoneId'], true, '--Thẻ điện thoại--'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_2" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label normal">Số tài khoản <i class="fa fa-pencil" id="iBank" title="Chọn ngân hàng" style="cursor: pointer;"></i></label>
                                            <input type="text" hidden="hidden" name="BankId" id="bankId" value="<?php echo $transaction['BankId']; ?>">
                                            <div class="text-success" id="bankInfo"></div>
                                            <select class="form-control" id="selectBank"></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 divMoneySource divMoneySource_1">
                                        <div class="form-group">
                                            <label class="control-label normal">Từ quỹ</label>
                                            <?php $this->Mconstants->selectObject($listFunds, 'FundId', 'FundName', 'FundId', $transaction['FundId'], true, '--Từ quỹ--'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p><input type="checkbox" class="iCheck" id="cbPrintStatus"<?php if($transaction['PrintStatusId'] == 2) echo ' checked'; ?>> Có in phiếu thu và giao cho khách</p>
                                            <p style="display: none;"><input type="checkbox" class="iCheck" id="cbHasDebt"<?php if($transaction['HasDebt'] == 2) echo ' checked'; ?>> Có công nợ tạm ứng tiền cho công ty</p>
                                            <input type="text" hidden="hidden" id="debtComment" value="<?php echo $transaction['DebtComment']; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>
                            </div>
                            <div class="box-header with-border" style="margin-top: -15px;"></div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label normal">Ghi chú</label>
                                    <textarea rows="2" class="form-control" id="comment"><?php echo $transaction['Comment']; ?></textarea>
                                </div>
                            </div>
                            <?php if($canEdit){ ?>
                            <div class="box-header with-border" style="margin-top: -15px;"></div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label normal lbIcon"><i class="glyphicon glyphicon-credit-card"></i> Hoàn thành dể cập nhật phiếu</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
                                            <li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
                                            <li><button class="btn btn-danger btnCancel">Hủy</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php $this->load->view('includes/action_logs_new'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?php if($canEdit){ ?>
                            <div class="box box-default" id="boxChooseCustomer">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Khách hàng</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="box-search-advance customer">
                                        <div>
                                            <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng">
                                        </div>
                                        <div class="panel panel-default" id="panelCustomer">
                                            <div class="panel-body">
                                                <div class="list-search-data">
                                                    <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                    <ul id="ulListCustomers"></ul>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="btn-group pull-right">
                                                    <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                    <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                    <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span>₫</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin khách hàng</h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div>
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default more-tabs padding20">
                            <div class="form-group">
                                <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                <input type="text" class="form-control" id="tags">
                            </div>
                            <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                            <div class="clearfix">
                                <?php foreach ($listTags as $t) { ?>
                                    <a href="javascript:void(0)" class="ntags"><?php echo $t['TagName']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="box box-default">
                            <?php $labelCss = $this->Mtransactions->labelCss; ?>
                            <table class="tbl-cod">
                                <tbody><tr>
                                    <th>Trạng thái phiếu</th>
                                    <td class="text-right" id="tdTransactionStatus"><span class="<?php echo $labelCss['TransactionStatusCss'][$transaction['TransactionStatusId']]; ?>"><?php echo $this->Mconstants->transactionStatus[$transaction['TransactionStatusId']]; ?></span></td>
                                </tr>
                                <tr>
                                    <th class="border-bottom">Trạng thái duyệt</th>
                                    <td class="text-right border-bottom" id="tdVerifyLevel"><span class="<?php echo $labelCss['VerifyLevelCss'][$verifyLevelId]; ?>"><?php echo $this->Mconstants->verifyLevels[$verifyLevelId]; ?></span></td>
                                </tr>
                                </tbody>
                            </table>
                            <?php $flag = 0;
                            if(!$canEdit) {
                                if ($canEditLevel == 1) { //NV
                                    if ($verifyLevelId == 4) {
                                        //gui QL duyet lại
                                        //ko duyet - hủy
                                        $flag = 1;
                                    }
                                }
                                elseif ($canEditLevel == 2) { //QL
                                    if ($verifyLevelId == 1) {
                                        //da duyet
                                        $flag = 2;
                                    }
                                    elseif ($verifyLevelId == 2) {
                                        //da duyet
                                        //gui NV check lai
                                        //ko duyet
                                        $flag = 3;
                                    }
                                    elseif ($verifyLevelId == 5) {
                                        //da duyet
                                        //gui NV check lai
                                        //ko duyet
                                        $flag = 4;
                                    }
                                }
                            }
                            if($flag == 1){ ?>
                            <div class="form-group padding15" style="margin-top: -14px;">
                                <label class="control-label">Nhân viên xử lý phiếu</label>
                                <div class="radio-group">
                                    <span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="5"> Gửi quản lý duyệt lại</span><br/>
                                    <span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="6"> Không duyệt</span>
                                </div>
                            </div>
                            <?php } else if($flag > 1){ ?>
                            <div class="form-group padding15" style="margin-top: -14px;">
                                <label class="control-label">Quản lý xử lý phiếu</label>
                                <div class="radio-group">
                                    <span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="3"> Duyệt phiếu</span><br/>
                                    <?php if($flag == 3 || $flag == 4){ ?>
                                    <span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="4"> Gửi nhân viên check lại</span><br/>
                                    <span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="6"> Không duyệt</span>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php }
                            if($flag > 0){ ?>
                                <div class="form-group padding15" style="margin-top: -14px;">
                                    <button class="btn btn-primary" type="button" id="btnUpdateVerifyLevel">Áp dụng</button>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="transactionId" value="<?php echo $transaction['TransactionId']; ?>">
                <input type="text" hidden="hidden" id="customerId" value="<?php echo $transaction['CustomerId']; ?>">
                <input type="text" hidden="hidden" id="transactionTypeId" value="<?php echo $transactionTypeId; ?>">
                <input type="text" hidden="hidden" id="orderId" value="<?php echo $transaction['OrderId']; ?>">
                <input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit ? 1 : 0; ?>">
                <input type="text" hidden="hidden" id="canEditLevel" value="<?php echo $canEditLevel; ?>">
                <input type="text" hidden="hidden" id="updateVerifyLevelUrl" value="<?php echo base_url('api/transaction/updateVerifyLevel'); ?>">
                <input type="text" hidden="hidden" id="cancelTransactionUrl" value="<?php echo base_url('api/transaction/cancel'); ?>">
                <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                <?php foreach($tagNames as $tagName){ ?>
                    <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
                <?php } ?>
                <?php echo form_close(); ?>
                <?php $this->load->view('transaction/modal_bank'); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php } else $this->load->view('includes/notice'); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>