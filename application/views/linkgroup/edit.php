<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
             <?php if($linkGroupId > 0) {?>
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('api/linkgroup/update', array('id' => 'linkGroupForm')); ?>
                <div class="row">
                    <div class="col-sm-7 no-padding">
                        <div class="box box-default padding20">
                            <div class="">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered-bottom">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 60px;">Đơn vị</th>
                                            <th class="text-center" style="width: 130px;">SKU</th>
                                            <th class="text-center" style="width: 120px;">Bảo hành</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct">
                                            <?php 
                                            if ($listProduct) {
                                                foreach ($listProduct as $product) {
                                                    $productName = $product['ProductName'];
                                                    $productImage = $product['ProductImage'];
                                                    $productKindId = $product['ProductKindId'];
                                                    $price = $product['Price'];
                                                    $barCode = $product['BarCode'];
                                                    $weight = $product['Weight'];
                                                    $productUnitId = $product['ProductUnitId'];
                                                    $guaranteeMonth = $product['GuaranteeMonth'];
                                                    $productChildName = '';
                                            ?>
                                            <tr data-id="<?php echo $product['ProductId']; ?>" data-child="0" data-weight="<?php echo $weight ?>" data-kind="<?php echo $product['ProductKindId']; ?>">
                                                <td>
                                                    <img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
                                                    <a href="<?php echo $productKindId == 3 ? 'javascript:void(0)' : base_url('product/edit/'.$product['ProductId']); ?>" class="light-dark aProductLink" target="_blank">
                                                        <?php echo $productName;
                                                        if(!empty($productChildName)) echo ' ('.$productChildName.')';
                                                        elseif($productKindId == 3) echo ' (Combo)'; ?>
                                                    </a>
                                                </td>
                                                <td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                                                <td class="text-center"><?php echo $barCode; ?></td>
                                                <td class="text-center"><?php echo $guaranteeMonth; ?> tháng</td>
                                                <td class="tdPrice text-right"><span class="spanPrice"><?php echo priceFormat($product['Price']); ?></span> ₫</td>
                                                <td class="text-right"<?php if(!$canEdit) echo ' style="display: none;"'; ?>>
                                                    <a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>
                                                </td>
                                            </tr>
                                            <?php } }?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch updated" id="txtSearchProductLink" placeholder="Tìm kiếm sản phẩm (F3)">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">SKU</th>
                                                                    <th style="width: 100px;">Giá</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="box box-default" id="boxChooseLinkGroup">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tên</h3>
                                <div>
                                    <input type="text" class="form-control textbox-advancesearch"
                                        name="linkGroupName" id="txtLinkGroupName" placeholder="Tên chiến dịch"
                                        value="<?php echo $linkGroup['LinkGroupName'];?>">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Mô tả</h3>
                                <div>
                                    <textarea class="form-control textbox-advancesearch"
                                        name="linkGroupCommnet" id="txtLinkGroupComment"
                                        ><?php echo $linkGroup['LinkGroupComment'];?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label light-blue">Cách tiếp thị</label>
                                <?php $this->Mconstants->selectObject($listApproachTypeId, 'approachTypeId', 'approachTypeIdName', 'approachTypeId', $linkGroup['ApproachTypeId'], true, '--Cách tiếp thị--'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getListLinkAff'); ?>">
                    <input type="text" hidden="hidden" id="canEdit" value="1">
                    <input type="text" hidden="hidden" id="linkGroupId" value="<?php echo $linkGroupId;?>">
                    <input type="text" hidden="hidden" id="linkGroupEditUrl" value="<?php echo base_url('linkgroup/edit');?>">
                </ul>
                <?php echo form_close(); ?>
            </section>
            <?php }?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>