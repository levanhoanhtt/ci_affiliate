<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <!--<li><button id="btnImportOrder" class="btn btn-primary">Import Excel</button></li>-->
                    
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả sản phẩm</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php $productStatus = $this->Mconstants->productStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả sản phẩm theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="product_status_trade">Tình trạng kinh doanh</option>
                                        <option value="product_status_display">Tình trạng hiển thị</option>
                                        <option value="product_type">Ngành kinh doanh</option>
                                        <option value="product_kind">Loại sản phẩm</option>
                                        <!--<option value="product_suppliers">Nhà cung cấp</option>-->
                                        <option value="product_manufacturer">Hãng sản xuất</option>
                                        <option value="product_group_1">Nhóm sản phẩm</option>
                                        <option value="product_group_2">Loại hàng hóa</option>
                                        <option value="product_unit">Đơn vị</option>
                                        <option value="product_image">Ảnh sản phẩm</option>
                                        <option value="product_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 product_status_trade product_status_display product_type product_kind product_suppliers product_manufacturer product_group_1 product_group_2 product_unit block-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="product_image none-display">
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    
                                </div>
                                <div class="form-group block-display widthauto">
                                    <input class="form-control product_tag none-display" type="text">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/product/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header with-border">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="add_tags">Thêm nhãn</option>
                            <option value="delete_tags">Bỏ nhãn</option>
                        </select>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Thời gian</th>
                                <th>H.hồng thành công</th>
                                <th>H.hồng đang xử lý</th>
                                <th>H.hồng thất bại</th>
                                <th>Công nợ cũ</th>
                                <th>Đã trả</th>
                                <th>Còn lại</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPayment"></tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>