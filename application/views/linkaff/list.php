<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <div class="text-center" style="margin-top: 30px;">
                            <div class="col-sm-6 text-center">
                                <figure id="aAddLink" style="width: 140px; float: right; cursor: pointer;">
                                    <img width="140px" height="120px" alt="Tạo Link" src="assets/img/create_link.png">
                                    <figcaption style="background-color: #d8d5d5;">Tạo Link</figcaption>
                                </figure>
                            </div>
                            <div class="col-sm-6 text-center">
                                <figure id="aAddBanner" style="width: 140px; float: left; cursor: pointer;">
                                    <img width="140px" height="120px" alt="Tạo Banner" src="assets/img/create_banner.png">
                                    <figcaption style="background-color: #d8d5d5;">Tạo Banner</figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">
                        <?php echo form_open('linkaff'); ?>
                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        <?php echo form_close(); ?>
                    </div>
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Thời gian</th>
                                <th>Link rút gọn</th>
                                <th>Link gốc</th>
                                <th>Sản phẩm</th>
                                <th>Chiến dịch</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyLink">
                                <?php $productNames = array();
                                foreach($listAffs as $v){
                                    if(!isset($productNames[$v['ProductId']])) $productNames[$v['ProductId']] = $this->Mproducts->getFieldValue(array('ProductId' => $v['ProductId']), 'ProductName'); ?>
                                    <tr class="text-left">
                                        <td><?php echo ddMMyyyy($v['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                        <td><?php echo $v['ShortLink']; ?></td>
                                        <td><a href="<?php echo $v['OriginalLink']; ?>" target="_blank"><?php echo $v['OriginalLink']; ?></a></td>
                                        <td><?php echo $productNames[$v['ProductId']]; ?></td>
                                        <td></td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modalAddLink" role="dialog" aria-labelledby="modalAddLink">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Tạo link Affiliate</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Tên chiến dịch</label>
                                                <input type="text" class="form-control" id="linkGroupName">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Cách tếp cận</label>
                                                <?php $this->Mconstants->selectObject($listApproachTypes, 'ApproachTypeId', 'ApproachTypeName', 'ApproachTypeId', 0, true, '--Chọn--'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Mô tả</label>
                                                <input type="text" class="form-control" id="linkGroupComment">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Chiến dịch đã có</label>
                                                <?php $this->Mconstants->selectObject($listLinkGroups, 'LinkGroupId', 'LinkGroupName', 'LinkGroupId', 0, true, '--Chọn--'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Link sản phẩm <span class="required">*</span></label>
                                        <input type="text" class="form-control" id="originalLink">
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="button" class="btn btn-success" id="btnCreateLink">Tạo link</button>
                                        <button type="button" class="btn btn-success" id="btnOtherLink" style="display: none">Tạo thêm link mới</button>
                                        <input type="text" hidden="hidden" id="updateLinkAff" value="<?php echo base_url('linkaff/update'); ?>">
                                    </div>
                                    <br>
                                    <div id="divLinkPreview" style="display: none;">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label class="control-label">Link rút gọn </label>
                                                    <input type="text" id="shortLink" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-4" style="margin-top: 30px;">
                                                <span class="copy-short-link tooltiptext" style="cursor: pointer;" id="myTooltip" onclick="copyClipboard('shortLink')"><i class="fa fa-copy"></i> COPY</span>&nbsp;
                                                <span><i class="fa fa-share-alt"></i> SHARE</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label class="control-label">Link gốc </label>
                                                    <input type="text" id="oldLink" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-4" style="margin-top: 30px;">
                                                <span class="copy-old-link tooltiptext" style="cursor: pointer;" id="myTooltip" onmouseout="outFunc()" onclick="copyClipboard('oldLink')"><i class="fa fa-copy"></i> COPY</span>&nbsp;
                                                <span><i class="fa fa-share-alt"></i> SHARE</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>