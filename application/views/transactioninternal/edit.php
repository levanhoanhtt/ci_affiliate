<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($transactionTypeId > 0 && $canEdit){ ?>
						<li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
						<li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
						<li><button class="btn btn-danger btnCancel">Hủy</button></li>
					<?php } ?>
                    <li><a href="<?php echo base_url('transactioninternal/'.$transactionTypeId); ?>" id="aTransactionInternalList" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('transactioninternal/update', array('id' => 'transactionInternalForm')); ?>
                	<div class="row">
						<?php $verifyLevelId = $transactionInternal['VerifyLevelId']; ?>
	                    <div class="col-sm-12 no-padding">
	                        <div class="box box-default">
	                            <div class="box-header with-border">
									<h3 class="box-title">Chi tiết phiếu <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?> nội bộ</h3>
	                            </div>
	                            <div class="box-body">
	                                <div class="row">
	                                    <div class="col-sm-4">
	                                        <div class="form-group">
	                                            <label class="control-label normal">Số tiền <span class="required">*</span></label>
	                                            <input class="form-control" type="text" value="<?php echo priceFormat($transactionInternal['PaidCost']); ?>" id="paidCost"/>
	                                        </div>
	                                    </div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal">Loại <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?></label>
												<?php $this->Mconstants->selectObject($listTransactionKinds, 'TransactionKindId', 'TransactionKindName', 'TransactionKindId', $transactionInternal['TransactionKindId']); ?>
											</div>
										</div>
										<!-- <div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal"><?php // echo $transactionTypeId == 1 ? 'Nhập vào' : 'Chi từ'; ?> quỹ</label>
												<?php // $this->Mconstants->selectObject($listFunds, 'FundId', 'FundName', 'FundId', $transactionInternal['FundId'], true, '--Chọn--'); ?>
											</div>
										</div> -->
										<div class="col-sm-4">
	                                        <div class="form-group">
	                                            <label class="control-label normal">Thủ quỹ</label>
	                                            <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'TreasurerId', $transactionInternal['TreasurerId'], true, '--Chọn--', ' select2'); ?>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal">Loại tiền</label>
												<?php $this->Mconstants->selectConstants('moneySources', 'MoneySourceId', $transactionInternal['MoneySourceId']); ?>
											</div>
										</div>
										<div class="col-sm-4 divMoneySource divMoneySource_3" style="display: none;">
											<div class="form-group">
												<label class="control-label normal">Thẻ điện thoại</label>
												<?php $this->Mconstants->selectObject($listMoneyPhones, 'MoneyPhoneId', 'MoneyPhoneName', 'MoneyPhoneId', $transactionInternal['MoneyPhoneId'], true, '--Thẻ điện thoại--'); ?>
											</div>
										</div>
										<div class="col-sm-4 divMoneySource divMoneySource_2" style="display: none;">
											<div class="form-group">
												<label class="control-label normal"><?php  echo $transactionTypeId == 1 ? 'STK nhận' : 'Xuất từ STK nào?'; ?> <i class="fa fa-pencil" id="iBank" title="Chọn ngân hàng" style="cursor: pointer;"></i></label>
												<input type="text" hidden="hidden" name="BankId" id="bankId" value="<?php echo $transactionInternal['BankId']; ?>">
												<div class="text-success" id="bankInfo"></div>
												<select class="form-control" id="selectBank"></select>
											</div>
										</div>
										<div class="col-sm-4 divMoneySource divMoneySource_2" style="display: none;">
											<div class="form-group">
												<label class="control-label normal">STK  <?php  echo $transactionTypeId == 1 ? 'chuyển tới' : 'nhận tiền'; ?></label>
												<input type="text" class="form-control" name="BankToText" id="bankToText" value="<?php echo $transactionInternal['BankToText'] ?>">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal">Người <?php  echo $transactionTypeId == 1 ? 'nộp' : 'nhận'; ?> tiền</label>
												<?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'PayerId', $transactionInternal['PayerId'], true, '--Chọn--', ' select2'); ?>
											</div>
										</div>
	                                    
	                                </div>
	                                <div class="row">
	                                    <div class="col-sm-4">
	                                        <div class="form-group">
	                                            <p><input type="checkbox" class="iCheck" id="cbPrintStatus"<?php if($transactionInternal['PrintStatusId'] == 2) echo ' checked'; ?>> Có in phiếu thu và giao cho khách</p>
                                            	<p><input type="checkbox" class="iCheck" id="cbHasDebt"<?php if($transactionInternal['HasDebt'] == 2) echo ' checked'; ?>> Có công nợ tạm ứng tiền cho công ty</p>
	                                        </div>
	                                    </div>
										<div class="col-sm-4">
											<div class="form-group" id="divDebtComment"<?php if($transactionInternal['HasDebt'] != 2) echo ' style="display: none;"'; ?>>
												<input type="text" class="form-control" id="debtComment" value="<?php echo $transactionInternal['DebtComment']; ?>" placeholder="Nhập ghi chú tạm ứng">
											</div>
										</div>
	                                </div>
	                            </div>
								<div class="box-header with-border" style="margin-top: -15px;"></div>
	                            <div class="box-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label normal">Ghi chú</label>
												<textarea rows="2" class="form-control" id="comment"><?php echo $transactionInternal['Comment']; ?></textarea>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label normal">Nhãn (cách nhau bởi dấu phẩy)</label>
												<input type="text" class="form-control" id="tags">
											</div>
										</div>
									</div>
	                            </div>
	                            <?php if($canEdit){ ?>
	                            <div class="box-header with-border" style="margin-top: -15px;"></div>
	                            <div class="box-body">
	                                <div class="row">
	                                    <div class="col-sm-6">
	                                        <label class="control-label normal lbIcon"><i class="glyphicon glyphicon-credit-card"></i> Hoàn thành dể tạo phiếu</label>
	                                    </div>
	                                    <div class="col-sm-6">
	                                        <ul class="list-inline pull-right">
												<li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
												<li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
												<li><button class="btn btn-danger btnCancel">Hủy</button></li>
	                                        </ul>
	                                    </div>
	                                </div>
	                            </div>
	                            <?php } ?>
	                        </div>
							<div class="row">
								<div class="col-sm-8">
									<?php $this->load->view('includes/action_logs_new'); ?>
								</div>
								<div class="col-sm-4">
									<div class="box box-default">
										<?php $labelCss = $this->Mtransactions->labelCss; ?>
										<table class="tbl-cod">
											<tbody><tr>
												<th>Trạng thái phiếu</th>
												<td class="text-right" id="tdTransactionStatus"><span class="<?php echo $labelCss['TransactionStatusCss'][$transactionInternal['TransactionStatusId']]; ?>"><?php echo $this->Mconstants->transactionStatus[$transactionInternal['TransactionStatusId']]; ?></span></td>
											</tr>
											<tr>
												<th class="border-bottom">Trạng thái duyệt</th>
												<td class="text-right border-bottom" id="tdVerifyLevel"><span class="<?php echo $labelCss['VerifyLevelCss'][$verifyLevelId]; ?>"><?php echo $this->Mconstants->verifyLevels[$verifyLevelId]; ?></span></td>
											</tr>
											</tbody>
										</table>
										<?php $flag = 0;
										if(!$canEdit) {
											if ($canEditLevel == 1) { //NV
												if ($verifyLevelId == 4) {
													//gui QL duyet lại
													//ko duyet - hủy
													$flag = 1;
												}
											}
											elseif ($canEditLevel == 2) { //QL
												if ($verifyLevelId == 1) {
													//da duyet
													$flag = 2;
												}
												elseif ($verifyLevelId == 2) {
													//da duyet
													//gui NV check lai
													//ko duyet
													$flag = 3;
												}
												elseif ($verifyLevelId == 5) {
													//da duyet
													//gui NV check lai
													//ko duyet
													$flag = 4;
												}
											}
										}
										if($flag == 1){ ?>
											<div class="form-group padding15" style="margin-top: -14px;">
												<label class="control-label">Nhân viên xử lý phiếu</label>
												<div class="radio-group">
													<span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="5"> Gửi quản lý duyệt lại</span><br/>
													<span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="6"> Không duyệt</span>
												</div>
											</div>
										<?php } else if($flag > 1){ ?>
											<div class="form-group padding15" style="margin-top: -14px;">
												<label class="control-label">Quản lý xử lý phiếu</label>
												<div class="radio-group">
													<span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="3"> Duyệt phiếu</span><br/>
													<?php if($flag == 3 || $flag == 4){ ?>
														<span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="4"> Gửi nhân viên check lại</span><br/>
														<span class="item"><input type="radio" name="VerifyLevelId" class="iCheckRadio" value="6"> Không duyệt</span>
													<?php } ?>
												</div>
											</div>
										<?php }
										if($flag > 0){ ?>
											<div class="form-group padding15" style="margin-top: -14px;">
												<button class="btn btn-primary" type="button" id="btnUpdateVerifyLevel">Áp dụng</button>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
              	<input type="text" hidden="hidden" id="transactionInternalId" value="<?php echo $transactionInternal['TransactionInternalId']; ?>">
                <input type="text" hidden="hidden" id="transactionTypeId" value="<?php echo $transactionTypeId; ?>">
                <input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit; ?>">
                <input type="text" hidden="hidden" id="canEditLevel" value="<?php echo $canEditLevel; ?>">
				<input type="text" hidden="hidden" id="updateVerifyLevelUrl" value="<?php echo base_url('transactioninternal/updateVerifyLevel'); ?>">
				<input type="text" hidden="hidden" id="cancelTransactionUrl" value="<?php echo base_url('transactioninternal/cancel'); ?>">
                <?php foreach($tagNames as $tagName){ ?>
                    <input type="text" hidden="hidden" class="tagName" value="<?php echo $tagName; ?>">
                <?php } ?>
                <?php echo form_close(); ?>
				<?php $this->load->view('transaction/modal_bank'); ?>
            </section>  
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>