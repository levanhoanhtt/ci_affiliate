<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
					<li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
					<li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
                    <li><a href="<?php echo base_url('transactioninternal/'.$transactionTypeId); ?>" id="aTransactionInternalList" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('transactioninternal/update', array('id' => 'transactionInternalForm')); ?>
                	<div class="row">
	                    <div class="col-sm-12 no-padding">
	                        <div class="box box-default">
	                            <div class="box-header with-border">
	                                <h3 class="box-title">Chi tiết phiếu <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?> nội bộ</h3>
	                            </div>
	                            <div class="box-body">
	                                <div class="row">
	                                    <div class="col-sm-4">
	                                        <div class="form-group">
	                                            <label class="control-label normal">Số tiền <span class="required">*</span></label>
	                                            <input class="form-control" type="text" value="0" id="paidCost"/>
	                                        </div>
	                                    </div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal">Loại <?php echo $transactionTypeId == 1 ? 'thu' : 'chi'; ?></label>
												<?php $this->Mconstants->selectObject($listTransactionKinds, 'TransactionKindId', 'TransactionKindName', 'TransactionKindId'); ?>
											</div>
										</div>
										<!-- <div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal"><?php // echo $transactionTypeId == 1 ? 'Nhập vào' : 'Chi từ'; ?> quỹ</label>
												<?php // $this->Mconstants->selectObject($listFunds, 'FundId', 'FundName', 'FundId', 0, true, '--Chọn--'); ?>
											</div>
										</div> -->
										<div class="col-sm-4">
	                                        <div class="form-group">
	                                            <label class="control-label normal">Thủ quỹ</label>
	                                            <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'TreasurerId', 0, true, '--Chọn--', ' select2'); ?>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal">Loại tiền</label>
												<?php $this->Mconstants->selectConstants('moneySources', 'MoneySourceId'); ?>
											</div>
										</div>
										<div class="col-sm-4 divMoneySource divMoneySource_3" style="display: none;">
											<div class="form-group">
												<label class="control-label normal">Thẻ điện thoại</label>
												<?php $this->Mconstants->selectObject($listMoneyPhones, 'MoneyPhoneId', 'MoneyPhoneName', 'MoneyPhoneId', 0, true, '--Thẻ điện thoại--'); ?>
											</div>
										</div>
										<div class="col-sm-4 divMoneySource divMoneySource_2" style="display: none;">
											<div class="form-group">
												<label class="control-label normal"> <?php  echo $transactionTypeId == 1 ? 'STK nhận' : 'Xuất từ STK nào?'; ?> <i class="fa fa-pencil" id="iBank" title="Chọn ngân hàng" style="cursor: pointer;"></i></label>
												<input type="text" hidden="hidden" name="BankId" id="bankId" value="0">
												<div class="text-success" id="bankInfo"></div>
												<select class="form-control" id="selectBank"></select>
											</div>
										</div>
										<div class="col-sm-4 divMoneySource divMoneySource_2" style="display: none;">
											<div class="form-group">
												<label class="control-label normal">STK  <?php  echo $transactionTypeId == 1 ? 'chuyển tới' : 'nhận tiền'; ?></label>
												<input type="text" class="form-control" name="BankToText" id="bankToText">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label normal">Người <?php  echo $transactionTypeId == 1 ? 'nộp' : 'nhận'; ?> tiền</label>
												<?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'PayerId', 0, true, '--Chọn--', ' select2'); ?>
											</div>
										</div>
	                                    
	                                </div>
	                                <div class="row">
	                                    <div class="col-sm-4">
	                                        <div class="form-group">
	                                            <p><input type="checkbox" class="iCheck" id="cbPrintStatus"> Có in phiếu thu và giao cho khách</p>
	                                            <p><input type="checkbox" class="iCheck" id="cbHasDebt"> Có công nợ tạm ứng tiền cho công ty</p>
	                                        </div>
	                                    </div>
										<div class="col-sm-4">
											<div class="form-group" id="divDebtComment" style="display: none;">
												<input type="text" class="form-control" id="debtComment" value="" placeholder="Nhập ghi chú tạm ứng">
											</div>
										</div>
	                                </div>
	                            </div>
								<div class="box-header with-border" style="margin-top: -15px;"></div>
	                            <div class="box-body">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label normal">Ghi chú</label>
												<textarea rows="2" class="form-control" id="comment"></textarea>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label normal">Nhãn (cách nhau bởi dấu phẩy)</label>
												<input type="text" class="form-control" id="tags">
											</div>
										</div>
									</div>
	                            </div>
	                            <div class="box-header with-border" style="margin-top: -15px;"></div>
	                            <div class="box-body">
	                                <div class="row">
	                                    <div class="col-sm-6">
	                                        <label class="control-label normal lbIcon"><i class="glyphicon glyphicon-credit-card"></i> Hoàn thành dể tạo phiếu</label>
	                                    </div>
	                                    <div class="col-sm-6">
	                                        <ul class="list-inline pull-right">
												<li><button class="btn btn-success submit" data-id="1">Xác thực và Hoàn thành</button></li>
												<li><button class="btn btn-primary submit" data-id="2">Hoàn thành</button></li>
	                                        </ul>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
              	<input type="text" hidden="hidden" id="transactionInternalId" value="0">
                <input type="text" hidden="hidden" id="transactionTypeId" value="<?php echo $transactionTypeId; ?>">
                <input type="text" hidden="hidden" id="canEdit" value="1">
                <input type="text" hidden="hidden" id="canEditLevel" value="<?php echo $canEditLevel; ?>">
                <?php echo form_close(); ?>
				<?php $this->load->view('transaction/modal_bank'); ?>
            </section>  
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>