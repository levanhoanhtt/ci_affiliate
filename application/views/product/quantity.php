<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <style>
                .list-his{font-size: 14px !important;}
                .list-his li{line-height: 34px;}
            </style>
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl list-his mgt-10">
                    <li><label><input type="radio" value="0" name="InventoryStatusId" checked class="history"><span>Tất cả</span></label></li>
                    <li><label><input type="radio" value="2" name="InventoryStatusId" class="history"><span>Còn hàng</span></label></li>
                    <li><label><input type="radio" value="1" name="InventoryStatusId" class="history"><span>Hết hàng</span></label></li>
                    <li>
                        <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', 0, true, "--Chọn cơ sở--"); ?>
                    </li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả sản phẩm</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php $productStatus = $this->Mconstants->productStatus; ?>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả sản phẩm theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <!--<option value="product_inventory_status">Tình trạng tồn kho</option>
                                        <option value="product_store">Cơ sở</option>-->
                                        <option value="product_status_trade">Tình trạng kinh doanh</option>
                                        <option value="product_status_display">Tình trạng hiển thị</option>
                                        <option value="product_type">Ngành kinh doanh</option>
                                        <option value="product_kind">Loại sản phẩm</option>
                                        <option value="product_suppliers">Nhà cung cấp</option>
                                        <option value="product_manufacturer">Hãng sản xuất</option>
                                        <option value="product_group_1">Nhóm sản phẩm</option>
                                        <option value="product_group_2">Loại hàng hóa</option>
                                        <option value="product_unit">Đơn vị</option>
                                        <option value="product_image">Ảnh sản phẩm</option>
                                        <option value="product_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 product_status_trade product_status_display product_type product_kind product_suppliers product_manufacturer product_group_1 product_group_2 product_unit block-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!--<select class="form-control product_inventory_status block-display">
                                        <option value="2">Còn hàng</option>
                                        <option value="1">Hết hàng</option>
                                    </select>
                                    <select class="form-control product_store none-display" id="selectStore">
                                        <?php //foreach($listStores as $s){ ?>
                                            <option value="<?php //echo $s['StoreId']; ?>"><?php //echo $s['StoreName']; ?></option>
                                        <?php //} ?>
                                    </select>-->
                                    <select class="form-control product_status_trade block-display">
                                        <?php foreach($productStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_status_display none-display">
                                        <?php foreach($this->Mconstants->productDisplayTypes as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_type none-display">
                                        <?php foreach($listProductTypes as $pt){ ?>
                                            <option value="<?php echo $pt['ProductTypeId']; ?>"><?php echo $pt['ProductTypeName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_kind none-display">
                                        <?php foreach($this->Mconstants->productKinds as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_suppliers none-display">
                                        <?php foreach($listSuppliers as $s){ ?>
                                            <option value="<?php echo $s['SupplierId']; ?>"><?php echo $s['SupplierName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_manufacturer none-display">
                                        <?php foreach($listManufacturers as $m){ ?>
                                            <option value="<?php echo $m['ManufacturerId']; ?>"><?php echo $m['ManufacturerName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_group_1 none-display">
                                        <?php foreach($listCategories as $c){
                                            if($c['ItemTypeId'] == 1){ ?>
                                                <option value="<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                    <select class="form-control product_group_2 none-display">
                                        <?php foreach($listCategories as $c){
                                            if($c['ItemTypeId'] == 2){ ?>
                                                <option value="<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                    <select class="form-control product_unit none-display">
                                        <?php foreach($listProductUnits as $pu){ ?>
                                            <option value="<?php echo $pu['ProductUnitId']; ?>"><?php echo $pu['ProductUnitName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control product_image none-display">
                                        <option value="2">Có</option>
                                        <option value="1">Không</option>
                                    </select>
                                    <select class="form-control product_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                </div>
                                <div class="form-group block-display widthauto">
                                    <input class="form-control product_tag none-display" type="text">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/product/searchByFilter/'.($canViewPrice ? 2 : 1)); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <style>
                        #tbodyProduct tr.cProduct td.productName{padding-left: 30px;}
                        #tbodyProduct .spanQuantity{cursor: pointer;}
                        #quantityInfo{position: absolute;left: 400px;top: 80px;border: 1px solid #3c8dbc;width: 300px;padding: 0 10px;background-color: #d2d6de;display: none;}
                        #quantityInfo #pProductName{text-align: center;padding-top: 5px;color: #1782db;}
                        #quantityInfo.active{display: block;}
                        #quantityInfo table{margin-bottom: 0;margin-top: 2px;}
                        #quantityInfo table td{padding: 5px !important;}
                        #quantityInfo #btnCloseQuantityInfo{margin-bottom: 10px;}
                    </style>
                    <div class="box-body table-responsive no-padding divTable" style="position: relative;">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th class="text-center">Tồn kho</th>
                                <th class="text-center">Đang về</th>
                                <th class="text-center">Loại</th>
                                <?php if($canViewPrice) echo '<th class="text-right">Giá vốn</th>'; ?>
                                <th>Barcode</th>
                                <th class="text-center">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct"></tbody>
                        </table>
                        <div id="quantityInfo">
                            <p id="pProductName"></p>
                            <img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter">
                            <table class="table no-padding" style="display: none;">
                                <tbody></tbody>
                            </table>
                            <button class="btn btn-default btn-sm pull-right" id="btnCloseQuantityInfo">Đóng</button>
                            <input type="text" hidden="hidden" id="getQuantityUrl" value="<?php echo base_url('api/product/getQuantityAllStore'); ?>">
                        </div>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="15">
                    <input type="text" hidden="hidden" id="canViewPrice" value="<?php echo $canViewPrice ? 1 : 0; ?>">
                    <input type="hidden" value="<?php echo base_url('product/edit')?>" id="urlEditProduct">
                    <input type="hidden" value="<?php echo base_url('product/editCombo')?>" id="urlEditProductCombo">
                    <input type="text" hidden="hidden" id="searchFilterUrl" value="<?php echo base_url('api/product/searchByFilter/'.($canViewPrice ? 2 : 1)); ?>/">
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <div id="divProductQuantity" style="display: none;"></div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>