<div class="modal fade" id="modalProductImage" tabindex="-1" role="dialog" aria-labelledby="modalProductImage">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ảnh sản phẩm</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="productImageUrl" placeholder="Link ảnh">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-info btn-flat" id="btnUploadImage">Upload</button>
                        </span>
                    </div>
                </div>
                <!--<div class="form-group">
                    <input type="text" class="form-control" id="productImageAlt" placeholder="ALT">
                </div>-->
                <div class="text-center">
                    <img src="<?php echo PRODUCT_PATH.NO_IMAGE; ?>" id="imgProduct" style="max-height: 500px;max-width: 100%;">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnUpdateImage">Cập nhật</button>
                <input type="text" hidden="hidden" id="imageIndex" value="0">
                <input type="text" hidden="hidden" id="noImageProduct" value="<?php echo PRODUCT_PATH.NO_IMAGE; ?>">
                <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalProductPrice" tabindex="-1" role="dialog" aria-labelledby="modalProductPrice" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cấu hình giá sản phẩm</h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered ">
                        <thead class="theadNormal">
                        <tr>
                            <th>Số lượng</th>
                            <th>Giá</th>
                            <th style="width: 50px;"></th>
                        </tr>
                        </thead>
                        <tbody id="tbodyMultiPrice">
                        <tr>
                            <td><input type="text" class="form-control cost" id="quantity2" value="0"></td>
                            <td><input type="text" class="form-control cost" id="price2" value="0"></td>
                            <td><a href="javascript:void(0)" id="link_update2" data-id="0"><i class="fa fa-save" title="Cập nhật"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnUpdatePrice">Hoàn thành</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                <input type="text" hidden="hidden" id="productChildId" value="0">
                <input type="text" hidden="hidden" id="productChildIndex" value="0">
            </div>
        </div>
    </div>
</div>



