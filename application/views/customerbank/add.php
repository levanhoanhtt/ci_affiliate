<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('customerbank'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
            	<?php echo form_open('customerbank/update', array('id' => 'customerbankForm')); ?>
            	<div class="row">
            		<div class="col-sm-3 no-padding"></div>
                    <div class="col-sm-6 no-padding">
                    	<div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="radio" name="radio_check" class="radio_check" value="1" checked> Có tài khoản ngân hàng
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="radio" name="radio_check" class="radio_check" value="2"> Không có
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="account_bank">
                        		<div class="col-sm-12">
                        			<div class="form-group">
                        				<label>Tên ngân hàng</label>
                        				<input type="text" class="form-control hmdrequired" id="bankName" name="BankName" data-field="Tên ngân hàng">
                        			</div>
                        		</div>
                        		<div class="col-sm-12">
                        			<div class="form-group">
                        				<label>Chi nhánh</label>
                        				<input type="text" class="form-control hmdrequired" id="branchName" name="BranchName" data-field="Chi nhánh">
                        				<label><i><span style="color: red">VD:</span> Ngân hàng BIDV chi nhánh Phù Ninh - tỉnh Phú Thọ</i></label>
                        			</div>
                        		</div>
                        		<div class="col-sm-12">
                        			<div class="form-group">
                        				<label>Chủ tài khoản</label>
                        				<input type="text" class="form-control" id="bankHolder" name="BankHolder" data-field="Chủ tài khoản">
                        				<label><i><span style="color: red">VD:</span> VU KIM LE</i></label>
                        			</div>
                        		</div>
                        		<div class="col-sm-12">
                        			<div class="form-group">
                        				<label>Số tài khoản</label>
                        				<input type="text" class="form-control hmdrequired" id="bankNumber" name="BankNumber" data-field="Số tài khoản">
                        			</div>
                        		</div>
                            </div>
                            <div class="not_account_bank" style="display: none">
                                <div class="col-sm-12">
                                    <span class="text-center">Tiền sẽ được chuyển đến bạn qua CMND, hãy ra bưu điện Việt Nam gần nhà để nhận!</span>
                                </div>
                            </div>
                    	</div>
                    </div>
                    <div class="col-sm-3 no-padding"></div>
                </div>
                <ul class="list-inline pull-right margin-right-10 account_bank">
                    <li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php echo base_url('customerbank'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="customerBankId" name="CustomerBankId" value="0">
                    <input type="text" hidden="hidden" id="customerId" name="CustomerId" value="0">
                </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>