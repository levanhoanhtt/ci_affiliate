<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('customerbank/add'); ?>" class="btn btn-primary">Thên ngân hàng</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('customerbank'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="BankName" class="form-control" value="<?php echo set_value('BankName'); ?>" placeholder="Tên ngân hàng">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="BankHolder" class="form-control" value="<?php echo set_value('BankHolder'); ?>" placeholder="Chủ tài khoản">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="BankNumber" class="form-control" value="<?php echo set_value('BankNumber'); ?>" placeholder="Số tài khoản">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                            	<?php $this->Mconstants->selectObject($listCustomers, 'CustomerId', 'FullName', 'CustomerId', set_value('CustomerId'), true, 'Tên CTV', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                 <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Tên ngân hàng</th>                                
                                <th>Chủ tài khoản</th>
                                <th>Số tài khoản</th>
                                <th>Tên CTV</th>
                                <th>Chi nhánh</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyBank">
                            <?php
                            foreach($listCustomerBanks as $b){ ?>
                                <tr id="bank_<?php echo $b['CustomerBankId']; ?>">
                                    <td class="actions">
                                        <div class="btn-group" id="btnGroup_<?php echo $b['CustomerBankId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)" class="link_delete" data-id="<?php echo $b['CustomerBankId']; ?>">Xóa</a></li>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $b['CustomerBankId']; ?>" value="<?php echo $b['StatusId']; ?>">
                                    </td>
                                    <td><?php echo $b['BankName']; ?></td>
                                    <td><a href="<?php echo base_url('customerbank/edit/'.$b['CustomerBankId']); ?>"><?php echo $b['BankHolder']; ?></a></td>
                                    <td><?php echo $b['BankNumber']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listCustomers, 'CustomerId', $b['CustomerId'], 'FullName'); ?></td>
                                    <td><?php echo $b['BranchName']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('customerbank/changeStatus'); ?>">
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>