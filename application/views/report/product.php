<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" id='dateRangePicker' value="" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group ctrl-filter updaten" style="width: 100%;margin: 0;">
                                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                <style>
                    #tbodyProduct .tdQuantity{color: #1782db;font-weight: 600;}
                </style>
                <div class="box-body divTable">
                    <div class=" table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Barcode</th>
                                <th class="text-center">Xếp hạng</th>
                                <th class="text-center">SL thành công</th>
                                <th class="text-center">SL đang giao</th>
                                <th class="text-center">SL Tổng hợp</th>
                                <th class="text-center">SL Tồn kho</th>
                                <th class="text-right">Tổng tiền</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct"></tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <input type="hidden" id="getReportUrl" value="<?php echo base_url("api/order/productSelling"); ?>">
    <input type="hidden" value="<?php echo base_url('product/edit')?>" id="urlEditProduct">
    <input type="hidden" value="<?php echo base_url('product/editCombo')?>" id="urlEditProductCombo">
<?php $this->load->view('includes/footer'); ?>