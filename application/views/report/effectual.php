<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-5 float-right">
                            	<div class="row">
                            		<div class="col-sm-9">
                            			<div class="form-group">
		                                    <div class='input-group' >
		                                        <input type='text' class="form-control daterangepicker" id='dateRangePicker' value="<?php echo date('01/m/Y').' - '.date('d/m/Y'); ?>" />
		                                        <span class="input-group-addon">
		                                            <span class="glyphicon glyphicon-calendar"></span>
		                                        </span>
		                                    </div>
		                                </div>
                            		</div>
                            		<div class="col-sm-3">
                            			<button type="button" class="btn btn-primary submit">Tìm kiếm</button>
                            		</div>
                            	</div>
                                
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-4 col-xs-12">
					          <!-- small box -->
					          <div class="small-box bg-yellow">
					            <div class="inner">
					              <h3 class="text-center total-click">Lược click (0)</h3>
					            </div>
					            <div class="icon">
					              <i class="fa fa-bar-chart"></i>
					            </div>
					            <div class="small-box-footer">
			                        <div class="row col-height">
			                        	<div class="col-lg-4 border_right">
			                        		<span class="span-center total-click-1">0 Click</span>
			                        	</div>
			                        	<div class="col-lg-4 border_right">
			                        		<span class="span-center frequency-click">0 tần xuất Click</span>
			                        	</div>
			                        	<div class="col-lg-4">
			                        		<span class="span-center percent-total-order">0% đơn hàng/Click</span>
			                        	</div>
			                        </div>
					            </div>
					          </div>
					        </div>
					        <div class="col-lg-4 col-xs-12">
					          <!-- small box -->
					          <div class="small-box bg-green">
					            <div class="inner">
					              <h3 class="text-center total-orders">Đơn hàng (0)</h3>
					            </div>
					            <div class="icon">
					              <i class="fa fa-bar-chart"></i>
					            </div>
					            <div class="small-box-footer">
			                        <div class="row col-height">
			                        	<div class="col-lg-4 border_right">
			                        		<span class="span-center order-success">Thành công 0 (0%)</span>
			                        	</div>
			                        	<div class="col-lg-4 border_right">
			                        		<span class="span-center order-processing">Đang xử lý 0 (0%)</span>
			                        	</div>
			                        	<div class="col-lg-4">
			                        		<span class="span-center order-failure">Thất bại 0 (0%)</span>
			                        	</div>
			                        </div>
					            </div>
					          </div>
					        </div>
					        <div class="col-lg-4 col-xs-12">
					          <!-- small box -->
					          <div class="small-box bg-aqua">
					            <div class="inner">
					              <h3 class="text-center total-commission">Hoa hồng (0đ)</h3>
					            </div>
					            <div class="icon">
					              <i class="fa fa-bar-chart"></i>
					            </div>
					            <div class="small-box-footer">
			                        <div class="row col-height">
			                        	<div class="col-lg-4 border_right">
			                        		<span class="span-center commission-success">Thành công 0đ (0%)</span>
			                        	</div>
			                        	<div class="col-lg-4 border_right">
			                        		<span class="span-center commission-waitting-processing">Chờ xử lý 0đ (0%)</span>
			                        	</div>
			                        	<div class="col-lg-4">
			                        		<span class="span-center commission-failure">Thất bại 0đ (0%)</span>
			                        	</div>
			                        </div>
					            </div>
					          </div>
					        </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-4">
                        		<div id="commission"></div>
                        	</div>
                        	<div class="col-lg-4">
                        		<div id="fan_chart"></div>
                        	</div>
                        	<div class="col-lg-4">
                        		<div id="best_selling"></div>
                        	</div>
                        </div>
                    </div>
                </div>
            </section>
            <input type="text" hidden="hidden" id="searchDataEffctualUrl" value="<?php echo base_url("api/order/searchDataEffctual"); ?>">
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">





</script>