<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class='input-group'>
                                        <input type='text' class="form-control daterangepicker" id='dateRangePicker' value="<?php echo date('01/m/Y').' - '.date('d/m/Y'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-20">
					          	<div class="small-box bg-aqua">
					            	<div class="inner text-center db-font">
						            	<h4><b>Tồn cuối kỳ</b></h4>
						              	<h4><b id="total-survival">0 đ</b></h4>
					            	</div>
					            	<div class="icon"><i class="fa fa-bar-chart"></i></div>
					            </div>
					        </div>
                            <div class="col-5 text-center">
                                <div class="icon"><img src="assets/vendor/dist/img/equal.png"></div>
                            </div>
					        <div class="col-20">
					          	<div class="small-box bg-green">
					            	<div class="inner text-center db-font">
						            	<h4><b>Quỹ đầu kỳ</b></h4>
						              	<h4><b>0 đ</b></h4>
					            	</div>
					            	<div class="icon"><i class="fa fa-bar-chart"></i></div>
					            </div>
					        </div>
                            <div class="col-5 text-center">
                                <div class="icon"><i class="fa fa-plus"></i></div>
                            </div>
					        <div class="col-20">
					          	<div class="small-box bg-yellow">
					            	<div class="inner text-center db-font">
						            	<h4><b>Tổng thu trong kỳ</b></h4>
						              	<h4><b id="total-revenue">0 đ</b></h4>
					            	</div>
					            	<div class="icon"><i class="fa fa-bar-chart"></i></div>
					            </div>
					        </div>
                            <div class="col-5 text-center">
                                <div class="icon"><i class="fa fa-minus"></i></div>
                            </div>
					        <div class="col-20">
					          	<div class="small-box bg-red">
					            	<div class="inner text-center db-font">
						            	<h4><b>Tổng chi trong kỳ</b></h4>
						              	<h4><b id="total-expenditure">0 đ</b></h4>
					            	</div>
					            	<div class="icon"><i class="fa fa-bar-chart"></i></div>
					            </div>
					        </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter" >
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả phiếu</a></li>
                        <?php foreach ($listFilters as $f) { ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả đơn hàng theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="group_money">Số tiền</option>
                                        <!--<option value="group_date">Thời điểm tạo phiếu</option>-->
                                        <option value="group_status_verify">Trạng thái duyệt</option>
                                        <option value="group_type">Loại thu chi</option>
                                        <option value="group_moneysource">Loại tiền</option>
                                        <option value="group_bank">Ngân hàng</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 group_money block-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 group_status_verify group_type group_moneysource group_bank none-display ">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control group_status_verify none-display">
                                        <?php foreach($this->Mconstants->verifyLevels as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_type none-display">
                                        <option value="1-1">Thu bán hàng</option>
                                        <option value="1-2">Chi bán hàng</option>
                                        <option value="2-1">Thu nội bộ</option>
                                        <option value="2-2">Chi nội bộ</option>
                                    </select>
                                    <select class="form-control group_moneysource none-display">
                                        <?php foreach($this->Mconstants->moneySources as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control group_bank none-display">
                                        <?php foreach($listBanks as $b){ ?>
                                            <?php echo $b['BankNumber'].' ( '.$b['BankName'].' - '.$b['BankHolder'].' )'; ?>
                                        <?php } ?>
                                    </select>
                                    <!--<select class="form-control group_date none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>-->
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control group_money input-number" type="text">
                                    <!--<input class="form-control datepicker group_date none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker group_date none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">-->
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/transaction/searchMixByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName">
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <div class=" table-responsive no-padding divTable">
                            <table class="table new-style table-hover table-bordered" id="table-data">
                                <thead>
                                <tr>
                                    <th>Mã phiếu</th>
                                    <th>Ngày tạo</th>
                                    <th>Người tạo</th>
                                    <th class="text-right">Số tiền</th>
                                    <th>Loại tiền</th>
                                    <th>Lý do</th>
                                    <th class="text-center">Loại thu chi</th>
                                    <th class="text-center">Trạng thái duyệt</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyTransaction"></tbody>
                            </table>
                        </div>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="26">
                    <input type="hidden" value="<?php echo base_url('transaction/edit');?>" id="urlEditTransaction">
                    <input type="hidden" value="<?php echo base_url('transactioninternal/edit'); ?>" id="urlEditTransactionInternal">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
   
<?php $this->load->view('includes/footer'); ?>