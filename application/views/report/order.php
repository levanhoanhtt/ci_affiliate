<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" id='dateRangePicker' value="<?php echo date('01/m/Y').' - '.date('d/m/Y'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', 0, true, '--Cơ sở--', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listProductTypes, 'ProductTypeId', 'ProductTypeName', 'ProductTypeId', 0, true, '--Lĩnh vực kinh doanh--', ' select2'); ?>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-sm-4">
                                <select class="form-control" id="deliveryTypeId">
                                    <option value="0">--Kênh bán hàng--</option>
                                    <option value="1">POS</option>
                                    <option value="2">Từ xa</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" id="btnGetReport" class="btn btn-primary">Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row no-margin">
                    <div class="col-md-3 col-sm-6 col-xs-12 fix-padding-bottom">
                        <div class="info-box bg-aqua">
                            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                            <div class="info-box-content ">
                                <div class="row col-sm-8">
                                    <span class="info-box-text"><?php echo $reportTypeName; ?> tổng hợp</span>
                                    <span class="info-box-number total-revence text-null">0 đ</span>
                                </div>
                                <div class="row col-sm-4 fix-order">
                                    <span class="info-box-text fix-font total-dh-revence text-null"></span>
                                    <span class="info-box-text fix-font ship-post-revence text-null"></span>
                                </div>
                                <div class="row col-sm-12">
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                <span class="progress-description">
                                    70% Increase in 30 Days
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 fix-padding-bottom">
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                            <div class="info-box-content ">
                                <div class="row col-sm-8">
                                    <span class="info-box-text"><?php echo $reportTypeName; ?> thành công</span>
                                    <span class="info-box-number total-success text-null">0 đ</span>
                                </div>
                                <div class="row col-sm-4 fix-order">
                                    <span class="info-box-text fix-font total-dh-success text-null"></span>
                                    <span class="info-box-text fix-font ship-post-success text-null"></span>
                                </div>
                                <div class="row col-sm-12">
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                <span class="progress-description">
                                    70% Increase in 30 Days
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 fix-padding-bottom">
                        <div class="info-box bg-yellow">
                            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                            <div class="info-box-content ">
                                <div class="row col-sm-8">
                                    <span class="info-box-text"><?php echo $reportTypeName; ?> COD</span>
                                    <span class="info-box-number total-cod text-null">0 đ</span>
                                </div>
                                <div class="row col-sm-4 fix-order">
                                    <span class="info-box-text fix-font total-dh-cod text-null"></span>
                                    <span class="info-box-text fix-font ship-post-cod text-null"></span>
                                </div>
                                <div class="row col-sm-12">
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                <span class="progress-description">
                                    70% Increase in 30 Days
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 fix-padding-bottom">
                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                            <div class="info-box-content ">
                                <div class="row col-sm-8">
                                    <span class="info-box-text"><?php echo $reportTypeName; ?> thất bại</span>
                                    <span class="info-box-number total-failure text-null">0 đ</span>
                                </div>
                                <div class="row col-sm-4 fix-order">
                                    <span class="info-box-text fix-font total-dh-failure text-null"></span>
                                    <span class="info-box-text fix-font ship-post-failure text-null"></span>
                                </div>
                                <div class="row col-sm-12">
                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                <span class="progress-description">
                                    70% Increase in 30 Days
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box-body divTable">
                    <div class=" table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Ngày tháng năm</th>
                                <th><?php echo $reportTypeName; ?> thành công</th>
                                <th><?php echo $reportTypeName; ?> COD</th>
                                <th><?php echo $reportTypeName; ?> thất bại</th>
                                <th><?php echo $reportTypeName; ?> tổng hợp</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyByDate"></tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <input type="hidden" id="getReportUrl" value="<?php echo base_url("api/order/revenue"); ?>">
    <input type="hidden" id="searchByOrderStatusUrl" value="<?php echo base_url("api/order/searchByOrderStatus"); ?>">
    <input type="hidden" id="reportTypeId" value="<?php echo $reportTypeId; ?>">
    <div class="modal fade" id="modalListOrder" role="dialog" aria-labelledby="modalListOrder">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Danh sách Đơn hàng</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Mã đơn</th>
                                <th>Ngày tạo đơn</th>
                                <th>Khách hàng</th>
                                <th class="text-center">TT đơn hàng</th>
                                <th class="text-center">TT giao hàng</th>
                                <th class="text-center">TT Thanh toán</th>
                                <th class="text-right">Tổng tiền</th>
                                <th class="text-right">Kênh</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder"></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>

