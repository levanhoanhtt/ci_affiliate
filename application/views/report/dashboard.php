<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-5 float-right">
                            	<div class="row">
                            		<div class="col-sm-9">
                            			<div class="form-group">
		                                    <div class='input-group' >
		                                        <input type='text' class="form-control daterangepicker" id='dateRangePicker' value="<?php echo date('01/m/Y').' - '.date('d/m/Y'); ?>" />
		                                        <span class="input-group-addon">
		                                            <span class="glyphicon glyphicon-calendar"></span>
		                                        </span>
		                                    </div>
		                                </div>
                            		</div>
                            		<div class="col-sm-3">
                            			<button type="button" class="btn btn-primary submit">Tìm kiếm</button>
                            		</div>
                            	</div>
                            </div>
                        </div>
                        
                        <div class="row">
                        	<div class="col_sm_5">
                        		<div class="small-box bg-green">
						            <div class="inner">
						              <h3 class="text-center">TỔNG SỐ</h3>
						              <h3 class="text-center">ĐƠN HÀNG</h3>
						              <h3 class="text-center">100 (100%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-yellow">
						            <div class="inner">
						              <h3 class="text-center">ĐƠN HÀNG</h3>
						              <h3 class="text-center">ĐANG XỬ LÝ</h3>
						              <h3 class="text-center">30 (30%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-aqua">
						            <div class="inner">
						              <h3 class="text-center">ĐƠN HÀNG</h3>
						              <h3 class="text-center">THÀNH CÔNG</h3>
						              <h3 class="text-center">60 (60%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-red">
						            <div class="inner">
						              <h3 class="text-center">ĐƠN HÀNG</h3>
						              <h3 class="text-center">HỦY</h3>
						              <h3 class="text-center">10 (10%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-green">
						            <div class="inner">
						              <h3 class="text-center">CLICK</h3>
						              <h3 class="text-center">SỐ LƯỢNG</h3>
						              <h3 class="text-center">81,170</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        </div>

                        <div class="row">
                        	<div class="col_sm_5">
                        		<div class="small-box bg-green">
						            <div class="inner">
						              <h3 class="text-center">TỔNG SỐ</h3>
						              <h3 class="text-center">HOA HỒNG</h3>
						              <h3 class="text-center">100 (100%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-yellow">
						            <div class="inner">
						              <h3 class="text-center">HOA HỒNG</h3>
						              <h3 class="text-center">CHỜ DUYỆT</h3>
						              <h3 class="text-center">30 (30%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-aqua">
						            <div class="inner">
						              <h3 class="text-center">HOA HỒNG</h3>
						              <h3 class="text-center">THÀNH CÔNG</h3>
						              <h3 class="text-center">60 (60%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-red">
						            <div class="inner">
						              <h3 class="text-center">HOA HỒNG</h3>
						              <h3 class="text-center">HỦY</h3>
						              <h3 class="text-center">10 (10%)</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        	<div class="col_sm_5">
                        		<div class="small-box bg-green">
						            <div class="inner">
						              <h3 class="text-center">HIỆU QUẢ</h3>
						              <h3 class="text-center">0%/0</h3>
						              <h3 class="text-center">VNĐ</h3>
						            </div>
						            <div class="icon">
						              <i class="fa fa-bar-chart"></i>
						            </div>
						        </div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-sm-6">
                        		<div id="line_chart"></div>
                        	</div>
                        	<div class="col-sm-6">
                        		<div id="fan_chart"></div>
                        	</div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>