<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Part extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Bộ phận/ Phòng ban',
			array('scriptFooter' => array('js' => 'js/part.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($listActions, 'part')) {
			$this->load->model('Mparts');
			$data['listParts'] = $this->Mparts->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/part', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('PartName', 'ParentPartId'));
		if(!empty($postData['PartName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$partId = $this->input->post('PartId');
			if($partId > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();
			}
			else{
				$postData['CrUserId'] = $user['UserId'];
				$postData['CrDateTime'] = getCurentDateTime();
			}
			$this->load->model('Mparts');
			$flag = $this->Mparts->save($postData, $partId);
			if ($flag > 0) {
				$postData['PartId'] = $flag;
				$postData['IsAdd'] = ($partId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Bộ phận/ Phòng ban thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$user = $this->checkUserLogin(true);
		$partId = $this->input->post('PartId');
		if($partId > 0){
			$this->load->model('Mparts');
			$flag = $this->Mparts->changeStatus(0, $partId, '', $user['UserId']);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Bộ phận/ Phòng ban thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
