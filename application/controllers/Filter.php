<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends MY_Controller{

    public function save(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('FilterName', 'FilterData', 'TagFilter', 'ItemTypeId'));
        $filterId = $this->input->post('FilterId');
        if($filterId > 0){
            unset($postData['FilterName']);
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = getCurentDateTime();
        }
        else{
            $postData['StatusId'] = STATUS_ACTIVED;
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
        }
        $this->load->model('Mfilters');
        $filterId = $this->Mfilters->save($postData, $filterId);
        if($filterId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật bộ lọc thành công", 'data' => $filterId));
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $user = $this->checkUserLogin(true);
        $filterId = $this->input->post('FilterId');
        if ($filterId > 0) {
            $this->load->model('Mfilters');
            $flag = $this->Mfilters->changeStatus(0, $filterId, '', $user['UserId']);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa bộ lọc thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}