<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {

	/*public function order($reportTypeId = 0){
        $user = $this->checkUserLogin();
        if(!is_numeric($reportTypeId) || $reportTypeId < 1 || $reportTypeId > 2) $reportTypeId = 1;
        $reportTypeName = 'Doanh '.($reportTypeId == 1 ? 'thu' : 'số');
        $data = $this->commonData($user,
            'Báo cáo '.$reportTypeName,
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_order.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/order/'.$reportTypeId)) {
            $data['reportTypeId'] = $reportTypeId;
            $data['reportTypeName'] = $reportTypeName;
            $this->loadModel(array('Mstores','Mproducttypes'));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('report/order', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function product(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thống kê sản phẩm bán được',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/report_product.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/product')) $this->load->view('report/product', $data);
        else $this->load->view('user/permission', $data);
    }

    public function fundBalance(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Sổ quỹ',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/report_fun_balance.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/fundBalance')) {
            $this->loadModel(array('Mfilters', 'Mbanks'));
            $data['listFilters'] = $this->Mfilters->getList(26);
            $data['listBanks'] = $this->Mbanks->search(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('report/fund_balance', $data);
        }
        else $this->load->view('user/permission', $data);
    }*/


    public function effectual(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Báo cáo hiệu quả',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/highcharts/code/highcharts.js','vendor/plugins/highcharts/code/modules/drilldown.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js','js/report_effectual.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/effectual')) {
            
            $this->load->view('report/effectual', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function dashboard(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Báo cáo',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'css/revenue.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/highcharts/code/highcharts.js','vendor/plugins/highcharts/code/modules/drilldown.js','vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js','js/report_dashboard.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'report/dashboard')) {
            
            $this->load->view('report/dashboard', $data);
        }
        else $this->load->view('user/permission', $data);
    }
}