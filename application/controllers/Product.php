<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Sản phẩm',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array(/*'ckfinder/ckfinder.js',*/ 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/product_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproductchilds', 'Mproducttypes', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList(3);
            $this->load->view('product/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Sản phẩm',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproducttypes', 'Mcategories', 'Mmanufacturers', 'Mtags', 'Mvariants', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
            $data['listVariants'] = $this->Mvariants->get();
            $this->load->view('product/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($productId = 0){
        if($productId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Sửa Sản phẩm',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
            );
            if($this->Mactions->checkAccess($data['listActions'], 'product')) {
                $this->loadModel(array('Mproducts', 'Mproducttypes', 'Mcategories', 'Mmanufacturers', 'Mtags', 'Mcategoryitems', 'Mitemmetadatas', 'Mfiles', 'Mvariants', 'Mproductunits', 'Mproductchilds', 'Mproductprices', 'Mpartners', 'Mactionlogs'));
                $product = $this->Mproducts->get($productId);
                if($product && $product['ProductKindId'] != 3) {
                    $data['productId'] = $productId;
                    //$product['ProductDesc'] = str_replace('/hmd/', IMAGE_PATH, $product['ProductDesc']);
                    $data['product'] = $product;
                    $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
                    //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
                    $data['cateIds'] = $this->Mcategoryitems->getCateIds($productId, 3);
                    $data['tagNames'] = $this->Mtags->getTagNames($productId, 3);
                    $data['listImages'] = $this->Mfiles->getFileUrls($productId, 3, 1);
                    $data['itemSEO'] = $this->Mitemmetadatas->getBy(array('ItemId' => $productId, 'ItemTypeId' => 3), true);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($productId, 3);
                    $data['listProductPrices'] = $this->Mproductprices->getByProductId($productId);
                    $data['listVariants'] = $this->Mvariants->get();
                    $data['variants'] = array();
                    $data['listProductChilds'] = array();
                    if($product['ProductKindId'] == 2) {
                        $listProductChilds = $this->Mproductchilds->getByProductId($productId);
                        $variants = array();
                        foreach($listProductChilds as $pc){
                            if($pc['VariantId1'] > 0) $variants[$pc['VariantId1']][] = $pc['VariantValue1'];
                            if($pc['VariantId2'] > 0) $variants[$pc['VariantId2']][] = $pc['VariantValue2'];
                            if($pc['VariantId3'] > 0) $variants[$pc['VariantId3']][] = $pc['VariantValue3'];
                        }
                        $data['variants'] = $variants;
                        $data['listProductChilds'] = $listProductChilds;
                    }
                }
                else{
                    $data['productId'] = 0;
                    $data['txtError'] = "Không tìm thấy sản phẩm";
                }
                $this->load->view('product/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('product');
    }

    public function addCombo(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Combo Sản phẩm',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproducttypes', 'Mcategories', 'Mtags', 'Mvariants', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
            $data['listVariants'] = $this->Mvariants->get();
            $this->load->view('product/add_combo', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function editCombo($productId = 0){
        if($productId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Sửa Combo',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
            );
            if($this->Mactions->checkAccess($data['listActions'], 'product')) {
                $this->loadModel(array('Mproducts', 'Mproducttypes', 'Mcategories', 'Mtags', 'Mcategoryitems', 'Mitemmetadatas', 'Mfiles', 'Mproductchilds', 'Mproductunits', 'Mproductprices', 'Mpartners', 'Mactionlogs'));
                $product = $this->Mproducts->get($productId);
                if($product && $product['ProductKindId'] == 3) {
                    $data['productId'] = $productId;
                    //$product['ProductDesc'] = str_replace('/hmd/', IMAGE_PATH, $product['ProductDesc']);
                    $data['product'] = $product;
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
                    $data['cateIds'] = $this->Mcategoryitems->getCateIds($productId, 3);
                    $data['tagNames'] = $this->Mtags->getTagNames($productId, 3);
                    $data['listImages'] = $this->Mfiles->getFileUrls($productId, 3, 1);
                    $data['itemSEO'] = $this->Mitemmetadatas->getBy(array('ItemId' => $productId, 'ItemTypeId' => 3), true);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($productId, 3);
                    $data['listProductPrices'] = $this->Mproductprices->getByProductId($productId);
                    $data['listProductChilds'] = $this->Mproductchilds->getByProductId($productId);
                }
                else{
                    $data['productId'] = 0;
                    $data['txtError'] = "Không tìm thấy combo sản phẩm";
                }
                $this->load->view('product/edit_combo', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('product');
    }

    public function quantity(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tồn kho Sản phẩm',
            array('scriptFooter' => array('js' => array('js/search_item.js', 'js/product_quantity.js')))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'product/quantity')) {
            $data['canViewPrice'] = $this->Mactions->checkAccess($listActions, 'product/price');
            $this->loadModel(array('Mproducttypes', 'Msuppliers', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mstores', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList(15);
            $this->load->view('product/quantity', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function price(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thay đổi giá Sản phẩm',
            array('scriptFooter' => array('js' => array('js/search_item.js', 'js/product_price.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product/price')) {
            $this->loadModel(array('Mproducttypes', 'Msuppliers', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mstores', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList(24);
            $this->load->view('product/price', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function exportExcel(){
        $user = $this->checkUserLogin();
        if($this->Mactions->checkAccessFromDb('product', $user['UserId'])){
            $this->loadModel(array('Mproducts', 'Mproductchilds', 'Mproducttypes'));
            $listProducts = $this->Mproducts->getBy(array('ProductStatusId' => STATUS_ACTIVED), false, '', 'ProductId, ProductName, Sku, BarCode, GuaranteeMonth, Price, Weight, ProductTypeId, ProductKindId');
            if(!empty($listProducts)){
                $listProductTypes = $this->Mproducttypes->getList();
                $fileUrl = FCPATH . 'assets/uploads/excels/export_product.xls';
                $this->load->library('excel');
                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                $objPHPExcel = $objReader->load($fileUrl);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $i = 1;
                foreach($listProducts as $p) {
                    $flag = false;
                    $productTypeName = $this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $p['ProductTypeId'], 'ProductTypeName');
                    if($p['ProductKindId'] == 2){
                        $listProductChilds =  $this->Mproductchilds->getBy(array('ProductId' => $p['ProductId']), false, '', 'ProductName, Sku, BarCode, GuaranteeMonth, Price, Weight');
                        if(!empty($listProductChilds)){
                            foreach($listProductChilds as $pc){
                                $i++;
                                $sheet->setCellValue('A' . $i, $p['ProductName'].' ('.$pc['ProductName'].')');
                                $sheet->setCellValue('B' . $i, $pc['Sku']);
                                $sheet->setCellValue('C' . $i, $pc['BarCode']);
                                $sheet->setCellValue('D' . $i, $pc['GuaranteeMonth']);
                                $sheet->setCellValue('E' . $i, priceFormat($pc['Price']));
                                $sheet->setCellValue('F' . $i, $pc['Weight']);
                                $sheet->setCellValue('G' . $i, $productTypeName);
                            }
                        }
                        else $flag = true;
                    }
                    else $flag = true;
                    if($flag){
                        $i++;
                        $sheet->setCellValue('A' . $i, $p['ProductName']);
                        $sheet->setCellValue('B' . $i, $p['Sku']);
                        $sheet->setCellValue('C' . $i, $p['BarCode']);
                        $sheet->setCellValue('D' . $i, $p['GuaranteeMonth']);
                        $sheet->setCellValue('E' . $i, priceFormat($p['Price']));
                        $sheet->setCellValue('F' . $i, $p['Weight']);
                        $sheet->setCellValue('G' . $i, $productTypeName);
                    }
                }
                $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                $filename = "exportProduct_".date('Y-m-d').".xls";
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                $objPHPExcel->disconnectWorksheets();
                unset($objPHPExcel);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    /*public function importExcel()
    {
        $user = $this->checkUserLogin(true);
        $fileUrl = trim($this->input->post('FileUrl'));
        if (!empty($fileUrl)) {
            if(ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
            $fileUrl = FCPATH . $fileUrl;
            $this->load->model(array('Mproducts', 'Msuppliers', 'Mproductoptions', 'Mproductchilds'));
            $this->load->library('excel');
            $inputFileType = PHPExcel_IOFactory::identify($fileUrl);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fileUrl);
            $objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
            $countRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

            for($row = 2; $row < $countRows; $row++){
                $namePrev = trim($objWorksheet->getCellByColumnAndRow(0, $row-1)->getValue());
                $name =  trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $sku =  trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $barcode =  trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $weight =  trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $color =  trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $supplier =  trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $price =  trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                $isContactPrice =  trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
                $oldPrice =  trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                $warranty =  trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
                $kindProduct =  trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());
                $isPublish =  trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue());
                $productStatus =  trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue());


                if($name == '')
                    continue;

                $isPublish = $isPublish == 'YES' ? 1 : 3;      

                if($isContactPrice == 'CO')
                    $isContactPrice = 2;
                else
                    $isContactPrice = 1;

                if($productStatus == 'ĐANG KINH DOANH')
                    $productStatus = 2;
                elseif($productStatus == 'TẠM DỪNG KINH DOANH')
                    $productStatus = 1;
                else
                    $productStatus = 3;

                $kindProductId = $kindProduct == 'ĐƠN' ? 1 : 2;
                

                //Check supplier
                $supplierExist = $this->Msuppliers->getBy(array('SupplierName'=>$supplier, 'ItemStatusId' => STATUS_ACTIVED),true);
                
                if(!$supplierExist)
                {

                    $supplier = array(
                        'SupplierCode' => $supplier,
                        'SupplierName' => $supplier,
                        'SupplierTypeId' => 1,
                        'ItemStatusId' => 2,
                        'HasBill' => 0,
                        'Comment' => '',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => getCurentDateTime()

                    );
                    $supplierId = $this->Msuppliers->save($supplier,0);
                }
                else
                    $supplierId = $supplierExist['SupplierId'];



                //Check if Exist SKU               
                $productExist = $this->Mproducts->getBy(array('Sku'=>$sku),true);
                // pre( $productExist );
                // pre($productExist);

                // die();
                if(!$productExist)
                {
                     
                    $data = array(
                        'ProductName' => $name,
                        'ProductSlug' => makeSlug($name),
                        'ProductShortDesc' => '',
                        'ProductDesc' => '',
                        'ProductTypeId' => 1,
                        'ProductStatusId' =>$productStatus,
                        'ProductKindId' => $kindProductId,
                        'VATStatusId' => 1,
                        'ProductDisplayTypeId' => $isPublish,
                        'ProductLevelId' => 2,
                        'ParentProductId' => 0,
                        'SupplierId' => $supplierId,
                        'Quantity' => 0,
                        'IsContactPrice' => $isContactPrice,
                        'Price'     =>  $price,
                        'OldPrice'     =>  $oldPrice,
                        'ProductImage'     =>  '',
                        'ProductCode'     =>  '',
                        'BarCode'     =>  $barcode,
                        'Sku'     =>  $sku,
                        'Weight'     =>  $weight,
                        'PublishDateTime'     =>  getCurentDateTime(),
                        'IsManageExtraWarehouse'     =>  1,
                        'GuaranteeMonth'     =>  $warranty,
                        'VideoUrls'     =>  '',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => getCurentDateTime()
                    );

                    if($kindProductId == 2 )
                    {

                        //Thêm ProductChild cho CurrentRow
                        $dataProductChild = array(
                                // 'ProductId' => $productPrev['ProductId'],
                                'VariantId1' => 1,
                                'VariantId2' => 0,
                                'VariantId3' => 0,
                                'VariantValue1' => $color,
                                'ProductName' => $name,
                                'BarCode'  => $barcode,
                                'Sku'  => $sku,
                                'StatusId'  => 2,
                                'Price'  => $price,
                                'OldPrice'  => $oldPrice,
                                'Weight'  => $weight,
                                'VATStatusId'  => 1,
                                'GuaranteeMonth'  => $warranty,

                        );
                        $checkProductChild = $this->Mproductchilds->getBy(array('Sku'=>$sku),true);
                        if($checkProductChild)
                        {
                            $this->Mproductchilds->save($dataProductChild,$checkProductChild['ProductChildId']);
                            continue;
                        }



                        if($name == $namePrev)
                        {

                            $skuPrev = trim($objWorksheet->getCellByColumnAndRow(1, $row - 1)->getValue());
                            $productPrev = $this->Mproducts->getBy(array('Sku'=>$skuPrev),true);

                            $dataProductChild['ProductId'] = $productPrev['ProductId'];
                            $this->Mproductchilds->save($dataProductChild,0);
                        }
                        else
                        {
                                //SẢN PHẨM ĐA  LẦN ĐẦU
                                
                                $ProductId = $this->Mproducts->save($data,0);
                                $dataProductChild = array(
                                        'ProductId' => $ProductId,
                                        'VariantId1' => 1,
                                        'VariantId2' => 0,
                                        'VariantId3' => 0,
                                        'VariantValue1' => $color,
                                        'ProductName' => $name,
                                        'BarCode'  => $barcode,
                                        'Sku'  => $sku,
                                        'StatusId'  => 2,
                                        'Price'  => $price,
                                        'OldPrice'  => $oldPrice,
                                        'Weight'  => $weight,
                                        'VATStatusId'  => 1,
                                        'GuaranteeMonth'  => $warranty,

                                );
                                $this->Mproductchilds->save($dataProductChild,0);

                        }

                        
                        

                    }
                    else
                    {
                        //SẢN PHẨM ĐƠN
                        
                        $ProductId = $this->Mproducts->save($data,0);
                        $dataOption = array(
                            'ProductId' => $ProductId,
                            'OptionName' => 'Màu sắc',
                            'OptionValue' => $color
                        );
                        $this->Mproductoptions->save($dataOption,0);
                    }
                    

                    


                }
                else
                {
                    $data = array(
                        'ProductName' => $name,
                        'ProductTypeId' => 1,
                        'ProductStatusId' =>$productStatus,
                        'ProductKindId' => $kindProductId,
                        'VATStatusId' => 1,
                        'ProductDisplayTypeId' => $isPublish,
                        'ProductLevelId' => 2,
                        'ParentProductId' => 0,
                        'SupplierId' => $supplierId,
                        'IsContactPrice' => $isContactPrice,
                        'Price'     =>  $price,
                        'OldPrice'     =>  $oldPrice,
                        'BarCode'     =>  $barcode,
                        'Sku'     =>  $sku,
                        'Weight'     =>  $weight,
                        'GuaranteeMonth'     =>  $warranty,
                        'UpdateUserId' => $user['UserId'],
                        'UpdateDateTime' => getCurentDateTime()
                    );
                    $ProductId = $this->Mproducts->save($data,$productExist['ProductId']);

                    if($kindProductId == 1)
                    {

                        $this->Mproductoptions->deleteMultiple(array('ProductId'=>$ProductId));

                        $dataOption = array(
                            'ProductId' => $ProductId,
                            'OptionName' => 'Màu sắc',
                            'OptionValue' => $color
                        );
                        $this->Mproductoptions->save($dataOption,0);

                    }
                    else
                    {
                        
                        $dataUpdateChild = array(
                            'VariantId1' => 1,
                            'VariantId2' => 0,
                            'VariantId3' => 0,
                            'VariantValue1' => $color,
                            'ProductName' => $name,
                            'BarCode'  => $barcode,
                            'Sku'  => $sku,
                            'StatusId'  => 2,
                            'Price'  => $price,
                            'OldPrice'  => $oldPrice,
                            'Weight'  => $weight,
                            'VATStatusId'  => 1,
                            'GuaranteeMonth'  => $warranty,
                        );
                        $this->db->where(array('ProductId'=>$productExist['ProductId'],'Sku'=>$productExist['Sku']));
                        $this->db->update('productchilds', $dataUpdateChild); 
                        // echo $this->db->last_query();
                        // die();
                    }
                }
                // die();
            }
            $objPHPExcel->disconnectWorksheets();
            // unset($objPHPExcel);
            // unlink($fileUrl);
            echo json_encode(array('code' => 1, 'message' => "Update Success"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Please choose excel file"));
    }*/
}