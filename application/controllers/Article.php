<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

    public function index($articleTypeId = 1){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Bài viết',
            array('scriptFooter' => array('js' => 'js/article.js'))
        );
        if(!is_numeric($articleTypeId) || !in_array($articleTypeId, array(1, 2))) $articleTypeId = 1;
        if($this->Mactions->checkAccess($data['listActions'], 'article/'.$articleTypeId)) {
            if($articleTypeId == 2) $data['title'] = 'Danh sách trang tĩnh';
            $data['articleTypeId'] = $articleTypeId;
            $this->loadModel(array('Marticles', 'Mcategories', 'Mcategoryitems'));
            $data['listCategories'] = $this->Mcategories->getListByItemType(4);
            $data['listUsers'] = $this->Musers->getListForSelect();
            $postData = $this->arrayFromPost(array('ArticleTitle', 'CategoryId'));
            $postData['ArticleTypeId'] = $articleTypeId;
            $rowCount = $this->Marticles->getCount($postData);
            $data['listArticles'] = array();
            if($rowCount > 0){
                $perPage = 20;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listArticles'] = $this->Marticles->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('article/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add($articleTypeId = 1){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới Bài viết',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/article_update.js')))
        );
        if(!is_numeric($articleTypeId) || !in_array($articleTypeId, array(1, 2))) $articleTypeId = 1;
        if($this->Mactions->checkAccess($data['listActions'], 'article/'.$articleTypeId)) {
            if($articleTypeId == 2) $data['title'] = 'Thêm mới trang tĩnh';
            $data['articleTypeId'] = $articleTypeId;
            if($articleTypeId == 1) {
                $this->load->model('Mcategories');
                $data['listCategories'] = $this->Mcategories->getListByItemType(4);
            }
            else $data['listCategories'] = array();
            $this->load->view('article/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($articleId){
        $user = $this->checkUserLogin();
        if($articleId > 0){
            $data = $this->commonData($user,
                'Cật nhật Bài viết',
                array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/article_update.js')))
            );
            $this->loadModel(array('Marticles','Mcategories', 'Mtags', 'Mcategoryitems'));
            $article = $this->Marticles->get($articleId);
            if($article) {
                if ($this->Mactions->checkAccess($data['listActions'], 'article/'.$article['ArticleTypeId'])) {
                    $data['articleId'] = $articleId;
                    $data['article'] = $article;
                    $data['tagNames'] = $this->Mtags->getTagNames($articleId, 4);
                    if ($article['ArticleTypeId'] == 1) {
                        $data['listCategories'] = $this->Mcategories->getListByItemType(4);
                        $data['categoryIds'] = $this->Mcategoryitems->getCateIds($articleId, 4);
                    }
                    else {
                        $data['listCategories'] = array();
                        $data['categoryIds'] = array();
                    }
                    $this->load->view('article/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['articleId'] = 0;
                $data['txtError'] = "Không tìm thấy Bài viết";
            }
        }
        else redirect('article');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ArticleTitle', 'ArticleSlug', 'ArticleLead', 'ArticleContent', 'ArticleTypeId', 'ArticleStatusId', 'ArticleImage'));
        if(empty($postData['ArticleSlug'])) $postData['ArticleSlug'] = makeSlug($postData['ArticleTitle']);
        else $postData['ArticleSlug'] = makeSlug($postData['ArticleSlug']);
        $postData['ArticleImage'] = replaceFileUrl($postData['ArticleImage']);
        $articleId = $this->input->post('ArticleId');
        $crDateTime = getCurentDateTime();
        if($articleId > 0){
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = $crDateTime;
        }
        else{
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = $crDateTime;
            $publishDateTime = trim($this->input->post('PublishDateTime'));
            if(!empty($publishDateTime)) $postData['PublishDateTime'] = $publishDateTime;
            else $postData['PublishDateTime'] = $crDateTime;
        }
        $this->load->model('Marticles');
        $categoryIds = json_decode(trim($this->input->post('CategoryIds')), true);
        $tagNames = json_decode(trim($this->input->post('TagNames')), true);
        $articleId = $this->Marticles->update($postData, $articleId, $categoryIds, $tagNames);
        if ($articleId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Bài viết thành công", 'data' => $articleId));
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $articleId = $this->input->post('ArticleId');
        $statusId = $this->input->post('StatusId');
        if($articleId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
            $this->load->model('Marticles');
            $flag = $this->Marticles->changeStatus($statusId, $articleId, 'ArticleStatusId', $user['UserId']);
            if($flag) {
                $txtSuccess = "";
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa Bài viết thành công";
                else{
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}