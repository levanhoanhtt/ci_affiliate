<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends MY_Controller {

    public function create() {
        $this->load->model('Mlinkaffs');
        $this->load->model('Mlinkgroups');
        $this->load->model('Mlinkgroupproducts');
        $this->load->model('Mproducts');
        $user = $this->checkUserLogin();
        $userId = $user['UserId'];
        $this->db->trans_begin();

        // Nếu chưa nhập url thì báo lỗi
        if($this->input->post('url')=="") {
            echo json_encode(['errors'=>true]);
            return true;
        }

        $nameGroup = $this->input->post('name');
        $approachId = $this->input->post('approach');
        $note = $this->input->post('note');
        $linkGroupId = $this->input->post('LinkGroupId');
        $url = $this->input->post('url');
        $crDateTime = getCurentDateTime();
        $slugProduct = getSlugProduct($url);
        $products = $this->Mproducts->getBy(array('ProductSlug' => $slugProduct, 'ProductStatusId' => STATUS_ACTIVED));
        
        // Nếu CookieTimeout = 0 thì sẽ không tạo được link rút gọn
        if ($products['CookieTimeout']==0) {
            echo json_encode(['errors'=>true, 'cookieTimeout' => true]);
            return true;
        }

        // Nếu link cần rút gọn đã tồn tại thì báo lỗi
        $checkOldUrlUnique = $this->Mlinkaffs->checkOldLink($url);
        if (count($checkOldUrlUnique) >= 1) {
            echo json_encode(['errors'=>true, 'oldUrlUnique' => true]);
            return true;
        }

        $check = true;
        while ($check) {
            $urlShort = DOMAIN_SHORT.substr(md5(microtime()*rand(0,9999)),0,15);
            $checkUrlUnique = $this->Mlinkaffs->checkShortLink($urlShort);
            if (count($checkUrlUnique) == 0) {
                $check = false;
            }
        }

        // Update linkaff
        $this->Mlinkaffs->save([
                        'CustomerId'=>$userId,
                        'ProductId'=>isset($products['ProductId'])?$products['ProductId']:0,
                        'OriginalLink'=>$url,
                        'ShortLink'=>$urlShort,
                        'CrUserId' => $userId,
                        'CrDateTime' => $crDateTime,
        ]);
        // Update link groups
        if ($nameGroup) {
            $this->Mlinkgroups->save([
                            'LinkGroupName'=>$nameGroup,
                            'LinkGroupComment'=>$note,
                            'ApproachTypeId' => $approachId,
                            'StatusId' => 2,
                            'CrUserId' => $userId,
                            'CustomerId' => $userId,
                            'CrDateTime' => $crDateTime,
            ]);
        }
        $links = $this->Mlinkaffs->get(0, false, "LinkAffId", "", 1, 0, 'desc');
        $linkGroups = $this->Mlinkgroups->get(0, false, "LinkGroupId", "", 1, 0, 'desc');
        // Update link group product
        $this->Mlinkgroupproducts->save(['LinkGroupId'=>$linkGroups[0]['LinkGroupId'], 'LinkAffId'=>$links[0]['LinkAffId']]);
        if ($linkGroupId) {
            foreach ($linkGroupId as $k=>$v) {
                $this->Mlinkgroupproducts->save(['LinkGroupId'=>$v, 'LinkAffId'=>$links[0]['LinkAffId']]);
            }
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
        }
        $listGroups = $this->Mlinkgroups->getListPluck();

        echo json_encode(['errors' => false, 'url'=>$urlShort, 'code'=>1, 'listGroups'=>$listGroups]);

        return true;
    }

    public function check(){
        $userName = $this->input->post('UserName');
        $userPass = $this->input->post('UserPass');
        if(!empty($userName) && !empty($userPass)) {
            $user = $this->Musers->login($userName, $userPass);
            if($user){
                $postData = $this->arrayFromPost(array('CTVId', 'LinkAffId', 'AffiliateName', 'OfferId', 'OfferName'));
                $products = json_decode(trim($this->input->post('Products')), true);
                if($postData['CTVId'] > 0 && $postData['LinkAffId'] > 0 && $postData['AffiliateName'] == 'Ricky' && $postData['OfferId'] == 1 && $postData['OfferName'] == 'Ricky' && !empty($products)){
                    $this->load->model('Mlinkaffs');
                    $linkAff = $this->Mlinkaffs->get($postData['LinkAffId']);
                    if($linkAff && $linkAff['CustomerId'] == $postData['CTVId']){
                        $flag = false;
                        foreach($products as $p){
                            if($p['ProductId'] == $linkAff['ProductId'] && $p['ProductChildId'] == $linkAff['ProductChildId']){
                                $flag = true;
                                break;
                            }
                        }
                        if($flag){
                            //log
                            echo json_encode(array('code' => 1, 'message' => "Check OK"));
                        }
                        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListByProductId(){
        $userName = $this->input->post('UserName');
        $userPass = $this->input->post('UserPass');
        $productPartnerId = $this->input->post('ProductPartnerId');
        if(!empty($userName) && !empty($userPass) && $productPartnerId > 0) {
            $user = $this->Musers->login($userName, $userPass);
            if($user) {
                $this->loadModel(array('Mproducts', 'Mlinkaffs'));
                $productId = $this->Mproducts->getFieldValue(array('ProductPartnerId' => $productPartnerId, 'ProductStatusId' => STATUS_ACTIVED), 'ProductId', 0);
                if($productId > 0){
                    $listLinkAffs = $this->Mlinkaffs->getBy(array('ProductId' => $productId), false, '', 'LinkAffId, CustomerId');
                    echo json_encode(array('code' => 1, 'data' => $listLinkAffs));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}