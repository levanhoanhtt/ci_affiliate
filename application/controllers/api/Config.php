<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MY_Controller{

    public function transportType(){
        header('Content-Type: application/json');
        $this->load->model('Mtransporttypes');
        echo json_encode(array('code' => 1, 'data' => $this->Mtransporttypes->getBy(array('StatusId' => STATUS_ACTIVED))));
    }

    public function store(){
        header('Content-Type: application/json');
        $this->load->model('Mstores');
        echo json_encode(array('code' => 1, 'data' => $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED), false, '', 'StoreId, StoreName')));
    }

    public function update($autoLoad = 1){
        $user = $this->checkUserLogin(true);
        $this->load->model('Mconfigs');
        $listConfigs = $this->Mconfigs->getBy(array('AutoLoad' => $autoLoad), false, "", "ConfigId,ConfigCode,ConfigValue");
        $valueData = array();
        $updateDateTime = getCurentDateTime();
        foreach($listConfigs as $c){
            $configValue = trim($this->input->post($c['ConfigCode']));
            if($c['ConfigCode'] == 'LOGO_IMAGE' || $c['ConfigCode'] == 'ADDRESS_IMAGE') $configValue = replaceFileUrl($configValue);
            if($c['ConfigValue'] != $configValue){
                $valueData[] = array('ConfigId' => $c['ConfigId'], 'ConfigValue' => $configValue, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $updateDateTime);
            }
        }
        $flag = $this->Mconfigs->updateBatch($valueData);
        if($flag){
            if($autoLoad == 1) {
                $configs = $this->Mconfigs->getListMap();
                $this->session->set_userdata('configs', $configs);
            }
            echo json_encode(array('code' => 1, 'message' => "Cập nhật cấu hình thành công"));
        }
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateItem(){
        $user = $this->checkUserLogin(true);
        $configCode = trim($this->input->post('ConfigCode'));
        $configValue = trim($this->input->post('ConfigValue'));
        if(!empty($configCode) && !empty($configValue)){
            $this->load->model('Mconfigs');
            $flag = $this->Mconfigs->updateItem($configCode, $configValue, $user['UserId']);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật cấu hình thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    /*public function getListDistrict(){
        $provinceId = $this->input->post('ProvinceId');
        $listDistricts = array();
        if($provinceId > 0){
            $this->load->model('Mdistricts');
            $listDistricts = $this->Mdistricts->getList($provinceId);
        }
        echo json_encode($listDistricts);
    }*/

    public function getListWard(){
        $districtId = $this->input->post('DistrictId');
        $listWards = array();
        if($districtId > 0){
            $this->load->model('Mwards');
            $listWards = $this->Mwards->getList($districtId);
        }
        echo json_encode($listWards);
    }
}