<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller{

    public function changeStatusBatch(){
        $user = $this->checkUserLogin(true);
        $productIds = json_decode(trim($this->input->post('ItemIds')), true);
        $statusId = $this->input->post('StatusId');
        if(!empty($productIds) && $statusId >= 0){
            $this->load->model('Mproducts');
            $flag = $this->Mproducts->changeStatusBatch($productIds, $statusId, $user);
            if($flag) {
                $msg = 'Xóa sản phẩm thành công';
                $statusName = '';
                if ($statusId > 0) {
                    $msg = 'Thay đổi trạng thái thành công';
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->productStatus[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $msg, 'data' => $statusName));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductName', 'ProductSlug', 'ProductShortDesc', 'ProductDesc', 'ProductTypeId', 'ProductStatusId', 'ProductKindId', 'VATStatusId', 'ProductDisplayTypeId', 'ProductLevelId', 'ParentProductId', 'SupplierId', 'ManufacturerId', 'IsContactPrice', 'Price', 'OldPrice', 'ProductImage', 'BarCode', 'Sku', 'Weight', 'IsManageExtraWarehouse', 'FormalStatus', 'UsageStatus', 'AccessoryStatus', 'ProductUnitId', 'GuaranteeMonth', 'VideoUrls', 'DiscountPercent', 'CookieTimeout', 'ProductPartnerId', 'PartnerId'));
        if(!empty($postData['ProductName']) && $postData['ProductStatusId'] > 0 && $postData['ProductKindId'] > 0 && $postData['ProductDisplayTypeId'] > 0 && $postData['PartnerId'] > 0) {
            $productId = $this->input->post('ProductId');
            $this->load->model('Mproducts');
            $flag = $this->Mproducts->checkExist($postData['BarCode'], $productId);
            if ($flag > 0) echo json_encode(array('code' => 0, 'message' => "BarCode sản phẩm đã tồn tại"));
            else {
                $crDateTime = getCurentDateTime();
                $actionLogs = array(
                    'ItemTypeId' => 3,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                if ($productId > 0) {
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 2;
                    $actionLogs['Comment'] = $user['FullName'] . ': Cập nhật sản phẩm';
                }
                else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = $crDateTime;
                    $publishDateTime = trim($this->input->post('PublishDateTime'));
                    if (!empty($publishDateTime)) $postData['PublishDateTime'] = $publishDateTime;
                    else $postData['PublishDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 1;
                    $actionLogs['Comment'] = $user['FullName'] . ': Thêm mới sản phẩm';
                }
                $productSEO = $this->arrayFromPost(array('TitleSEO', 'MetaDesc', 'Canonical', 'IsRobotIndex', 'IsRobotFollow', 'IsOnSitemap'));
                $productSEO['ItemTypeId'] = 3;
                if (empty($postData['ProductSlug'])) {
                    $postData['ProductSlug'] = makeSlug($postData['ProductName']);
                    $productSEO['Canonical'] = $postData['ProductSlug'];
                } else {
                    $postData['ProductSlug'] = makeSlug($postData['ProductSlug']);
                    $productSEO['Canonical'] = $postData['ProductSlug'];
                }
                //$postData['ProductDesc'] = replaceFileUrl($postData['ProductDesc'], IMAGE_PATH, '/hmd/');
                if (empty($productSEO['TitleSEO'])) $productSEO['TitleSEO'] = $postData['ProductName'];
                $postData['ProductImage'] = replaceFileUrl($postData['ProductImage'], PRODUCT_PATH);
                $images = json_decode(replaceFileUrl(trim($this->input->post('Images')), PRODUCT_PATH), true);
                $cateIds1 = json_decode(trim($this->input->post('CateIds1')), true);
                $cateIds2 = json_decode(trim($this->input->post('CateIds2')), true);
                $tagNames = json_decode(trim($this->input->post('TagNames')), true);
                $productChilds = array();
                if ($postData['ProductKindId'] == 2) {
                    $variants = json_decode(trim($this->input->post('Variants')), true);
                    unset($variants[0]);
                    foreach ($variants as $i => $variant) {
                        if (!is_array($variant)) unset($variants[$i]);
                    }
                    $variantOptions = json_decode(trim($this->input->post('VariantOptions')), true);
                    $variantId1 = $variantId2 = $variantId3 = 0;
                    $variantValues1 = $variantValues2 = $variantValues3 = array();
                    $i = 0;
                    foreach ($variants as $variantId => $variantValues) {
                        $i++;
                        if ($i == 1) {
                            $variantId1 = $variantId;
                            $variantValues1 = $variantValues;
                        }
                        elseif ($i == 2) {
                            $variantId2 = $variantId;
                            $variantValues2 = $variantValues;
                        }
                        elseif ($i == 3) {
                            $variantId3 = $variantId;
                            $variantValues3 = $variantValues;
                        }
                    }
                    $variantCount = count($variants);
                    foreach ($variantOptions as $variantOption) {
                        $variantValues = explode('-', $variantOption['VariantValue']);
                        $variantOption['ProductName'] = $variantOption['VariantValue'];
                        $variantOption['ProductImage'] = replaceFileUrl($variantOption['ProductImage'], PRODUCT_PATH);
                        unset($variantOption['VariantValue']);
                        $variantOption['VATStatusId'] = $postData['VATStatusId'];
                        $variantOption['StatusId'] = STATUS_ACTIVED;
                        if (count($variantValues) == $variantCount) {
                            $i = 0;
                            foreach ($variantValues as $variantValue) {
                                $variantValue = trim($variantValue);
                                $i++;
                                if ($i == 1) {
                                    $variantOption['VariantValue1'] = $variantValue;
                                    if (in_array($variantValue, $variantValues1)) $variantOption['VariantId1'] = $variantId1;
                                    elseif (in_array($variantValue, $variantValues2)) $variantOption['VariantId1'] = $variantId2;
                                    elseif (in_array($variantValue, $variantValues3)) $variantOption['VariantId1'] = $variantId3;
                                }
                                elseif ($i == 2) {
                                    $variantOption['VariantValue2'] = $variantValue;
                                    if (in_array($variantValue, $variantValues1)) $variantOption['VariantId2'] = $variantId1;
                                    elseif (in_array($variantValue, $variantValues2)) $variantOption['VariantId2'] = $variantId2;
                                    elseif (in_array($variantValue, $variantValues3)) $variantOption['VariantId2'] = $variantId3;
                                }
                                elseif ($i == 3) {
                                    $variantOption['VariantValue3'] = $variantValue;
                                    if (in_array($variantValue, $variantValues1)) $variantOption['VariantId3'] = $variantId1;
                                    elseif (in_array($variantValue, $variantValues2)) $variantOption['VariantId3'] = $variantId2;
                                    elseif (in_array($variantValue, $variantValues3)) $variantOption['VariantId3'] = $variantId3;
                                }
                            }
                        }
                        if(isset($variantOption['ProductPrices']) && !empty($variantOption['ProductPrices'])){
                            $productPrices = array();
                            $pps = json_decode($variantOption['ProductPrices'], true);
                            foreach($pps as $pp){
                                if($pp['ProductPriceId'] == 0){
                                    unset($pp['ProductPriceId']);
                                    $pp['CrUserId'] = $user['UserId'];
                                    $pp['CrDateTime'] = $crDateTime;
                                    $productPrices[] = $pp;
                                }
                            }
                            $variantOption['ProductPrices'] = $productPrices;
                        }
                        else $variantOption['ProductPrices'] = array();
                        $productChilds[] = $variantOption;
                    }
                    if (empty($productChilds)) $postData['ProductKindId'] = 1;
                }
                elseif ($postData['ProductKindId'] == 3) $productChilds = json_decode(trim($this->input->post('VariantOptions')), true);
                $prices = json_decode(trim($this->input->post('Prices')), true);
                $productId = $this->Mproducts->update($postData, $productId, $images, $productSEO, $cateIds1, $cateIds2, $tagNames, $productChilds, $prices, $actionLogs);
                if ($productId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật sản phẩm thành công", 'data' => $productId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updatePriceTable(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductId', 'ProductChildId', 'Quantity', 'Price'));
        if($postData['ProductId'] > 0 && $postData['Quantity'] > 0 && $postData['Price'] > 0) {
            $productPriceId = $this->input->post('ProductPriceId');
            if ($productPriceId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $this->load->model('Mproductprices');
            $productPriceId = $this->Mproductprices->save($postData, $productPriceId);
            if ($productPriceId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật gíá sản phẩm thành công", 'data' => $productPriceId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function deletePriceTable(){
        $productPriceId = $this->input->post('ProductPriceId');
        if($productPriceId > 0){
            $this->load->model('Mproductprices');
            $flag = $this->Mproductprices->delete($productPriceId);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa gíá sản phẩm thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function get(){
        $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        if($productId > 0){
            $this->loadModel(array('Mproducts', 'Mproductchilds', 'Mproductprices', 'Mproductunits', 'Mpricechanges'));
            $product = $this->Mproducts->get($productId, true, '', 'ProductId, ProductName, ProductImage, ProductStatusId, ProductKindId, Price, BarCode, Weight, GuaranteeMonth, ProductUnitId, VATStatusId');
            if($product && $product['ProductStatusId'] == STATUS_ACTIVED){
                if(empty($product['ProductImage'])) $product['ProductImage'] = NO_IMAGE;
                $productUnitName = $product['ProductUnitId'] > 0 ? $this->Mproductunits->getFieldValue(array('ProductUnitId' => $product['ProductUnitId']), 'ProductUnitName') : '';
                $product['ProductUnitName'] = $productUnitName;
                $isGetPrice = $this->input->post('IsGetPrice');
                $productChildId = $this->input->post('ProductChildId');
                if($productChildId > 0){
                    $productChild = $this->Mproductchilds->get($productChildId, true, '', 'ProductChildId, ProductId, ProductName, ProductImage, StatusId, Price, BarCode, Weight, GuaranteeMonth, VATStatusId');
                    if($productChild && $productChild['StatusId'] == STATUS_ACTIVED){
                        if(empty($productChild['ProductImage'])) $productChild['ProductImage'] = NO_IMAGE;
                        $listProductPrices = $this->Mproductprices->getByProductId($productId);
                        $productRetVal = $productChild;
                        $productRetVal['ProductKindId'] = $product['ProductKindId'];
                        $productRetVal['ProductChildName'] = $productChild['ProductName'];
                        $productRetVal['ProductName'] = $product['ProductName'].' ('.$productChild['ProductName'].')';
                        $productRetVal['ProductUnitName'] = $productUnitName;
                        $productRetVal['PriceCapital'] = $this->Mpricechanges->getPrice($productId, $productChildId, $productChild['Price']);
                        echo json_encode(array('code' => 1, 'data' => array('Products' => array($productRetVal), 'ProductPrices' => $listProductPrices)));
                    }
                    else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
                }
                else{
                    $listProductPrices = $this->Mproductprices->getByProductId($productId);
                    $product['ProductChildId'] = 0;
                    $productRetVal = array();
                    if($product['ProductKindId'] == 1){
                        if($isGetPrice == 1) $product['PriceCapital'] = $this->Mpricechanges->getPrice($productId, 0, $product['Price']);
                        $productRetVal[] = $product;
                    }
                    elseif($product['ProductKindId'] == 2){
                        $listProductChilds = $this->Mproductchilds->getByProductId($productId);
                        foreach($listProductChilds as $pc){
                            $pc['ProductKindId'] = $product['ProductKindId'];
                            $pc['ProductChildName'] = $pc['ProductName'];
                            $pc['ProductName'] = $product['ProductName'].' ('.$pc['ProductName'].')';
                            if(empty($pc['ProductImage'])) $pc['ProductImage'] = NO_IMAGE;
                            $pc['ProductUnitName'] = $productUnitName;
                            if($isGetPrice == 1) $pc['PriceCapital'] = $this->Mpricechanges->getPrice($productId, $pc['ProductChildId'], $pc['Price']);
                            $productRetVal[] = $pc;
                        }
                    }
                    else{
                        $product['ProductName'] .= ' (Combo)';
                        if($isGetPrice == 1) $product['PriceCapital'] = $product['Price'];
                        $productRetVal[] = $product;
                    }
                    echo json_encode(array('code' => 1, 'data' => array('Products' => $productRetVal, 'ProductPrices' => $listProductPrices)));
                }
            }
            else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getProductChildCombo(){
        $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        if($productId > 0){
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $listProductChilds = $this->Mproductchilds->getByProductId($productId);
            if(!empty($listProductChilds)){
                $data = array();
                $products = array();
                $productChilds = array();
                foreach($listProductChilds as $pc){
                    if($pc['ProductPartId'] > 0) {
                        if(!isset($products[$pc['ProductPartId']])) $products[$pc['ProductPartId']] = $this->Mproducts->get($pc['ProductPartId'], true, '', 'ProductName, ProductImage, Price, BarCode');
                        $productName = $products[$pc['ProductPartId']]['ProductName'];
                        $productImage = $products[$pc['ProductPartId']]['ProductImage'];
                        $price = $products[$pc['ProductPartId']]['Price'];
                        $barCode = $products[$pc['ProductPartId']]['BarCode'];
                        if($pc['ProductPartChildId'] > 0) {
                            if(!isset($productChilds[$pc['ProductPartChildId']])) $productChilds[$pc['ProductPartChildId']] = $this->Mproductchilds->get($pc['ProductPartChildId'], true, '', 'ProductName, ProductImage, Price, BarCode');
                            $productName .= ' (' . $productChilds[$pc['ProductPartChildId']]['ProductName'] .')';
                            $productImage = $productChilds[$pc['ProductPartChildId']]['ProductImage'];
                            $price = $productChilds[$pc['ProductPartChildId']]['Price'];
                            $barCode = $productChilds[$pc['ProductPartChildId']]['BarCode'];
                        }
                        $data[] = array(
                            'ProductName' => $productName,
                            'ProductImage' => empty($productImage) ? NO_IMAGE : $productImage,
                            'Price' => $price,
                            'BarCode' => $barCode,
                            'Quantity' => $pc['Quantity'],
                            'GuaranteeMonth' => $pc['GuaranteeMonth'],
                            'VATStatusId' => $pc['VATStatusId']
                        );
                    }
                }
                if(!empty($data)) echo json_encode(array('code' => 1, 'data' => $data));
                else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getList(){
        $this->checkUserLogin(true);
        $pageId = trim($this->input->post('PageId'));
        $limit = trim($this->input->post('Limit'));
        if ($pageId > 0 && $limit > 0) {
            $postData = $this->arrayFromPost(array('SearchText', 'CategoryId'));
            $postData['ProductStatusId'] = STATUS_ACTIVED;
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $data = array();
            $listProducts = $this->Mproducts->search($postData, $limit, $pageId);
            foreach ($listProducts as $p) {
                if ($p['ProductKindId'] == 2) $listProductChilds = $this->Mproductchilds->getByProductId($p['ProductId']);
                else $listProductChilds = array();
                $p['Childs'] = $listProductChilds;
                $data[] = $p;
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListLinkAff(){
        $this->checkUserLogin(true);
        $pageId = trim($this->input->post('PageId'));
        $limit = trim($this->input->post('Limit'));
        if ($pageId > 0 && $limit > 0) {
            $postData = $this->arrayFromPost(array('SearchText', 'CategoryId'));
            $postData['ProductStatusId'] = STATUS_ACTIVED;
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $data = array();
            $listProducts = $this->Mproducts->searchLinkAff($postData, $limit, $pageId);
            foreach ($listProducts as $p) {
                $data[] = $p;
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getQuantityAllStore(){
        $productId = trim($this->input->post('ProductId'));
        $productChildId = trim($this->input->post('ProductChildId'));
        if($productId > 0){
            $this->loadModel(array('Mstores', 'Mproductquantity'));
            $listStores = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $listProductQuantity = $this->Mproductquantity->getAllStore($productId, $productChildId);
            $storeIds = array();
            $data = array();
            foreach($listProductQuantity as $pq){
                $pq['StoreName'] = $this->Mconstants->getObjectValue($listStores, 'StoreId', $pq['StoreId'], 'StoreName');
                $data[] = $pq;
                $storeIds[] = $pq['StoreId'];
            }
            if(count($data) < count($listStores)){
                foreach($listStores as $s){
                    if(!in_array($s['StoreId'], $storeIds)){
                        $data[] = array(
                            'StoreId' => $s['StoreId'],
                            'StoreName' => $s['StoreName'],
                            'Quantity' => 0
                        );
                        $storeIds[] = $s['StoreId'];
                    }
                }
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function importFromPartner(){
        $this->checkUserLogin(true);
        $partnerId = $this->input->post('PartnerId');
        if($partnerId > 0){
            $json = curlCrawl('https://ricky.vn/api/product/getListForAffiliate', 'UserName=hoanmuada&UserPass=123456789');
            if(!empty($json)){
                $json = @json_decode($json, true);
                if($json['code'] == 1){
                    $this->load->model('Mproducts');
                    $productIds = $this->Mproducts->getListFieldValue(array('ProductStatusId' => STATUS_ACTIVED, 'PartnerId' => $partnerId), 'ProductId');
                    $productInserts = array();
                    foreach($json['data'] as $p) {
                        $p['ProductPartnerId'] = $p['ProductId'];
                        if (!in_array($p['ProductPartnerId'], $productIds)) {
                            $p['VideoUrls'] = @json_encode($p['VideoUrls']);
                            $p['PartnerId'] = $partnerId;
                            unset($p['ProductId']);
                            if ($p['ProductKindId'] == 1) {
                                unset($p['Childs']);
                                $productInserts[] = $p;
                            }
                            else {
                                $childs = $p['Childs'];
                                unset($p['Childs']);
                                $productId = $this->Mproducts->save($p);
                                if ($productId > 0) {
                                    $productChilds = array();
                                    foreach($childs as $pc) {
                                        $pc['ProductId'] = $productId;
                                        unset($pc['ProductChildId']);
                                        $productChilds[] = $pc;
                                    }
                                    if (!empty($productChilds)) $this->db->insert_batch('productchilds', $productChilds);
                                }
                            }
                            $productIds[] = $p['ProductPartnerId'];
                        }
                    }
                    if(!empty($productInserts)) $this->db->insert_batch('products', $productInserts);
                    echo json_encode(array('code' => 1, 'message' => "Lấy sản phẩm từ RICKY thành công"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    //mobile
    public function getInfo(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        if(!empty($barCode)){
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $flag = false;
            $productChild = $this->Mproductchilds->getBy(array('BarCode' => $barCode, 'StatusId' => STATUS_ACTIVED), true);
            if($productChild){
                $flag = true;
                $product = $this->Mproducts->get($productChild['ProductId']);
                $data = array(
                    'ProductName' => $product['ProductName'].' ('.$productChild['ProductName'].')',
                    'BarCode' => $barCode,
                    'Quantity' => $productChild['Quantity'],
                    'message' => 'Lấy thông tin sản phẩm thành công'
                );
                echo json_encode(array('code' => 1, 'message' => "Lấy thông tin sản phẩm thành công", 'data' => $data));
            }
            else{
                $product = $this->Mproducts->getBy(array('BarCode' => $barCode), true);
                if($product){
                    $flag = true;
                    $data = array(
                        'ProductName' => $product['ProductName'],
                        'BarCode' => $barCode,
                        'Quantity' => $product['Quantity'],
                        'message' => 'Lấy thông tin sản phẩm thành công'
                    );
                    echo json_encode(array('code' => 1, 'message' => "Lấy thông tin sản phẩm thành công", 'data' => $data));
                }
            }
            if(!$flag) echo json_encode(array('code' => 0, 'message' => "Sản phẩm không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function warehouse(){ // Nhap kho - Kiem kho
        header('Content-Type: application/json');
        $userId = trim($this->input->post('UserId'));
        $scanName = trim($this->input->post('Name'));
        $scanTypeId = trim($this->input->post('ScanTypeId'));
        $storeId = trim($this->input->post('StoreId'));
        $createdDate = trim($this->input->post('CreatedDate'));
        $products = json_decode(trim($this->input->post('Products')), true);
        if($userId > 0 && $storeId > 0 && !empty($scanName) && $scanTypeId > 0 && !empty($products)){
            $crDateTime = getCurentDateTime();
            if(!empty($createdDate)) $createdDate = ddMMyyyyToDate($createdDate, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
            else $createdDate = $crDateTime;
            $postData = array(
                'ScanName' => $scanName,
                'ScanTypeId' => $scanTypeId,
                'ItemId' => 0,
                'StoreId' => $storeId,
                'ScanDateTime' => $createdDate,
                'CrUserId' => $userId,
                'CrDateTime' => getCurentDateTime()
            );
            $this->load->model('Mscanbarcodes');
            $scanBarCodeId = $this->Mscanbarcodes->update($postData, 0, $products);
            if($scanBarCodeId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật dữ liệu thành công"));
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function genBarCodePackage(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        $packageCount = trim($this->input->post('PackageCount'));
        if(!empty($barCode) && $packageCount > 0){
            $packageCodes = array();
            if($packageCount == 1) $packageCodes[] = $barCode;
            else{
                for($i = 1; $i <= $packageCount; $i++) $packageCodes[] = $barCode.'_'.$i;
            }
            echo json_encode(array('code' => 1, 'message' => "Sinh mã kiện thành công", 'data' => array('BarCode' => $barCode, 'PackageCodes' => implode(',', $packageCodes))));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter($searchTypeId = 0){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $postData = $this->arrayFromGet(array('InventoryStatusId', 'StoreId'));
        $postData['SearchTypeId'] = $searchTypeId;
        $this->load->model('Mproducts');
        $data1 = $this->Mproducts->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}