<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends MY_Controller{

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransactionTypeId', 'TransactionStatusId', 'MoneySourceId', 'MoneyPhoneId', 'VerifyLevelId', 'StoreId', 'TransactionReasonId', 'CustomerId', 'BankId', 'PaidCost', 'OrderId', 'Comment', 'PrintStatusId', 'HasDebt', 'DebtComment', 'TransportId', 'FundId'));
        $postData['PaidCost'] = replacePrice($postData['PaidCost']);
        if($postData['PaidCost'] > 0 && $postData['TransactionTypeId'] > 0 && $postData['TransactionStatusId'] > 0 && $postData['VerifyLevelId'] > 0){
            $crDateTime = getCurentDateTime();
            $transactionTypeName = $this->Mconstants->transactionTypes[$postData['TransactionTypeId']];
            $actionLogs = array(
                'ItemTypeId' => $postData['TransactionTypeId'] == 1 ? 17 : 18,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $canUpdate = true;
            $this->load->model('Mtransactions');
            $transactionId = $this->input->post('TransactionId');
            if($transactionId > 0){
                $transactionStatusIdOld = $this->Mtransactions->getFieldValue(array('TransactionId' => $transactionId), 'TransactionStatusId', 0);
                if($transactionStatusIdOld == 1){
                    $actionLogs['ActionTypeId'] = 2;
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $comment = $user['FullName'] . ': ';
                    if($postData['VerifyLevelId'] == 1) $comment .= 'Cập nhật ';
                    elseif($postData['VerifyLevelId'] == 2) $comment .= 'Xác thực ';
                    elseif($postData['VerifyLevelId'] == 3) $comment .= 'Duyệt ';
                    elseif($postData['VerifyLevelId'] == 4) $comment .= 'Yêu cầu check lại ';
                    elseif($postData['VerifyLevelId'] == 6) $comment .= 'Không duyệt ';
                    $actionLogs['Comment'] = $comment.$transactionTypeName;
                }
                else $canUpdate = false;
            }
            else{
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLogs['ActionTypeId'] = 1;
                $comment = $user['FullName'] . ': Thêm mới ';
                if($postData['VerifyLevelId'] == 2) $comment .= ' và Xác thực ';
                elseif($postData['VerifyLevelId'] == 3) $comment .= ' và Duyệt ';
                $actionLogs['Comment'] = $comment.$transactionTypeName;
            }
            if($canUpdate) {
                $tagNames = json_decode(trim($this->input->post('TagNames')), true);
                $transactionId = $this->Mtransactions->update($postData, $transactionId, $tagNames, $actionLogs);
                if ($transactionId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu {$transactionTypeName} thành công", 'data' => $transactionId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function cancel(){
        $user = $this->checkUserLogin(true);
        $transactionId = $this->input->post('TransactionId');
        if($transactionId > 0){
            $this->load->model('Mtransactions');
            $transactionStatusIdOld = $this->Mtransactions->getFieldValue(array('TransactionId' => $transactionId), 'TransactionStatusId', 0);
            if($transactionStatusIdOld == 1){
                $crDateTime = getCurentDateTime();
                $postData = array('TransactionStatusId' => 3, 'VerifyLevelId' => 6, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
                $transactionTypeId = $this->input->post('TransactionTypeId');
                $actionLogs = array(
                    'ItemId' => $transactionId,
                    'ItemTypeId' => $transactionTypeId == 1 ? 18 : 17,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ': Hủy phiếu '.$this->Mconstants->transactionTypes[$transactionTypeId],
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $flag = $this->Mtransactions->updateField($postData, $transactionId, $actionLogs);
                if($flag) echo json_encode(array('code' => 1, 'message' => "Hủy phiếu thành công"));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateVerifyLevel(){
        $user = $this->checkUserLogin(true);
        $transactionId = $this->input->post('TransactionId');
        $verifyLevelId = $this->input->post('VerifyLevelId');
        if($transactionId > 0 && in_array($verifyLevelId, array(3, 4, 5, 6))){
            $this->load->model('Mtransactions');
            $transaction = $this->Mtransactions->get($transactionId);
            if($transaction && $transaction['TransactionStatusId'] > 0){
                $transactionTypeName = $transaction['TransactionTypeId'] == 1 ? 'thu' : 'chi';
                $transaction['CrUserId'] = $user['UserId'];
                $verifyLevelIdOld = $transaction['VerifyLevelId'];
                if($this->Mactions->checkAccessFromDb('transaction/editLevel_2', $user['UserId'])){ //QL
                    if(in_array($verifyLevelIdOld, array(1, 2)) && $verifyLevelId == 3){
                        $transaction['Comment'] = $user['FullName'].' đã duyệt phiếu '.$transactionTypeName;
                        $flag = $this->Mtransactions->updateVerifyLevel($transactionId, $verifyLevelId, $transaction);
                        if($flag) echo json_encode(array('code' => 1, 'message' => "Duyệt phiếu {$transactionTypeName} thành công"));
                        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    elseif(in_array($verifyLevelIdOld, array(2, 5)) && in_array($verifyLevelId, array(4, 6))){
                        $comment = $user['FullName'].' '.($verifyLevelId == 4 ? 'gửi nhân viên check lại phiếu' : 'hủy phiểu');
                        $message = $verifyLevelId == 4 ? 'Gửi nhân viên check lại phiếu thành công' : 'Hủy phiểu thành công';
                        $transaction['Comment'] = $comment;
                        $flag = $this->Mtransactions->updateVerifyLevel($transactionId, $verifyLevelId, $transaction);
                        if($flag) echo json_encode(array('code' => 1, 'message' => $message));
                        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                elseif($this->Mactions->checkAccessFromDb('transaction/editLevel_1', $user['UserId'])){ //NV
                    if($verifyLevelIdOld == 4 && in_array($verifyLevelId, array(5, 6))){
                        $comment = $user['FullName'].' '.($verifyLevelId == 5 ? 'gửi quản lý duyệt lại phiếu' : 'hủy phiểu');
                        $message = $verifyLevelId == 5 ? 'Gửi quản lý duyệt lại phiếu thành công' : 'Hủy phiểu thành công';
                        $transaction['Comment'] = $comment;
                        $flag = $this->Mtransactions->updateVerifyLevel($transactionId, $verifyLevelId, $transaction);
                        if($flag) echo json_encode(array('code' => 1, 'message' => $message));
                        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter($transactionTypeId = 0){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if(!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if(!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->load->model('Mtransactions');

        $data1 = $this->Mtransactions->searchByFilter($searchText, $itemFilters, $limit, $page, array('TransactionTypeId' => $transactionTypeId, 'CustomerId' => $this->input->post('CustomerId')));
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function searchMixByFilter(){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if(!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if(!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->load->model('Mtransactions');

        $data1 = $this->Mtransactions->searchMixByFilter($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}