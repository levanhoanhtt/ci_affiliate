<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller{

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OrderCode','CustomerId','PartnerId', 'OrderChanelId', 'OrderStatusId', 'DiscountCost', 'DiscountStatusId', 'OrderTypeId', 'TotalCost'));
        if($postData['CustomerId'] > 0 && $postData['OrderStatusId'] > 0){
            $postData['TotalCost'] = replacePrice($postData['TotalCost']);
            $this->loadModel(array('Mcustomers', 'Morders', 'Mproductquantity'));
            $orderId = $this->input->post('OrderId');
            // $isPOS = $postData['DeliveryTypeId'] == 1 && $postData['OrderStatusId'] == 6;
            $crDateTime = getCurentDateTime();
            $actionLog = array(
                'ItemTypeId' => 6,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $products = json_decode(trim($this->input->post('Products')), true);
            $services = json_decode(trim($this->input->post('OrderServices')), true);
            // $POSData = array();
            // if($isPOS){
            //     $actionLog['Comment'] = $user['FullName'] . ': Chốt đơn hàng';
            //     $debitCost = 0;
            //     $inventoryData = array();
            //     foreach($services as $s){
            //         if($s['OtherServiceId'] == DEBIT_OTHER_TYPE_ID){
            //             $debitCost = replacePrice($s['ServiceCost']);
            //             break;
            //         }
            //     }
            //     $orderCode = $this->Morders->genOrderCode($orderId);
            //     foreach($products as $op){
            //         $inventoryData[] = array(
            //             'ProductId' => $op['ProductId'],
            //             'ProductChildId' => $op['ProductChildId'],
            //             'OldQuantity' => $this->Mproductquantity->getQuantity($op['ProductId'], $op['ProductChildId'], $postData['StoreId']),
            //             'Quantity' => $op['Quantity'],
            //             'InventoryTypeId' => 2,
            //             'StoreId' => $postData['StoreId'],
            //             'StatusId' => STATUS_ACTIVED,
            //             'Comment' => 'Trừ số lượng từ đơn hàng '.$orderCode,
            //             'CrUserId' => $user['UserId'],
            //             'CrDateTime' => $crDateTime,
            //             'UpdateUserId' => $user['UserId'],
            //             'UpdateDateTime' => $crDateTime
            //         );
            //     }
            //     $POSData = array('DebitCost' => $debitCost, 'InventoryData' => $inventoryData, 'FullName' => $user['FullName']);
            //     $POSData['MoneySourceId'] = $this->input->post('MoneySourceId');
            //     $POSData['BankId'] = $this->input->post('BankId');
            //     $POSData['RealPaymentCost'] = replacePrice($this->input->post('RealPaymentCost'));
            // }
            if($orderId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 2;
                $actionLog['Comment'] = $user['FullName'] . ': Cập nhật đơn hàng';
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 1;
                // if($isPOS) $actionLog['Comment'] = $user['FullName'] . ': Thêm và chốt đơn hàng';
                // else 
                $actionLog['Comment'] = $user['FullName'] . ': Thêm mới đơn hàng';
            }
            $tagNames = json_decode(trim($this->input->post('TagNames')), true);
            $comments = json_decode(trim($this->input->post('Comments')), true);
            $promotion = array();
            if($postData['DiscountCost'] > 0) {
                $promotion = array(
                    'PromotionId' => $this->input->post('PromotionId'),
                    'PromotionCode' => $this->input->post('PromotionCode'),
                    'DiscountPercent' => $this->input->post('DiscountPercent'),
                    'DiscountCost' => $postData['DiscountCost'],
                    'StatusId' => STATUS_ACTIVED,
                    'Comment' => ($this->input->post('PromotionComment')),
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
            $remindData = $this->arrayFromPost(array('OwnCost', 'RemindDate', 'RemindComment'));
            $remindData['OwnCost'] = replacePrice($remindData['OwnCost']);
            if ($remindData['OwnCost'] > 0 && !empty($remindData['RemindDate'])){
                $remindData['RemindDate'] = ddMMyyyyToDate($remindData['RemindDate'], 'd/m/Y H:i', 'Y-m-d H:i');
                $remindData = array_merge($remindData, array(
                    'IsRepeat' => 1,
                    'RepeatDay' => 0,
                    'RepeatHour' => 0,
                    'RemindStatusId' => 1,
                    'RemindTypeId' => 2,
                    'UserId' => $user['UserId'],
                    'PartId' => 0,
                    'CustomerId' => $postData['CustomerId'],
                    'CustomerConsultId' => 0,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                ));
            }
            else $remindData = array();
            // var_dump($remindData);
            $orderId = $this->Morders->update($postData, $orderId, $products, $tagNames, $services, $promotion, $comments, $remindData, $actionLog); //, $POSData
            if ($orderId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật đơn hàng thành công", 'data' => $orderId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getProductByOrder(){
        $this->checkUserLogin(true);
        $orderId = $this->input->post('OrderId');
        if ($orderId > 0) {
            $this->loadModel(array('Morderproducts', 'Mproductunits', 'Mproducts', 'Mproductchilds'));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $listOrderProducts = $this->Morderproducts->getBy(array('OrderId' => $orderId));
            $listProductUnits = $this->Mproductunits->getBy($whereStatus);
            $products = array();
            $productChilds = array();
            //$productPrices = array();
            $totalWeight = 0;
            $arrDataProduct = array();
            foreach($listOrderProducts as $op){
                if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, ProductKindId, Price, BarCode, Weight, ProductUnitId, GuaranteeMonth');
                $productName = $products[$op['ProductId']]['ProductName'];
                $productImage = $products[$op['ProductId']]['ProductImage'];
                $productKindId = $products[$op['ProductId']]['ProductKindId'];
                //$price = $products[$op['ProductId']]['Price'];
                $barCode = $products[$op['ProductId']]['BarCode'];
                $weight = $products[$op['ProductId']]['Weight'];
                $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
                $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                $productChildName = '';
                if($op['ProductChildId'] > 0){
                    if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, Price, BarCode, Weight, GuaranteeMonth');
                    //$productName .= '<br/>(' . $productChilds[$op['ProductChildId']]['ProductName'] .')';
                    $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                    $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                    //$price = $productChilds[$op['ProductChildId']]['Price'];
                    $barCode = $productChilds[$op['ProductChildId']]['BarCode'];
                    $weight = $productChilds[$op['ProductChildId']]['Weight'];
                    $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
                }
                $totalWeight += $weight * $op['Quantity'];
                if(empty($productImage)) $productImage = NO_IMAGE;
                $productChild = "";
                if(!empty($productChildName)) $productChild =  ' ('.$productChildName.')';
                elseif($productKindId == 3) $productChild = ' (Combo)';
                $arrDataProduct[] = array(
                    'ProductImage' => PRODUCT_PATH.$productImage,
                    'ProductName' => $productName.$productChild,
                    'ProductUnitName' => $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'),
                    'BarCode' => $barCode,
                    'GuaranteeMonth' => $guaranteeMonth.' tháng',
                    'Price' => priceFormat($op['Price']).' đ',
                    'Quantity' => priceFormat($op['Quantity']),
                    'Total' => priceFormat($op['Quantity'] * $op['Price']),

                );
            }
            echo json_encode(array('code' => 1, 'data' => $arrDataProduct));

        }
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OrderId', 'Comment'));
        if($postData['OrderId'] > 0 && !empty($postData['Comment'])){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mordercomments');
            $flag = $this->Mordercomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeVerifyStatusBatch(){
        $user = $this->checkUserLogin(true);
        $orderIds = json_decode(trim($this->input->post('OrderIds')), true);
        $verifyStatusId = $this->input->post('VerifyStatusId');
        if (!empty($orderIds) && $verifyStatusId >= 0) {
            $this->load->model('Morders');
            $flag = $this->Morders->changeVerifyStatusBatch($orderIds, $verifyStatusId, $user);
            if ($flag) echo json_encode(array('code' => 1, 'message' => 'Cập nhật trạng thái xác thực thành công', 'data' => $verifyStatusId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateField(){
        $user = $this->checkUserLogin(true);
        $orderId = $this->input->post('OrderId');
        $fieldName = trim($this->input->post('FieldName'));
        $fieldValue = trim($this->input->post('FieldValue'));
        if($orderId > 0 && !empty($fieldName) && !empty($fieldValue)){
            $label = '';
            $value = '';
            $comment = '';
            $message = '';
            $data = array();
            $crDateTime = getCurentDateTime();
            $postData = array($fieldName => $fieldValue, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
            $this->loadModel(array('Morders', 'Mactionlogs'));
            switch($fieldName){
                case 'OrderStatusId';
                    if($fieldValue > 0 && $fieldValue < 7) {
                        $orderStatusIdOld = $this->Morders->getFieldValue(array('OrderId' => $orderId), 'OrderStatusId', 0);
                        if ($orderStatusIdOld == 1 || $orderStatusIdOld == 5) {
                            $label = 'Trạng thái';
                            $value = $this->Mconstants->orderStatus[$fieldValue];
                            if($fieldValue == 3){
                                $postData['CancelReasonId'] = $this->input->post('CancelReasonId');
                                $postData['CancelComment'] = trim($this->input->post('CancelComment'));
                            }
                        }
                    }
                    break;
                case 'PendingStatusId':
                    if($fieldValue > 0){
                        $label = 'Trạng thái chờ xử lý';
                        $this->load->model('Mpendingstatus');
                        $value = $this->Mpendingstatus->getFieldValue(array('PendingStatusId' => $fieldValue), 'PendingStatusName');
                        $data['StatusName'] = $this->Mconstants->orderStatus[1].' ('.$value.')';
                    }
                    break;
                case 'StoreId':
                    if($fieldValue > 0){
                        $label = 'Cở sở';
                        $this->load->model('Mstores');
                        $value = $this->Mstores->getFieldValue(array('StoreId' => $fieldValue), 'StoreName');
                        $data['StoreName'] = $value;
                    }
                    break;
                case 'IsShare':
                    if($fieldValue > 0){
                        if($fieldValue == 2){
                            $comment = $user['FullName'] . " đưa đơn hàng lên sàn chung";
                            $postData['ShareUserId'] = $user['UserId'];
                            $postData['ShareDateTime'] = $crDateTime;
                            $postData['ShareComment'] = trim($this->input->post('ShareComment'));
                            $postData['StaffId'] = 0;
                            $message = 'Đưa đơn hàng lên sàn thành công';
                        }
                        else{
                            $comment = $user['FullName'] . " nhận xử lý đơn hàng";
                            $postData['StaffId'] = $user['UserId'];
                            $message = 'Lấy đơn hàng về thành công';
                        }
                        $label = 'Đưa lên sàn';
                    }
                    break;
                case 'OrderReasonId':
                    if($fieldValue > 0){
                        $label = 'Lý do biết đến mua hàng';
                        $this->load->model('Morderreasons');
                        $value = $this->Morderreasons->getFieldValue(array('OrderReasonId' => $fieldValue), 'OrderReasonName');
                    }
                    break;
                case 'TransportCost':
                    if($fieldValue >= 0){
                        $label = 'Phí vận chuyển';
                        $value = priceFormat($fieldValue).' VNĐ';
                        $postData['TransportTypeId'] = $this->input->post('TransportTypeId');
                        $postData['TransportReasonId'] = $this->input->post('TransportReasonId');
                    }
                    break;
                default: break;
            }
            if(!empty($label)){
                if(empty($comment)) $comment = $user['FullName'] . " cập nhật {$label} thành {$value}";
                $data['Comment'] = $comment;
                $actionLog = array(
                    'ItemId' => $orderId,
                    'ItemTypeId' => 6,
                    'ActionTypeId' => 2,
                    'Comment' => $comment,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $flag = $this->Morders->updateField($postData, $orderId, $actionLog);
                if ($flag) echo json_encode(array('code' => 1, 'message' => empty($message) ? "Cập nhật {$label} thành công" : $message, 'data' => $data));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function checkQuantity(){
        $this->checkUserLogin(true);
        $products = $this->input->post('Products');
        $countProduct = count($products);
        if($countProduct > 0){
            $productIds = array();
            foreach($products as $p){
                if(!in_array($p['ProductId'], $productIds)) $productIds[] = $p['ProductId'];
            }
            $storeId = $this->input->post('StoreId');
            $this->loadModel(array('Mproductquantity', 'Mstores'));
            $this->load->model('Mproductquantity');
            $listProductQuantity = $this->Mproductquantity->getListQuantity($productIds, $storeId);
            $listStores = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data = array();
            foreach($listStores as $s){
                $productStocks = array();
                $i = 0;
                foreach($products as $p){
                    $productTmp = array(
                        'ProductId' => $p['ProductId'],
                        'ProductChildId' => $p['ProductChildId'],
                        'Quantity' => intval($p['Quantity']),
                        'StockQuantity' => 0
                    );
                    foreach($listProductQuantity as $pq){
                        if($p['ProductId'] == $pq['ProductId'] && $p['ProductChildId'] == $pq['ProductChildId'] && $pq['StoreId'] == $s['StoreId']){
                            $productTmp['StockQuantity'] = $pq['Quantity'];
                            break;
                        }
                    }
                    $productStocks[] = $productTmp;
                    if($productTmp['StockQuantity'] > $productTmp['Quantity']) $i++;
                }
                $data[] = array(
                    'StoreId' => $s['StoreId'],
                    'StoreName' => $s['StoreName'],
                    'Products' => $productStocks,
                    'IsInStock' => $i == $countProduct
                );
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function revenue(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate', 'StoreId', 'ProductTypeId', 'DeliveryTypeId'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->load->model('Morders');
        $revenue = $this->Morders->revenue($postData);
        echo json_encode(array('code' => 1, 'data' => $revenue));
    }

    public function searchByOrderStatus(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate', 'OrderStatusId'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->load->model(array('Morders','Mtransports'));
        $listOrders = $this->Morders->searchByOrderStatus($postData);
        echo json_encode(array('code' => 1, 'data' => $listOrders));
    }

    public function searchDataEffctual(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $postData['CustomerId'] = $user['CustomerId'];
        $this->loadModel(array('Mlinklogs', 'Morders','Mlinkaffs'));
        $listOrders = $this->Mlinkaffs->searchDataEffctual($postData);
        $listLinklogs = $this->Mlinklogs->searchDataEffctual($postData);
        echo json_encode(array('code' => 1, 'data' => array('Orders' => $listOrders, 'linkLogs' => $listLinklogs)));
    }

    public function productSelling(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->loadModel(array('Morders', 'Mproducts', 'Mproductchilds', 'Mproductquantity'));
        $products = $this->Morders->productSelling($postData);
        echo json_encode(array('code' => 1, 'data' => $products));
    }

    public function searchByFilter($customerId = 0){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        if($customerId > 0) $postData = array('CustomerId' => $customerId);
        else $postData = array('CustomerId' => $this->input->post('CustomerId'));
        $this->loadModel(array('Morders', 'Mtransports'));
        $data1 = $this->Morders->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}