<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LinkGroup extends MY_Controller {
    public function getList() {
        $this->load->model('Mlinkgroups');

        $listGroups = $this->Mlinkgroups->getListPluck();

        echo json_encode(['errors' => false, 'listGroups'=>$listGroups]);
        return true;
    }

    public function update()
    {
        $user = $this->checkUserLogin(true);
        $products = json_decode(trim($this->input->post('products')), true);
        $data['LinkGroupName'] = $this->input->post('linkGroupName');
        $data['LinkGroupComment'] = $this->input->post('linkGroupCommnet');
        $data['ApproachTypeId'] = $this->input->post('approachTypeId');
        $data['StatusId'] = STATUS_ACTIVED;
        $linkGroupIdPost = $this->input->post('linkGroupId');
        if ($linkGroupIdPost) {
            $data['UpdateUserId'] = $user['UserId'];
            $data['UpdateDateTime'] = getCurentDateTime();
        } else {
            $data['CrUserId'] = $user['UserId'];
            $data['CrDateTime'] = getCurentDateTime();
        }
        $this->loadModel(array('Mlinkgroups', 'Mlinkgroupproducts'));

        $linkGroupId = $this->Mlinkgroups->update($products, $data, $linkGroupIdPost);

        if ($linkGroupId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật chiến dịch thành công", 'data' => $linkGroupId));
        else echo json_encode(array('code' => 0, 'message' => 'Có lỗi xảy ra trong quá trình thực hiện'));
    }
}