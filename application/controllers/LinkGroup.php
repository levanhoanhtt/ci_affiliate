<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LinkGroup extends MY_Controller{

    public function index($customerKindId = 0){
        //
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
                    'Tạo chiến dịch',
                    array(
                        'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css', 'css/order.css')),
                        'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js','vendor/plugins/jwerty/jwerty.js', 'js/choose_item_link.js', 'js/linkgroup_update.js'))
                    )
                );
        $data['canEdit'] = true;
        $this->loadModel(array('Mcategories'));
        $whereStatus = array('StatusId' => STATUS_ACTIVED);
        $data['listCategories'] = $this->Mcategories->getListByItemType(1);
        $data['listApproachTypeId'] = null;

        $this->load->view('linkgroup/add', $data);
    }

    public function edit($linkGroupId = 0){
        if ($linkGroupId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Chiến dịch',
                array(
                                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css', 'css/order.css')),
                                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js','vendor/plugins/jwerty/jwerty.js', 'js/choose_item_link.js', 'js/linkgroup_update.js'))
                )
            );
            $this->loadModel(array('Mlinkgroups','Mcategories','Mlinkgroupproducts','Mproductunits'));
            $linkGroup = $this->Mlinkgroups->get($linkGroupId);
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $data['listApproachTypeId'] = null;
            if ($linkGroup && $linkGroup['StatusId'] > 0) {
                $data['canEdit'] = true;
                $whereStatus = array('StatusId' => STATUS_ACTIVED);
                $data['linkGroupId'] = $linkGroupId;
                $data['linkGroup'] = $linkGroup;
                $data['listProduct'] = $this->Mlinkgroupproducts->getListProductByArrId($linkGroupId);
                $data['listProductUnits'] = $this->Mproductunits->getBy($whereStatus);

                $this->load->view('linkgroup/edit', $data);
            }
            else {
                $data['canEdit'] = false;
                $data['linkGroupId'] = 0;
                $data['txtError'] = "Không tìm thấy Chiến dịch";
                $this->load->view('linkgroup/edit', $data);
            }
        }
        else redirect('linkgroup');
    }
}