<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Khuyến mại',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/promotion_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'promotion')) {
            $this->loadModel(array('Mfilters', 'Mprovinces', 'Mcustomergroups', 'Mcategories', 'Mpromotions', 'Mcustomers', 'Mproducts'));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $data['listFilters'] = $this->Mfilters->getList(12);
            $data['listPromotions'] = $this->Mpromotions->getBy(array('PromotionStatusId >' => 0));
            $this->load->view('promotion/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tạo Khuyến mại',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/choose_item.js', 'js/promotion_update.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'promotion')) {
            $this->loadModel(array('Mprovinces', 'Mcustomergroups', 'Mcategories'));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $this->load->view('promotion/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('PromotionName', 'PromotionTypeId', 'PromotionStatusId', 'ReduceTypeId', 'BeginDate', 'EndDate', 'IsSharePromotion', 'IsUnLimit', 'NumberUse', 'ReduceNumber', 'MinimumCost', 'ProvinceId', 'PromotionItemId', 'PromotionItemTypeId', 'ProductNumber', 'DiscountTypeId'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
        $promotionId = $this->input->post('PromotionId');
        if($promotionId > 0){
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = getCurentDateTime();
        }
        else{
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
        }
        $this->load->model('Mpromotions');
        $promotionId = $this->Mpromotions->save($postData, $promotionId, array('EndDate'));
        if($promotionId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Khuyến mại thành công", 'data' => $promotionId));
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
    
    public function changeStatusBatch(){
        $user = $this->checkUserLogin(true);
        $promotionIds = json_decode(trim($this->input->post('ItemIds')), true);
        $statusId = $this->input->post('StatusId');
        if(!empty($promotionIds) && $statusId >= 0){
            $this->load->model('Mpromotions');
            $flag = $this->Mpromotions->changeStatusBatch($promotionIds, $statusId, $user['UserId']);
            if($flag) {
                $msg = 'Xóa mã khuyến mại thành công';
                $statusName = '';
                if ($statusId > 0){
                    $msg = 'Thay đổi trạng thái thành công';
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->promotionStatus[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $msg, 'data' => $statusName));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    private function calcReduceCost($reduceNumber, $reduceTypeId, $totalCost){
        $retVal = 0;
        if($reduceTypeId == 1) $retVal = $reduceNumber;
        elseif($reduceTypeId == 2) $retVal = round($totalCost * $reduceNumber / 100);
        return $retVal;
    }

    private function getReduceCost($promotion, $products, $customerId, $customerGroupId, $totalCost, $transportCost, $provinceId, &$errMsg){
        $retVal = 0;
        $reduceTypeId = $promotion['ReduceTypeId'];
        $promotionItemId = $promotion['PromotionItemId'];
        $promotionItemTypeId = $promotion['PromotionItemTypeId'];
        $discountTypeId = $promotion['DiscountTypeId'];
        if($reduceTypeId == 1 || $reduceTypeId == 2){
            if($promotionItemTypeId == 0) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
            elseif($promotionItemTypeId == 6){
                if($totalCost >= $promotion['MinimumCost']) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                else $errMsg = 'Khuyến mại chỉ áp dụng với trị giá đơn hàng từ ' . priceFormat($promotion['MinimumCost']) . 'VNĐ';
            }
            elseif($promotionItemTypeId == 1){
                if($promotionItemId > 0){
                    foreach($products as $p){
                        $cateIds = $this->Mcategoryitems->getCateIds($p['ProductId'], 3);
                        if(in_array($promotionItemId, $cateIds)){
                            if($discountTypeId == 1){
                                $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                                break;
                            }
                            elseif($discountTypeId == 2) $retVal += $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                            else break;
                        }
                    }
                }
                else{
                    if($discountTypeId == 1) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                    elseif($discountTypeId == 2) $retVal = count($products) * $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                }
            }
            elseif($promotionItemTypeId == 3){
                if($promotionItemId > 0){
                    foreach($products as $p){
                        if($p['ProductChildId'] == 0 && $p['ProductId'] == $promotionItemId){
                            if($discountTypeId == 1){
                                $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                                break;
                            }
                            elseif($discountTypeId == 2) $retVal += $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                            else break;
                        }
                    }
                }
                else{
                    if($discountTypeId == 1) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                    elseif($discountTypeId == 2) $retVal = count($products) * $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                }
            }
            elseif($promotionItemTypeId == 13){
                if($promotionItemId > 0){
                    foreach($products as $p){
                        if($p['ProductChildId'] == $promotionItemId){
                            if($discountTypeId == 1){
                                $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                                break;
                            }
                            elseif($discountTypeId == 2) $retVal += $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                            else break;
                        }
                    }
                }
                else{
                    if($discountTypeId == 1) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                    elseif($discountTypeId == 2) $retVal = count($products) * $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
                }
            }
            elseif($promotionItemTypeId == 11){
                if($promotionItemId == $customerGroupId || $promotionItemId == 0) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
            }
            elseif($promotionItemTypeId == 5){
                if($promotionItemId == $customerId || $promotionItemId == 0) $retVal = $this->calcReduceCost($promotion['ReduceNumber'], $reduceTypeId, $totalCost);
            }
        }
        elseif($promotion['ReduceTypeId'] == 3){
            if($promotion['ProvinceId'] == 0){
                if($promotion['ReduceNumber'] <= $transportCost) $retVal = $transportCost;
                else $errMsg = 'Khuyến mại chỉ áp dụng với mức phí vận chuyển nhỏ hơn hoặc bằng ' . priceFormat($promotion['ReduceNumber']) . 'VNĐ';
            }
            else{
                if($promotion['ProvinceId'] == $provinceId) $retVal = $transportCost;
                else $errMsg = 'Khuyến mại chỉ áp dụng với vận chuyển ở tỉnh ' . $this->Mprovinces->getFieldValue(array('ProvinceId' => $promotion['ProvinceId']), 'ProvinceName');
            }
        }
        return $retVal;
    }

    public function checkPromotion(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('PromotionCode', 'CustomerId', 'CustomerGroupId', 'TotalPrice', 'TransportCost', 'ProvinceId'));
        $postData['TotalPrice'] = replacePrice($postData['TotalPrice']);
        $products = $this->input->post('Products');
        if(!empty($postData['PromotionCode']) && $postData['TotalPrice'] > 0 && !empty($products)){
            $this->loadModel(array('Mcategoryitems', 'Mpromotions', 'Mprovinces', 'Morderpromotions', 'Morders', 'Mactionlogs'));
            $promotion = $this->Mpromotions->getBy(array('PromotionName' => $postData['PromotionCode']), true);
            if($promotion && $promotion['PromotionStatusId'] == 2){
                $flag = false;
                if($promotion['NumberUse'] > 0 || $promotion['IsUnLimit'] == 2) {
                    $flag = true;
                    if (!empty($promotion['EndDate'])) {
                        if (strtotime(getCurentDateTime()) > strtotime($promotion['EndDate'])) $flag = false;
                    }
                }
                if($flag){
                    $postData['TransportCost'] = replacePrice($postData['TransportCost']);
                    $errMsg = '';
                    $reduceCost = $this->getReduceCost($promotion, $products, $postData['CustomerId'], $postData['CustomerGroupId'], $postData['TotalPrice'], $postData['TransportCost'], $postData['ProvinceId'], $errMsg);
                    if($reduceCost > 0){
                        if($reduceCost > $postData['TotalPrice']) echo json_encode(array('code' => -1, 'message' => "Số tiền khuyến mại không được lớn hơn tổng tiền hàng"));
                        else {
                            $data = array(
                                'PromotionId' => $promotion['PromotionId'],
                                'DiscountPercent' => $promotion['ReduceTypeId'] == 2 ? $promotion['ReduceNumber'] : 0,
                                'DiscountCost' => $reduceCost,
                            );
                            $orderId = $this->input->post('OrderId');
                            if($orderId > 0){
                                $data['OrderId'] = $orderId;
                                $data['PromotionCode'] = $postData['PromotionCode'];
                                $data['StatusId'] = 1;
                                $data['Comment'] = trim($this->input->post('Comment'));
                                $data['CrUserId'] = $user['UserId'];
                                $data['CrDateTime'] = getCurentDateTime();
                                $orderPromotionId = $this->Morderpromotions->getFieldValue(array('OrderId' => $orderId, 'StatusId' => 1), 'OrderPromotionId', 0);
                                $orderActionLog = array(
                                    'ItemId' => $orderId,
                                    'ItemTypeId' => 6,
                                    'ActionTypeId' => 2,
                                    'Comment' => $user['FullName'] . " cập nhật Khuyến mại thành ".priceFormat($postData['DiscountCost']).' VNĐ',
                                    'CrUserId' => $user['UserId'],
                                    'CrDateTime' => $data['CrDateTime']
                                );
                                $this->Morderpromotions->update($data, $orderPromotionId, $orderActionLog);
                                $data['Comment'] = $orderActionLog['Comment'];
                            }
                            echo json_encode(array('code' => 1, 'data' => $data));
                        }
                    }
                    else{
                        if(empty($errMsg)) $errMsg = 'Mã khuyến mại không còn hoạt động';
                        echo json_encode(array('code' => -1, 'message' => $errMsg));
                    }
                }
                else echo json_encode(array('code' => -1, 'message' => "Mã khuyến mại không còn hoạt động"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy mã khuyến mại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateOrderPromotion(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OrderId', 'DiscountPercent', 'DiscountCost', 'Comment'));
        $postData['DiscountCost'] = replacePrice($postData['DiscountCost']);
        if($postData['OrderId'] > 0 && $postData['DiscountCost'] > 0){
            $postData['PromotionId'] = 0;
            $postData['PromotionCode'] = '';
            $postData['StatusId'] = 1;
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->loadModel(array('Morderpromotions', 'Morders', 'Mactionlogs'));
            $orderPromotionId = $this->Morderpromotions->getFieldValue(array('OrderId' => $postData['OrderId'], 'StatusId' => 1), 'OrderPromotionId', 0);
            $orderActionLog = array(
                'ItemId' => $postData['OrderId'],
                'ItemTypeId' => 6,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . " cập nhật Khuyến mại thành ".priceFormat($postData['DiscountCost']).' VNĐ',
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $postData['CrDateTime']
            );
            $flag = $this->Morderpromotions->update($postData, $orderPromotionId, $orderActionLog);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật khuyến mại thành công", 'data' => array('Comment' => $orderActionLog['Comment'])));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
    
    public function searchByFilter(){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->load->model(array('Mprovinces', 'Mcustomergroups', 'Mcategories', 'Mpromotions'));
        $data1 = $this->Mpromotions->searchByFilter($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}