<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Remind extends MY_Controller {

    public function index($remindTypeId = 0, $remindStatusId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user, 'Danh sách nhắc nhở');
        if($this->Mactions->checkAccess($data['listActions'], 'remind')){
            $this->loadModel(array('Mparts', 'Mreminds', 'Mcustomers'));
            if(!is_numeric($remindTypeId)) $remindStatusId = 1;
            if(!in_array($remindTypeId, array(1, 2, 3))) $remindTypeId = 1;
            if($remindTypeId == 2) $data['title'] = 'Danh sách phiếu chờ thanh toán';
            elseif($remindTypeId == 3) $data['title'] = 'Danh sách nhắc nhở tư vấn lại';
            $data['remindTypeId'] = $remindTypeId;
            if(!is_numeric($remindStatusId)) $remindStatusId = 0;
            if(!in_array($remindStatusId, array(1, 4, 5, 6, 7))) $remindStatusId = 0;
            $data['remindStatusId'] = $remindStatusId;
            $postData = array('RemindStatusId' => $remindStatusId, 'RemindTypeId' => $remindTypeId, 'UserId' => $user['UserId']);
            if($remindStatusId == 7){
                $postData['RemindStatusId'] = 1;
                $postData['OutOfDate'] = 2;
            }
            $rowCount = $this->Mreminds->getCount($postData);
            $data['listReminds'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listReminds'] = $this->Mreminds->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            if($remindTypeId == 2) $this->load->view('remind/order', $data);
            else $this->load->view('remind/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới nhắc nhở',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'js/remind_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'remind')) {
            $this->load->model('Mparts');
            $data['listParts'] = $this->Mparts->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
            $this->load->view('remind/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($remindId = 0){
        if($remindId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật nhắc nhở',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'js/remind_update.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'remind')) {
                $this->loadModel(array('Mreminds', 'Mparts', 'Mremindcomments', 'Mactionlogs'));
                $remind = $this->Mreminds->get($remindId);
                if($remind){
                    $data['remindId'] = $remindId;
                    $data['remind'] = $remind;
                    $data['canEdit'] = $remind['RemindStatusId'] != 4 && $remind['RemindStatusId'] != 5;
                    $objName = '';
                    if($remind['UserId'] > 0) $objName = $this->Musers->getFieldValue(array('UserId' => $remind['UserId']), 'FullName');
                    elseif($remind['PartId'] > 0) $objName = $this->Musers->getFieldValue(array('PartId' => $remind['PartId']), 'PartName');
                    $data['objName'] = $objName;
                    $data['listRemindComments'] = $this->Mremindcomments->getListByRemindId($remindId);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($remindId, 23);
                }
                else{
                    $data['canEdit'] = false;
                    $data['remindId'] = 0;
                    $data['txtError'] = "Không tìm thấy nhắc nhở";
                }
                $this->load->view('remind/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('remind');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('RemindTitle', 'RemindDate', 'IsRepeat', 'RepeatDay', 'RepeatHour', 'RemindStatusId', 'RemindTypeId', 'UserId', 'PartId', 'OrderId', 'CustomerId', 'OwnCost', 'CustomerConsultId', 'TimeProcessed'));
        if(!empty($postData['RemindTitle']) && !empty($postData['RemindDate'])){
            $postData['RemindDate'] = ddMMyyyyToDate($postData['RemindDate'], 'd/m/Y H:i', 'Y-m-d H:i');
            $crDateTime = getCurentDateTime();
            $isOutOfDate = strtotime($postData['RemindDate']) <= strtotime($crDateTime);
            if($postData['RemindStatusId'] == 1 && $isOutOfDate) echo json_encode(array('code' => -1, 'message' => "Thời điểm cần xử lý không hợp lệ"));
            else {
                $postData['OwnCost'] = replacePrice($postData['OwnCost']);
                $postData['OutOfDate'] = $isOutOfDate ? 2 : 1;
                $remindId = $this->input->post('RemindId');
                $actionLogs = array(
                    'ItemTypeId' => 23,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                if ($remindId > 0) {
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 2;
                    $actionLogs['Comment'] = $user['FullName'] . ': Cập nhật nhắc nhở';
                }
                else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 1;
                    $actionLogs['Comment'] = $user['FullName'] . ': Thêm mớí nhắc nhở';
                }
                $comments = json_decode(trim($this->input->post('Comments')), true);
                $this->load->model('Mreminds');
                $remindId = $this->Mreminds->update($postData, $remindId, $comments, $actionLogs);
                if($remindId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật nhắc nhở thành công", 'data' => $remindId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertOwnOrder(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OrderId', 'CustomerId', 'OwnCost', 'RemindDate'));
        $postData['OwnCost'] = replacePrice($postData['OwnCost']);
        if($postData['OrderId'] > 0 && $postData['OwnCost'] > 0 && !empty($postData['RemindDate'])){
            $this->loadModel(array('Morders', 'Mreminds'));
            $postData['RemindDate'] = ddMMyyyyToDate($postData['RemindDate']);
            $postData = array_merge($postData, array(
                'RemindTitle' => 'Nhắc khách thanh toán '.priceFormat($postData['OwnCost']).' VNĐ tiền đơn hàng '.$this->Morders->genOrderCode($postData['OrderId']),
                'IsRepeat' => 1,
                'RepeatDay' => 0,
                'RepeatHour' => 0,
                'RemindStatusId' => 1,
                'RemindTypeId' => 2,
                'UserId' => $user['UserId'],
                'PartId' => 0,
                'CustomerConsultId' => 0,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => getCurentDateTime()
            ));
            $comments = array();
            $comment = trim($this->input->post('Comment'));
            if(!empty($comment)) $comments[] = $comments;
            $flag =  $this->Mreminds->insertOwnOrder($postData, $comments);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật nhắc nhở thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('RemindId', 'Comment'));
        if($postData['RemindId'] > 0 && !empty($postData['Comment'])){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mremindcomments');
            $flag = $this->Mremindcomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getReminds(){
        $user = $this->checkUserLogin(true);
        $timeNow = date("Y-m-d H:i");
        $timeEnd = date("Y-m-d H:i", strtotime($timeNow) + 15*60);//15p
        $this->load->model('Mreminds');
        $listReminds = $this->Mreminds->getReminds($timeNow, $timeEnd, $user['UserId']);
        echo json_encode(array('code' => 1, 'data' => $listReminds));
    }
}