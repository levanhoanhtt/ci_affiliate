<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Đơn hàng',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/order_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'order')) {
            $this->loadModel(array('Mfilters', 'Mordertypes', 'Mstores', 'Mcustomers'));
            if($data['appTypeId'] == ADMIN_TYPE_ID) {
                $data['listFilters'] = $this->Mfilters->getList(6);
                $data['listCustomers'] = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
                $data['customerId'] = 0;
            }
            else{
                $data['listFilters'] = array();
                $data['listCustomers'] = array();
                $data['customerId'] = $user['CustomerId'];
            }
            $data['listOrderTypes'] = $this->Mordertypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listUsers'] = $this->Musers->getListForSelect();
            $this->load->view('order/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    /*public function collaborator($customerKindId = 0){
        $user = $this->checkUserLogin();
        $customerKindId = 1;
        $data = $this->commonData($user,
            $this->getTitle('Danh sách Đơn hàng CTV', $customerKindId),
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/collaborator_order_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'order')) {
            $data['customerKindId'] = $customerKindId;
            $this->loadModel(array('Mfilters', 'Mordertypes', 'Mstores', 'Mcustomers'));
            $data['listFilters'] = $this->Mfilters->getList(6);
            //$data['listTransporters'] = $this->Mtransporters->getBy(array('ItemStatusId ' => STATUS_ACTIVED));
            //$data['listTransportTypes'] = $this->Mtransporttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listOrderTypes'] = $this->Mordertypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCustomers'] = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listUsers'] = $this->Musers->getListForSelect();
            $this->load->view('order/collaborator_list', $data);
        }
        else $this->load->view('user/permission', $data);
    }*/

    public function share(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Đơn hàng chung',
            array('scriptFooter' => array('js' => 'js/order_share.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'order')){
            $this->load->model('Morders');
            $postData = array('IsShare' => 2);
            $rowCount = $this->Morders->getCount($postData);
            $data['listOrders'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('order/share', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add($customerKindId = 0, $customerId = 0){
        $user = $this->checkUserLogin();
        if(!is_numeric($customerKindId) || $customerKindId < 0 || $customerKindId > 3) $customerKindId = 0;
        $data = $this->commonData($user,
            $this->getTitle('Tạo đơn hàng Ship', $customerKindId),
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css', 'css/order.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js','vendor/plugins/jwerty/jwerty.js', 'js/choose_item.js', 'js/order_update.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'order')) {
            //$canGrantStaff = $this->Mactions->checkAccess($data['listActions'], 'order/grantStaff');
            //$data['canGrantStaff'] = $canGrantStaff;
            $data['canEdit'] = true;
            $data['customerKindId'] = $customerKindId;
            if(!is_numeric($customerId) || $customerId < 0) $customerId = 0;
            $data['customerId'] = $customerId;
            $this->loadModel(array('Mordertypes', 'Motherservices', 'Morderreasons', 'Mstores', 'Mprovinces', 'Mdistricts', 'Mcategories', 'Mcustomergroups', 'Mcountries', 'Mwards', 'Mtransporttypes', 'Mtransportreasons', 'Mparts', 'Mtags', 'Mconfigs','Mpartners'));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $data['listOrderTypes'] = $this->Mordertypes->getBy($whereStatus);
            $data['listOrderReasons'] = $this->Morderreasons->getBy($whereStatus);
            $data['listOtherServices'] = $this->Motherservices->getBy($whereStatus);
            $data['listTransportTypes'] = $this->Mtransporttypes->getBy($whereStatus);
            $data['listTransportReasons'] = $this->Mtransportreasons->getBy($whereStatus);
            $data['listParts'] = $this->Mparts->getBy($whereStatus);
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy($whereStatus);
            $data['listPartners'] = $this->Mpartners->getBy($whereStatus);
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCountries'] = $this->Mcountries->getList();
            $data['listDistricts'] = $this->Mdistricts->get();
            $data['listDistricts'] = $this->Mdistricts->getList();
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 6));
            $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
            $data['listConfigs'] = $this->Mconfigs->getListMap(2);
            /*if($canGrantStaff) $data['listUsers'] = $this->Musers->getListForSelect();
            else $data['listUsers'] = array($user);*/
            $this->load->view('order/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    

    

    public function appendHtml(){
        $response = $this->load->view('order/_tags');
        var_dump($response);
        echo $response;
    }

    public function edit($orderId = 0){
        if ($orderId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Đơn hàng',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css', 'vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css', 'css/order.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js','vendor/plugins/jwerty/jwerty.js', 'js/choose_item.js', 'js/order_update.js'))
                )
            );
            $this->loadModel(array('Morders', 'Mordertypes', 'Morderreasons', 'Motherservices', 'Morderservices', 'Mpendingstatus', 'Mstores', 'Mprovinces', 'Mdistricts', 'Mcategories', 'Mcustomers', 'Mcustomeraddress', 'Mtags', 'Morderpromotions', 'Morderproducts', 'Mproductunits', 'Mproducts', 'Mproductchilds', 'Mproductprices', 'Mtransporters', 'Mtransporttypes', 'Mparts', 'Mtransports', 'Mactionlogs', 'Mcustomergroups', 'Mcountries', 'Mwards', 'Mtransporttypes', 'Mtransportreasons', 'Mordercomments', 'Mcancelreasons', 'Mtransportcaretypes', 'Mreminds', 'Mbanks', 'Mconfigs', 'Mpromotiontypes', 'Mtransportstatuslogs','Mpartners'));
            $order = $this->Morders->get($orderId);
            if ($order && $order['OrderStatusId'] > 0) {
                $flag = $this->Mactions->checkAccess($data['listActions'], 'order');
                /*if($flag){
                    $canGrantStaff = $this->Mactions->checkAccess($data['listActions'], 'order/grantStaff');
                    if($canGrantStaff) $flag = true;
                    else $flag = $order['StaffId'] == 0 || $order['StaffId'] == $user['UserId'];
                }*/
                if($flag){
                    //$canGrantStaff = false;
                    //$data['canGrantStaff'] = $canGrantStaff;
                    $data['title'] .= ' ' . $order['OrderCode'];
                    $canEdit = $order['OrderStatusId'] == 1 || $order['OrderStatusId'] == 5;
                    $data['canEdit'] = $canEdit;
                    $data['orderId'] = $orderId;
                    $data['order'] = $order;
                    $data['customerAddress'] = $this->Mcustomeraddress->getInfo(0, $order['CustomerId']);
                    $data['customerKindId'] = $this->Mcustomers->getFieldValue(array('CustomerId' => $order['CustomerId']), 'CustomerKindId', 0);
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listOrderTypes'] = $this->Mordertypes->getBy($whereStatus);
                    $data['listOrderReasons'] = $this->Morderreasons->getBy($whereStatus);
                    $data['listOtherServices'] = $this->Motherservices->getBy($whereStatus);
                    $data['listTransportReasons'] = $this->Mtransportreasons->getBy($whereStatus);
                    $data['listPendingStatus'] = $this->Mpendingstatus->getList(6);
                    $data['listCancelReasons'] = $this->Mcancelreasons->getBy($whereStatus);
                    $data['listTransportCareTypes'] = $this->Mtransportcaretypes->getBy($whereStatus);
                    $data['listTransportTypes'] = $this->Mtransporttypes->getBy($whereStatus);
                    $data['listCustomerGroups'] = $this->Mcustomergroups->getBy($whereStatus);
                    $data['listPartners'] = $this->Mpartners->getBy($whereStatus);
                    $listBanks = array();
                    // if($canEdit) $listBanks = $this->Mbanks->search($whereStatus);
                    $data['listBanks'] = $listBanks;
                    $data['listParts'] = $this->Mparts->getBy($whereStatus);
                    $data['listProductUnits'] = $this->Mproductunits->getBy($whereStatus);
                    $whereStatus = array('ItemStatusId' => STATUS_ACTIVED);
                    $data['listStores'] = $this->Mstores->getBy($whereStatus);
                    $data['listTransporters'] = $this->Mtransporters->getBy($whereStatus);
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                    $data['listCategories'] = $this->Mcategories->getListByItemType(1);
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 6));
                    $data['tagNames'] = $this->Mtags->getTagNames($orderId, 6);
                    $data['listOrderProducts'] = $this->Morderproducts->getBy(array('OrderId' => $orderId));
                    $data['listOrderServices'] = $this->Morderservices->getBy(array('OrderId' => $orderId));
                    $data['orderPromotion'] = $this->Morderpromotions->getByOrderId($orderId);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($orderId, 6);
                    $data['listOrderComments'] = $this->Mordercomments->getListByOrderId($orderId);
                    $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');

                    /*if($canGrantStaff) $data['listUsers'] = $this->Musers->getListForSelect();
                    else $data['listUsers'] = array($user);*/
                    $data['remindOwnCost'] = $this->Mreminds->getFieldValue(array('OrderId' => $orderId, 'RemindStatusId >' => 0), 'OwnCost', 0);
                    $data['listConfigs'] = $this->Mconfigs->getListMap(2);
                    $listTransports = $this->Mtransports->getBy(array('OrderId' => $orderId, 'TransportStatusId >' => 0), false, 'TransportId');
                    $transport = false;
                    $transportId = 0;
                    $transportIds = array();
                    $transportStatusIds = array();
                    foreach($listTransports as $t){
                        if($t['TransportId'] > $transportId){
                            $transportId = $t['TransportId'];
                            $transport = $t;
                        }
                        $transportIds[] = $t['TransportId'];
                        $transportStatusIds[$t['TransportId']] = $t['TransportStatusId'];
                    }
                    $data['listTransports'] = $listTransports;
                    $data['transport'] = $transport;
                    if(!empty($transportIds)) $data['transportStatusLogIds'] = $this->Mtransportstatuslogs->getListStatusIdsBatch($transportIds, $transportStatusIds);
                    else $data['transportStatusLogIds'] = array();
                    $this->load->view('order/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['canEdit'] = false;
                $data['orderId'] = 0;
                $data['txtError'] = "Không tìm thấy Đơn hàng";
                $this->load->view('order/edit', $data);
            }
        }
        else redirect('order');
    }

    private function getTitle($title, $customerKindId){
        $customerKindName = '';
        if($customerKindId == 1) $customerKindName = ' | Khách lẻ';
        elseif($customerKindId == 2) $customerKindName = ' | Khách buôn';
        elseif($customerKindId == 3) $customerKindName = ' | Khách CTV';
        return $title.$customerKindName;
    }

    public function printPdf($orderId = 0){
        if ($orderId > 0) {
            $this->checkUserLogin();
            $this->loadModel(array('Morders', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mproducts', 'Mproductchilds', 'Mtransporttypes', 'Mstores', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $order = $this->Morders->get($orderId);
            if ($order) {
                $customerAddress = $this->Mcustomeraddress->getInfo($order['CustomerAddressId'], $order['CustomerId']);
                if($customerAddress){
                    if($customerAddress['CountryId'] == 0 || $customerAddress['CountryId'] == 232){
                        $address = $customerAddress['Address'];
                        if($customerAddress['WardId'] > 0) $address .= ', xã '.$this->Mwards->getFieldValue(array('WardId' => $customerAddress['WardId']), 'WardName');
                        if($customerAddress['DistrictId'] > 0) $address .= ', huyện '.$this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress['DistrictId']), 'DistrictName');
                        if($customerAddress['ProvinceId'] > 0) $address .= ', tỉnh '.$this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress['ProvinceId']), 'ProvinceName');
                        $customerAddress['Address'] = $address;
                    }
                    else $customerAddress['Address'] .= $customerAddress['ZipCode'].' '.$this->Mcountries->getFieldValue(array('CountryId' => $customerAddress['CountryId']), 'CountryName');
                }
                $this->load->library('ciqrcode');
                $params = array();
                $orderCode = $order['OrderCode'];
                $params['data'] = $orderCode;
                $params['savename'] = "./assets/uploads/qr/{$orderCode}.png";
                $this->ciqrcode->generate($params);
                $data = array(
                    'configs' => $this->session->userdata('configs'),
                    'order' => $order,
                    'storeName' => $order['StoreId'] > 0 ? $this->Mstores->getFieldValue(array('StoreId' => $order['StoreId']), 'StoreName') : '',
                    'crFullName' => $this->Musers->getFieldValue(array('UserId' => $order['CrUserId']), 'FullName'),
                    'customerAddress' => $customerAddress,
                    'transportTypeName' => $order['TransportTypeId'] > 0 ? $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $order['TransportTypeId']), 'TransportTypeName') : '',
                    'listOrderProducts' => $this->Morderproducts->getBy(array('OrderId' => $orderId)),
                    'barcodeSrc' => $params['savename']
                );
                $this->load->view('order/print_pdf', $data);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }
    public function printPdfMultipleByTransport(){
        $this->checkUserLogin();
        $transportIds = json_decode(trim($this->input->post('TransportIds')), true);
        if(!empty($transportIds)){
            $this->loadModel(array('Morders', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mproducts', 'Mproductchilds', 'Mtransporttypes', 'Mstores', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $listOrders = $this->Morders->search(array('TransportIds' => $transportIds));
            if(!empty($listOrders)) $this->load->view('order/print_pdf_multi', array('configs' => $this->session->userdata('configs'), 'printData' => $this->getPrintData($listOrders)));
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    public function printPdfMultiple(){
        $this->checkUserLogin();
        $orderIds = json_decode(trim($this->input->post('OrderIds')), true);
        if(!empty($orderIds)){
            $this->loadModel(array('Morders', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mproducts', 'Mproductchilds', 'Mtransporttypes', 'Mstores', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $listOrders = $this->Morders->search(array('OrderIds' => $orderIds));
            if(!empty($listOrders)) $this->load->view('order/print_pdf_multi', array('configs' => $this->session->userdata('configs'), 'printData' => $this->getPrintData($listOrders)));
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    private function getPrintData($listOrders){
        $printData = array();
        $storeNames = array(0 => '');
        $crFullNames = array();
        $customerAddress = array();
        $wardNames = array();
        $districtNames = array();
        $provinceNames = array();
        $countryNames = array();
        $transportTypeNames = array(0 => '');
        $this->load->library('ciqrcode');
        foreach($listOrders as $order) {
            $params = array();
            $orderCode = $order['OrderCode'];
            $params['data'] = $orderCode;
            $params['savename'] = "./assets/uploads/qr/{$orderCode}.png";
            $this->ciqrcode->generate($params);
            if(!isset($storeNames[$order['StoreId']])) $storeNames[$order['StoreId']] = $this->Mstores->getFieldValue(array('StoreId' => $order['StoreId']), 'StoreName');
            if(!isset($crFullNames[$order['CrUserId']])) $crFullNames[$order['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $order['CrUserId']), 'FullName');
            if(!isset($customerAddress[$order['CustomerId']])){
                $customerAddress[$order['CustomerId']] = $this->Mcustomeraddress->getInfo($order['CustomerAddressId'], $order['CustomerId']);
                if($customerAddress[$order['CustomerId']]){
                    if($customerAddress[$order['CustomerId']]['CountryId'] == 0 || $customerAddress[$order['CustomerId']]['CountryId'] == 232){
                        $address = $customerAddress[$order['CustomerId']]['Address'];
                        if($customerAddress[$order['CustomerId']]['WardId'] > 0){
                            if(!isset($wardNames[$customerAddress[$order['CustomerId']]['WardId']])) $wardNames[$customerAddress[$order['CustomerId']]['WardId']] = $this->Mwards->getFieldValue(array('WardId' => $customerAddress[$order['CustomerId']]['WardId']), 'WardName');
                            $address .= ', xã '.$wardNames[$customerAddress[$order['CustomerId']]['WardId']];
                        }
                        if($customerAddress[$order['CustomerId']]['DistrictId'] > 0){
                            if(!isset($districtNames[$customerAddress[$order['CustomerId']]['DistrictId']])) $districtNames[$customerAddress[$order['CustomerId']]['DistrictId']] = $this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress[$order['CustomerId']]['DistrictId']), 'DistrictName');
                            $address .= ', huyện '.$districtNames[$customerAddress[$order['CustomerId']]['DistrictId']];
                        }
                        if($customerAddress[$order['CustomerId']]['ProvinceId'] > 0){
                            if(!isset($provinceNames[$customerAddress[$order['CustomerId']]['ProvinceId']])) $provinceNames[$customerAddress[$order['CustomerId']]['ProvinceId']] = $this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress[$order['CustomerId']]['ProvinceId']), 'ProvinceName');
                            $address .= ', tỉnh '.$provinceNames[$customerAddress[$order['CustomerId']]['ProvinceId']];
                        }
                        $customerAddress[$order['CustomerId']]['Address'] = $address;
                    }
                    else{
                        if(!isset($countryNames[$customerAddress[$order['CustomerId']]['CountryId']])) $countryNames[$customerAddress[$order['CustomerId']]['CountryId']] = $this->Mcountries->getFieldValue(array('CountryId' => $customerAddress[$order['CustomerId']]['CountryId']), 'CountryName');
                        $customerAddress[$order['CustomerId']]['Address'] .= $customerAddress[$order['CustomerId']]['ZipCode'].' '.$countryNames[$customerAddress[$order['CustomerId']]['CountryId']];
                    }
                }
            }
            if(!isset($transportTypeNames[$order['TransportTypeId']])) $transportTypeNames[$order['TransportTypeId']] = $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $order['TransportTypeId']), 'TransportTypeName');
            $printData[] = array(
                'order' => $order,
                'storeName' => $storeNames[$order['StoreId']],
                'crFullName' => $crFullNames[$order['CrUserId']],
                'customerAddress' => $customerAddress[$order['CustomerId']],
                'transportTypeName' => $transportTypeNames[$order['TransportTypeId']],
                'listOrderProducts' => $this->Morderproducts->getBy(array('OrderId' => $order['OrderId'])),
                'barcodeSrc' => $params['savename']
            );
        }
        return $printData;
    }

    /*public function checkOrder(){
        $user = $this->session->userdata('user');
        $orderId = $this->input->post('OrderId');
        if ($user && $orderId > 0) {
            $this->loadModel(array('Mcustomers', 'Mstores', 'Mproducts', 'Morders'));
            // lấy đia chỉ đặt hàng và số lượng sản phẩm đặt của đơn hàng của người dùng
            $result_select_customer_info = $this->Mcustomers->getInfoWithOrder($orderId);
            $storesDataCheck = $this->Mstores->getWithLocation();
            // lấy trường hợp perfect nhất
            $result_select_check_quantity_perfect = $this->Mproducts->getListPerfectByOrder($orderId, ' AND productquantity.Quantity >= orderproducts.Quantity');

            // sắp lại data theo StoreId để xem cơ sở này có đủ số lượng hay không , tiện thể nhét luôn cái khoảng cách vào cho nó để tính tiếp
            $checkDatas = [];
            $api_distance = "http://maps.googleapis.com/maps/api/distancematrix/json?origins={origins}&destinations={destinations}&language=vn-VN&sensor=false";
            if (!empty($result_select_check_quantity_perfect) && count($result_select_check_quantity_perfect) >0) $checkDatas = $this->mergerStore($result_select_check_quantity_perfect, $result_select_customer_info, $api_distance, $storesDataCheck);
            $dataFiltersPerfect = [];
            foreach ($checkDatas as $keyStore => $checkData) {
                if ($checkData['count_product'] == $result_select_customer_info['count_product']) {
                    $dataFiltersPerfect[] = [
                        'StoreData' => $storesDataCheck[$keyStore],
                        'DataProduct' => $checkData
                    ];
                }
            }
            // sắp xếp lại mảng theo khoảng cách và kiểm tra điều kiện thỏa mãn
            for ($i = 0; $i < count($dataFiltersPerfect); $i++) {
                for ($j = $i + 1; $j < count($dataFiltersPerfect); $j++) {
                    if ($dataFiltersPerfect[$i]['distance'] > $dataFiltersPerfect[$j]['distance']) {
                        $tmp = $dataFiltersPerfect[$i];
                        $dataFiltersPerfect[$i] = $dataFiltersPerfect[$j];
                        $dataFiltersPerfect[$j] = $tmp;
                    }
                }
                //Sắp xếp xong lấy luôn phần tử đầu tiền
                if (!empty($dataFiltersPerfect)) {
                    echo json_encode($dataFiltersPerfect[0]);
                    return;
                }
            }
            // kết thúc kiểm tra trường hợp perfect nhất

            //Kiểm tra trường hợp bình thường
            $result_select_check_quantity_nomarl = $this->Mproducts->getListPerfectByOrder($orderId);

            // sắp lại data theo StoreId , tiện thể nhét luôn cái khoảng cách vào cho nó để tính tiếp
            $dataFilterNomarl = [];
            if (!empty($result_select_check_quantity_nomarl)) $checkDatas = $this->mergerStore($result_select_check_quantity_nomarl, $result_select_customer_info, $api_distance, $storesDataCheck);

            foreach ($checkDatas as $checkData) $dataFilterNomarl[] = $checkData;
            //sx lại mảng theo số lượng trước , đông thời tạo các nhóm có cùng số lượng với nhau lại để còn sắp xếp theo khoảng cách
            $dataGroupCountProduct = [];
            for ($i = 0; $i < count($dataFilterNomarl); $i++) {
                for ($j = $i + 1; $j < count($dataFilterNomarl); $j++) {
                    if ($dataFilterNomarl[$i]['count_product'] < $dataFilterNomarl[$j]['count_product']) {
                        $tmp = $dataFilterNomarl[$i];
                        $dataFilterNomarl[$i] = $dataFilterNomarl[$j];
                        $dataFilterNomarl[$j] = $tmp;
                    }
                }
                $dataGroupCountProduct[$dataFilterNomarl[$i]['count_product']][] = $dataFilterNomarl[$i];
            }
            //sx lại mảng theo khoảng cách của từng nhóm có cùng số lượng
            $dataFilterNomarl = [];
            foreach ($dataGroupCountProduct as $dataGroup) {
                for ($i = 0; $i < count($dataGroup); $i++) {
                    for ($j = $i + 1; $j < count($dataGroup); $j++) {
                        if ($dataGroup[$i]['distance'] > $dataGroup[$j]['distance']) {
                            $tmp = $dataGroup[$i];
                            $dataGroup[$i] = $dataGroup[$j];
                            $dataGroup[$j] = $tmp;
                        }
                    }
                    $dataFilterNomarl [] = $dataGroup[$i];
                }
            }

            $storeDataFilter = [];
            $keyStore = [];
            $quantity_sub = [];
            foreach ($dataFilterNomarl as $filters) {
                foreach ($filters as $key => $data) {
                    if (is_numeric($key) && is_array($data)) {
                        if (!array_key_exists($data['ProductId'] . "-" . $data['ProductChildId'], $quantity_sub)) $quantity_sub[$data['ProductId'] . "-" . $data['ProductChildId']] = intval($data['order_quantity']) - intval($data['product_quantity']);
                        else $quantity_sub[$data['ProductId'] . "-" . $data['ProductChildId']] = $quantity_sub[$data['ProductId'] .'-'. $data['ProductChildId']] - intval($data['product_quantity']);
                        if (!in_array($data['StoreId'], $keyStore)) {
                            $keyStore[] = $data['StoreId'];
                            $storeDataFilter[] = [
                                'StoreData' => $storesDataCheck[$data['StoreId']],
                                'DataProduct' => $filters
                            ];
                        }
                        if ($this->checkFullProduct($quantity_sub) && count($quantity_sub) == $result_select_customer_info['count_product']) {
                            $this->Morders->insertChild($storeDataFilter, $orderId, $user['UserId']);
                            echo json_encode($storeDataFilter);
                            return;
                        }
                    }
                }
            }
            // Lưu lại thông tin theo sản phẩm
            $dataWithProduct = [];
            $indexOrderCode = 1;
            $orderCode = "";
            foreach ($storeDataFilter as $data) {
                $dataProduct = $data['DataProduct'];
                $orderCode = $dataProduct['OrderCode'];
                $storeData = $data['StoreData'];
                $distance = $dataProduct['distance'];
                unset($dataProduct['distance']);
                unset($dataProduct['count_product']);
                unset($dataProduct['OrderCode']);
                foreach ($dataProduct as $iDataProduct) {
                    $iDataProduct['Store'] = $storeData;
                    $iDataProduct['distance'] = $distance;
                    if (array_key_exists($iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId'], $dataWithProduct)) {
                        if (!empty($dataWithProduct[$iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId']]['sub_quantity']) && $dataWithProduct[$iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId']]['sub_quantity'] > 0) {
                            $dataWithProduct[$iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId']][] = $iDataProduct;
                            $dataWithProduct[$iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId']]['sub_quantity'] -= $iDataProduct['order_quantity'];
                        }
                    }
                    else {
                        $dataWithProduct[$iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId']][] = $iDataProduct;
                        $dataWithProduct[$iDataProduct['ProductId'] . "-" . $iDataProduct['ProductChildId']]['sub_quantity'] = $iDataProduct['order_quantity'] - $iDataProduct['product_quantity'];
                    }
                }
            }
            $subProducts = [];
            $dataWithStore = [];
            // xử lý Lưu lại đơn mà sản phẩm đã có 1 phần nào đấy , theo store
            foreach ($dataWithProduct as $key => $dataProducts) {
                foreach ($dataProducts as $keyP => $dataProduct) {
                    if (is_numeric($keyP)) {
                        if ($dataProducts['sub_quantity'] > 0) {
                            $quantity = $dataProduct['product_quantity'];
                            $this->addProduct($dataWithStore,$dataProduct,$orderId,$quantity);
                            $subProducts[$key] = $dataProduct['order_quantity'] - $dataProduct['product_quantity'];
                        }
                        else {
                            if (array_key_exists($key, $subProducts)) {
                                if ($subProducts[$key] > 0) {
                                    if ($dataProduct['product_quantity'] >= $subProducts[$key]) {
                                        $quantity = $subProducts[$key];
                                    } else {
                                        $quantity = $dataProduct['product_quantity'];
                                    }
                                    $this->addProduct($dataWithStore,$dataProduct,$orderId,$quantity);
                                    $subProducts[$key] -= $dataProduct['product_quantity'];
                                }
                            }
                            else {
                                $quantity = $dataProduct['product_quantity'] > $dataProduct['order_quantity'] ? $dataProduct['order_quantity'] : $dataProduct['product_quantity'];
                                $this->addProduct($dataWithStore,$dataProduct,$orderId,$quantity);
                                $subProducts[$key] = $dataProduct['order_quantity'] - $dataProduct['product_quantity'];
                            }
                        }
                        if(empty($dataWithStore[$dataProduct['StoreId']]['OrderCode'])){
                            $dataWithStore[$dataProduct['StoreId']]['OrderCode'] = $orderCode."-".$indexOrderCode;
                            $indexOrderCode++;
                        }
                    }
                }
            }
            $productKeys = [];
            $orderWait = ['OrderCode' => $orderCode."-".$indexOrderCode];
            // lưu lại có sản phẩm thiếu
            foreach ($subProducts as $key => $value){
                if($value > 0){
                    list($productId,$productChildId) = explode('-',$key);
                    $orderWait[$key] = [
                        'ProductId' => $productId,
                        'ProductChildId' => $productChildId,
                        'order_quantity' => $value,
                        'IsChildren' => 1,
                    ];
                }
                    $productKeys[] = $key;
            }
            // get lấy thông tin các sản phẩm còn thiếu
            $resultProducts = $this->Mproducts->getListMissingByOrder($orderId);
            foreach ($resultProducts as $data){
                $key = $data['ProductId']."-".$data['ProductChildId'];
                if(!in_array($key,$productKeys)){
                    $data['IsChildren'] = 1;
                    $data['OrderParentId'] = $orderId;
                    $orderWait[$key] = $data;
                }
                else{
                    if(!empty($orderWait[$key]['order_quantity']) && $orderWait[$key]['order_quantity'] >0 ) {
                        $orderWait[$key]['Price'] = $data['Price'];
                        $orderWait[$key]['OriginalPrice'] = $data['OriginalPrice'];
                        $orderWait[$key]['DiscountReason'] = $data['DiscountReason'];
                        $orderWait[$key]['ProductName'] = $data['ProductName'];
                        $orderWait[$key]['ProductNameChild'] = $data['ProductNameChild'];
                        $orderWait[$key]['OrderParentId'] = $orderId;
                    }
                }
            }
            $_SESSION['dataWithStore'] = $dataWithStore;
            $_SESSION['orderWait'] = $orderWait;

            // xử lý khi đơn hàng này không đáp ứng được hết
            echo json_encode(['status' => -1,'productExits'=>json_encode($dataWithStore),'productWait' => json_encode($orderWait)]);
            return;
        }
        //else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertOrderWait(){
        $user = $this->checkUserLogin(true);
        $dataWithStore = $_SESSION['dataWithStore'];
        $orderWait = $_SESSION['orderWait'];
        if(!empty($dataWithStore) && !empty($orderWait)){
            $this->load->model('Morders');
            $flag = $this->Morders->insertOrderWait($dataWithStore, $orderWait, $user['UserId']);
            if ($flag) echo json_encode(array('code' => 1, 'message' => 'Lưu đơn hàng con thành công'));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            // xử lý lưu đơn gồm các sản phẩm còn thiếu
            //$_SESSION['dataWithStore'] = $dataWithStore;
            //$_SESSION['orderWait'] = $orderWait;
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    private function checkFullProduct($data){
        foreach ($data as $value) {
            if (intval($value) > 0) return false;
        }
        return true;
    }

    private function addProduct(&$dataWithStore,$dataProduct,$orderId,$quantity){
        $dataWithStore[$dataProduct['StoreId']][] = [
            'OriginalPrice' => $dataProduct['OriginalPrice'],
            'DiscountReason' => $dataProduct['DiscountReason'],
            'Price' => $dataProduct['Price'],
            'StoreId' => $dataProduct['StoreId'],
            'ProductId' => $dataProduct['ProductId'],
            'ProductChildId' => $dataProduct['ProductChildId'],
            'Quantity' => $quantity,
            'OrderParentId' => $orderId,
            'ProductName' => $dataProduct['ProductName'],
            'ProductNameChild' => $dataProduct['ProductNameChild'],
            'Location' => $dataProduct['Store']['StoreName']." ".$dataProduct['Store']['ProvinceName']." ".$dataProduct['Store']['DistrictName']."( ".number_format($dataProduct['distance'])."M )"
        ];
    }

    private function mergerStore($dataQuantity, $result_select_customer_info, $api_distance, $storesDataCheck){
        $checkDatas = [];
        foreach ($dataQuantity as $check_quantity) {
            //gộp các bảng ghi có cùng store lại với nhau
            if (array_key_exists($check_quantity['StoreId'], $checkDatas)) {
                $checkDatas[$check_quantity['StoreId']][] = $check_quantity;
                $checkDatas[$check_quantity['StoreId']]['count_product'] += 1;
            } else {
                // chuẩn hóa string để search với api của google
                $district_name_origin = trim(str_replace('Quận', '', $storesDataCheck[$check_quantity['StoreId']]['DistrictName']));
                $district_name_origin = trim(str_replace('Huyện', '', $district_name_origin));
                $province_name_origin = trim($storesDataCheck[$check_quantity['StoreId']]['ProvinceName']);
                $origins = $district_name_origin . ", " . $province_name_origin;
                $origins = preg_replace('/\s+/', '+', $origins);

                $address_des = $result_select_customer_info['Address'];
                $district_name_des = trim(str_replace('Quận ', '', $result_select_customer_info['DistrictName']));
                $district_name_des = trim(str_replace('Huyện ', '', $district_name_des));
                $province_name_des = trim($result_select_customer_info['ProvinceName']);
                $destinations = $address_des . ", " . $district_name_des . ', ' . $province_name_des;
                $destinations = preg_replace('/\s+/', '+', $destinations);
                // kết thúc chuẩn hóa

                // thực hiện việc lấy khoảng cách
                $url_distance = str_replace('{origins}', $origins, $api_distance);
                $url_distance = str_replace('{destinations}', $destinations, $url_distance);
                $ch = curl_init($url_distance);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $data_distance = json_decode(curl_exec($ch));
                $length_distance = $data_distance->rows[0]->elements[0]->distance->value;
                // kết thúc việc lấy khoảng cách
                $checkDatas[$check_quantity['StoreId']]['distance'] = $length_distance;
                $checkDatas[$check_quantity['StoreId']]['count_product'] = 1;
                $checkDatas[$check_quantity['StoreId']]['OrderCode'] = $result_select_customer_info['OrderCode'];
                $checkDatas[$check_quantity['StoreId']][] = $check_quantity;
            }
            // kết thúc việc gộp
        }
        return $checkDatas;
    }*/
}