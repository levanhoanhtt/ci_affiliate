<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productunit extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Đơn vị sản phẩm',
            array('scriptFooter' => array('js' => 'js/product_unit.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'productunit')) {
            $this->load->model('Mproductunits');
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/product_unit', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductUnitName'));
        if(!empty($postData['ProductUnitName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $cancelReasonId = $this->input->post('ProductUnitId');
            $this->load->model('Mproductunits');
            $flag = $this->Mproductunits->save($postData, $cancelReasonId);
            if ($flag > 0) {
                $postData['ProductUnitId'] = $flag;
                $postData['IsAdd'] = ($cancelReasonId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật Đơn vị sản phẩm thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $cancelReasonId = $this->input->post('ProductUnitId');
        if($cancelReasonId > 0){
            $this->load->model('Mproductunits');
            $flag = $this->Mproductunits->changeStatus(0, $cancelReasonId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Đơn vị sản phẩm thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
