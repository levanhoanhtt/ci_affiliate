<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionreason extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lý do thu chi',
            array('scriptFooter' => array('js' => 'js/transaction_reason.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'transactionreason')) {
            $this->load->model('Mtransactionreasons');
            $data['listTransactionReasons'] = $this->Mtransactionreasons->getBy(array('StatusId' => STATUS_ACTIVED), false, 'TransactionTypeId');
            $this->load->view('setting/transaction_reason', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransactionReasonName', 'TransactionTypeId'));
        if(!empty($postData['TransactionReasonName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $transactionReasonId = $this->input->post('TransactionReasonId');
            $this->load->model('Mtransactionreasons');
            $flag = $this->Mtransactionreasons->save($postData, $transactionReasonId);
            if ($flag > 0) {
                $postData['TransactionReasonId'] = $flag;
                $postData['IsAdd'] = ($transactionReasonId > 0) ? 0 : 1;
                $postData['TransportTypeName'] = '<span class="'.$this->Mconstants->labelCss[$postData['TransactionTypeId']].'">'.$this->Mconstants->transactionTypes[$postData['TransactionTypeId']].'</span>';
                echo json_encode(array('code' => 1, 'message' => "Cập nhật lý do thu chi thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $transactionReasonId = $this->input->post('TransactionReasonId');
        if($transactionReasonId > 0){
            $this->load->model('Mtransactionreasons');
            $flag = $this->Mtransactionreasons->changeStatus(0, $transactionReasonId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa lý do thu chi thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
