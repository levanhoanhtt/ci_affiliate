<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productprint extends MY_Controller{

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách phiếu in barcode',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/productprint_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'productprint')) {
            $this->load->model('Mfilters');
            $data['listFilters'] = $this->Mfilters->getList(22);
            $this->load->view('productprint/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tạo phiếu in barcode',
            array('scriptFooter' => array('js' => array('js/choose_item.js', 'js/productprint_update.js')))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'productprint')) {
            $this->load->model('Mcategories');
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $this->load->view('productprint/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($productPrintId = 0){
        if ($productPrintId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật phiếu in barcode',
                array('scriptFooter' => array('js' => array('js/choose_item.js', 'js/productprint_update.js')))
            );
            $this->loadModel(array('Mproductprints', 'Mproducts', 'Mproductchilds', 'Mcategories', 'Mproductprintcomments', 'Mactionlogs','Mproductprintproducts', 'Mproductunits'));
            $productPrint = $this->Mproductprints->get($productPrintId);
            if ($productPrint && $productPrint['StatusId'] > 0) {
                if ($this->Mactions->checkAccess($data['listActions'], 'productprint')) {
                    $data['title'] .= ' ' . $productPrint['ProductPrintCode'];
                    $data['productPrintId'] = $productPrintId;
                    $data['productPrint'] = $productPrint;
                    $data['listCategories'] = $this->Mcategories->getListByItemType(1);
                    $data['listProductPrintProducts'] = $this->Mproductprintproducts->getBy(array('ProductPrintId' => $productPrintId));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listActionLogs'] = $this->Mactionlogs->getList($productPrintId, 22);
                    $data['listProductPrintComments'] = $this->Mproductprintcomments->getListByProductPrintId($productPrintId);
                    $this->load->view('productprint/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['productPrintId'] = 0;
                $data['txtError'] = "Không tìm thấy phiếu in barcode";
                $this->load->view('productprint/edit', $data);
            }
        }
        else redirect('productprint');
    }

    public function printPdf($productPrintId = 0){
        if($productPrintId > 0){
            $this->loadModel(array('Mproductprints', 'Mproducts', 'Mproductchilds', 'Mproductprintproducts'));
            $productPrint = $this->Mproductprints->get($productPrintId);
            if($productPrint && $productPrint['StatusId'] > 0) {
                $products = array();
                $listProductPrintProducts = $this->Mproductprintproducts->getBy(array('ProductPrintId' => $productPrintId));
                $listBarcode = array();
                $j = 0;
                foreach($listProductPrintProducts as $op){
                    if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName,BarCode');
                    $barCode = $products[$op['ProductId']]['BarCode'];
                    $productName = $products[$op['ProductId']]['ProductName'];
                    if($op['ProductChildId'] > 0){
                        if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName,BarCode');
                        $barCode = $productChilds[$op['ProductChildId']]['BarCode'];
                        $productName = $productName . '|' . $productChilds[$op['ProductChildId']]['ProductName'];
                    }
                    $this->genBarcode($barCode, 'code128', 40);
                    $listBarcode[$j]['link'] = base_url('./assets/uploads/qr/' . $barCode . '.png');
                    $this->genBarcode($barCode);
                    for ($i = 0; $i < $op['Quantity']; $i++) {
                        $listBarcode[$j]['link'] = base_url('assets/uploads/qr/' . $barCode . '.png');
                        $listBarcode[$j]['name'] = $productName;
                        $listBarcode[$j]['barcode'] = $barCode;
                        $j++;
                    }
                }
                $this->load->view('productprint/print_pdf', array('listBarcode' => $listBarcode));
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    private function genBarcode($code, $bcs = 'code128', $height = 40){
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $barcodeOptions = array('text' => $code, 'barHeight' => $height, 'drawText' => false, 'factor' => 1, 'fontSize' => 0, 'font' => 4);
        $rendererOptions = array('imageType' => 'png', 'horizontalPosition' => 'center', 'verticalPosition' => 'middle');
        $imageResource = Zend_Barcode::draw($bcs, 'image', $barcodeOptions, $rendererOptions);
        imagepng($imageResource, "./assets/uploads/qr/{$code}.png");
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductPrintId', 'Comment'));
        if($postData['ProductPrintId'] > 0 && !empty($postData['Comment'])){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mproductprintcomments');
            $flag = $this->Mproductprintcomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $products = json_decode(trim($this->input->post('Products')), true);
        if(!empty($products)) {
            $postData = array('StatusId' => STATUS_ACTIVED);
            $productPrintId = $this->input->post('ProductPrintId');
            $crDateTime = getCurentDateTime();
            $actionLogs = array(
                'ItemTypeId' => 22,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            if ($productPrintId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                $actionLogs['ActionTypeId'] = 2;
                $actionLogs['Comment'] = $user['FullName'] . ': Cập nhật phiếu in barcode';
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLogs['ActionTypeId'] = 1;
                $actionLogs['Comment'] = $user['FullName'] . ': Thêm mớí phiếu in barcode';
            }
            $comments = json_decode(trim($this->input->post('Comments')), true);
            $this->load->model('Mproductprints');
            $productPrintId = $this->Mproductprints->update($postData, $productPrintId, $products, $comments, $actionLogs);
            if ($productPrintId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu in barcode thành công", 'data' => $productPrintId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatusBatch(){
        $user = $this->checkUserLogin(true);
        $productIds = json_decode(trim($this->input->post('ItemIds')), true);
        $statusId = $this->input->post('StatusId');
        if(!empty($productIds) && $statusId >= 0){
            $this->load->model('Mproductprints');
            $flag = $this->Mproductprints->changeStatusBatch($productIds, $statusId, $user);
            if($flag) {
                $msg = 'Xóa sản phẩm thành công';
                $statusName = '';
                if ($statusId > 0) {
                    $msg = 'Thay đổi trạng thái thành công';
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $msg, 'data' => $statusName));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter(){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $this->load->model('Mproductprints');
        $data1 = $this->Mproductprints->searchByFilter($searchText, $itemFilters, $limit, $page);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}