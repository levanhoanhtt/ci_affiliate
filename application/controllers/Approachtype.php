<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approachtype extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Cách tiếp cận',
            array('scriptFooter' => array('js' => 'js/approach_type.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'approachtype')) {
            $this->load->model('Mapproachtypes');
            $data['listApproachTypes'] = $this->Mapproachtypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/approach_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ApproachTypeName'));
        if(!empty($postData['ApproachTypeName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $approachTypeId = $this->input->post('ApproachTypeId');
            $this->load->model('Mapproachtypes');
            $flag = $this->Mapproachtypes->save($postData, $approachTypeId);
            if ($flag > 0) {
                $postData['ApproachTypeId'] = $flag;
                $postData['IsAdd'] = ($approachTypeId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật cách tiếp cận thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $approachTypeId = $this->input->post('ApproachTypeId');
        if($approachTypeId > 0){
            $this->load->model('Mapproachtypes');
            $flag = $this->Mapproachtypes->changeStatus(0, $approachTypeId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa cách tiếp cận thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
