<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller{

    //const COOKIE_TIME_DEFAULT = 2592000;

    public function index($code = ''){
        $baseUrl = 'http://demo.rickyteam.com/';
        if(!empty($code)) {
            $this->loadModel(array('Mlinkaffs', 'Mlinklogs', 'Mproducts'));
            $linkAff = $this->Mlinkaffs->getLinkAffByShortLink(DOMAIN_SHORT . $code);
            if($linkAff && $linkAff['ProductId'] > 0){
                $product = $this->Mproducts->get($linkAff['ProductId']);
                if($product && $product['ProductStatusId'] == STATUS_ACTIVED){
                    $linkLog = array(
                        'LinkAffId' => $linkAff['LinkAffId'],
                        'IpAddress' => $this->input->ip_address(),
                        'UserAgent' => $this->getAgent(),
                        'StatusId' => 1, //cookie het han
                        'CookieStatusId' => 1, //cookie dc truyen di
                        'CrDateTime' => getCurentDateTime()
                    );
                    $cookieTimeout = $product['CookieTimeout'] * 24 * 3600;
                    if($cookieTimeout > 0) $linkLog['StatusId'] = STATUS_ACTIVED;
                    $cookie_key = 'User_LinkAffId_' . $linkAff['LinkAffId'];
                    if(!isset($_COOKIE[$cookie_key])){
                        if($cookieTimeout > 0){ //lan dau tien vao -> tao cookie
                            setcookie($cookie_key, $cookie_key, time() + $cookieTimeout, '/', 'go.ricky.vn');
                            setcookie($cookie_key, $cookie_key, time() + $cookieTimeout, '/', 'ricky.vn');
                            redirect($baseUrl.'products/'.$product['ProductSlug'].'?affiliate_name=ricky&offer_id=1&offer_name=Ricky&affiliate_id='.$linkAff['LinkAffId'].'&ctv_id='.$linkAff['CustomerId']);
                        }
                        else{
                            $this->Mlinklogs->save($linkLog);
                            redirect($baseUrl.'products/'.$product['ProductSlug']);
                        }
                    }
                    else{
                        //
                        setcookie($cookie_key, $cookie_key, time() + $cookieTimeout, '/', 'go.ricky.vn');
                        setcookie($cookie_key, $cookie_key, time() + $cookieTimeout, '/', 'ricky.vn');
                        //xoa

                        $linkLog['CookieStatusId'] = STATUS_ACTIVED;
                        $this->Mlinklogs->save($linkLog);
                        redirect($baseUrl.'products/'.$product['ProductSlug'].'?affiliate_name=Ricky&offer_id=1&offer_name=Ricky&affiliate_id='.$linkAff['LinkAffId'].'&ctv_id='.$linkAff['CustomerId']);
                    }
                }
                else redirect($baseUrl.'san-pham.html');
            }
            else redirect($baseUrl.'san-pham.html');
        }
        else redirect($baseUrl.'san-pham.html');
    }

    public function getAgent(){
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) $agent = $this->agent->browser().' '.$this->agent->version();
        elseif ($this->agent->is_robot()) $agent = $this->agent->robot();
        elseif ($this->agent->is_mobile()) $agent = $this->agent->mobile();
        else $agent = 'Unidentified User Agent';
        return $agent;
    }
}