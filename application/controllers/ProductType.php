<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producttype extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Mảng kinh doanh',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/product_type.js'))
            )
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'producttype')) {
            $this->load->model('Mproducttypes');
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/product_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductTypeName', 'IsShare', 'ActiveDate'));
        $activeDate =  $postData['ActiveDate'];
        $postData['ActiveDate'] = ddMMyyyyToDate($activeDate);
        $postData['StatusId'] = STATUS_ACTIVED;
        $productTypeId = $this->input->post('ProductTypeId');
        if($productTypeId > 0){
            $postData['UpdateUserId'] = $user['UserId'];
            $postData['UpdateDateTime'] = getCurentDateTime();
        }
        else {
            $postData['CrUserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
        }
        $this->load->model('Mproducttypes');
        $flag = $this->Mproducttypes->save($postData, $productTypeId);
        if ($flag > 0){
            $postData['ProductTypeId'] = $flag;
            $postData['IsAdd'] = ($productTypeId > 0) ? 0 : 1;
            $postData['ActiveDate'] = $activeDate;
            echo json_encode(array('code' => 1, 'message' => "Cập nhật mảng kinh doanh thành công", 'data' => $postData));
        }
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $user = $this->checkUserLogin(true);
        $productTypeId = $this->input->post('ProductTypeId');
        if($productTypeId  > 0){
            $this->load->model('Mproducttypes');
            $flag = $this->Mproducttypes->changeStatus(0, $productTypeId, '', $user['UserId']);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa mảng kinh doanh thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}