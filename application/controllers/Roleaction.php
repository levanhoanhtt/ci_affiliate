<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roleaction extends MY_Controller {

	public function index($roleId = 0, $partId = 0){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Cấp quyền cho nhóm',
			array('scriptFooter' => array('js' => 'js/role_action.js'))
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'role')) {
			$data['roleId'] = $roleId > 0 ? $roleId : 0;
			$data['partId'] = $partId > 0 ? $partId : 0;
			$this->loadModel(array('Mroles', 'Mparts'));
			$data['listRoles'] = $this->Mroles->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listParts'] = $this->Mparts->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listActiveActions'] = $this->Mactions->getHierachy();
			$this->load->view('setting/role_action', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function getAction(){
		$this->checkUserLogin(true);
		$roleId = $this->input->post('RoleId');
		$partId = $this->input->post('PartId');
		if($roleId > 0 && $partId >= 0){
			$this->load->model('Mroleactions');
			echo json_encode(array('code' => 1, 'data' => $this->Mroleactions->getBy(array('RoleId' => $roleId, 'PartId' => $partId), false, '', 'ActionId')));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$roleId = $this->input->post('RoleId');
		$partId = $this->input->post('PartId');
		if ($roleId > 0 && $partId >= 0) {
			$valueData = array();
			$actionIds = $this->input->post('ActionIds');
			if(!empty($actionIds)){
				$crDateTime = getCurentDateTime();
				$actionIds = json_decode($actionIds, true);
				foreach($actionIds as $actionId) $valueData[] = array('RoleId' => $roleId, 'ActionId' => $actionId, 'PartId' => $partId, 'CrUserId' => $user['UserId'], 'CrDateTime' => $crDateTime);
			}
			$this->load->model('Mroleactions');
			$flag = $this->Mroleactions->updateBatch($roleId, $partId, $valueData);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Cấp quyên truy cập thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
