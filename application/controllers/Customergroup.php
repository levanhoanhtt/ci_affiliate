<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customergroup extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Nhóm khách hàng',
            array('scriptFooter' => array('js' => 'js/customer_group.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'customer_group')) {
            $this->load->model('Mcustomergroups');
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId >' => 0));
            $this->load->view('customer/customer_group', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('CustomerGroupName', 'CustomerKindId'));
        if(!empty($postData['CustomerGroupName']) && $postData['CustomerKindId'] > 0){
            $postData['StatusId'] = STATUS_ACTIVED;
            $customerGroupId = $this->input->post('CustomerGroupId');
            $this->load->model('Mcustomergroups');
            $flag = $this->Mcustomergroups->save($postData, $customerGroupId);
            if ($flag > 0) {
                $postData['CustomerGroupId'] = $flag;
                $postData['CustomerKindName'] = $this->Mconstants->customerKinds[$postData['CustomerKindId']];
                $postData['IsAdd'] = ($customerGroupId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật Nhóm khách hàng thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $customerGroupId = $this->input->post('CustomerGroupId');
        if($customerGroupId > 0){
            $this->load->model('Mcustomergroups');
            $flag = $this->Mcustomergroups->changeStatus(0, $customerGroupId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Nhóm khách hàng thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
