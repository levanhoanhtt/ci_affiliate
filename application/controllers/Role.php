<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Chức vụ',
			array('scriptFooter' => array('js' => 'js/role.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($listActions, 'role')) {
			$this->load->model(array('Mroles','Mparts'));
			$data['listRoles'] = $this->Mroles->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listParts'] = $this->Mparts->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/role', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('RoleName','PartId'));
		if(!empty($postData['RoleName']) &&  $postData['PartId'] > 0) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$roleId = $this->input->post('RoleId');
			$this->load->model(array('Mroles','Mparts'));
			$flag = $this->Mroles->save($postData, $roleId);
			if ($flag > 0) {
				$partName = $this->Mparts->getFieldValue(array('PartId' => $postData['PartId']), 'PartName');
				$postData['RoleId'] = $flag;
				$postData['PartName'] = $partName;
				$postData['IsAdd'] = ($roleId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Chức vụ thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin();
		$roleId = $this->input->post('RoleId');
		if($roleId > 0){
			$this->load->model('Mroles');
			$flag = $this->Mroles->changeStatus(0, $roleId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Chức vụ thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function listRole(){
		$this->checkUserLogin();
		$partId = $this->input->post('PartId');
		$this->load->model(array('Mroles'));
		if($partId == 0) $listRoles = $this->Mroles->getBy(array('StatusId' => STATUS_ACTIVED));
		else $listRoles = $this->Mroles->getBy(array('StatusId' => STATUS_ACTIVED, 'PartId' => $partId));
		echo json_encode(array('code' => 1, 'data' => $listRoles));
	}
}
