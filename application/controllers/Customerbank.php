<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customerbank extends MY_Controller {

	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách tài khoản ngân hàng',
            array('scriptFooter' => array('js' => 'js/customerbank.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'customerbank')) {
            $this->loadModel(array('Mcustomerbanks', 'Mcustomers'));
            $postData = $this->arrayFromPost(array('CustomerId', 'BankNumber', 'BankName', 'BankHolder','BranchName', 'StatusId'));
            $rowCount = $this->Mcustomerbanks->getCount($postData);
            $data['listCustomerBanks'] = array();
            $data['listCustomers'] = $this->Mcustomers->getBy(array('StatusId' => STATUS_ACTIVED));
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;

                $data['listCustomerBanks'] = $this->Mcustomerbanks->search($postData, $perPage, $page);
            }
            $this->load->view('customerbank/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

   
    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Ngân hàng CTV',
            array('scriptFooter' => array('js' => 'js/customerbank_update.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customerbank')) {
            $this->loadModel(array('Mcustomerbanks'));
            $this->load->view('customerbank/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }


    public function edit($customerBankId = 0){
        if($customerBankId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Ngân hàng CTV',
                array('scriptFooter' => array('js' => 'js/customerbank_update.js'))
            );
            $this->loadModel(array('Mcustomerbanks'));
            $customerbank = $this->Mcustomerbanks->get($customerBankId);
            if ($customerbank) {
                if($this->Mactions->checkAccess($data['listActions'], 'customerbank')) {
                    $data['customerBankId'] = $customerBankId;
                    $data['customerbank'] = $customerbank;
                    $this->load->view('customerbank/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['customerBankId'] = 0;
                $data['txtError'] = "Không tìm thấy Nhà cung cấp";
                $this->load->view('customerbank/edit', $data);
            }
        }
        else redirect('customerbank');
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $customerBankId = $this->input->post('CustomerBankId');
        $statusId = $this->input->post('StatusId');
        if($customerBankId > 0 && $statusId >= 0) {
            $this->load->model('Mcustomerbanks');
            $flag = $this->Mcustomerbanks->changeStatus($statusId, $customerBankId ,'StatusId', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa Tài khoản ngân hàng thành công";
                else{
                     $txtSuccess = "Đổi trạng thái thành công";
                     $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }


   

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BankNumber', 'BankName', 'BankHolder', 'BranchName','CustomerId'));
        
        $customerBankId = $this->input->post('CustomerBankId');
        $this->load->model('Mcustomerbanks');
        $checkBanks = $this->Mcustomerbanks->checkBank($customerBankId, $postData['BankNumber'], $postData['BankHolder']);
        // var_dump($checkBanks);
        if($checkBanks == 0){
            if($customerBankId == 0){
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
                $postData['StatusId'] = STATUS_ACTIVED;
            }
            else{
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            
            $customerBankId = $this->Mcustomerbanks->update($postData, $customerBankId);
            if ($customerBankId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Ngân hàng CTV thành công", 'data' => $customerBankId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }else echo json_encode(array('code' => -1, 'message' => "Tài khoản ngân hàng bị trùng"));
        
    }
}