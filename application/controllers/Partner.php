<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách đối tác',
            array('scriptFooter' => array('js' => 'js/partner.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'partner')) {
            $this->load->model('Mpartners');
            $data['listPartners'] = $this->Mpartners->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/partner', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('PartnerName'));
        if(!empty($postData['PartnerName'])){
            $postData['StatusId'] = STATUS_ACTIVED;
            $partnerId = $this->input->post('PartnerId');
            if($partnerId > 0){
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            else{
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            $this->load->model('Mpartners');
            $flag = $this->Mpartners->save($postData, $partnerId);
            if ($flag > 0) {
                $postData['PartnerId'] = $flag;
                $postData['IsAdd'] = ($partnerId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật đối tác thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $user = $this->checkUserLogin(true);
        $partnerId = $this->input->post('PartnerId');
        if($partnerId > 0){
            $this->load->model('Mpartners');
            $flag = $this->Mpartners->changeStatus(0, $partnerId, '', $user['UserId']);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa đối tác thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}