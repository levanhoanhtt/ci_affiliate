<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends MY_Controller{

    /*public function index(){
        $str = file_get_contents ('assets/sql/data.json');
        $arr = json_decode($str, true);
        $data = $arr['Data'];
        foreach($data as $k => $v) {
            $provinceId = $v['Id'];
            $provinceName = $v['Name'];
            $data_province = array(
                'ProvinceId' => $provinceId,
                'ProvinceName' => $provinceName,
                'DisplayOrder' => 1
            );
            //$this->db->insert('provinces', $data_province);
            $districts = isset($v['Districts']) ? $v['Districts'] : array();
            foreach($districts as $d) {
                $districtId = $d['Id'];
                $districtData = array(
                    'DistrictId' => $districtId,
                    'ProvinceId'      => $provinceId,
                    'DistrictName' => $d['DistrictName'],
                    'GHNSupport'   => $d['GHNSupport'],
                    'TTCSupport'   => $d['TTCSupport'],
                    'VNPTSupport'  => $d['VNPTSupport'],
                    'ViettelPostSupport' => $d['ViettelPostSupport'],
                    'ShipChungSupport' => $d['ShipChungSupport'],
                    'GHNDistrictCode' => $d['GHNDistrictCode'],
                    'ViettelPostDistrictCode' => $d['ViettelPostDistrictCode'],
                    'ShipChungDistrictCode' => $d['ShipChungDistrictCode'],
                    'DisplayOrder' => 1
                );
                //$this->db->insert('districts', $districtData);
                $wards = isset($d['Wards']) ? $d['Wards'] : array();
                $wardData = array();
                foreach($wards as $w){
                    $wardData[] = array(
                        'WardId' => $w['Id'],
                        'WardName' => $w['WardName'],
                        'ProvinceId'      => $provinceId,
                        'DistrictId' => $districtId,
                        'ShipChungSupport' => $w['ShipChungSupport'],
                        'ShipChungWardCode' => $w['ShipChungWardCode']
                    );
                }
                //if(!empty($wardData)) $this->db->insert_batch('wards', $wardData);
            }
            echo $provinceName.'<br/>';
        }
    }*/

    public function test(){
        echo getDayDiff('2018-03-20 12:32:14', new DateTime(date('Y-m-d')));
    }
}