<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linkaff extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user, 'Trình tạo link Affiliate',
            array(
                //'scriptHeader' => array('css' => array('css/link.css')),
                'scriptFooter' => array('js' => array('js/link_aff.js')),
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'linkaff')) {
            $this->loadModel(array('Mlinkaffs', 'Mapproachtypes', 'Mlinkgroups', 'Mlinkgroupproducts', 'Mproducts'));
            $data['listApproachTypes'] = $this->Mapproachtypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listLinkGroups'] = $this->Mlinkgroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $rowCount = $this->Mlinkaffs->getCount(array());
            $data['listAffs'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listAffs'] = $this->Mlinkaffs->search(array(), $perPage, $page);
                //$data['listProducts'] = pluckData('ProductId', 'ProductName', $this->Mproducts->get());
                //$data['listGroupAffs'] = $this->Mlinkgroupproducts->groupLinkGroup();
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('linkaff/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        //$postData = $this->arrayFromPost(array('ProductId', 'ProductChildId', 'OriginalLink', 'ShortLink', 'LinkGroupId'));
        $postData = $this->arrayFromPost(array('OriginalLink', 'LinkGroupId'));
        if(!empty($postData['OriginalLink'])){
            $this->loadModel(array('Mproducts', 'Mlinkaffs'));
            $flag = $this->Mlinkaffs->getFieldValue(array('OriginalLink' => $postData['OriginalLink'], 'CustomerId' => $user['UserId']), 'LinkAffId', 0);
            if($flag > 0) {
                $originalLink = str_replace('https://ricky.vn/products/', '', $postData['OriginalLink']);
                $productId = $this->Mproducts->getFieldValue(array('ProductSlug' => $originalLink, 'ProductStatusId' => STATUS_ACTIVED, 'CookieTimeout >' => 0), 'ProductId', 0);
                if ($productId > 0) {
                    $postData['ProductId'] = $productId;
                    $postData['ProductChildId'] = 0;
                    $linkAffId = $this->input->post('LinkAffId');
                    $shotLink = DOMAIN_SHORT . substr(md5(microtime() * rand(0, 9999)), 0, 15);
                    /*while(true){
                        $flag = $this->Mlinkaffs->getFieldValue(array('ShortLink' => $shotLink), 'LinkAffId', 0);
                        if($flag == $linkAffId) $shotLink = DOMAIN_SHORT.substr(md5(microtime()*rand(0,9999)), 0, 15);
                        else break;
                    }*/
                    $postData['ShortLink'] = $shotLink;
                    if ($linkAffId > 0) {
                        $postData['UpdateUserId'] = $user['UserId'];
                        $postData['UpdateDateTime'] = getCurentDateTime();
                    }
                    else {
                        $postData['CustomerId'] = $user['UserId'];
                        $postData['CrUserId'] = $user['UserId'];
                        $postData['CrDateTime'] = getCurentDateTime();
                    }
                    $linkAffId = $this->Mlinkaffs->save($postData, $linkAffId);
                    if($linkAffId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Link Affiliate thành công", 'data' => $postData));
                    else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy sản phẩm"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Link sản phẩm đã tồn tại"));

        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}