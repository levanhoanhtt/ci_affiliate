<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotiontype extends MY_Controller {
	public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Loại khuyến mại',
            array('scriptFooter' => array('js' => 'js/promotion_type.js'))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'promotiontype')) {
            $this->load->model('Mpromotiontypes');
            $data['listPromotionTypes'] = $this->Mpromotiontypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/promotion_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('PromotionTypeName'));
        if(!empty($postData['PromotionTypeName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $promotionTypeId = $this->input->post('PromotionTypeId');
            $this->load->model('Mpromotiontypes');
            $flag = $this->Mpromotiontypes->save($postData, $promotionTypeId);
            if ($flag > 0) {
                $postData['PromotionTypeId'] = $flag;
                $postData['IsAdd'] = ($promotionTypeId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật loại khuyến mại thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $promotionTypeId = $this->input->post('PromotionTypeId');
        if($promotionTypeId > 0){
            $this->load->model('Mpromotiontypes');
            $flag = $this->Mpromotiontypes->changeStatus(0, $promotionTypeId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa loại khuyến mại thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}