<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends MY_Controller{

    public function index($transactionTypeId = 0){
        $user = $this->checkUserLogin();
        if(!in_array($transactionTypeId, array(1, 2))) $transactionTypeId = 1;
        $data = $this->commonData($user,
            'Danh sách phiếu '.($transactionTypeId == 1 ? 'thu' : 'chi'),
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/transaction_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'transaction/'.$transactionTypeId)) {
            $this->loadModel(array('Mfilters', 'Mstores', 'Mbanks', 'Mtransactionreasons'));
            $itemTypeId = $transactionTypeId == 1 ? 17 : 18;
            $data['itemTypeId'] = $itemTypeId;
            $data['transactionTypeId'] = $transactionTypeId;
            $data['listFilters'] = $this->Mfilters->getList($itemTypeId);
            $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listBanks'] = $this->Mbanks->search(array('StatusId' => STATUS_ACTIVED));
            $data['listTransactionReasons'] = $this->Mtransactionreasons->getBy(array('StatusId' => STATUS_ACTIVED, 'TransactionTypeId' => $transactionTypeId));
            $this->load->view('transaction/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add($transactionTypeId = 0, $customerId = 0){
        if(in_array($transactionTypeId, array(1, 2))) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thêm mới '.$this->Mconstants->transactionTypes[$transactionTypeId],
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js','vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/transaction_update.js'))
                )
            );
            $listAction = $data['listActions'];
            $mAction = $this->Mactions;
            if ($mAction->checkAccess($listAction, 'transaction/'.$transactionTypeId)) {
                if(!is_numeric($customerId) || $customerId < 0) $customerId = 0;
                $data['customerId'] = $customerId;
                $data['transactionTypeId'] = $transactionTypeId;
                $data['canEditLevel'] = $this->canEditLevel($mAction, $listAction);
                $this->loadModel(array('Mmoneyphones', 'Mstores', 'Mbanktypes', 'Mtransactionreasons' , 'Mbanks', 'Mfunds', 'Mstorefunds', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards','Musers','Mcustomergroups', 'Mtags'));
                $whereStatus = array('StatusId' => STATUS_ACTIVED);
                $data['listMoneyPhones'] = $this->Mmoneyphones->getBy($whereStatus);
                $data['listBankTypes'] = $this->Mbanktypes->getBy($whereStatus);
                $data['listBanks'] = $this->Mbanks->search($whereStatus);
                $data['listCustomerGroups'] = $this->Mcustomergroups->getBy($whereStatus);
                $whereStatus = array('ItemStatusId' => STATUS_ACTIVED);
                $data['listStores'] = $this->Mstores->getBy($whereStatus);
                $listFunds = $this->Mfunds->getBy($whereStatus);
                $data['listFunds'] = $listFunds;
                $data['listCountries'] = $this->Mcountries->getList();
                $data['listProvinces'] = $this->Mprovinces->getList();
                $data['listDistricts'] = $this->Mdistricts->getList();
                $data['listUsers'] = $this->Musers->getListForSelect();
                $data['listTransactionReasons'] = $this->Mtransactionreasons->getBy(array('StatusId' => STATUS_ACTIVED, 'TransactionTypeId' => $transactionTypeId));
                $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $transactionTypeId == 1 ? 17 : 18));
                $listStoreFunds = $this->Mstorefunds->get();
                $data['listStoreFunds'] = $listStoreFunds;
                $fundId = 0;
                $storeId = 0;
                foreach ($listFunds as $f){
                    if($f['TreasureId'] == $user['UserId']){
                        $fundId = $f['FundId'];
                        break;
                    }
                }
                if($fundId > 0) $storeId = $this->Mconstants->getObjectValue($listStoreFunds, 'FundId', $fundId, 'StoreId');
                $data['fundId'] = $fundId;
                $data['storeId'] = $storeId;
                $this->load->view('transaction/add_'.$transactionTypeId, $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('transaction');
    }

    public function edit($transactionId = 0){
        if ($transactionId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật phiếu thu chi',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'js/transaction_update.js',))
                )
            );
            $this->loadModel(array('Mtransactions', 'Mcustomers', 'Mmoneyphones', 'Mbanktypes', 'Mstores', 'Mfunds', 'Mstorefunds', 'Mtags', 'Mactionlogs', 'Mtransactionreasons', 'Musers', 'Mbanks','Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards','Musers','Mcustomergroups'));
            $transaction = $this->Mtransactions->get($transactionId);
            if($transaction) {
                $transactionTypeId = $transaction['TransactionTypeId'];
                $listAction = $data['listActions'];
                $mAction = $this->Mactions;
                if ($mAction->checkAccess($listAction, 'transaction/'.$transactionTypeId)){
                    $data['title'] = 'Cập nhật phiếu ' . $this->Mconstants->transactionTypes[$transactionTypeId];
                    $data['canEditLevel'] = $this->canEditLevel($mAction, $listAction);
                    $data['canEdit'] = $transaction['VerifyLevelId'] == 1;
                    $data['transactionTypeId'] = $transactionTypeId;
                    $data['transaction'] = $transaction;
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listMoneyPhones'] = $this->Mmoneyphones->getBy($whereStatus);
                    $data['listBankTypes'] = $this->Mbanktypes->getBy($whereStatus);
                    $data['listCustomerGroups'] = $this->Mcustomergroups->getBy($whereStatus);
                    $data['listBanks'] = $this->Mbanks->search($whereStatus);
                    $whereStatus = array('ItemStatusId' => STATUS_ACTIVED);
                    $data['listStores'] = $this->Mstores->getBy($whereStatus);
                    $data['listFunds'] = $this->Mfunds->getBy($whereStatus);
                    $data['listStoreFunds'] = $this->Mstorefunds->get();
                    $itemTypeId = $transactionTypeId == 1 ? 17 : 18;
                    //$data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 10));
                    $data['tagNames'] = $this->Mtags->getTagNames($transactionId, $itemTypeId);
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => $itemTypeId));
                    $data['listActionLogs'] = $this->Mactionlogs->getList($transactionId, $itemTypeId);
                    $data['listTransactionReasons'] = $this->Mtransactionreasons->getBy(array('StatusId' => STATUS_ACTIVED, 'TransactionTypeId' => $transactionTypeId));
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                    $data['listUsers'] = $this->Musers->getListForSelect();
                    $this->load->view('transaction/edit_'.$transactionTypeId, $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else{
                $data['canEdit'] = false;
                $data['transactionTypeId'] = 0;
                $data['txtError'] = "Không tìm thấý phiếu";
                $this->load->view('transaction/edit_1', $data);
            }
        }
        else redirect('transaction');
    }

    private function canEditLevel($mAction, $listAction){
        $canEditLevel = 0;
        for($i = 2; $i > 0; $i--) {
            if ($mAction->checkAccess($listAction, 'transaction/editLevel_'.$i)){
                $canEditLevel = $i;
                break;
            }
        }
        return $canEditLevel;
    }
}