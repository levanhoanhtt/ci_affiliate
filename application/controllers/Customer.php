<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách CTV',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/customer_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
            $this->loadModel(array('Mfilters', 'Mcustomergroups'));
            $data['listFilters'] = $this->Mfilters->getList(5);
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('customer/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    /*public function collaborator(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'CTV',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/collaborator_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
            $this->loadModel(array('Mfilters', 'Mcustomergroups'));
            $data['listFilters'] = $this->Mfilters->getList(5);
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('customer/collaborator', $data);
        }
        else $this->load->view('user/permission', $data);
    }*/

    /*public function editCollaborator($customerId = 0){
        if ($customerId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Chứng minh thư nhân dân',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'js/search_item.js', 'js/collaborator_update.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
                $this->loadModel(array('Mcustomers'));
                $customer = $this->Mcustomers->get($customerId);
                if ($customer) {
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                }else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy CTV";
                }
                $this->load->view('customer/collaborator_edit', $data);
            }else $this->load->view('user/permission', $data);
        }
        else redirect('customer');
    }*/

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới CTV',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/customer_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customer') && $data['appTypeId'] == ADMIN_TYPE_ID) {
            $this->loadModel(array('Mcustomergroups', 'Mprovinces', 'Mdistricts', 'Mtags', 'Mcountries', 'Mwards','Mcustomers'));
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCountries'] = $this->Mcountries->getList();
            //$data['listUsers'] = $this->Musers->getListForSelect();
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 5));
            $this->load->view('customer/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($customerId = 0){
        if($customerId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thông tin CTV',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/customer_update.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
                $this->loadModel(array('Mcustomers', 'Mcustomergroups', 'Mprovinces', 'Mdistricts', 'Mcustomercomments', 'Mtags', 'Mactionlogs', 'Mcountries', 'Mwards', 'Morders','Mcustomerbanks'));
                $customer = $this->Mcustomers->get($customerId);
                if ($customer) {
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                    $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                    //$data['listUsers'] = $this->Musers->getListForSelect();
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 5));
                    $data['tagNames'] = $this->Mtags->getTagNames($customerId, 5);
                    $data['customerbank'] = $this->Mcustomerbanks->getBy(array('CustomerId' => $customerId));
                    //$data['listImages'] = $this->Mfiles->getFileUrls($customerId, 5, 1);
                    /*$data['listCustomerComments'] = $this->Mcustomercomments->getListByCustomerId($customerId);
                    $listOrders = $this->Morders->search(array('CustomerId' => $customerId), 0, 1, 'OrderStatusId, TotalCost');
                    $data['totalOrder'] = count($listOrders);
                    $successOrder = 0;
                    $successCost = 0;
                    foreach($listOrders as $o){
                        if($o['OrderStatusId'] == 6){
                            $successOrder++;
                            $successCost += $o['TotalCost'];
                        }
                    }
                    $data['successOrder'] = $successOrder;
                    $data['successCost'] = $successCost;*/
                    //$data['listActionLogs'] = $this->Mactionlogs->getList($customerId, 5);
                }
                else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy CTV";
                }
                $this->load->view('customer/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('customer');
    }


    /*public function edit($customerId = 0, $tabId = 0){
        if($customerId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thông tin CTV',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/customer_update.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
                $this->loadModel(array('Mcustomers', 'Mcustomergroups', 'Mprovinces', 'Mdistricts', 'Mcustomercomments', 'Mtags', 'Mactionlogs', 'Mcountries', 'Mwards', 'Morders', 'Mfiles'));
                $customer = $this->Mcustomers->get($customerId);
                if ($customer) {
                    if(!in_array($tabId, array(1, 2, 3))) $tabId = 1;
                    $data['tabId'] = $tabId;
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                    $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                    $data['listUsers'] = $this->Musers->getListForSelect();
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 5));
                    $data['tagNames'] = $this->Mtags->getTagNames($customerId, 5);
                    $data['listImages'] = $this->Mfiles->getFileUrls($customerId, 5, 1);
                    $data['listCustomerComments'] = $this->Mcustomercomments->getListByCustomerId($customerId);
                    $listOrders = $this->Morders->search(array('CustomerId' => $customerId), 0, 1, 'OrderStatusId, TotalCost');
                    $data['totalOrder'] = count($listOrders);
                    $successOrder = 0;
                    $successCost = 0;
                    foreach($listOrders as $o){
                        if($o['OrderStatusId'] == 6){
                            $successOrder++;
                            $successCost += $o['TotalCost'];
                        }
                    }
                    $data['successOrder'] = $successOrder;
                    $data['successCost'] = $successCost;
                    //$data['listActionLogs'] = $this->Mactionlogs->getList($customerId, 5);
                }
                else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy CTV";
                }
                $this->load->view('customer/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('customer');
    }*/

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $customerId = $this->input->post('CustomerId');
        $statusId = $this->input->post('StatusId');
        if($customerId > 0 && $statusId >= 0) {
            $this->load->model('Mcustomers');
            $flag = $this->Mcustomers->changeStatus($statusId, $customerId ,'StatusId', $user['UserId']);
            if($flag) {
                $statusName = "";
                if($statusId == 0) $txtSuccess = "Xóa Tài khoản CTV thành công";
                else{
                     $txtSuccess = "Đổi trạng thái thành công";
                     $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    /*public function resetPassword($customerId = 0){
        if($customerId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Đổi mật khẩu CTV: ',
                array(
                    'scriptFooter' => array('js' => array('js/reset_password.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
                $this->loadModel(array('Mcustomers'));
                $customer = $this->Mcustomers->get($customerId);
                if ($customer) {
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                }else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy CTV";
                }
                $this->load->view('customer/reset_password', $data);
            }
        }else redirect('customer');
    }*/
    
}