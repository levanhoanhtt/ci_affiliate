ALTER TABLE `linklogs` ADD COLUMN `StatusId` TINYINT NOT NULL  AFTER `UserAgent` ;
ALTER TABLE `linklogs` ADD COLUMN `CookieStatusId` TINYINT NOT NULL  AFTER `StatusId` ;
ALTER TABLE `products` ADD COLUMN `ProductPartnerId` INT NOT NULL  AFTER `CookieTimeout` , ADD COLUMN `PartnerId` SMALLINT NOT NULL  AFTER `ProductPartnerId` ;
ALTER TABLE `affiliate`.`partners` DROP COLUMN `CustomerId` ;