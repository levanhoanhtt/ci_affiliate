CREATE  TABLE `affiliate`.`partners` (
  `PartnerId` SMALLINT NOT NULL AUTO_INCREMENT ,
  `PartnerName` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  `CrUserId` INT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`PartnerId`) );

  CREATE  TABLE `affiliate`.`customerbanks` (
  `CustomerBankId` INT NOT NULL AUTO_INCREMENT ,
  `CustomerId` INT NOT NULL ,
  `BankNumber` VARCHAR(45) NOT NULL ,
  `BankName` VARCHAR(250) NOT NULL ,
  `BankHolder` VARCHAR(250) NOT NULL ,
  `BranchNamr` VARCHAR(250) NOT NULL ,
  `StatusId` TINYINT NOT NULL ,
  `CrUserId` TINYINT NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  `UpdateUserId` INT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`CustomerBankId`) );
ALTER TABLE `customers` ADD `CustomerCode` VARCHAR(100) NOT NULL AFTER `CustomerId`;

CREATE  TABLE `affiliate`.`linklogs` (
  `LinkLogId` INT NOT NULL AUTO_INCREMENT ,
  `LinkAffId` INT NOT NULL ,
  `StatusId` INT NOT NULL ,
  `IpAddress` VARCHAR(15) NOT NULL ,
  `UserAgent` VARCHAR(250) NOT NULL ,
  `CrDateTime` DATETIME NOT NULL ,
  PRIMARY KEY (`LinkLogId`) );