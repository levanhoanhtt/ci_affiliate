ALTER TABLE `affiliate`.`orders` DROP COLUMN `CancelComment` , DROP COLUMN `CancelReasonId` , ADD COLUMN `OrderParnerId` INT NOT NULL  AFTER `OrderTypeId` ;
ALTER TABLE `affiliate`.`orderproducts` CHANGE COLUMN `PriceCapital` `LinkAffId` INT(11) NOT NULL  ;
ALTER TABLE `affiliate`.`customers` ADD COLUMN `CustomerCode` VARCHAR(45) NULL  AFTER `CustomerId` ;