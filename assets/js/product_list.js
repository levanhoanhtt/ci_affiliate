$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
    $('#aImport').click(function(){
        $('#modalImportProduct').modal('show');
        return false;
    });
    $('#btnImportProduct').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $('.imgLoading').show();
        $.ajax({
            type: "POST",
            url: $('input#importProductUrl').val(),
            data: {
                PartnerId: 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) redirect(true, '');
                else{
                    btn.prop('disabled', false);
                    $('.imgLoading').hide();
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
                $('.imgLoading').hide();
            }
        });
    });
});


function renderContentProducts(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].ProductId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].ProductId + '"></td>';
            html += '<td><a href="' + ((data[item].ProductKindId == 3) ? urlEditProductCombo + data[item].ProductId : urlEditProduct + data[item].ProductId)  + '">' + data[item].ProductName + '</a></td>';
            html += '<td>'+ (data[item].ProductTypeName != null ? data[item].ProductTypeName : '') +'</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductKindId] + '">' + data[item].ProductKind + '</span></td>';
            html += '<td>'+ ((data[item].SupplierName != null && data[item].ProductKindId != 3) ? data[item].SupplierName : '') + '</td>';
            html += '<td>'+ data[item].BarCode + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductStatusId] + '">' + data[item].ProductStatus + '</span></td>';
            html += '<td class="text-center">'+ data[item].DiscountPercent + ' (%)</td>';
            html += '<td class="text-center">'+ data[item].CookieTimeout + '</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="9" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}