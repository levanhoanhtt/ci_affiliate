$(document).ready(function(){
	dateRangePicker();
	time();


	$("body").on('click', '.submit', function(){
		time();
	})
	
});

function time(){
	var beginDate = '';
	var flag = false;
	var dates = $("#dateRangePicker").val().trim().split('-');
	if(dates.length == 2){
		beginDate =  dates[0].trim();
		if(beginDate != '') getReport(beginDate, dates[1].trim());
		else flag = true;
	}
	else if(dates.length == 1){
		beginDate =  dates[0].trim();
		if(beginDate != '') getReport(beginDate, '');
		else flag = true;
	}
	else flag = true;
	if(flag) showNotification("Vui lòng nhập ngày bắt đầu", 0);
}

function getReport(beginDate, endDate){
	$.ajax({
        type: "POST",
        url: $('input#searchDataEffctualUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate
        },
        success: function (response) {

            var json = $.parseJSON(response);

            console.log(json.data)
            $(".total-click").html('Lược click ('+json.data.linkLogs.totalClick+')');
            $(".total-click-1").html(json.data.linkLogs.totalClick+' Click');
            $(".frequency-click").html(json.data.linkLogs.frequency+' tần xuất Click');
            $(".percent-total-order").html(json.data.linkLogs.percentTotalOrder+'% đơn hàng/Click');

            $(".total-orders").html('Đơn hàng ('+json.data.Orders.order.totalOrders+')');
            $(".order-success").html('Thành công '+json.data.Orders.order.orderSuccess+' ('+json.data.Orders.order.percentOrderSuccess+'%)');
            $(".order-processing").html('Đang xử lý '+json.data.Orders.order.orderProcessing+' ('+json.data.Orders.order.percentOrderProcessing+'%)');
            $(".order-failure").html('Thất bại '+json.data.Orders.order.orderFailure+' ('+json.data.Orders.order.percentOrderFailure+'%)');
        
            $(".total-commission").html('Hoa hồng ('+formatDecimal(json.data.Orders.commission.totalCommission.toString())+'đ)');
            $(".commission-success").html('Thành công '+formatDecimal(json.data.Orders.commission.discountSuccess.toString())+'đ ('+json.data.Orders.commission.percentCommissionSuccess+'%)');
            $(".commission-waitting-processing").html('Chờ xử lý '+formatDecimal(json.data.Orders.commission.discountWaittingProcessing.toString())+'đ ('+json.data.Orders.commission.percentCommissionWaittingProcessing+'%)');
            $(".commission-failure").html('Thất bại '+formatDecimal(json.data.Orders.commission.discountFailure.toString())+'đ ('+json.data.Orders.commission.percentCommissionFailure+'%)');
        
            // Biểu đồ tiền hoa hồng
			Highcharts.chart('commission', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: 'Tiền hoa hồng'+formatDecimal(json.data.Orders.commission.totalCommission.toString())+'đ',
			    },
			    subtitle: {
			        text: ''
			    },
			    xAxis: {
			        type: 'category',
			        labels: {
			            rotation: -45,
			            style: {
			                fontSize: '13px',
			                fontFamily: 'Verdana, sans-serif'
			            }
			        }
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Tiền hoa hồng'+formatDecimal(json.data.Orders.commission.totalCommission.toString())+'đ',
			        }
			    },
			    legend: {
			        enabled: false
			    },
			    tooltip: {
			        pointFormat: 'Tiền hoa hồng: <b>{point.y:.1f} VNĐ</b>'
			    },
			    series: [{
			        name: 'Tiền hoa hồng',
			        data: [
			            ['Thành công', json.data.Orders.commission.discountSuccess],
			            ['Chờ xử lý', json.data.Orders.commission.discountWaittingProcessing],
			            ['Thất bại', json.data.Orders.commission.discountFailure],
			            
			        ],
			        dataLabels: {
			            enabled: true,
			            rotation: -90,
			            color: '#FFFFFF',
			            align: 'right',
			            format: '{point.y:.1f}', // one decimal
			            y: 10, // 10 pixels down from the top
			            style: {
			                fontSize: '13px',
			                fontFamily: 'Verdana, sans-serif'
			            }
			        }
			    }]
			});

			// biểu đồ quạt
			Highcharts.chart('fan_chart', {
			    chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			    },
			    title: {
			        text: 'Biểu đồ quạt % tiền hoa hồng'
			    },
			    tooltip: {
			        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			    },
			    plotOptions: {
			        pie: {
			            allowPointSelect: true,
			            cursor: 'pointer',
			            dataLabels: {
			                enabled: false
			            },
			            showInLegend: true
			        }
			    },
			    series: [{
			        name: 'Phần trăm',
			        colorByPoint: true,
			        data: [{
			            name: 'Thành công',
			            y: json.data.Orders.commission.percentCommissionSuccess,
			            sliced: true,
			            selected: true
			        }, {
			            name: 'Chờ xử lý',
			            y: json.data.Orders.commission.percentCommissionWaittingProcessing
			        }, {
			            name: 'Thất bại',
			            y: json.data.Orders.commission.percentCommissionFailure
			        }]
			    }]
			});

			// Biểu đồ san pham bán chạy
			Highcharts.chart('best_selling', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: '3 sản phẩm bán chạy nhất',
			    },
			    subtitle: {
			        text: ''
			    },
			    xAxis: {
			        type: 'category',
			        labels: {
			            rotation: -45,
			            style: {
			                fontSize: '13px',
			                fontFamily: 'Verdana, sans-serif'
			            }
			        }
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: '3 sản phẩm bán chạy nhất',
			        }
			    },
			    legend: {
			        enabled: false
			    },
			    tooltip: {
			        pointFormat: 'Tên sản phẩm: <b>{point.y:.1f}</b>'
			    },
			    series: [{
			        name: 'Sản phẩm bán chạy nhất',
			        data: [
			            [json.data.Orders.product.p1.ProductName, parseInt(json.data.Orders.product.p1.Count)],
			            [json.data.Orders.product.p2.ProductName, parseInt(json.data.Orders.product.p2.Count)],
			            [json.data.Orders.product.p3.ProductName, parseInt(json.data.Orders.product.p3.Count)],
			            
			        ],
			        dataLabels: {
			            enabled: true,
			            rotation: -90,
			            color: '#FFFFFF',
			            align: 'right',
			            format: '{point.y:.1f}', // one decimal
			            y: 10, // 10 pixels down from the top
			            style: {
			                fontSize: '13px',
			                fontFamily: 'Verdana, sans-serif'
			            }
			        }
			    }]
			});

        }
    });
}