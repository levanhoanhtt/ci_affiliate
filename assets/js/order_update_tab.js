var app = app || {};

app.init = function (orderId, canEdit, transportId) {
    app.initLibrary();
    app.customer(canEdit, orderId);
    if(canEdit == 1){
        app.promotion();
        app.configTransport();
        app.configPayment();
        app.configExpand();
        app.sendOffset(); //gui bu
        if($('input#deliveryTypeId').val() == '1') app.orderPOS();
    }
    app.orderComment(orderId);
    app.product(canEdit);
    app.checkOrder();
    app.chooseStore(orderId);
    if(orderId > 0){
        if($('.tbodyProduct tr').length > 0) calcPrice(0);
        app.cancelOrder(orderId);
        app.verifyOrder(orderId);
        app.updatePendingStatus(orderId);
        //app.createOrderChild(orderId);
        if (transportId == 0 || $('input#transportStatusId').val() == '5') app.addTransport(orderId);
        else app.cancelTransport(transportId, orderId);
        app.shareOrder(orderId);
        app.orderReason(orderId, canEdit);
    }
    app.remind();
};

app.initLibrary = function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('body').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.cost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).click(function (e) {
        var div = $('#boxPromotion');
        if(div.css('display') == 'block' && !div.is(':hover')) div.toggle();
        div = $('#boxTransport');
        if(div.css('display') == 'block' && !div.is(':hover')) div.toggle();
    });
    $('#orderForm').on('submit', function (e) {
        e.preventDefault();
    });
    if($('.bxslider').length > 0) {
        var minNumber = 3;
        var maxNumber = 3;
        if($('.bxslider.short').length > 0) {
            minNumber = 3;
            maxNumber = 3;
        }
        else{
            minNumber = 4;
            maxNumber = 4;
        }
        if ($(window).width() < 1300) {
            if($('.bxslider.short').length > 0) {
                minNumber = 2;
                maxNumber = 2;
            }
            else{
                minNumber = 3;
                maxNumber = 3;
            }
        }
        $('.bxslider').bxSlider({
            minSlides: minNumber,
            maxSlides: maxNumber,
            moveSlides: 1,
            slideWidth: 500,
            pager: false,
            infiniteLoop: false,
            nextText: ">",
            prevText: "<"
        });
    }
};

app.customer = function(canEdit, orderId){
    if($('input#verifyStatusId').val() == '2'){
        if(!canEdit || $('input#config_ALLOW_VERIFY_ORDER').val() == 'ON') $('#btnCloseBoxCustomer_'+$("#idcheck").val()).remove();
    }
    if(canEdit) {
        chooseCustomer(function (li) {
            var customerId = parseInt(li.attr('data-id'));
            $('input#customerId').val('0');
            $('.btnTransport').addClass('actived');
            //$('#aCustomer').attr('href', 'javascript:void(0)').text(''); divCustomer
            $('p#customerGroupName').text('');
            $('input#customerName').val('');
            $('input#customerEmail').val('');
            $('input#customerPhone').val('');
            $('input#customerAddress').val('');
            if (customerId > 0) showInfoCustomer(customerId, true);
        });
        province('provinceId', 'districtId', 'wardId');
        province('customerProvinceId', 'customerDistrictId', 'customerWardId');
        $('input.iCheckCustomerKind').on('ifToggled', function(e){
            if(e.currentTarget.checked){
                if(e.currentTarget.value == '2') $('#divWholesale').show();
                else{
                    $('input#debtCost, select#paymentTimeId').val('0');
                    $('#divWholesale').hide();
                }
            }
        });
        $('input.iCheckCustomerType').on('ifToggled', function (e) {
            if (e.currentTarget.checked) {
                if (e.currentTarget.value == '2') $('#divCompany').show();
                else $('#divCompany').hide();
            }
        });
        $('select#countryId, select#customerCountryId').change(function () {
            if ($(this).val() == '232' || $(this).val() == '0') {
                $('.VNoff').css('display', 'none');
                $('.VNon').fadeIn();
            }
            else {
                $('.VNon').css('display', 'none');
                $('.VNoff').fadeIn();
            }
        });
        $("body").on('click', '.btnCloseBoxCustomer', function(){
            var idCheck = $("#idcheck").val();
            if(idCheck == $(this).attr('id-check')){
                $('#divCustomer_'+idCheck).hide();
                $('#boxChooseCustomer_'+idCheck).show();
                $('input#customer_'+idCheck).val('0');
                $('.btnTransport').addClass('actived');
                return false;
            }
        });

        $("body").on('click', '.btnAddCustomer', function(){
            var idcheck = $("#idcheck").val();
            if(idcheck == $(this).attr('id-check')){
                $('#customerForm').trigger('reset');
                $('#modalAddCustomer').modal('show');
            }
            
        });
        $('#btnUpdateCustomer').click(function (){
            var btn = $(this);
            if(validateEmpty('#customerForm')){
                var password = $('input#password').val().trim();
                if(password != ''){
                    if(password != $('input#rePass').val().trim()){
                        showNotification('Mật khẩu không trùng', 0);
                        return false;
                    }
                }
                btn.prop('disabled', true);
                var isReceiveAd = 1;
                if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
                $.ajax({
                    type: "POST",
                    url: $('#customerForm').attr('action'),
                    data: {
                        CustomerId: 0,
                        FirstName: $('input#firstName').val().trim(),
                        LastName: $('input#lastName').val().trim(),
                        //FullName: $('input#fullName').val().trim(),
                        Email: $('input#email').val().trim(),
                        PhoneNumber: $('input#phoneNumber').val().trim(),
                        //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                        GenderId: $('input[name="GenderId"]:checked').val(),
                        StatusId: 2,
                        BirthDay: $('input#birthDay').val(),
                        CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                        CustomerKindId: $('input[name="CustomerKindId"]:checked').val(),
                        CountryId: $('select#countryId').val(),
                        ProvinceId: $('select#provinceId').val(),
                        DistrictId: $('select#districtId').val(),
                        WardId: $('select#wardId').val(),
                        ZipCode: $('input#zipCode').val(),
                        Address: $('input#address').val().trim(),
                        CustomerGroupId: $('select#customerGroupId').val(),
                        FaceBook: '',
                        Comment: '',// $('#customerComment').val().trim(),
                        CareStaffId: $('select#careStaffId').val(),
                        DiscountTypeId: $('select#discountTypeId').val(),
                        PaymentTimeId: $('select#paymentTimeId').val(),
                        PositionName: $('input#positionName').val().trim(),
                        CompanyName: $('input#companyName').val().trim(),
                        TaxCode: $('input#taxCode').val().trim(),
                        DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                        Password: password,
                        //TagNames: JSON.stringify(tags),
                        IsReceiveAd: isReceiveAd
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) {
                            showInfoCustomer(json.data, true);
                            $('#modalAddCustomer').modal('hide');
                        }
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            return false;
        });
    }
    else $('#btnCloseBoxCustomer').remove();
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customer_'+$("#idcheck").val()).val(), orderId == 0);
    $('#aCustomerAddress').click(function(){
        if($('input#orderStatusId').val() == '1') {
            $('input#customerName').val($('.i-name').first().text());
            $('input#customerEmail').val($('.i-email').first().text());
            $('input#customerPhone').val($('.i-phone').first().text());
            $('input#customerAddress').val($('.spanAddress').first().text());
            var iCountry = $('.i-country').first();
            var countryId = iCountry.attr('data-id');
            if (countryId == '0' || countryId == '') countryId = '232';
            $('select#customerCountryId').val(countryId);
            $('select#customerProvinceId').val(iCountry.attr('data-province'));
            $('select#customerDistrictId').val(iCountry.attr('data-district'));
            $('select#customerWardId').val(iCountry.attr('data-ward'));
            $('input#customerZipCode').val(iCountry.attr('data-zip'));
            $('#modalCustomerAddress').modal('show');
        }
        return false;
    });
    $('#btnUpdateCustomerAddress').click(function(){
        var btn = $(this);
        if(validateEmpty('#modalCustomerAddress')) {
            btn.prop('disabled', true);
            var data = {
                CustomerId: $('input#customerId').val(),
                CustomerName: $('input#customerName').val().trim(),
                Email: $('input#customerEmail').val().trim(),
                PhoneNumber: $('input#customerPhone').val().trim(),
                Address: $('input#customerAddress').val().trim(),
                ProvinceId: $('select#customerProvinceId').val(),
                DistrictId: $('select#customerDistrictId').val(),
                WardId: $('select#customerWardId').val(),
                CountryId: $('select#customerCountryId').val(),
                ZipCode: $('input#customerZipCode').val().trim(),

                ItemId: orderId,
                ItemTypeId: 6
            };
            $.ajax({
                type: "POST",
                url: $('input#insertCustomerAddressUrl').val(),
                data: data,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        $('input#customerAddressId').val(json.data);
                        $('.i-name').text(data.CustomerName);
                        $('.i-phone').text(data.PhoneNumber);
                        $('.i-email').text(data.Email);
                        $('.i-country').text($('select#customerCountryId option[value="' + data.CountryId + '"]').text()).attr('data-id', data.CountryId).attr('data-province', data.ProvinceId).attr('data-district', data.DistrictId).attr('data-ward', data.WardId).attr('data-zip', data.ZipCode);
                        var address = '';
                        if(data.CountryId == '232' || data.CountryId == '0') {
                            address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                            var itemName  = '';
                            if(data.WardId != '0') itemName = $('select#customerWardId option[value="' + data.WardId + '"]').text();
                            address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam> ' + itemName + '</span>';
                            itemName = data.DistrictId != '0' ? $('select#customerDistrictId option[value="' + data.DistrictId + '"]').text() : '';
                            if(itemName != '') address += '<span class="br-line i-district">' + itemName + '</span>';
                            itemName = data.ProvinceId != '0' ? $('select#customerProvinceId option[value="' + data.ProvinceId + '"]').text() : '';
                            if(itemName != '') address += '<span class="br-line i-province">' + itemName + '</span>';
                        }
                        else {
                            address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                            address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                        }
                        $('.i-address').html(address);
                        $('#modalCustomerAddress').modal('hide');
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

app.product = function(canEdit){
    
    $("body").on('keydown', 'input.quantity_keyup', function (e) {
        var idCheck = $("#idcheck").val();
        if($(this).attr('id-check') == idCheck){
            if(checkKeyCodeNumber(e)) e.preventDefault();
        }
    }).on('keyup', 'input.quantity_keyup', function () {
        var idCheck = $("#idcheck").val();
        if($(this).attr('id-check') == idCheck){
            var value = $(this).val();
            $(this).val(formatDecimal(value));
            var tr = $(this).parent().parent();
            var price = replaceCost(tr.find('span.originalPrice'+idCheck).text(), true);
            value = replaceCost(value, true);
            var productPrices = tr.find('span.productPrices_'+idCheck).text();
            if(productPrices != ''){
                var productId = tr.attr('data-id');
                var childid = tr.attr('data-child');
                productPrices = $.parseJSON(productPrices);
                for(var i = 0; i < productPrices.length; i++){
                    if(productPrices[i].ProductId == productId && productPrices[i].ProductChildId == childid && value >= parseInt(productPrices[i].Quantity)) price = parseInt(productPrices[i].Price);
                }
            }
            tr.find('.spanPrice_'+idCheck).text(formatDecimal(price.toString()));
            var totalPrice = value * price;
            tr.find('input.sumPrice_'+idCheck).val(formatDecimal(totalPrice.toString()));
            totalPrice = 0;
            $('.tbodyProduct tr').each(function () {
                if($(this).attr('id-check') == idCheck){
                    totalPrice += replaceCost($(this).find('input.sumPrice_'+idCheck).val(), true);
                }
            });
            $('span#totalPrice_'+idCheck).text(formatDecimal(totalPrice.toString()));
            calcPrice(totalPrice);
        }
        
    }).on('click', '.link_delete', function () {
        if(canEdit == 1) {
            var tr = $(this).parent().parent();
            tr.remove();
            calcPrice(0);
            if($('input#transportWeight').length > 0){
                var totalWeight = 0;
                $('.tbodyProduct tr').each(function(){
                    totalWeight += parseInt($(this).attr('data-weight')) * replaceCost($(this).find('input.quantity').val(), true);
                });
                $('input#transportWeight').val(formatDecimal(totalWeight.toString()));
            }
        }
        return false;
    }).on('click', '.aProductLink', function () {
        var tr = $(this).parent().parent();
        if(tr.attr('data-kind') == '3'){
            var productName = $(this).text();
            $.ajax({
                type: "POST",
                url: $('input#getProductChildComboUrl').val(),
                data: {
                    ProductId: tr.attr('data-id')
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        $('#spanProductComboName').text(productName);
                        var products = json.data;
                        var productPath = $('input#productPath').val();
                        var html = '';
                        for (var i = 0; i < products.length; i++) {
                            html += '<tr><td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + products[i].ProductName + '</a></td>';
                            html += '<td class="text-center">' + products[i].BarCode + '</td>';
                            html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                            html += '<td class="text-right">' + formatDecimal(products[i].Price.toString()) + ' ₫</td>';
                            html += '<td class="text-center">' + products[i].Quantity + '</td>';
                            html += '<td class="text-center">';
                            if(products[i].VATStatusId) html += '<span class="label label-success">Có</span>';
                            else html += '<span class="label label-default">Không</span>';
                            html += '</td></tr>';
                        }
                        $('#tbodyProductCombo').html(html);
                        $('#modalProductCombo').modal('show');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    if(canEdit == 1){
        chooseProduct(function (tr) { chooseProductSearch(tr);});
    }
};

app.checkOrder = function(){
    $('#aCheckOrder').click(function(){
        $('#modalCheckQuantity .modal-body').html('');
        var products = getListOrderProducts();
        if(products.length == 0){
            showNotification('Vui lòng chọn sản phẩm', 0);
            return false;
        }
        /*var storeId = parseInt($('input[name="OrderStoreId"]:checked').val());
         storeId = (isNaN(storeId)) ? 0 : storeId;*/
        $.ajax({
            type: "POST",
            url: $('input#checkOrderUrl').val(),
            data: {
                Products: products,
                StoreId: 0//storeId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    var html = '';
                    var j;
                    for(var i = 0; i < data.length; i++){
                        html += '<div class="no-padding">';
                        if(data[i].IsInStock) html += '<label class="text-success"><i class="fa fa-check-circle">';
                        else html += '<label class="text-danger"><i class="fa fa-times-circle">';
                        html += '</i> ' + data[i].StoreName + '</label>';
                        html += '<div class="table-responsive divTable"><table class="table table-hover"><thead class="theadNormal">';
                        html += '<tr><th>Sản phẩm</th><th class="text-center">Số lượng cần</th><th class="text-center">Số lượng tồn</th><th class="text-center"></th></tr></thead><tbody>';
                        for(j = 0; j < data[i].Products.length; j++){
                            html += '<tr>';
                            html += '<td>' + $('.tbodyProduct tr[data-id="'+data[i].Products[j].ProductId+'"][data-child="'+data[i].Products[j].ProductChildId+'"] td').first().text() + '</td>';
                            html += '<td class="text-center">' + data[i].Products[j].Quantity + '</td>';
                            html += '<td class="text-center">' + data[i].Products[j].StockQuantity + '</td>';
                            if(data[i].Products[j].StockQuantity >= data[i].Products[j].Quantity) html += '<td class="text-center text-success"><i class="fa fa-check-circle"></i></td>';
                            else html += '<td class="text-center text-danger"><i class="fa fa-times-circle"></i></td>';
                            html += '</tr>';
                        }
                        html += '</tbody></table></div></div>';
                    }
                    $('#modalCheckQuantity .modal-body').html(html);
                    $('#modalCheckQuantity').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
};

app.promotion = function(){
    $("body").on("click", '.aPromotion', function(){
        var idCheck = $("#idcheck").val();
        if($(this).attr('id-check') == idCheck){
            $(this).next().toggle();
            return false;
        }
    });
    $('.footer-promotion .btn-default').click(function (e) {
        $(this).parent().parent().hide();
    });
    $("body").on('click', '.spanPromotionCost', function(e){
        var idCheck = $("#idcheck").val();
        if(idCheck == $(this).attr('id-check')){
            $('input#reduceTypeId_'+idCheck).val('1');
            $('.promotion-input').find('span').removeClass('active');
            $(this).addClass('active');
            $('input#reduceNumber_'+idCheck).val('0');
        }
    });

    $("body").on('click', '.spanPromotionPercent', function(e){
        var idCheck = $("#idcheck").val();
        if(idCheck == $(this).attr('id-check')){
            $('input#reduceTypeId_'+idCheck).val('2');
            $('.promotion-input').find('span').removeClass('active');
            $(this).addClass('active');
            $('input#reduceNumber_'+idCheck).val('0');
        }
    });

    $("body").on('keydown', 'input.reduceNumber', function(e){
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).keyup(function (e) {
        var idCheck = $("#idcheck").val();
        var value = formatDecimal($("#reduceNumber_"+idCheck).val());
        $("#reduceNumber_"+idCheck).val(value);
        if(value != '0') $('input#promotionCode_'+idCheck).val('').prop('disabled', true);
        else $('input#promotionCode_'+idCheck).prop('disabled', false);
    });
    $("body").on('keyup', 'input.promotionCode', function(e){
        var idCheck = $("#idcheck").val();
        if(idCheck == $(this).attr('id-check')){
            if($(this).val().trim() != '') $('input#reduceNumber_'+idCheck).val('0').prop('disabled', true);
            else $('input#reduceNumber_'+idCheck).prop('disabled', false);
        }
    });

    $("body").on('click', '.btnApplyPromotion', function(){

        var idCheck = $("#idcheck").val();
        if($(this).attr('id-check') == idCheck){
            $('input#promotionId_'+idCheck).val('0');
            $('input#discountPercent_'+idCheck).val('0');
            var customerId = $('input#customer_'+idCheck).val();
            if(customerId == '0'){
                showNotification('Vui lòng chọn khách hàng', 0);
                return false;
            }
            var products = getListOrderProducts();
            if(products.length == 0){
                showNotification('Vui lòng chọn sản phẩm', 0);
                return false;
            }
            var reduceNumber = replaceCost($('input#reduceNumber_'+idCheck).val(), true);
            var totalPrice = replaceCost($('span#totalPrice_'+idCheck).text(), true);
            var promotionCode = $('input#promotionCode_'+idCheck).val().trim();
            if(promotionCode == ''){
                var promotionComment = $('input#promotionComment_'+idCheck).val().trim();
                if(promotionComment == '' && $('input#config_ALLOW_ORDER_DISCOUNT_REASON').val() == 'ON'){
                    showNotification('Lý do giảm giá không được bỏ trống', 0);
                    return false;
                }
                if($('input#reduceTypeId_'+idCheck).val() == '1'){
                    if(reduceNumber < 0){
                        showNotification('Số tiền giảm không được nhỏ hơn 0', 0);
                        return false;
                    }
                    if(reduceNumber > totalPrice){
                        showNotification('Số tiền giảm không được lớn hơn tổng tiền hàng', 0);
                        return false;
                    }
                    $('span#promotionCost_'+idCheck).text(formatDecimal(reduceNumber.toString()));
                    $('span#vnd1').show();
                    if(promotionComment != '') $('#iPromotionText_'+idCheck).text('(' + promotionComment + ')');
                    calcPrice(0);
                    $('#aPromotion_'+idCheck).next().toggle();
                }
                else{
                    if(reduceNumber <= 0 || reduceNumber > 100){
                        showNotification('Phần trăm giảm giá không hợp lệ', 0);
                        return false;
                    }
                    reduceNumber = Math.ceil(totalPrice * reduceNumber / 100);
                    $('span#promotionCost_'+idCheck).text(formatDecimal(reduceNumber.toString()));
                    $('span#vnd1_'+idCheck).show();
                    $('#iPromotionText_'+idCheck).text('(' + promotionComment + ')');
                    calcPrice(0);
                    $('#aPromotion_'+idCheck).next().toggle();
                }
            }
            else {
                var transportCost = $('span#transportCost_'+idCheck).text();
                if(transportCost == '-') transportCost = 0;
                else transportCost = replaceCost(transportCost, true);
                $.ajax({
                    type: 'POST',
                    url: $('input#checkPromotionUrl').val(),
                    data: {
                        Products: products,
                        PromotionCode: promotionCode,
                        CustomerId: customerId,
                        CustomerGroupId: $('input#customerGroupId1_'+idCheck).val(),
                        TotalPrice: totalPrice,
                        TransportCost: transportCost,
                        ProvinceId: $('.i-country').first().attr('data-province')
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var data = json.data;
                            $('input#promotionId_'+idCheck).val(data.PromotionId);
                            $('input#reduceTypeId_'+idCheck).val(data.DiscountPercent > 0 ? 2 : 1);
                            $('input#discountPercent_'+idCheck).val(data.DiscountPercent);
                            $('span#promotionCost_'+idCheck).text(formatDecimal(data.DiscountCost.toString()));
                            $('span#vnd1_'+idCheck).show();
                            $('#iPromotionText_'+idCheck).text('(' + promotionCode + ')');
                            $('#aPromotion_'+idCheck).next().toggle();
                            calcPrice(0);
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        
    });
};

app.configTransport = function(){
    $('#aTransport').click(function(){
        $(this).next().toggle();
        return false;
    });
    $('.footer-transport .btn-default').click(function (e) {
        $(this).parent().parent().hide();
    });
    $('input.transportType').on('ifChecked', function (event) {
        if (event.target.value == '1') {
            $('#divTransportCost').hide();
            $('#divFreeShip').show();
            $('select#transportTypeId1, input#transportFee').val('0');
        }
        else {
            $('#divFreeShip').hide();
            $('#divTransportCost').show();
            $('select#transportReasonId').val('0');
        }
    });
    $('#btnApplyConfigTransport').click(function() {
        var transportTypeId = 0;
        var transportFee = 0;
        var transportText = '(Miễn phí vận chuyển)';
        var type = $('input[name="TransportType"]:checked').val();
        if(type == '1'){
            //var transportReasonId = $('select#transportReasonId').val();
            if($('select#transportReasonId').val() == '0'){
                showNotification('Vui lòng chọn lý do miễn phí ship', 0);
                return false;
            }
            //transportText = '(' + $('select#transportReasonId option[value="'+transportReasonId+'"]').text() + ')';
            transportText = '(Miễn phí vận chuyển)';
        }
        else{
            transportTypeId = $('select#transportTypeId1').val();
            if(transportTypeId == '0'){
                showNotification('Vui lòng chọn phương thức vận chuyển', 0);
                return false;
            }
            transportFee = replaceCost($('input#transportFee').val().trim(), true);
            if(transportFee <= 0){
                showNotification('Phí vận chuyển phải lớn hơn 0', 0);
                return false;
            }
            transportText = '(' + $('select#transportTypeId1 option[value="'+transportTypeId+'"]').text() + ')';
        }
        $('span#transportCost').text(formatDecimal(transportFee.toString()));
        $('span#vnd2').show();
        $('#iTransportText').text(transportText);
        calcPrice(0);
        $('#aTransport').next().toggle();
    });
};

app.configPayment = function(){
    $('body').on('keydown', 'input#paymentCost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input#paymentCost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        calcPrice(replaceCost($('span#totalPrice').text(), true));
    });
};

app.configExpand = function(){
    $("body").on('click', '.aExpand', function(){
        $('#modalConfigExpand').modal('show');
        $(".btnApplyConfigExpand").attr('id-check', $("#idcheck").val())
        return false;
    });
    $('.btnApplyConfigExpand').click(function(){
        var idCheck = $("#idcheck").val();

        if(idCheck == $(this).attr('id-check')){
            var id = $(this).attr('data-id');
            var cost = replaceCost($('input#otherCost_' + id).val().trim(), true);
            if(cost <= 0){
                showNotification('Số tiền phải lớn hơn 0', 0);
                return false;
            }
            var isDebit = $('input#debitOtherTypeId').val() == id;
            if(isDebit){
                if($('input#customer_'+idCheck).val() == '0'){
                    showNotification('Vui lòng chọn khách hàng', 0);
                    return false;
                }
                if(!checkDebtCost(cost, 'Ghi nợ lớn hơn số tiền cho phép')) return false;
            }
            var flag = true;
            $('.divConfigExpand div.item').each(function(){
                if($(this).attr('data-id') == id){
                    flag = false;
                    return false;
                }
            });
            if(flag){
                var html = '<div class="item" data-id="' + id + '">';
                html += '<div class="col-md-6" style="clear: both">';
                html += '<a href="javascript:void(0)" class="aExpand">' + $('#pOtherServiceName_' + id).text() + '</a></div>';
                html += '<div class="col-md-6 text-right"><span id="spanOtherCost_' + id + '" class="spanOtherCost_'+idCheck+'">' + formatDecimal(cost.toString()) + '</span> đ</div>';
                html += '</div>';
                $('#divService_'+idCheck).append(html);
            }
            else {
                $('span#spanOtherCost_' + id).text(formatDecimal(cost.toString()));
                if(isDebit) $('#divOrderOwn').show();
            }
            $('#modalConfigExpand').modal('hide');
            calcPrice(0);
        }
        
    });
};

app.sendOffset = function(){
    $('#aTotalPrice').click(function(){
        if($('select#orderTypeId').val() == $('input#offsetOrderTypeId').val()){
            $('input#offsetTotalPrice').val($('span#totalPrice').text());
            $('#modalSendOffset').modal('show');
        }
        return false;
    });
    $('#btnSendOffset').click(function(){
        var valueStr = $('input#offsetTotalPrice').val();
        var value = replaceCost(valueStr, true);
        if(value >= 0){
            $('span#totalPrice, span#totalCost').text(valueStr);
            $('input#realPaymentCost').val(valueStr);
            calcPrice(value);
            $('#modalSendOffset').modal('hide');
        }
        else showNotification('Tổng tiền hàng không được nhỏ hơn 0', 0);
    });
};

app.orderComment = function(orderId){
    $("body").on('click', '.btnInsertComment' , function(){
        var idCheck = $("#idcheck").val();
        if($(this).attr('id-check') == idCheck){
            var comment = $('input#comment_'+idCheck).val().trim();
            if(comment != ''){
                if(orderId > 0){
                    var btn = $(this);
                    btn.prop('disabled', true);
                    $.ajax({
                        type: "POST",
                        url: $('input#insertOrderCommentUrl').val(),
                        data: {
                            OrderId: orderId,
                            Comment: comment
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            if(json.code == 1) {
                                $('div#listComment_'+idCheck).prepend(genItemCommentTag(comment,idCheck));
                                $('input#comment_'+idCheck).val('');
                            }
                            else showNotification(json.message, json.code);
                            btn.prop('disabled', false);
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                            btn.prop('disabled', false);
                        }
                    });
                }
                else{
                    $('div#listComment_'+idCheck).prepend(genItemCommentTag(comment,idCheck));
                    $('input#comment_'+idCheck).val('');
                }
            }
            else{
                showNotification('Vui lòng nhập ghi chú', 0);
                $('input#comment_'+idCheck).focus();
            }
        }
        
    });
    if(orderId > 0){
        $('#aShowComment').click(function(){
            $('#modalItemComment').modal('show');
            return false;
        });
    }
};

app.cancelOrder = function(orderId){
    $('#aCancelOrder').click(function(){
        if($('input#orderStatusId').val() == '1') $('#modalCancelOrder').modal('show');
        return false;
    });
    $('#btnCancelOrder').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#updateFieldUrl').val(),
            data: {
                OrderId: orderId,
                FieldName: 'OrderStatusId',
                FieldValue: 3,
                CancelReasonId: $('select#cancelReasonId').val(),
                CancelComment: $('input#cancelComment').val().trim()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1){
                    $('#aCancelOrder').hide();
                    $('#divCancelOrder').fadeIn();
                    $('#modalCancelOrder').modal('hide');
                    $('#ulActionLogs').prepend('<li>' + $('input#fullNameLoginId').val() + ' thay đổi trạng thái đơn hàng về Đã hủy bỏ <span class="time">' + getCurrentDateTime(2) + '</span></li>');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });
};

app.verifyOrder = function(orderId){
    var aVerifyOrder = $('#aVerifyOrder');
    if(aVerifyOrder.attr('title') != '') aVerifyOrder.tooltip();
    aVerifyOrder.click(function(){
        if($('input#verifyStatusId').val() == '1' && $('input#config_ALLOW_VERIFY_ORDER').val() == 'ON') $('#modalVerifyOrder').modal('show');
        return false;
    });
    $('#btnVerifyOrder').click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#updateVerifyOrderUrl').val(),
            data: {
                OrderIds: JSON.stringify([orderId]),
                VerifyStatusId: 2
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    $('input#verifyStatusId').val('2');
                    $('#btnCloseBoxCustomer').remove();
                    var txt = 'Đã được xác thực bởi ' + $('input#fullNameLoginId').val() + ' lúc ' + getCurrentDateTime(1);
                    aVerifyOrder.addClass('verify').attr('title', txt).attr('data-original-title', txt).tooltip();
                    $('#modalVerifyOrder').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });

    });
};

app.updatePendingStatus = function(orderId){
    $('#aUpdatePending').click(function(){
        if($('input#orderStatusId').val() == '1') $('#modalUpdatePending').modal('show');
        return false;
    });
    $('#btnUpdatePending').click(function(){
        var pendingStatusId = parseInt($('input[name="PendingStatusId"]:checked').val());
        if(pendingStatusId > 0){
            if(pendingStatusId != parseInt($('input#pendingStatusId').val())){
                var btn = $(this);
                btn.prop('disabled', true);
                updateOrderField(orderId, 'PendingStatusId', pendingStatusId, function(data){
                    $('input#pendingStatusId').val(pendingStatusId);
                    $('#spanOrderStatus').text(data.StatusName);
                    $('#modalUpdatePending').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn trạng thái Chờ xử lý khác', 0);
        }
        else showNotification('Vui lòng chọn trạng thái Chờ xử lý', 0);
    });
};

app.chooseStore = function(orderId){
    $("body").on('click', '.aChooseStore' , function(){
        $('#modalChooseStore').modal('show');
        return false;
    });
    $("body").on('click', '#btnUpdateStore' , function(){
        var idCheck = $("#idcheck").val();
        var orderStoreId = parseInt($('input[name="OrderStoreId"]:checked').val());
        if(orderStoreId > 0){
            if(orderStoreId != parseInt($('input#orderStoreId_'+idCheck).val())){
                if(orderId > 0) {
                    var btn = $(this);
                    btn.prop('disabled', true);
                    updateOrderField(orderId, 'StoreId', orderStoreId, function (data) {
                        $('input#orderStoreId_'+idCheck).val(orderStoreId);
                        $('#spanStoreName_'+idCheck).text('CS: ' + data.StoreName);
                        $('#modalChooseStore').modal('hide');
                        $('select#storeId').val(orderStoreId).trigger('change');
                        btn.prop('disabled', false);
                    }, function () {
                        btn.prop('disabled', false);
                    });
                }
                else{
                    $('input#orderStoreId_'+idCheck).val(orderStoreId);
                    $('#spanStoreName_'+$("#idcheck").val()).text('CS: ' + $('#spanStoreName_' + orderStoreId).text());
                    $('#modalChooseStore').modal('hide');
                    //$('select#storeId').val(orderStoreId).trigger('change');
                }
            }
            else showNotification('Vui lòng chọn Cơ sở xử lý khác', 0);
        }
        else showNotification('Vui lòng chọn Cơ sở xử lý', 0);
    });
};

app.addTransport = function(orderId){
    $('.btnTransport').click(function () {
        if(!$(this).hasClass('actived')){
            if($('input#verifyStatusId').val() == '2'){
                if($('select#orderReasonId').val() == '0'){
                    showNotification('Bạn chưa chọn Lý do mua hàng', 0);
                    return false;
                }
                else if($('select#orderChanelId').val() == '0'){
                    showNotification('Bạn chưa chọn Kênh bán hàng', 0);
                    return false;
                }
                var paymentCost = replaceCost($('input#paymentCost').val());
                var flag = false;
                var paymentStatusId = parseInt($('input#paymentStatusId').val());
                if(paymentStatusId > 0) {
                    if (paymentStatusId == 1) {
                        paymentCost = 0;
                        flag = true;
                    }
                    else flag = paymentCost > 0;
                }
                if(!flag){
                    showNotification('Bạn chưa cấu hình thanh toán', 0);
                    return false;
                }
                if($('input#config_ALLOW_WAIT_PAYMENT_TICKET').val() == 'ON') {
                    if (!checkDebtCost(replaceCost($('#divOrderOwn .spanOtherCost').first().text(), true), 'Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán')) {
                        if (replaceCost($('input#remindOwnCost').val(), true) != replaceCost($('input#ownCost').val(), true)) {
                            $('input#isUpdateOrder').val('0');
                            $('#modalWaitPayment').modal('show');
                        }
                        return false;
                    }
                }
                $('#modalTransport').modal('show');
            }
            else showNotification('Đơn hàng chưa được xác thực', 0);
        }
    });
    $('#btnAddTransport').click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        var transportCODCost = replaceCost($('input#transportCODCost').val(), true);
        var transportCost = $('span#transportCost').text();
        transportCost = transportCost == '-' ? 0 : replaceCost(transportCost, true);
        $.ajax({
            type: "POST",
            url: $('input#updateTransportUrl').val(),
            data: {
                TransportId: 0,
                TransportCode: '',
                OrderId: orderId,
                CustomerId: $('input#customerId').val(),
                CustomerAddressId: $('input#customerAddressId').val(),
                TransportUserId: 0,
                TransportStatusId: 1,
                PendingStatusId: 0,
                TransportTypeId: $('select#transportTypeId').val(),
                TransporterId: 0, //$('select#transporterId').val(),
                StoreId: $('select#storeId').val(),
                Tracking: '',//$('input#transportTracking').val().trim(),
                Weight: $('input#transportWeight').val().trim(),
                CODCost: transportCODCost,
                CODStatusId: transportCODCost == 0 ? 1 : 3,
                ShipCost: 0,
                Comment: $('#transportComment').val().trim(),
                EnhancedService: '',// $('#transportEnhancedService').val().trim(),
                CancelReasonId: 0,
                CancelReasonText: '',

                TagNames: JSON.stringify([]),

                PaymentCost: replaceCost($('input#paymentCost').val()),
                OrderCode: $('#aVerifyOrder').text().trim(),
                OrderReasonId: $('select#orderReasonId').val(),
                OrderChanelId: $('select#orderChanelId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });
};

app.cancelTransport = function(transportId, orderId){
    $('.btn-cancle-cart').click(function(){
        $('#modalCancelTransport').modal('show');
        return false;
    });
    $('#btnCancelTransport').click(function(){
        var btn = $(this);
        var transportStatusId = parseInt($('input#transportStatusId').val());
        if(transportStatusId == 1){ //huy thang, ko can thong bao
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changeTransportStatusUrl').val(),
                data: {
                    TransportId: transportId,
                    FieldName: 'TransportStatusId',
                    FieldValue: 5,
                    OrderId: orderId,
                    CancelReasonId: $('select#cancelReasonId1').val(),
                    CancelComment: $('input#cancelComment1').val().trim(),
                    StoreId: $('input#transportStoreId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else if(transportStatusId != 5){ //gui thong bao den VC

        }
    });
};

app.shareOrder = function(orderId){
    $('#aShareOrder').click(function(){
        if($('input#orderStatusId').val() == '1' && $('input#isShare').val() != '2') $('#modalShareOrder').modal('show');
        return false;
    });
    $('#btnShareOrder').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#updateFieldUrl').val(),
            data: {
                OrderId: orderId,
                FieldName: 'IsShare',
                FieldValue: 2,
                ShareComment: $('input#shareComment').val().trim()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    $('input#isShare').val('2');
                    $('#modalShareOrder').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });
};

app.orderReason = function(orderId, canEdit){
    if($('select#orderReasonId').val() == '0' || canEdit){
        $('select#orderReasonId').change(function(){
            updateOrderField(orderId, 'OrderReasonId', $(this).val(), function (data){}, function(){});
        });
    }
};

app.remind = function(){
    $("body").on('click', '.aRemind', function(){
        if($("#idcheck").val() == $(this).attr('id-check')){
            $('#modalRemind').modal('show');
            return false;
        }
       
    });
    $('select#userId').change(function(){
        if($(this).val() != '0') $('select#partId').val('0').trigger('change');
    });
    $('select#partId').change(function(){
        if($(this).val() != '0') $('select#userId').val('0').trigger('change');
    });
    $("body").on('click', '#btnAddRemind', function(){
        if(validateEmpty('#modalRemind')){
            var btn = $(this);
            btn.prop('disabled', true);
            var comments = [];
            var remindComment = $('input#remindComment').val().trim();
            if(remindComment != '') comments.push(remindComment);
            $.ajax({
                type: "POST",
                url: $('input#insertRemindUrl').val(),
                data: {
                    RemindId: 0,
                    RemindTitle: $('input#remindTitle').val().trim(),
                    RemindDate: $('input#remindDate1').val().trim(),
                    IsRepeat: 1,
                    RepeatDay: 0,
                    RepeatHour: 0,
                    RemindStatusId: 1,
                    UserId: $('select#userId').val(),
                    PartId: $('select#partId').val(),

                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        $('#modalRemind input').val('');
                        $('select#userId').val($('input#userLoginId').val());
                        $('select#partId').val('0');
                        $('#modalRemind').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

function verifyComplete(){
    var idCheck = $("#idcheck").val();
        
    $('.submit, #btnSaveOwnOrder, #btnNoSaveOwnOrder, #btnSubmitPos').prop('disabled', false);
    
    if($('input#verifyStatusId').val() != '2' && $('input#config_ALLOW_VERIFY_ORDER').val() == 'ON'){
        showNotification('Đơn hàng chưa được xác thực', 0);
        return false;
    }
    if($('input#orderStoreId_'+idCheck).val() == '0'){
        showNotification('Chưa chọn cơ sở cho đơn hàng', 0);
        return false;
    }
    if ($('.tbodyProduct tr').length > 0) {
        if(idCheck == $(this).attr('id-check')){
            var customerId = parseInt($('input#customer_'+idCheck).val());
            if(customerId > 0){
                if(validateNumber('#orderForm', true, ' không được bỏ trống')){
                    var realPaymentCost = $('input#realPaymentCost_'+idCheck).val();
                    if(replaceCost(realPaymentCost, true) >= replaceCost($('span#totalCost_'+idCheck).text(), true)){
                        var moneySourceId = $('select#moneySourceId_'+idCheck).val();
                        var html = '<tr><td>Khởi tạo phiếu thu</td><td>' + realPaymentCost + ' VNĐ</td></tr>';
                        html += '<tr><td>Phương thức thanh toán</td><td><span class="label label-success">' + $('select#moneySourceId_'+idCheck+' option[value="' + moneySourceId + '"]').text() + '</span></td></tr>';
                        var debitCost = $('.spanOtherCost_'+idCheck).first().text();
                        if (debitCost != '0') html += '<tr><td>Công nợ</td><td>' + debitCost + ' VNĐ</td></tr>';
                        $('#tbobyVerifyPayment').html(html);
                        $('#modalVerifyComplete').modal('show');
                    }
                    else showNotification('KH thanh toán không được nhỏ hơn Tổng cần thanh toán', 0);
                }
            }
            else showNotification('Vui lòng chọn khách hàng', 0);
        }
    }
    else showNotification('Vui lòng chọn sản phẩm', 0);
}

app.orderPOS = function(){
    $("body").on('click','.btnVerifyComplete', function(){
        verifyComplete();
    });

    // key f9
    jwerty.key('f9', function () { 
        verifyComplete();
    })

    $('select#moneySourceId').change(function(){
        if($(this).val() == '2') $('#modalChooseBank').modal('show');
        else{
            $('input#bankIdTmp').val('0');
            $('#aBank').attr('title', '').attr('data-original-title', '').tooltip('destroy');
        }
    });
    $('#aBank').click(function(){
        if($('select#moneySourceId').val() == '2') $('#modalChooseBank').modal('show');
        return false;
    });
    $('input.iCheckBankType').on('ifToggled', function(e){
        if(e.currentTarget.checked){
            $('#listBank li').hide();
            $('.liBank_' + e.currentTarget.value).show();
        }
    });
    $('#listBank').on('click', 'li', function(){
        $('#listBank li').removeClass('active');
        $(this).addClass('active');
    });
    $('#btnChooseBank').click(function(){
        var li = $('#listBank li.active').first();
        $('input#bankIdTmp').val(li.attr('data-id'));
        var bankName = li.find('.pBankName').text() + ' - ' + li.find('.spanBankName').text();
        $('#aBank').attr('title', bankName).attr('data-original-title', bankName).tooltip();
        $('#modalChooseBank').modal('hide');
    });
    $('#modalChooseBank').on('click', '.btn[data-dismiss="modal"]', function(){
        $('#listBank li').removeClass('active');
        $('#listBank li[data-id="'+$('input#bankIdTmp').val()+'"]').addClass('active');
    });
    
};

$(document).ready(function () {
    var orderId = parseInt($('input#orderId').val());
    var canEdit = parseInt($('input#canEdit').val());
    var transportId = parseInt($('input#transportId').val());
    app.init(orderId, canEdit, transportId);
    var tags = [];
    //var transportTags = [];
    var inputTag = $('input.tags');
    $("body").on('click','.ntags', function(){
        var idCheck = $("#idcheck").val();
        if(idCheck == $(this).attr('id-check')){
            var tag = $(this).text();
            if(!$('input#tags_'+idCheck).tagExist(tag)) $('input#tags_'+idCheck).addTag(tag);
            return false;
        }
        
    });
    if (orderId > 0) {
        $('input.tagName').each(function () {
            inputTag.addTag($(this).val());
        });
    }
    $("body").on('click', '.submit', function(){
        $('.submit, #btnSaveOwnOrder, #btnNoSaveOwnOrder, #btnSubmitPos').prop('disabled', false);
        var orderStatusId = $(this).attr('data-id');
        var idCheck = $("#idcheck").val();
        if(idCheck == $(this).attr('id-check')){
            if(validateNumber('#orderForm', true, ' không được bỏ trống')){
                var customerId = parseInt($('input#customer_'+idCheck).val());
                if (customerId > 0) {
                    var products = getListOrderProducts();
                    if (products.length > 0) {
                        if($('input#deliveryTypeId').val() == '1'){ //pos
                            if($('input#config_ALLOW_WAIT_PAYMENT_TICKET').val() == 'ON') {
                                if (!checkDebtCost(replaceCost($('.spanOtherCost_'+idCheck).first().text(), true), 'Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán')) {
                                    if (replaceCost($('input#remindOwnCost_'+idCheck).val(), true) != replaceCost($('input#ownCost_'+idCheck).val(), true)) {
                                        $('input#orderStatusId').val(orderStatusId);
                                        $('input#isUpdateOrder').val('1');
                                        $('#modalWaitPayment').modal('show');
                                    }
                                }
                                else saveOrder(orderId, customerId, orderStatusId, replaceCost($('span#totalCost_'+idCheck).text(), true), 3, tags, products, 0, '', '',idCheck);
                            }
                            else saveOrder(orderId, customerId, orderStatusId, replaceCost($('span#totalCost_'+idCheck).text(), true), 3, tags, products, 0, '', '',idCheck);
                        }
                        else{
                            var paymentStatusId = parseInt($('input#paymentStatusId').val());
                            if(paymentStatusId > 0){
                                var paymentCost = replaceCost($('input#paymentCost_'+idCheck).val());
                                var flag = false;
                                if(paymentStatusId == 1){
                                    paymentCost = 0;
                                    flag = true;
                                }
                                else flag = paymentCost > 0;
                                if(flag){
                                    if($('input#config_ALLOW_WAIT_PAYMENT_TICKET').val() == 'ON') {
                                        flag = checkDebtCost(replaceCost($('.spanOtherCost_'+idCheck).first().text(), true), 'Tài khoản khách hàng không đủ tiền, cần hoàn thành phiếu chờ thanh toán');
                                        if(flag) saveOrder(orderId, customerId, orderStatusId, paymentCost, paymentStatusId, tags, products, 0, '', '');
                                        else if (replaceCost($('input#remindOwnCost_'+idCheck).val(), true) != replaceCost($('input#ownCost_'+idCheck).val(), true)) {
                                            $('input#orderStatusId').val(orderStatusId);
                                            $('input#isUpdateOrder').val('1');
                                            $('#modalWaitPayment').modal('show');
                                        }
                                    }
                                    else saveOrder(orderId, customerId, orderStatusId, paymentCost, paymentStatusId, tags, products, 0, '', '',idCheck);
                                }
                                else showNotification('Bạn chưa cấu hình thanh toán', 0);
                            }
                            else showNotification('Bạn chưa cấu hình thanh toán', 0);
                        }
                    }
                    else showNotification('Vui lòng chọn sản phẩm', 0);
                }
                else showNotification('Vui lòng chọn khách hàng', 0);
            }
        }
        
        return false;
    });
    $("body").on('click', '#btnSaveOwnOrder', function(){
        var idCheck = $("#idcheck").val();
        var ownCost = replaceCost($('input#ownCost_'+idCheck).val(), true);
        if(ownCost <= 0){
            showNotification('Số tiền chờ thanh toán phải lớn hơn 0', 0);
            $('input#ownCost_'+idCheck).focus();
            return false;
        }
        var remindDate = $('input#remindDate').val().trim();
        if(remindDate == ''){
            showNotification('Vui lòng chọn ngày nhắc thanh toán', 0);
            $('input#remindDate').focus();
            return false;
        }
        if($('input#isUpdateOrder').val() == '1') saveOrder(orderId, $('input#customer_'+idCheck).val(), $('input#orderStatusId').val(), replaceCost($('input#paymentCost_'+idCheck).val()), $('input#paymentStatusId').val(), tags, getListOrderProducts(), ownCost, remindDate, $('input#ownComment').val().trim());
        else{
            $.ajax({
                type: "POST",
                url: $('input#insertOwnOrderUrl').val(),
                data: {
                    OrderId: orderId,
                    CustomerId: $('input#customer_'+idCheck).val(),
                    OwnCost: ownCost,
                    RemindDate: remindDate,
                    Comment: $('input#ownComment').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        $('input#remindOwnCost_'+idCheck).val(ownCost);
                        $('#modalWaitPayment').modal('hide');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });

    $("body").on('click', '#btnNoSaveOwnOrder', function(){
        var idCheck = $("#idcheck").val();
        saveOrder(orderId, $('input#customer_'+idCheck).val(), $('input#orderStatusId').val(), replaceCost($('input#paymentCost_'+idCheck).val()), $('input#paymentStatusId').val(), tags, getListOrderProducts(), 0, '', '',idCheck);
    });
    $("body").on('click', '#btnSubmitPos', function(){
        var idCheck = $("#idcheck").val();
        saveOrder(0, $('input#customer_'+idCheck).val(), 6, replaceCost($('span#totalCost_'+idCheck).text(), true), 3, tags, getListOrderProducts(), 0, '', '',idCheck);
    });

    $("body").on('click', '.tagPos', function(){
        var id = $(this).attr('data-id');
        $("#tabo_"+id).show();

        if($("#check_tag_"+id).attr('flags') === '0'){
            $(".tab_comtent").append(`<div class="tab-pane" id="tabo_${id}">${htmlTag(id)}</div>`);
            loadProductSlider(id)
            $("#check_tag_"+id).attr('flags', '1');
            var tags = [];
            //var transportTags = [];
            var inputTag = $('input#tags_'+id);
            inputTag.tagsInput({
                'width': '100%',
                'height': '90px',
                'interactive': true,
                'defaultText': '',
                'onAddTag': function (tag) {
                    tags.push(tag);
                },
                'onRemoveTag': function (tag) {
                    var index = tags.indexOf(tag);
                    if (index >= 0) tags.splice(index, 1);
                },
                'delimiter': [',', ';'],
                'removeWithBackspace': true,
                'minChars': 0,
                'maxChars': 0
            });
        }
        $('.tagsOrder li').each(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                if($(this).find('a').attr('data-id') == id){
                    $("#tabo_"+$(this).find('a').attr('data-id')).show();
                    $("#idcheck").val(id)
                }else{
                    $("#tabo_"+$(this).find('a').attr('data-id')).hide();
                }
                
            }else{
                if($(this).find('a').attr('data-id') == id){
                    $(this).addClass('active');
                    $("#tabo_"+$(this).find('a').attr('data-id')).show();
                    $("#idcheck").val(id)
                }else{
                    
                }
            }
        })
    })
    // load tag auto
    loadTags('1');


    // crated tag create-tag
    $("body").on('click', '.create-tag', function(){
        var idTag = parseInt($(this).parent().parent().find("li").eq(-2).find('a').attr('data-id')) + 1;
        $(this).parent().before(`<li><a href="#tabo_${idTag}" data-toggle="tab" data-id="${idTag}" flags="0" class="tagPos" id="check_tag_${idTag}">Tạo đơn POS ${idTag}</a></li>`);
        if($("#check_tag_"+idTag).attr('flags') === '0'){
            $(".tab_comtent").append(`<div class="tab-pane" id="tabo_${idTag}">${htmlTag(idTag)}</div>`);
            loadProductSlider(idTag)
            $("#check_tag_"+idTag).attr('flags', '1');
            var tags = [];
            //var transportTags = [];
            var inputTag = $('input#tags_'+idTag);
            inputTag.tagsInput({
                'width': '100%',
                'height': '90px',
                'interactive': true,
                'defaultText': '',
                'onAddTag': function (tag) {
                    tags.push(tag);
                },
                'onRemoveTag': function (tag) {
                    var index = tags.indexOf(tag);
                    if (index >= 0) tags.splice(index, 1);
                },
                'delimiter': [',', ';'],
                'removeWithBackspace': true,
                'minChars': 0,
                'maxChars': 0
            });
        }
        $('.tagsOrder li').each(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                if($(this).find('a').attr('data-id') == idTag){
                    $("#tabo_"+$(this).find('a').attr('data-id')).show();
                    $("#idcheck").val(idTag)
                }else{
                    $("#tabo_"+$(this).find('a').attr('data-id')).hide();
                }
                
            }else{
                if($(this).find('a').attr('data-id') == idTag){
                    $(this).addClass('active');
                    $("#tabo_"+$(this).find('a').attr('data-id')).show();
                    $("#idcheck").val(idTag)
                }else{
                    
                }
            }
        })
    })

    // outputs "print!" to the console when pressed.
    jwerty.key('f9', function () { console.log('print!') });
});



function loadTags(id){
    $("#idcheck").val(id)
    if($("#check_tag_"+id).attr('flags') == 0){
        $(".tab_comtent").append(`<div class="tab-pane" id="tabo_${id}">${htmlTag(id)}</div>`);
        loadProductSlider(id)
        $("#check_tag_"+id).attr('flags', '1');
        var tags = [];
    //var transportTags = [];
        var inputTag = $('input#tags_'+id);
        inputTag.tagsInput({
            'width': '100%',
            'height': '90px',
            'interactive': true,
            'defaultText': '',
            'onAddTag': function (tag) {
                tags.push(tag);
            },
            'onRemoveTag': function (tag) {
                var index = tags.indexOf(tag);
                if (index >= 0) tags.splice(index, 1);
            },
            'delimiter': [',', ';'],
            'removeWithBackspace': true,
            'minChars': 0,
            'maxChars': 0
        });
    }
}

function htmlTag(id){
    
    var selectCategories = '<select class="form-control select2 categoryId" id="categoryId_'+id+'" id-check="'+id+'" name="CategoryId"><option value="0">Nhóm sản phẩm</option>';
    $.each(listCategories, function(key1, value){
        selectCategories += `<option value="${value['CategoryId']}">${value['CategoryName']}</option>`;
    });
    selectCategories += '</select>';

    var selectOrderTypes = '<select class="form-control select2 orderTypeId" id="orderTypeId_'+id+'" id-check="'+id+'" name="OrderTypeId"><option value="0">--Loại đơn hàng---</option>';
    $.each(listOrderTypes, function(key2, value){
        selectOrderTypes += `<option value="${value['OrderTypeId']}">${value['OrderTypeName']}</option>`;
    });
    selectOrderTypes += '</select>';

    var selectOrderReasons = '<select class="form-control select2 orderReasonId" id="orderReasonId_'+id+'" id-check="'+id+'" name="OrderReasonId"><option value="0">--Chọn lý do--</option>';
    $.each(listOrderReasons, function(key3, value){
        selectOrderReasons += `<option value="${value['OrderReasonId']}">${value['OrderReasonName']}</option>`;
    });
    selectOrderReasons += '</select>';

    var listTag = "";
    $.each(listTags, function(key4, t){
        listTag+= `<a href="javascript:void(0)" class="ntags" id="ntags_${id}" id-check="${id}">${t['TagName']}</a>`;
    });

    var selectPromotiontypes = '<select class="form-control select2 promotionTypeId" id="promotionTypeId_'+id+'" id-check="'+id+'" name="PromotionTypeId"><option value="0">--Chọn lý do--</option>';
    $.each(listPromotiontypes, function(key5, value){
        selectPromotiontypes += `<option value="${value['PromotionTypeId']}">${value['PromotionTypeName']}</option>`;
    });
    selectPromotiontypes += '</select>';

    
    return `
        <div class="">
            <div class="col-sm-12">
            <ul class="list-inline" style="float:right">
                    <li><button class="btn btn-primary submit" id-check="${id}" type="button" data-id="1">Lưu</button></li>
                    <li><button type="button" class="btn btn-default btnPrint" id-check="${id}" id="btnPrint"><i class="fa fa-print"></i> In</button></li>
                    <li><a href="#" class="btn btn-default">Đóng</a></li>
                </ul>
            </div>
            
            <div class="col-sm-8">
                <div class="box box-default padding20">
                    <div class="clearfix top-aded mb10">
                    <div class="left"></div>
                    <div class="right" style="margin-right: 0;">
                        <span class="dropdown">
                            <span class="dropdown-toggle" style="cursor: pointer;" type="button" data-toggle="dropdown">
                                <span class="light-blue pos-re" id="spanStoreName_${id}">CS: ${storeName}</span>
                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                            </span>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)" class="aChooseStore" id-check="${id}" id="aChooseStore_${id}"><img src="assets/vendor/dist/img/pop03.png">&nbsp;&nbsp;Chọn cơ sở xử lý</a></li>
                            </ul>
                        </span>
                    </div>
                </div>
                    <div class="">
                        <div class="table-responsive no-padding divTable">
                            <table class="table table-hover table-bordered-bottom">
                                <thead class="theadNormal">
                                <tr>
                                    <th>Sản phẩm</th>
                                    <th class="text-center" style="width: 60px;">Đơn vị</th>
                                    <th class="text-center" style="width: 130px;">SKU</th>
                                    <th class="text-center" style="width: 120px;">Bảo hành</th>
                                    <th class="text-center" style="width: 150px;">Giá</th>
                                    <th style="width: 85px;">Số lượng</th>
                                    <th class="text-right" style="width: 100px;">Thành tiền</th>
                                    <th style="width: 30px;"></th>
                                </tr>
                                </thead>
                                <tbody class="tbodyProduct_${id} tbodyProduct" id-check="${id}"></tbody>
                            </table>
                        </div>
                        <div class="border-top-title-main">
                            <div class="clearfix">
                                <div class="box-search-advance product">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch updated txtSearchProduct" id="" id-check="${id}" placeholder="Tìm kiếm sản phẩm (F3)">
                                    </div>
                                    <div class="panel panel-default panelProduct" id="panelProduct_${id}" id-check="${id}">
                                        <div class="panel-body" style="width:100%;">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <div>
                                                    <div class="form-group pull-right" style="width: 300px;">
                                                        ${selectCategories}
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="table-responsive no-padding divTable">
                                                    <table class="table table-hover table-bordered">
                                                        <thead class="theadNormal">
                                                        <tr>
                                                            <th style="width: 100px;">Ảnh</th>
                                                            <th>Sản phẩm</th>
                                                            <th style="width: 100px;">SKU</th>
                                                            <th style="width: 100px;">Giá</th>
                                                            <th style="width: 100px;">Bảo hành</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="tbodyProductSearch" id-check="${id}"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default btnPrevProduct" id="" id-check="${id}"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default btnNextProduct" id="" id-check="${id}"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdProduct" id-check="${id}" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="light-blue">Ghi chú</label>
                                <div class="box-transprt clearfix mb10">
                                    <button type="button" class="btn-updaten save btnInsertComment" id-check="${id}">
                                        Lưu
                                    </button>
                                    <input type="text" class="add-text" id="comment_${id}" value="">
                                </div>
                                <div class="listComment" id="listComment_${id}"></div>
                            </div>
                            <div class="col-sm-6"></div>
                        </div>
                    </div>
                </div>
                <div class="box box-default padding20">
                    <div class="box-step-order has-slider">
                        <ul class="clearfix" id="ulProduct_${id}"></ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box box-default" id="boxChooseCustomer_${id}">
                    <div class="box-header with-border">
                        <h3 class="box-title">Khách hàng</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool btnAddCustomer" id="btnAddCustomer_${id}" id-check="${id}"><i class="fa fa-plus"></i> Thêm</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-search-advance customer">
                            <div>
                                <input type="text" class="form-control textbox-advancesearch txtSearchCustomer" id-check="${id}" placeholder="Tìm khách hàng (F4)">
                            </div>
                            <div class="panel panel-default panelCustomer" id="panelCustomer_${id}">
                                <div class="panel-body">
                                    <div class="list-search-data">
                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                        <ul class="ulListCustomers" id-check="${id}" id="ulListCustomers_${id}"></ul>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default btnPrevCustomer" id-check="${id}"><i class="fa fa-chevron-left"></i></button>
                                        <button type="button" class="btn btn-default btnNextCustomer" id-check="${id}"><i class="fa fa-chevron-right"></i></button>
                                        <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default mh-wrap-customer" id="divCustomer_${id}" style="display: none;">
                    <div class="with-border">
                        <h3 class="box-title">Thông tin khách hàng</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool btnCloseBoxCustomer" id-check="${id}" id="btnCloseBoxCustomer_${id}" ><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                        </div>
                        <div class="mh-info-customer">
                            <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                            <div class="name-info">
                                <h4 class="i-name_${id}"></h4>
                                <div class="phones i-phone_${id}"></div>
                            </div>
                            <div class="i-cusType_${id}">
                                <span class="label label-success"></span>
                            </div>
                            <div class="total-orders">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders_${id}"></span>
                            </div>
                            <div class="bank">
                                <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance_${id}">0</span> ₫</span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div style="display: none;">
                            <h4 class="mgbt-20 light-blue">Thông tin giao hàng &nbsp;&nbsp;<a href="javascript:void(0)" id="aCustomerAddress_${id}"><i class="fa fa-pencil"></i></a></h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="item"  style="display: none;">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <span class="i-name_${id}"></span>
                                </div>
                                <div class="item" style="display: none;">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <span class="i-phone_${id}"></span>
                                </div>
                                <div class="item">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <span class="i-email_${id}"></span>
                                </div>
                                <div class="item i-address_${id}">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span class="i-ward"></span>
                                    <span class="br-line i-district"></span>
                                    <span class="br-line i-province"></span>
                                </div>
                                <div class="item" style="display: none;">
                                    <i class="fa fa-id-card" aria-hidden="true"></i>
                                    <span class="i-country_${id}" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="nav-tabs-custom" style="box-shadow: none;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1_${id}" data-toggle="tab" aria-expanded="true">Thanh toán</a></li>
                            <li class=""><a href="#tab_2_${id}" data-toggle="tab" aria-expanded="false">Mở rộng</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_${id}">
                                <div class="row tb-sead">
                                    <div class="col-sm-6"><a href="javascript:void(0)" id="aTotalPrice">Tổng</a></div>
                                    <div class="col-sm-6 text-right"><span id="totalPrice_${id}">0</span> đ</div>
                                    <div class="col-sm-6">
                                        <a href="javascript:void(0)" class="aPromotion" id-check="${id}" id="aPromotion_${id}">Triết khấu</a>
                                        <div class="promotion" style="display: none;" id="boxPromotion_${id}">
                                            <div class="content-promotion">
                                                <div class="form-group">
                                                    <label>Nhập giá trị giảm giá cho đơn hàng</label>
                                                    <div class="input-group promotion-input">
                                                        <input class="reduceNumber" id-check="${id}" id="reduceNumber_${id}" type="text" class="form-control" value="0">
                                                        <span class="input-group-addon active spanPromotionCost" id-check="${id}" id="spanPromotionCost_${id}">đ</span>
                                                        <span class="input-group-addon spanPromotionPercent" id-check="${id}" id="spanPromotionPercent_${id}">%</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Hoặc sử dụng mã khuyến mại</label>
                                                    <input type="text" class="form-control promotionCode"  id="promotionCode_${id}" placeholder="Mã khuyến mại">
                                                </div>
                                                <div class="form-group">
                                                    <label>Loại khuyến mại</label>
                                                    ${selectPromotiontypes}
                                                </div>
                                                <div class="form-group">
                                                    <label>Lý do</label>
                                                    <input type="text" class="form-control" placeholder="Lý do giảm giá cho đơn hàng" id="promotionComment_${id}">
                                                </div>
                                            </div>
                                            <div class="footer-promotion">
                                                <button type="button" class="btn btn-default">Đóng</button>
                                                <button type="button" class="btn btn-primary btnApplyPromotion"  id="btnApplyPromotion_${id}" id-check="${id}">Áp dụng</button>
                                                <input type="text" hidden="hidden" id="promotionId_${id}" value="0">
                                                <input type="text" hidden="hidden" id="reduceTypeId_${id}" value="1">
                                                <input type="text" hidden="hidden" id="discountPercent_${id}" value="0">
                                                <input type="text" hidden="hidden" id="checkPromotionUrl_${id}" value="${linkcheckPromotion}">
                                            </div>
                                        </div>
                                        <br/><i id="iPromotionText"></i>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <span id="promotionCost_${id}">-</span>
                                        <span id="vnd1_${id}" style="display: none;">đ</span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6" style="display: none;">
                                        <a href="javascript:void(0)" id="aTransport">Cấu hình vận chuyển</a>
                                        <div class="transport" style="display: none;" id="boxTransport">
                                            <div class="content-transport">
                                                <div class="radio-group">
                                                    <p class="item"><input type="radio" name="TransportType" class="iCheck transportType" value="1"> Miễn phí vận chuyển</p>
                                                    <p class="item"><input type="radio" name="TransportType" class="iCheck transportType" value="2" checked> Vận chuyển tùy chọn</p>
                                                </div>
                                                <div class="form-group mb10" id="divFreeShip" style="display: none;">
                                                    <label>Lý do miễn phí ship</label>
                                                    <?php echo $this->Mconstants->selectObject($listTransportReasons, 'TransportReasonId', 'TransportReasonName', 'TransportReasonId', 0, true, "--Chọn--"); ?>
                                                </div>
                                                <div id="divTransportCost">
                                                    <div class="form-group mb10">
                                                        <label>Phương thức vận chuyển</label>
                                                        <?php echo $this->Mconstants->selectObject($listTransportTypes, 'TransportTypeId', 'TransportTypeName', 'TransportTypeId1', 0, true, "--Chọn--"); ?>
                                                    </div>
                                                    <div class="form-group mb10">
                                                        <label>Phí vận chuyển</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control cost" id="transportFee" value="0">
                                                            <div class="input-group-addon">đ</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer-transport">
                                                <button type="button" class="btn btn-default">Đóng</button>
                                                <button type="button" class="btn btn-primary" id="btnApplyConfigTransport">Áp dụng</button>
                                            </div>
                                        </div>
                                        <br/><i id="iTransportText"></i>
                                    </div>
                                    <div class="col-sm-6 text-right" style="display: none;">
                                        <span id="transportCost_${id}">-</span>
                                        <span id="vnd2" style="display: none;">đ</span>
                                    </div>
                                    <div class="divConfigExpand" id="divService_${id}"></div>
                                </div>
                                <hr class="hr-ths">
                                <div class="row mb10">
                                    <div class="col-sm-6">Tổng tiền</div>
                                    <div class="col-sm-6 text-right"><span id="orderCost_${id}"></span> đ</div>
                                </div>
                                <div class="row mb10" style="display: none;">
                                    <div class="col-sm-6">
                                        <a href="javascript:void(0)" id="aPayment" style="line-height: 34px;">Thanh toán trước</a><br/>
                                        <i id="iPaymentText"></i>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="paymentCost_${id}" value="0">
                                            <span class="input-group-addon"> đ</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="divConfigExpand mb10" id="divOrderOwn" style="display: none;">
                                    <div class="row item" data-id="${DEBIT_OTHER_TYPE_ID}">
                                        <div class="col-sm-6"><a href="javascript:void(0)" class="aExpand">Ghi nợ</a></div>
                                        <div class="col-sm-6 text-right"><span id="spanOtherCost_${DEBIT_OTHER_TYPE_ID}" class="spanOtherCost_${id}">0</span> đ</div>
                                    </div>
                                </div>
                                <div class="row tb-sead mb10">
                                    <div class="col-sm-6">Tổng cần thanh toán</div>
                                    <div class="col-sm-6 text-right bold light-dark2 fs-16">
                                        <span id="totalCost_${id}">0</span> đ
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6">
                                        <a href="javascript:void(0)" id="aBank" style="line-height: 34px;">Loại tiền</a><br/>
                                    </div>
                                    <div class="col-sm-6 text-right light-dark2 fs-16">
                                        <select class="form-control moneySourceId" id="moneySourceId_${id}" id-check="${id}">
                                            <option value="1">Tiền mặt</option>
                                            <option value="2">Chuyển khoản</option>
                                            <option value="3">Quẹt thẻ</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6" style="line-height: 34px;">KH thanh toán</div>
                                    <div class="col-sm-6 text-right">
                                        <div class="input-group">
                                            <input type="text" class="form-control cost" id="realPaymentCost_${id}" value="0">
                                            <span class="input-group-addon"> đ</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 10px;">
                                        <p class="pull-right"><a href="javascript:void(0)" id="aExpand_${id}" id-check="${id}" class="aExpand">Mở rộng &gt;&gt;</a></p>
                                    </div>
                                </div>
                                <!--<p class="text-right list-btnn">
                                    <a href="javascript:void(0)" class="btn btn-default check-order" id="aCheckOrder"><span>Check đơn</span></a>
                                </p>
                                <div class="clearfix"></div>-->
                                <p class="text-center">
                                    <button class="btn btn-primary btnVerifyComplete" type="button" id="btnVerifyComplete_${id}" id-check="${id}" style="width: 100%;">Hoàn thành (F9)</button>
                                </p>
                            </div>
                            <div class="tab-pane" id="tab_2_${id}">
                                <div class="classify">
                                    <div class="form-group">
                                        <label class="control-label light-blue">Loại đơn hàng</label>
                                        ${selectOrderTypes}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label light-blue">Lý do biết đến mua hàng</label>
                                        ${selectOrderReasons}
                                    </div>
                                    <div class="form-group" style="display: none;">
                                        <label class="control-label light-blue">Kênh bán hàng</label>
                                        <?php $this->Mconstants->selectConstants('orderChannels', 'OrderChanelId', 4); ?>
                                    </div>
                                </div>
                                <div class="more-task">
                                    <a href="javascript:void(0)" class="aRemind" id="aRemind_${id}" id-check="${id}" style="padding-left: 0;"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                                </div>
                                <div class="more-tabs">
                                    <div class="form-group">
                                        <label class="control-label">Nhãn (cách nhau bởi dấu phẩy)</label>
                                        <input type="text" class="form-control tags" id="tags_${id}" id-check="${id}">
                                    </div>
                                    <p class="light-gray">Bạn có thể chọn những tag đã được sử dụng</p>
                                    <div class="clearfix">
                                        ${listTag}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="text" hidden="hidden" id="customer_${id}" value="0">
        <input type="text" hidden="hidden" id="customerGroupId1_${id}" value="0">
        <input type="text" hidden="hidden" id="debtCost1_${id}" value="0">
        <input type="text" hidden="hidden" id="orderStoreId_${id}" value="${orderStoreId}">
    `;


}

function loadProductSlider(id){
    $('#ulProduct_'+id).html("");
    $.ajax({
        type: "POST",
        url: $('input#getListProductUrl').val(),
        data: {
            SearchText: '',
            CategoryId: 0,
            PageId: 1,
            Limit: 100
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
               
                var data = json.data;
                var html = '';
                var i, j;
                var productPath = $('input#productPath').val();
                var noImage = 'logo.png';
                for(i = 0; i < data.length; i++){
                    if(data[i].Childs.length > 0){
                        for(j = 0; j < data[i].Childs.length; j++){
                            html += '<li data-id="' + data[i].ProductId + '" data-child="' + data[i].Childs[j].ProductChildId + '">';
                            html += '<a href="javascript:void(0)"><img src="' + productPath + (data[i].Childs[j].ProductImage == '' ? noImage : data[i].Childs[j].ProductImage) + '" width="150">';
                            html += '<span>' + data[i].ProductName + ' (' + data[i].Childs[j].ProductName + ')</span><span>' + formatDecimal(data[i].Childs[j].Price.toString()) + ' VNĐ</span></a></li>';
                        }
                    }
                    else{
                        html += '<li data-id="' + data[i].ProductId + '" data-child="0">';
                        html += '<a href="javascript:void(0)"><img src="' + productPath + (data[i].ProductImage == '' ? noImage : data[i].ProductImage) + '" width="150" height="150">';
                        html += '<span>' + data[i].ProductName + '</span><span>' + formatDecimal(data[i].Price.toString()) + ' VNĐ</span></a></li>';
                    }
                }
                $('#ulProduct_'+id).html(html).bxSlider({
                    minSlides: 4,
                    maxSlides: 4,
                    moveSlides: 1,
                    slideWidth: 500,
                    pager: false,
                    infiniteLoop: false,
                    nextText: ">",
                    prevText: "<"
                });
            }
        },
        error: function (response) {}
    });
    $('#ulProduct_'+id).on('click', 'li', function(){
        chooseProductSearch($(this));
    });
}



// chosse customer


//=====================================================================

function saveOrder(orderId, customerId, orderStatusId, paymentCost, paymentStatusId, tags, products, ownCost, remindDate, ownComment, idCheck){
    if(idCheck == $("#idcheck").val()){
        $('.submit, #btnSaveOwnOrder, #btnNoSaveOwnOrder, #btnSubmitPos').prop('disabled', true);
        var orderServices = [];
        var otherServiceId = 0;
        var serviceCost = 0;
        var debtCost = 0;
        var debitOtherTypeId = $('input#debitOtherTypeId').val();
        $('.divConfigExpand div.item').each(function(){
            otherServiceId = $(this).attr('data-id');
            serviceCost = replaceCost($(this).find('.spanOtherCost_'+idCheck).text(), true);
            if (otherServiceId != '0' && serviceCost > 0){
                orderServices.push({
                    OtherServiceId: otherServiceId,
                    ServiceCost: serviceCost
                });
                if(otherServiceId == debitOtherTypeId) debtCost = serviceCost;
            }
        });
        var comments = [];
        if(orderId == 0){
            $('.listComment #pComment_'+idCheck).each(function(){
                comments.push($(this).text());
            });
        }
        // console.log(JSON.stringify(products))
        // return false;
        var deliveryTypeId = $('input#deliveryTypeId').val();
        $.ajax({
            type: "POST",
            url: $('#orderForm').attr('action'),
            data: {
                OrderId: orderId,
                CustomerId: customerId,
                CustomerAddressId: $('input#customerAddressId').val(),
                StaffId: $('input#userLoginId').val(),
                OrderChanelId: $('select#orderChanelId').val(),
                OrderStatusId: orderStatusId,
                PendingStatusId: $('input#pendingStatusId').val(),
                OrderParentId: 0,
                Discount: $('span#promotionCost_'+idCheck).text() == '-' ? 0 : replaceCost($('span#promotionCost_'+idCheck).text(), true),
                TransportCost: $('span#transportCost_'+idCheck).text() == '-' ? 0 : replaceCost($('span#transportCost_'+idCheck).text(), true),
                TransportTypeId: 0,
                TransportReasonId: 0,
                PaymentCost: paymentCost,
                PaymentStatusId: paymentStatusId,
                PaymentComment: '',// $('#paymentComment').val().trim(),
                TotalCost: replaceCost($('span#orderCost_'+idCheck).text(), true),
                VerifyStatusId: $('input#config_ALLOW_VERIFY_ORDER').val() == 'ON' ? $('input#verifyStatusId').val() : 2,
                OrderTypeId: $('select#orderTypeId_'+idCheck).val(),
                DeliveryTypeId: deliveryTypeId,
                StoreId: $('input#orderStoreId_'+idCheck).val(),
                OrderReasonId: $('select#orderReasonId').val(),

                TagNames: JSON.stringify(tags),
                Products: JSON.stringify(products),
                OrderServices: JSON.stringify(orderServices),

                PromotionId: $('input#promotionId_'+idCheck).val(),
                PromotionCode: $('input#promotionCode_'+idCheck).val().trim(),
                DiscountPercent: $('input#discountPercent_'+idCheck).val(),
                PromotionComment: $('input#promotionComment_'+idCheck).val().trim(),

                Comments: JSON.stringify(comments),

                OwnCost: ownCost,
                RemindDate: remindDate,
                RemindComment: ownComment,

                MoneySourceId: $('#moneySourceId_'+idCheck).val(),
                BankId: $('input#bankIdTmp').val(),
                RealPaymentCost: replaceCost($('input#realPaymentCost_'+idCheck).val(), true)
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    if(orderId == 0){} // redirect(false, $('input#orderEditUrl').val() + '/' + json.data);
                    else{
                        $('input#orderStatusId').val(orderStatusId);
                        $('input#remindOwnCost_'+idCheck).val(ownCost);
                        $('#modalWaitPayment').modal('hide');
                        if(deliveryTypeId == '1' && orderStatusId == 6){
                            $('#aCheckOrder').remove();
                            $('#aNoneCOD').show();
                            $('#spanOrderStatus').text('Thành công');
                            showInfoCustomer($('input#customer_'+idCheck).val(), false);
                        }
                    }
                    $("#modalVerifyComplete").modal('hide');
                    $('.tagsOrder li').each(function(){
                        if($(this).hasClass('active')){
                            $(this).remove();
                            $("#tabo_"+idCheck).remove();
                        }else{
                            $(".tagPos").parent().parent().find('li:eq(0)').addClass('active');
                            var id = $(".tagPos").parent().parent().find('li:eq(0)').find('a').attr('data-id');
                            $("#idcheck").val(id);
                            if($("#check_tag_"+id).attr('flags') === '0'){
                                $(".tab_comtent").append(`<div class="tab-pane" id="tabo_${id}">${htmlTag(id)}</div>`);
                                loadProductSlider(id)
                            }else{
                                $("#tabo_"+$(this).find('a').attr('data-id')).show();
                            }
                        }
                    })
                }
                $('.submit, #btnSaveOwnOrder, #btnNoSaveOwnOrder, #btnSubmitPos').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('.submit, #btnSaveOwnOrder, #btnNoSaveOwnOrder, #btnSubmitPos').prop('disabled', false);
            }
        });
    }
    return false;
    
}

function showInfoCustomer(customerId, isShowCustomerAdress) {
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            var idCheck = $("#idcheck").val();
            if(json.code == 1) {
                var data = json.data;
                $('input#customer_'+idCheck).val(customerId);
                $('input#customerGroupId1_'+idCheck).val(data.CustomerGroupId);
                $('input#debtCost1_'+idCheck).val(data.DebtCost);
                if (data.PhoneNumber != '' && data.PhoneNumber2 != '') data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                if(isShowCustomerAdress) {
                    var address = '';
                    if(data.CountryId == '232' || data.CountryId == '0') {
                        address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                        address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam> ' + data.WardName + '</span>';
                        if (data.DistrictName != '') address += '<span class="br-line i-district">' + data.DistrictName + '</span>';
                        if (data.ProvinceName != '') address += '<span class="br-line i-province">' + data.ProvinceName + '</span>';
                    }
                    else {
                        address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                        address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                    }
                    $('.i-name_'+idCheck).text(data.FullName);
                    $('.i-phone_'+idCheck).text(data.PhoneNumber);
                    $('.i-email_'+idCheck).text(data.Email);
                    $('.i-address_'+idCheck).html(address);
                    $('.i-country_'+idCheck).text(data.CountryName).attr('data-id', data.CountryId).attr('data-province', data.ProvinceId).attr('data-district', data.DistrictId).attr('data-ward', data.WardId).attr('data-zip', data.ZipCode);
                }
                var editCustomerUrl = $('input#editCustomerUrl').val() + '/' + customerId + '/';
                $('h4.i-name_'+idCheck).html('<a href="' + editCustomerUrl + '3" target="_blank">' + data.FullName + '</a>');
                $('div.i-phone_'+idCheck).text(data.PhoneNumber);
                if(data.CustomerGroupName != '') $('.i-cusType_'+idCheck).html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType_'+idCheck).hide();
                $('.i-total-orders_'+idCheck).html('<a href="' + editCustomerUrl + '1" target="_blank">' + data.TotalOrders + '</a>');
                $('span#customerBalance_'+idCheck).html('<a href="' + editCustomerUrl + '2" target="_blank">' + formatDecimal(data.Balance) + '</a>');
                if($('select#orderTypeId').val() != $('input#offsetOrderTypeId').val() && replaceCost($('span#totalPrice').text(), true) > 0 && $('input#deliveryTypeId').val() == '2')  $('.btnTransport').removeClass('actived');
                else if($('select#orderTypeId').val() == $('input#offsetOrderTypeId').val() && $('input#deliveryTypeId').val() == '2')  $('.btnTransport').removeClass('actived');
                else $('.btnTransport').addClass('actived');
                $('#boxChooseCustomer_'+idCheck).hide();
                $('#divCustomer_'+idCheck).show();
            }
            else{
                showNotification(json.message, json.code);
                $('input#customerId').val('0');
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            $('input#customerId').val('0');
        }
    });
}

function chooseProductSearch(tr){
    var id = tr.attr('data-id');
    var idCheck = tr.attr('id-check');
    var childId = tr.attr('data-child');
    var flag = false;
    $(".tbodyProduct").find('tr').each(function(){
        if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId && $(this).attr('id-check') == $("#idcheck").val()) {
            
            flag = true;
            var quantity = replaceCost($(this).find('input.quantity_'+$(this).attr('id-check')).val(), true) + 1;
            $(this).find('input.quantity_'+$(this).attr('id-check')).val(formatDecimal(quantity.toString()));
            quantity = quantity * replaceCost($(this).find('.spanPrice_'+$(this).attr('id-check')).text(), true);
            $(this).find('input.sumPrice_'+$(this).attr('id-check')).val(formatDecimal(quantity.toString()));
            calcPrice(0);
            return false;
        }
    });
    if (!flag) {
        $.ajax({
            type: "POST",
            url: $('input#getProductDetailUrl').val(),
            data: {
                ProductId: id,
                ProductChildId: childId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var data = json.data;
                    var products = data.Products;
                    var productPath = $('input#productPath').val();
                    var html = '';
                    var totalPrice = replaceCost($('span#totalPrice_'+$("#idcheck").val()).text(), true);
                    var totalWeight = 0;
                    for (var i = 0; i < products.length; i++) {
                        html += '<tr id-check="'+idCheck+'" data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '" data-weight="' + products[i].Weight + '" data-kind="' + products[i].ProductKindId + '">';
                        html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank">' + products[i].ProductName + '</a></td>';
                        html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                        html += '<td class="text-center">' + products[i].BarCode + '</td>';
                        html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                        html += '<td class="tdPrice text-right"><span class="spanPrice_'+idCheck+'">' + formatDecimal(products[i].Price.toString()) + '</span> ₫</td>';
                        html += '<td><input class="form-control quantity_keyup quantity_'+idCheck+' sll" id-check="'+idCheck+'" value="1"></td>';
                        html += '<td><input class="form-control sumPrice_'+idCheck+' text-right" disabled value="' + formatDecimal(products[i].Price.toString()) + '"></td>';
                        html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>';
                        html += '<span class="productPrices_'+idCheck+'" style="display: none;">' + JSON.stringify(data.ProductPrices) + '</span><span class="originalPrice_'+idCheck+'" style="display: none;">' + products[i].Price + '</span></td></tr>';
                        totalPrice += parseInt(products[i].Price.toString());
                        totalWeight += parseInt(products[i].Weight);
                    }
                        $('.tbodyProduct_'+idCheck).append(html);
                        $('span#totalPrice').text(formatDecimal(totalPrice.toString()));
                        calcPrice(0);
                        var inputWeight = $('input#transportWeight');
                        if(inputWeight.length > 0){
                            totalWeight += replaceCost(inputWeight.val(), true);
                            inputWeight.val(formatDecimal(totalWeight.toString()));
                        }
                    
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}

function calcPrice(totalPrice){
    var idCheck = $("#idcheck").val();
    var orderTypeId = parseInt($('select#orderTypeId').val());
    var offsetOrderTypeId = parseInt($('input#offsetOrderTypeId').val());
    if(orderTypeId != offsetOrderTypeId) {
        if (totalPrice == 0) {
            $('.tbodyProduct tr').each(function () {
                if($(this).attr('id-check') == idCheck){
                    totalPrice += replaceCost($(this).find('input.sumPrice_'+idCheck).val(), true);
                }
                
            });
        }
    }
    else totalPrice = replaceCost($('span#totalCost_'+idCheck).text(), true);
    $('span#totalPrice_'+idCheck).text(formatDecimal(totalPrice.toString()));
    var isShip = $('input#deliveryTypeId').val() == '2';
    var orderCost = totalPrice;
    var cost = $('span#promotionCost_'+idCheck).text();
    orderCost -= (cost == '-' ? 0 : replaceCost(cost, true));
    cost = $('span#transportCost_'+idCheck).text();
    orderCost += (cost == '-' ? 0 : replaceCost(cost, true));
    var totalCost = orderCost;
    var debitOtherTypeId = $('input#debitOtherTypeId').val();
    var serviceCost = 0;
    $('.divConfigExpand div.item').each(function(){
        serviceCost = replaceCost($(this).find('.spanOtherCost_'+idCheck).text(), true);
        if($(this).attr('data-id') == debitOtherTypeId) totalCost -= serviceCost;
        else{
            orderCost += serviceCost;
            totalCost += serviceCost
        }
    });
    $('span#orderCost_'+idCheck).text(formatDecimal(orderCost.toString()));
    var paymentCost = replaceCost($('input#paymentCost_'+idCheck).val(), true);
    var validPayment = true;
    if(paymentCost > orderCost){
        paymentCost = 0;
        validPayment = false;
    }
    if(isShip) totalCost -= paymentCost;
    if(orderTypeId == offsetOrderTypeId) totalCost = formatDecimal(totalPrice.toString());
    else totalCost = formatDecimal(totalCost.toString());
    $('span#totalCost_'+idCheck).text(totalCost);
    $('input#realPaymentCost_'+idCheck).val(totalCost);
    var flag = $('input#customerId').val() != '0' && isShip;
    if(orderTypeId != offsetOrderTypeId && totalPrice > 0 && flag) $('.btnTransport').removeClass('actived');
    else if(orderTypeId == offsetOrderTypeId && flag) $('.btnTransport').removeClass('actived');
    else $('.btnTransport').addClass('actived');
    if($('input#transportCODCost').length > 0) $('input#transportCODCost').val(totalCost);
    keyUpPayment(paymentCost, orderCost, validPayment);
}

function keyUpPayment(paymentCost, orderCost, validPayment){
    if(paymentCost == 0){
        if(validPayment) {
            $('input#paymentStatusId').val('1');
            $('#aPayment').css('line-height', '20px');
            $('#iPaymentText').text('(Không thanh toán trước)');
        }
        else{
            $('input#paymentCost').val('0');
            $('input#paymentStatusId').val('0');
            $('#aPayment').css('line-height', '34px');
            $('#iPaymentText').text('');
            showNotification('Số tiền thanh toán không được lớn hơn tổng cần thanh toán', 0);
        }
    }
    else{
        if(paymentCost == orderCost){
            $('input#paymentStatusId').val('3');
            $('#aPayment').css('line-height', '20px');
            $('#iPaymentText').text('(Thanh toán trước toàn bộ)');
        }
        else{
            $('input#paymentStatusId').val('2');
            $('#aPayment').css('line-height', '20px');
            $('#iPaymentText').text('(Thanh toán trước 1 phần)');
        }
    }
}

function checkDebtCost(debtCost, msg){
    var ownCost = 0;
    var isShip = $('input#deliveryTypeId').val() == '2';
    var idCheck = $("#idcheck").val();
    if(isShip){
        var paymentCost = replaceCost($('input#paymentCost_'+idCheck).val());
        ownCost = debtCost + paymentCost - replaceCost($('span#customerBalance_'+idCheck).text(), true);
        if(ownCost > replaceCost($('input#debtCost1_'+idCheck).val(), true)) {
            $('input#ownCost_'+idCheck).val(formatDecimal(ownCost.toString()));
            showNotification(msg, 0);
            return false;
        }
    }
    else{
        ownCost = debtCost + replaceCost($('span#orderCost_'+idCheck).text(), true) - replaceCost($('span#customerBalance_'+idCheck).text(), true);
        if(ownCost > replaceCost($('input#debtCost1_'+idCheck).val(), true)) {
            $('input#ownCost_'+idCheck).val(formatDecimal(ownCost.toString()));
            showNotification(msg, 0);
            return false;
        }
    }
    return true;
}

function getListOrderProducts(){
    var products = [];
    var idCheck = $("#idcheck").val();
    $('.tbodyProduct tr').each(function () {
        if(idCheck == $(this).attr('id-check')){
            products.push({
                ProductId: parseInt($(this).attr('data-id')),
                ProductChildId: parseInt($(this).attr('data-child')),
                Quantity: replaceCost($(this).find('input.quantity_'+idCheck).val(), true),
                Price: replaceCost($(this).find('.spanPrice_'+idCheck).text(), true),
                OriginalPrice: 0,
                DiscountReason: ''
            });
        }
        
    });
    return products;
}

function updateOrderField(orderId, fieldName, fieldValue, fnSuccess, fnError){
    $.ajax({
        type: "POST",
        url: $('input#updateFieldUrl').val(),
        data: {
            OrderId: orderId,
            FieldName: fieldName,
            FieldValue: fieldValue
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                fnSuccess(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            fnError();
        }
    });
}

function genItemCommentTag(comment,idCheck){
    var html = '<div class="box-customer mb10"><table><tbody><tr><th rowspan="2" valign="top"><img src="assets/vendor/dist/img/users2.png" alt=""></th>';
    html +='<th><a href="javascript:void(0)" class="name">' + $('input#fullNameLoginId').val() + '</a></th>';
    html += '<th class="time">' + getCurrentDateTime(6) + '</th></tr><tr><td colspan="2"><p class="pComment" id="pComment_'+idCheck+'">' + comment + '</p></td></tr></tbody></table></div>';
    return html;
}