$(document).ready(function(){
	$('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
	$('#btnUpImage2').click(function(){
        chooseFile('Products', function(fileUrl) {
            $('img#productImage1').attr('src', fileUrl).show();
        });
    });
    $('#btnUpImage3').click(function(){
        chooseFile('Products', function(fileUrl) {
            $('img#productImage2').attr('src', fileUrl).show();
        });
    });

    $('.submit').click(function(){
    	if(validateEmpty('#collaboratorForm')){
    		var imgBefore = $("img#productImage1").attr('src');
	    	var imgAfter  = $("img#productImage2").attr('src');
	    	if(imgBefore == '' && imgAfter == ''){
	    		showNotification('Vui lòng chọn hình', 0);
	    		return false;
	    	}
	    	var images = [];
	    	images.push(imgBefore);
	    	images.push(imgAfter);
	    	$.ajax({
                type: "POST",
                url: $('#collaboratorForm').attr('action'),
                data: {
                	CustomerId : $('input#customerId').val(),
                	IDCardNumber : $('input#iDCardNumber').val(),
                	FullName : 	$('input#fullName').val(),
                	IDCardDate : $('input#iDCardDate').val(),
                	IDCardAddress : $('input#iDCardAddress').val(),
                	BirthDay: $('input#birthDay').val(),
                	IDCardImages : JSON.stringify(images),
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(customerId == 0) redirect(false, $('a#customerListUrl').attr('href'));
                        else redirect(true, '');
                    }
                    // else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    // $('.submit').prop('disabled', false);
                }
            })
    	}
    	return false;

    })
})