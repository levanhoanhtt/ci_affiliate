$(document).ready(function(){
    /*actionItemAndSearch({
        ItemName: 'Phiếu',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });*/
    getListTransaction($('#dateRangePicker').val());
    dateRangePicker(function(dates){
        getListTransaction(dates);
    });
});

function getListTransaction(dates){
    var beginDate = '';
    var endDate = '';
    dates = dates.trim().split('-');
    if(dates.length == 2){
        beginDate =  dates[0].trim();
        endDate =  dates[1].trim();
    }
    if(beginDate != '' && endDate != ''){
        //check xem co chua
        var index = -1;
        var i = 0;
        for(i = 0; i < data_filter.itemFilters.length; i++){
            if(data_filter.itemFilters[i].field_name == "group_date"){
                index = i;
                break;
            }
        }
        if(index > -1){
            data_filter.itemFilters[index].conds = ["between", beginDate, endDate];
            data_filter.itemFilters[index].tag = 'Thời điểm tạo phiếu trong khoảng ' + beginDate + ' đến ' + endDate;
            for(i = 0; i < data_filter.tagFilters.length; i++){
                if(data_filter.tagFilters[i].indexOf('Thời điểm tạo phiếu') > -1){
                    data_filter.tagFilters[i] = 'Thời điểm tạo phiếu trong khoảng ' + beginDate + ' đến ' + endDate;
                    break;
                }
            }
        }
        else{
            data_filter.itemFilters.push({
                conds : ["between", beginDate, endDate],
                field_name: "group_date",
                tag: 'Thời điểm tạo phiếu trong khoảng ' + beginDate + ' đến ' + endDate
            });
            data_filter.tagFilters.push('Thời điểm tạo phiếu trong khoảng ' + beginDate + ' đến ' + endDate);
        }
        actionItemAndSearch({
            ItemName: 'Phiếu',
            IsRenderFirst: 1,
            extendFunction: function(itemIds, actionCode){}
        });
    }
    else showNotification("Vui lòng nhập ngày tháng", 0);
}

function renderContentTransactions(datas) {
    var data = datas.data;
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditTransaction = $('#urlEditTransaction').val() + '/';
        var urlEditTransactionInternal = $('#urlEditTransactionInternal').val() + '/';
        //var sumPaidCost = 0;
        for (var item = 0; item < data.length; item++) {
            //sumPaidCost += parseInt(data[item].PaidCost);
            html += '<tr>';
            if(data[item].TypeId == 1) html += '<td><a href="' + urlEditTransaction + data[item].TransactionId + '">' + data[item].TransactionCode + ' <i class="fa fa-check"></i></a></td>';
            else html += '<td><a href="' + urlEditTransactionInternal + data[item].TransactionInternalId + '">' + data[item].TransactionCode + ' <i class="fa fa-check"></i></a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td>' + data[item].FullName + '</td>';
            html += '<td class="text-right">' + formatDecimal(data[item].PaidCost) + '</td>';
            html += '<td>' + data[item].MoneySourceName + '</td>';
            html += '<td>' + (data[item].TransactionReasonName != null ? data[item].TransactionReasonName : '') + '</td>';
            html += '<td class="text-center"><span class="' + labelCss.TypeNameCss[data[item].TypeId1] + '">' + data[item].TypeName + '</span></td>';
            html += '<td class="text-center"><span class="' + labelCss.VerifyLevelCss[data[item].VerifyLevelId] + '">' + data[item].VerifyLevelName + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="8" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
        $('#total-survival').text(formatDecimal(datas.total.totalSurvival.toString())+ ' đ');
        $("#total-revenue").text(formatDecimal(datas.total.totalReceive.toString())+ ' đ');
        $("#total-expenditure").text(formatDecimal(datas.total.totalPayment.toString())+ ' đ');
    }
}