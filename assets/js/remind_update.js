var app = app || {};

app.init = function (remindId) {
    app.initLibrary();
    app.remindComment(remindId);
    app.submit(remindId);
};

app.initLibrary = function(){
    //var dateNow = new Date();
    $('input#remindDate').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('select#userId').change(function(){
        if($(this).val() != '0') $('select#partId').val('0').trigger('change');
    });
    $('select#partId').change(function(){
        if($(this).val() != '0') $('select#userId').val('0').trigger('change');
    });
};

app.remindComment = function(remindId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            if(remindId > 0){
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertRemindCommentUrl').val(),
                    data: {
                        RemindId: remindId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            $('div#listComment').prepend(genItemComment(comment));
                            $('input#comment').val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment').prepend(genItemComment(comment));
                $('input#comment').val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    if(remindId > 0){
        $('#aShowComment').click(function(){
            $('#modalItemComment').modal('show');
            return false;
        });
    }
};

app.submit = function(remindId){
    $('.submit').click(function(){
        if(validateEmpty('#remindForm')){
            var userId = $('#userId').val();
            var partId = $('#partId').val();
            if(userId == '0' && partId == '0'){
                showNotification('Chọn nhắc nhở cho đối tượng nào', 0);
                return false;
            }
            $('.submit').prop('disabled', true);
            var comments = [];
            if(remindId == 0){
                $('#listComment .pComment').each(function(){
                    comments.push($(this).text());
                });
            }
            var timeProcessed = 0;
            var remindDate = $('input#remindDate').val().trim();
            if(remindId > 0) {
                if ($("input#remindDateOld").val() != remindDate) timeProcessed = parseInt($('input#timeProcessed').val()) + 1;
                else timeProcessed = $('input#timeProcessed').val();
            }
            $.ajax({
                type: "POST",
                url: $('#remindForm').attr('action'),
                data: {
                    RemindId: remindId,
                    RemindTitle: $('input#remindTitle').val().trim(),
                    RemindDate: remindDate,
                    IsRepeat: 1,
                    RepeatDay: 0,
                    RepeatHour: 0,
                    RemindStatusId: $('select#remindStatusId').val(),
                    RemindTypeId: $('input#remindTypeId').val(),
                    UserId: userId,
                    PartId: partId,
                    OrderId: $('input#orderId').val(),
                    CustomerId: $('input#customerId').val(),
                    OwnCost: $('input#ownCost').val(),
                    CustomerConsultId: $('input#customerConsultId').val(),
                    TimeProcessed : timeProcessed,

                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(remindId == 0) redirect(false, $('#aRemindList').attr('href'));
                        else redirect(true, '');
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
};

$(document).ready(function(){
	var remindId = parseInt($('input#remindId').val());
    app.init(remindId);
});