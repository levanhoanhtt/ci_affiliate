var app = app || {};

app.init = function(customerConsultId) {
    app.initLibrary();
    app.customer();
    app.product();
    app.addComment(customerConsultId);
    app.submit(customerConsultId);
};
app.initLibrary = function() {
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input#consultDate').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
};

app.customer = function(){
    chooseCustomer(function (li) {
        var customerId = parseInt(li.attr('data-id'));
        $('input#customerId').val('0');
        if (customerId > 0) showInfoCustomer(customerId);
    });
    province('provinceId', 'districtId', 'wardId');
    province('customerProvinceId', 'customerDistrictId', 'customerWardId');
    $('#ulCustomerKindId').on('click', 'a', function(){
        var id = $(this).attr('data-id');
        $('input#customerKindId').val(id);
        if(id == '2') $('#divWholesale').show();
        else{
            $('input#debtCost, select#paymentTimeId').val('0');
            $('#divWholesale').hide();
        }
        $('select#customerGroupId .op').hide();
        $('select#customerGroupId .op_' + id).show();
        $('select#customerGroupId').val('0');
    });
    $('input.iCheckCustomerType').on('ifToggled', function (e) {
        if (e.currentTarget.checked) {
            if (e.currentTarget.value == '2') $('#divCompany').show();
            else $('#divCompany').hide();
        }
    });
    $('select#countryId, select#customerCountryId').change(function () {
        if ($(this).val() == '232' || $(this).val() == '0') {
            $('.VNoff').css('display', 'none');
            $('.VNon').fadeIn();
        }
        else {
            $('.VNon').css('display', 'none');
            $('.VNoff').fadeIn();
        }
    });
    //END add customer===================
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customerId').val());
    $('#btnCloseBoxCustomer').click(function (event) {
        $('#divCustomer').hide();
        $('#boxChooseCustomer').show();
        $('input#customerId').val('0');
        return false;
    });
    $('#btnAddCustomer').click(function () {
        $('#customerForm').trigger('reset');
        $('#modalAddCustomer').modal('show');
    });
    $('#btnUpdateCustomer').click(function () {
        var btn = $(this);
        if (validateEmpty('#customerForm')) {
            var password = $('input#password').val().trim();
            if(password != ''){
                if(password != $('input#rePass').val().trim()){
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            btn.prop('disabled', true);
            var isReceiveAd = 1;
            if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
            $.ajax({
                type: "POST",
                url: $('#customerForm').attr('action'),
                data: {
                    CustomerId: 0,
                    FirstName: $('input#firstName').val().trim(),
                    LastName: $('input#lastName').val().trim(),
                    //FullName: $('input#fullName').val().trim(),
                    Email: $('input#email').val().trim(),
                    PhoneNumber: $('input#phoneNumber').val().trim(),
                    //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                    GenderId: $('input[name="GenderId"]:checked').val(),
                    StatusId: 2,
                    BirthDay: $('input#birthDay').val(),
                    CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                    CustomerKindId: $('input#customerKindId').val(),
                    CountryId: $('select#countryId').val(),
                    ProvinceId: $('select#provinceId').val(),
                    DistrictId: $('select#districtId').val(),
                    WardId: $('select#wardId').val(),
                    ZipCode: $('input#zipCode').val(),
                    Address: $('input#address').val().trim(),
                    CustomerGroupId: $('select#customerGroupId').val(),
                    FaceBook: $('input#facebook').val().trim(),
                    Comment: $('input#customerComment').val().trim(),
                    CareStaffId: $('select#careStaffId').val(),
                    DiscountTypeId: $('select#discountTypeId').val(),
                    PaymentTimeId: $('select#paymentTimeId').val(),
                    PositionName: $('input#positionName').val().trim(),
                    CompanyName: $('input#companyName').val().trim(),
                    TaxCode: $('input#taxCode').val().trim(),
                    DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                    Password: password,
                    //TagNames: JSON.stringify(tags),
                    IsReceiveAd: isReceiveAd
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        showInfoCustomer(json.data);
                        $('#modalAddCustomer').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
};

app.product = function(){
    chooseProduct(function(tr){
        var id = tr.attr('data-id');
        var childId = tr.attr('data-child');
        var flag = false;
        $('#tbodyProduct tr').each(function(){
            if($(this).attr('data-id') == id && $(this).attr('data-child') == childId){
                flag = true;
                var quantity = replaceCost($(this).find('input.quantity').val(), true) + 1;
                $(this).find('input.quantity').val(formatDecimal(quantity.toString()));
                quantity = quantity * replaceCost($(this).find('.spanPrice').text(), true);
                $(this).find('input.sumPrice').val(formatDecimal(quantity.toString()));
                calcPrice(0);
                return false;
            }
        });
        if(!flag) {
            $.ajax({
                type: "POST",
                url: $('input#getProductDetailUrl').val(),
                data: {
                    ProductId: id,
                    ProductChildId: childId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var products = json.data.Products;
                        var productPath = $('input#productPath').val();
                        var html = '';
                        for(var i = 0; i < products.length; i++){
                            html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '">';
                            html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + products[i].ProductName + '</a></td>';
                            html += '<td><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                        }
                        $('#tbodyProduct').append(html);
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    $('#tbodyProduct').on('click', '.link_delete', function () {
        $(this).parent().parent().remove();
        return false;
    });
};

app.addComment = function(customerConsultId){
    $('#btnAddComment').click(function(){
        var btn = $(this);
        var comment = $('input#staffComment').val().trim();
        if(comment != ''){
            if(customerConsultId > 0){
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#addCommentUrl').val(),
                    data: {
                        CustomerConsultId: customerConsultId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) addHtmlComment(comment);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else addHtmlComment(comment);
        }
        else{
            showNotification('Bạn chưa nhập ghi chú', 0);
            $('input#staffComment').focus();
        }
    });
};

app.submit = function(customerConsutId){
    $("#customerConsultForm").submit(function(e){
        e.preventDefault();
    });
    $('.submit').click(function() {
        if(validateEmpty('#customerConsultForm')){
            var customerId = parseInt($('input#customerId').val());
            if(customerId > 0){
                $('.submit').prop('disabled', true);
                var productId = 0;
                var productChildid = 0;
                var productName = '';
                if($('#tbodyProduct tr').length > 0){
                    var trFirst = $('#tbodyProduct tr').first();
                    productId = trFirst.attr('data-id');
                    productChildid = trFirst.attr('data-child');
                    productName = trFirst.find('a.light-dark').text()
                }
                var comments = [];
                if(customerConsutId == 0){
                    $('#ulComments li h3').each(function(){
                        comments.push({
                            UserId: $('input#userLoginId').val(),
                            Comment: $(this).text(),
                            CrDateTime: $(this).attr('data-time')
                        });
                    });
                }
                $.ajax({
                    type: "POST",
                    url: $('#customerConsultForm').attr('action'),
                    data: {
                        CustomerConsultId: customerConsutId,
                        CustomerId: customerId,
                        FullName: $('h4.i-name').first().text(),
                        PhoneNumber: $('div.i-phone').first().text(),
                        ConsultDate: $('input#consultDate').val().trim(),
                        ProductId: productId,
                        ProductChildId: productChildid,
                        Comment: $('input#comment').val().trim(),
                        Comments: JSON.stringify(comments),

                        ConsultDateOld: $('input#consultDateOld').val(),
                        ProductName: productName,
                        IsFront: 0
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1){
                            if(customerConsutId == 0) redirect(false, $('input#ediCustomerConsultUrl').val() + '/' + json.data);
                            else $('.submit').prop('disabled', false);
                        }
                        else $('.submit').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('.submit').prop('disabled', false);
                    }
                });
            }
            else showNotification('Vui lòng chọn Khách hàng', 0);
        }
    });
};

$(document).ready(function () {
    var customerConsultId = parseInt($('input#customerConsultId').val());
    app.init(customerConsultId);
});

function showInfoCustomer(customerId) {
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
                $('input#customerId').val(customerId);
                var data = json.data;
                if (data.PhoneNumber != '' && data.PhoneNumber2 != '') data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                //==================
                $('input#customerName').val(data.FullName);
                $('input#customerEmail').val(data.Email);
                $('input#customerPhone').val(data.PhoneNumber);
                $('select#customerCountryId').val(data.CountryId);
                $('select#customerProvinceId').val(data.ProvinceId);
                $('select#customerDistrictId').val(data.DistrictId);
                $('select#customerWardId').val(data.WardId);
                $('input#customerZipCode').val(data.ZipCode);
                $('input#customerAddress').val(data.Address);
                if (data.CountryId == '232' || data.CountryId == '0') {
                    $('.VNoff').css('display', 'none');
                    $('.VNon').fadeIn();
                }
                else {
                    $('.VNon').css('display', 'none');
                    $('.VNoff').fadeIn();
                }
                //==================
                var address = '';
                if (data.CountryId == '232') {
                    address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                    //if (data.WardName != '')
                    address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam>> ' + data.WardName + '</span>';
                    if (data.DistrictName != '') address += '<span class="br-line i-district">' + data.DistrictName + '</span>';
                    if (data.ProvinceName != '') address += '<span class="br-line i-province">' + data.ProvinceName + '</span>';
                }
                else {
                    address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                    address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                }
                $('.i-name').text(data.FullName);
                $('.i-phone').text(data.PhoneNumber);
                $('.i-total-orders').text(data.TotalOrders);
                if(data.CustomerGroupName != '') $('.i-cusType').html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType').hide();
                $('.i-email').text(data.Email);
                $('.i-country').text(data.CountryName);
                $('.i-address').html(address);
                $('#boxChooseCustomer').hide();
                $('#divCustomer').show();
                $('span#customerBalance').text(formatDecimal(data.Balance));
                if(replaceCost($('span#totalCost').text(), true) > 0 && $('input[name="DeliveryTypeId"]:checked').val() == '2')  $('.btnTransport').removeClass('actived');
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function addHtmlComment(comment){
    var now = new Date();
    var html = '<li class="time-label"><span class="bg-blue">' + getCurrentDateTime(3, now) + '</span></li>';
    html += '<li><i class="fa fa-user bg-blue"></i><div class="timeline-item"><span class="time"><i class="fa fa-clock-o"></i> ' + getCurrentDateTime(4, now) + '</span>';
    html += '<h3 class="timeline-header" data-time="' + getCurrentDateTime(5, now) + '">' + comment + '</h3></div></li>';
    $('#ulComments').prepend(html);
    $('input#staffComment').val('');
}