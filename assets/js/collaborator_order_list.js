$(document).ready(function () {
    actionItemAndSearch({
        ItemName: 'Đơn hàng',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
            // if(actionCode == 'print_order'){
            //     $('input#orderIds').val(JSON.stringify(itemIds));
            //     $('#printForm').submit();
            // }
            // else if(actionCode.indexOf('verify_order-') >= 0) {
            //     actionCode = actionCode.split('-');
            //     if (actionCode.length == 2) {
            //         var verifyStatusId = parseInt(actionCode[1]);
            //         $.ajax({
            //             type: "POST",
            //             url: $('input#changeVerifyStatusBatchUrl').val(),
            //             data: {
            //                 OrderIds: JSON.stringify(itemIds),
            //                 VerifyStatusId: verifyStatusId
            //             },
            //             success: function (response) {
            //                 var json = $.parseJSON(response);
            //                 showNotification(json.message, json.code);
            //                 if (json.code == 1){
            //                     var i;
            //                     if(verifyStatusId == 2){
            //                         for (i = 0; i < itemIds.length; i++){
            //                             if($('td#orderCode_' + itemIds[i] + ' i.fa-check').length == 0) $('td#orderCode_' + itemIds[i]).append('<i class="fa fa-check tooltip1 active" title="Đã xác thực"></i>');
            //                         }
            //                         $('#tbodyOrder i.fa-check').tooltip();
            //                     }
            //                     else{
            //                         for (i = 0; i < itemIds.length; i++) $('td#orderCode_' + itemIds[i] + ' i.fa-check').remove();
            //                     }
            //                 }
            //             },
            //             error: function (response) {
            //                 showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            //             }
            //         });
            //     }
            // }
        }
    });
    $('#tbodyOrder i.tooltip1').tooltip();

    $("#tbodyOrder").on('click','.modalDetailOrder', function(){
        var orderId = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: $('input#listDetailOrderUrl').val(),
            data: {
                OrderId: orderId,
            },
            success: function (response) {
                var json = $.parseJSON(response);
                var html = "";
                $.each(json['data'], function(keu, value){
                    html += `<tr>
                        <td>
                            <img src="${value.ProductImage}" class="productImg">
                            ${value.ProductName}
                        </td>
                        <td class="text-center">${value.ProductUnitName}</td>
                        <td class="text-center">${value.BarCode}</td>
                        <td class="text-center">${value.GuaranteeMonth}</td>
                        <td class="text-center">${value.Price}</td>
                        <td>${value.Quantity}</td>
                        <td class="text-right">${value.Total}</td>
                    </tr>`;
                })
                $("#tbodyProduct").html(html);
                $("#modalDetailOrder").modal('show');
                console.log(response)
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    })
});

function renderContentOrders(data) {
    var html = '';
    if(data!=null) {
        var labelCss    = [];
        var orderLabelCss   = [];
        var transportStatusCss = [];
        if(data.length > 0){
            labelCss = data[0].labelCss;
            orderLabelCss = data[0].orderLabelCss;
            transportStatusCss = data[0].transportStatusCss;
        }
        var urlEditOrder = $('#urlEditOrder').val() + '/';
        // var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var sumTotalCost = 0;
        var totalDiscountCost = 0;
        for (var item = 0; item < data.length; item++) {
            sumTotalCost += parseInt(data[item].TotalCost);
            totalDiscountCost += parseInt(data[item].DiscountCost);
            html += '<tr id="trItem_'+data[item].OrderId+'" class="orderStatus_'+data[item].OrderStatusId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].OrderId + '"></td>';
            html += '<td id="orderCode_'+data[item].OrderId+'"><a href="javascript:void(0)" class="modalDetailOrder" data-id="'+data[item].OrderId+'">' + data[item].OrderCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td class="text-right">' + formatDecimal(data[item].TotalCost) + '</td>';
            html += '<td class="text-right">' + formatDecimal(data[item].DiscountCost) + '</td>';
            html += '<td class="text-center"><span class="' + orderLabelCss.OrderStatusCss[data[item].OrderStatusId] + '">' + data[item].OrderStatus + '</span></td>';
            html += '<td class="text-right"><span class="' + labelCss[data[item].DiscountStatusId] + '">' + data[item].DiscountStatus + '</span></td>';
            html += '<td>'+data[item].PartnerName+'</td>';
            html += '</tr>';
        }
        if(html != '') html += '<tr><td colspan="3" class="text-right"></td><td class="text-right">' + formatDecimal(sumTotalCost.toString()) + '</td><td class="text-right">' + formatDecimal(totalDiscountCost.toString()) + '</td></tr>';
        html += '<tr><td colspan="8" class="paginate_table"></td></tr>';
        $('#tbodyOrder').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('#tbodyOrder i.tooltip1').tooltip();
}