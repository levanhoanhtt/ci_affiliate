var app = app || {};

app.init = function(transactionId, canEdit) {
    app.initLibrary();
    app.customer(canEdit);
    app.updateVerifyLevel(transactionId);
    app.chooseBank(transactionId);
    if(transactionId > 0 && canEdit == 1) app.cancelTransaction(transactionId);
};

app.initLibrary = function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.iCheckRadio').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    $('input#cbHasDebt').on('ifToggled', function (e) {
        if(e.currentTarget.checked) $('#divDebtComment').show();
        else{
            $('#divDebtComment').hide();
            $('#debtComment').val('');
        }
    });
    var moneySourceId = $('#moneySourceId').val();
    $('.divMoneySource').hide();
    $('.divMoneySource_' + moneySourceId).show();
    $('#moneySourceId').change(function(e) {
        moneySourceId = $(this).val();
        $('.divMoneySource select').val('0');
        $('input#bankId').val('0');
        $('#bankInfo').html('');
        $('.divMoneySource').hide();
        $('.divMoneySource_' + moneySourceId).show();
        if(moneySourceId == '2'){
            $('#listBank li').removeClass('active');
            $('input#bankIdTmp').val('0');
            //$('#modalChooseBank').modal('show');
            $('#selectBank').show();
        }
    });

    $('input#paidCost').keydown(function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).keyup(function (e) {
        var value = formatDecimal($(this).val());
        $(this).val(value);
    });
};

app.customer = function(canEdit){
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customerId').val());
    if(canEdit == 1) {
        chooseCustomer(function (li) {
            var customerId = parseInt(li.attr('data-id'));
            $('input#customerId').val('0');
            if (customerId > 0) showInfoCustomer(customerId);
        });
        province('provinceId', 'districtId', 'wardId');
        province('customerProvinceId', 'customerDistrictId', 'customerWardId');
        $('#ulCustomerKindId').on('click', 'a', function(){
            var id = $(this).attr('data-id');
            $('input#customerKindId').val(id);
            if(id == '2') $('#divWholesale').show();
            else{
                $('input#debtCost, select#paymentTimeId').val('0');
                $('#divWholesale').hide();
            }
            $('select#customerGroupId .op').hide();
            $('select#customerGroupId .op_' + id).show();
            $('select#customerGroupId').val('0');
        });
        $('input.iCheckCustomerType').on('ifToggled', function (e) {
            if (e.currentTarget.checked) {
                if (e.currentTarget.value == '2') $('#divCompany').show();
                else $('#divCompany').hide();
            }
        });
        $('select#countryId, select#customerCountryId').change(function () {
            if ($(this).val() == '232' || $(this).val() == '0') {
                $('.VNoff').css('display', 'none');
                $('.VNon').fadeIn();
            }
            else {
                $('.VNon').css('display', 'none');
                $('.VNoff').fadeIn();
            }
        });
        $('#btnCloseBoxCustomer').click(function (event) {
            $('#divCustomer').hide();
            $('#boxChooseCustomer').show();
            $('input#customerId').val('0');
            return false;
        });
        $('#btnAddCustomer').click(function () {
            $('#customerForm').trigger('reset');
            $('#modalAddCustomer').modal('show');
        });
        $('#btnUpdateCustomer').click(function () {
            var btn = $(this);
            if (validateEmpty('#customerForm')) {
                var password = $('input#password').val().trim();
                if (password != '') {
                    if (password != $('input#rePass').val().trim()) {
                        showNotification('Mật khẩu không trùng', 0);
                        return false;
                    }
                }
                btn.prop('disabled', true);
                var isReceiveAd = 1;
                if ($('input#isReceiveAd').parent('div').hasClass('checked')) isReceiveAd = 2;
                $.ajax({
                    type: "POST",
                    url: $('#customerForm').attr('action'),
                    data: {
                        CustomerId: 0,
                        FirstName: $('input#firstName').val().trim(),
                        LastName: $('input#lastName').val().trim(),
                        //FullName: $('input#fullName').val().trim(),
                        Email: $('input#email').val().trim(),
                        PhoneNumber: $('input#phoneNumber').val().trim(),
                        //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                        GenderId: $('input[name="GenderId"]:checked').val(),
                        StatusId: 2,
                        BirthDay: $('input#birthDay').val(),
                        CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                        CustomerKindId: $('input#customerKindId').val(),
                        CountryId: $('select#countryId').val(),
                        ProvinceId: $('select#provinceId').val(),
                        DistrictId: $('select#districtId').val(),
                        WardId: $('select#wardId').val(),
                        ZipCode: $('input#zipCode').val(),
                        Address: $('input#address').val().trim(),
                        CustomerGroupId: $('select#customerGroupId').val(),
                        FaceBook: $('input#facebook').val().trim(),
                        Comment: $('input#customerComment').val().trim(),
                        CareStaffId: $('select#careStaffId').val(),
                        DiscountTypeId: $('select#discountTypeId').val(),
                        PaymentTimeId: $('select#paymentTimeId').val(),
                        PositionName: $('input#positionName').val().trim(),
                        CompanyName: $('input#companyName').val().trim(),
                        TaxCode: $('input#taxCode').val().trim(),
                        DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                        Password: password,
                        //TagNames: JSON.stringify(tags),
                        IsReceiveAd: isReceiveAd
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) {
                            showInfoCustomer(json.data);
                            $('#modalAddCustomer').modal('hide');
                        }
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            return false;
        });
    }
    else $('#btnCloseBoxCustomer').remove();
};

app.updateVerifyLevel = function(transactionId){
    $('#btnUpdateVerifyLevel').click(function(){
        var btn = $(this);
        var verifyLevelId = parseInt($('input[name="VerifyLevelId"]:checked').val());
        if(verifyLevelId > 0){
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateVerifyLevelUrl').val(),
                data: {
                    TransactionId: transactionId,
                    VerifyLevelId: verifyLevelId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái duyệt', 0);
    });
};

app.chooseBank = function(transactionId){
    var bankId = $('input#bankId').val();
    if(bankId != '0' && transactionId > 0){
        $('#bankInfo').html($('#listBank li[data-id="' + bankId + '"]').html());
        $('#selectBank').hide();
    }
    else $('#selectBank').show();
    $('#iBank, #selectBank').click(function(){
        bankId = $('input#bankId').val();
        $('input#bankIdTmp').val(bankId);
        if(bankId != '0'){
            $('#listBank li').removeClass('active');
            $('#listBank li[data-id="' + bankId + '"]').addClass('active');
        }
        $('#modalChooseBank').modal('show');
    });
    $('input.iCheckBankType').on('ifToggled', function(e){
        if(e.currentTarget.checked){
            $('#listBank li').hide();
            $('.liBank_' + e.currentTarget.value).show();
        }
    });
    $('#listBank').on('click', 'li', function(){
        $('#listBank li').removeClass('active');
        $(this).addClass('active');
        $('input#bankIdTmp').val($(this).attr('data-id'));
    });
    $('#btnChooseBank').click(function(){
        bankId = $('input#bankIdTmp').val();
        $('input#bankId').val(bankId);
        if(bankId != '0'){
            $('#bankInfo').html($('#listBank li[data-id="' + bankId + '"]').html());
            $('#selectBank').hide();
        }
        else $('#bankInfo').html('');
        $('#modalChooseBank').modal('hide');
    });
};

app.cancelTransaction = function(transactionId){
    $('.btnCancel').click(function(){
        $('.btnCancel').prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#cancelTransactionUrl').val(),
            data: {
                TransactionId: transactionId,
                TransactionTypeId: $('input#transactionTypeId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) redirect(true, '');
                else $('.btnCancel').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('.btnCancel').prop('disabled', false);
            }
        });
    });
};

$(document).ready(function () {
    var transactionId = parseInt($('input#transactionId').val());
    var canEdit = parseInt($('input#canEdit').val());
    app.init(transactionId, canEdit);

    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '50px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('.ntags').click(function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
        return false;
    });
    if(transactionId > 0) {
        $('input.tagName').each(function () {
            inputTag.addTag($(this).val());
        });
    }
    $('#transactionForm').submit(function(e) {
        e.preventDefault();
    });

    if(canEdit){
        $('.submit').click(function() {
            var paidCost = replaceCost($('input#paidCost').val().trim(), true);
            if(paidCost <= 0){
                showNotification('Số tiền phải lớn hơn 0', 0);
                $('input#paidCost').focus();
                return false;
            }
            var transactionTypeId = parseInt($('input#transactionTypeId').val());
            var moneySourceId = parseInt($('select#moneySourceId').val());
            var moneyPhoneId = parseInt($('select#moneyPhoneId').val());
            var storeId = $('select#storeId').val();
            var fundId = $('select#fundId').val();
            var bankId = $('input#bankId').val();
            var customerId = $('input#customerId').val();
            if(transactionTypeId == 1){
                if(customerId == '0'){
                    showNotification('Vui lòng chọn khách hàng', 0);
                    return false;
                }
                if(moneySourceId == 1){
                    if(storeId == '0'){
                        showNotification('Vui lòng chọn cở sở', 0);
                        return false;
                    }
                    if(fundId == '0'){
                        showNotification('Vui lòng chọn quỹ tiền mặt', 0);
                        return false;
                    }
                }
                else if(moneySourceId == 2){
                    if(bankId == '0'){
                        showNotification('Vui lòng chọn số tài khoản', 0);
                        return false;
                    }
                }
                else if(moneySourceId == 3){
                    if(moneyPhoneId == '0'){
                        showNotification('Vui lòng chọn thẻ điện thoại', 0);
                        return false;
                    }
                }
                else if(moneySourceId == 4){
                    if(storeId == '0'){
                        showNotification('Vui lòng chọn cở sở', 0);
                        return false;
                    }
                }
            }
            $('.submit').prop('disabled', true);
            var transactionStatusId = 1;
            var verifyLevelId = 1;
            if($(this).attr('data-id') == '1'){ //xac thuc va hoan thanh
                if($('input#canEditLevel').val() == '2'){ //quan ly
                    verifyLevelId = 3;
                    transactionStatusId = 2;
                }
                else{ //nhan vien
                    verifyLevelId = 2;
                    transactionStatusId = 2;
                }
            }
            $.ajax({
                type: 'POST',
                url: $('#transactionForm').attr('action'),
                data : {
                    TransactionId: transactionId,
                    TransactionTypeId: transactionTypeId,
                    TransactionStatusId: transactionStatusId,
                    MoneySourceId: moneySourceId,
                    MoneyPhoneId: moneyPhoneId,
                    VerifyLevelId: verifyLevelId,
                    StoreId: storeId,
                    TransactionReasonId: $('select#transactionReasonId').val(),
                    CustomerId: customerId,
                    BankId: bankId,
                    PaidCost: paidCost,
                    OrderId: $('input#orderId').val(),
                    Comment: $('#comment').val().trim(),
                    TreasurerId: $('select#treasurerId').val(),
                    PrintStatusId: $('input#cbPrintStatus').parent('div').hasClass('checked') ? 2 : 1,
                    HasDebt: $('input#cbHasDebt').parent('div').hasClass('checked') ? 2 : 1,
                    DebtComment: $('input#debtComment').val().trim(),
                    TransportId: 0,
                    FundId: fundId,
                    TagNames: JSON.stringify(tags)
                },
                success: function(response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        if(transactionId == 0) redirect(false, $('#aTransactionList').attr('href'));
                        else redirect(true, '');
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function(er) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
            return false;
        });
    }


});

function showInfoCustomer(customerId){
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
                $('input#customerId').val(customerId);
                var data = json.data;
                if(data.PhoneNumber != '' && data.PhoneNumber2 != '') data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                var address = '';
                if(data.ZipCode == null || data.ZipCode == 0){
                    address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                    address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam> ' + data.WardName + '</span>';
                    if (data.DistrictName != '') address += '<span class="br-line i-district">' + data.DistrictName + '</span>';
                    if (data.ProvinceName != '') address += '<span class="br-line i-province">' + data.ProvinceName + '</span>';
                }
                else{
                    address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                    address += '<span class="i-province">ZipCode: '+data.ZipCode+'</span>';
                }
                $('.i-name').text(data.FullName);
                $('.i-phone').text(data.PhoneNumber);
                if(data.CustomerGroupName != '') $('.i-cusType').html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType').hide();
                $('.i-email').text(data.Email);
                $('.i-comment').text(data.Comment);
                $('.i-country').text(data.CountryName);
                $('.i-address').html(address);
                var editCustomerUrl = $('input#editCustomerUrl').val() + '/' + customerId + '/';
                $('h4.i-name').html('<a href="' + editCustomerUrl + '3" target="_blank">' + data.FullName + '</a>');
                $('.i-total-orders').html('<a href="' + editCustomerUrl + '1" target="_blank">' + data.TotalOrders + '</a>');
                $('span#customerBalance').html('<a href="' + editCustomerUrl + '2" target="_blank">' + formatDecimal(data.Balance) + '</a>');
                if(replaceCost($('span#totalCost').text(), true) > 0 && $('input[name="DeliveryTypeId"]:checked').val() == '2')  $('.btnTransport').removeClass('actived');
                $('#boxChooseCustomer').hide();
                $('#divCustomer').show();
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}