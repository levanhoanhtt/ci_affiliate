$(document).ready(function(){
    $("#tbodyTransactionKind").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transactionKindId').val(id);
        $('input#transactionKindName').val($('td#transactionKindName_' + id).text());
        $('select#transactionTypeId').val($('input#transactionTypeId_' + id).val());
        scrollTo('input#transactionKindName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteTransactionKindUrl').val(),
                data: {
                    TransactionKindId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#transactionKind_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transactionKindForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#transactionKindForm')) {
            var form = $('#transactionKindForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="transactionKind_' + data.TransactionKindId + '">';
                            html += '<td id="transactionKindName_' + data.TransactionKindId + '">' + data.TransactionKindName + '</td>';
                            html += '<td id="transactionType_' + data.TransactionKindId + '">' + data.TransportTypeName +'</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransactionKindId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransactionKindId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="transactionTypeId_' + data.TransactionKindId + '" value="' + data.TransactionTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyTransactionKind').prepend(html);
                        }
                        else{
                            $('td#transactionKindName_' + data.TransactionKindId).text(data.TransactionKindName);
                            $('td#transactionType_' + data.TransactionKindId).html(data.TransportTypeName);
                            $('input#transactionTypeId_' + data.TransactionKindId).val(data.TransactionTypeId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});