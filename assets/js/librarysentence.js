$(document).ready(function(){
    $("#tbodyLibrarySentence").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#librarySentenceId').val(id);
        $('input#libraryName').val($('td#libraryName_' + id).text());
        scrollTo('input#libraryName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteLibrarySentenceUrl').val(),
                data: {
                    LibrarySentenceId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#librarySentence_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    }).on('click', 'a.link_import', function(){
    	$('input#fileExcelUrl').val('');
    	$("#btnImportExcel").attr('data-id', $(this).attr('data-id'));
    	$('#modalImportExcel').modal('show');
    	return false;
    });

    $('#btnUploadExcel').click(function(){
        chooseFile('Files', function(fileUrl){
            $('input#fileExcelUrl').val(fileUrl);
        });
    });

    $('#btnImportExcel').click(function(){
        var fileExcelUrl = $('input#fileExcelUrl').val();
        if(fileExcelUrl != ''){
            var btn = $(this);
            btn.prop('disabled', true);
            $('.imgLoading').show();
            $.ajax({
                type: "POST",
                url: $('input#importExcelUrl').val(),
                data: {
                    FileUrl: fileExcelUrl,
                    LibrarySentenceId: btn.attr('data-id')
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    $('.imgLoading').hide();
                    $('#modalImportExcel').modal('hide');
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.imgLoading').hide();
                    btn.prop('disabled', false);
                }
            });
            
        }
        else showNotification('Bạn chưa chọn file Excel', 0);
    });

    $('a#link_cancel').click(function(){
        $('#librarySentenceForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#librarySentenceForm')) {
            var form = $('#librarySentenceForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="librarySentence_' + data.LibrarySentenceId + '">';
                            html += '<td id="libraryName_' + data.LibrarySentenceId + '">' + data.LibraryName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.LibrarySentenceId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.LibrarySentenceId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<a href="javascript:void(0)" class="link_import" data-id="' + data.LibrarySentenceId + '" title="Import"><i class="fa fa-upload"></i></a>' +
                                '<a href="' + $('input#editSentenceUrl').val() + data.LibrarySentenceId + '" target="_bank" title="Mẫu câu"><i class="fa fa-plus-square"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyLibrarySentence').prepend(html);
                        }
                        else $('td#libraryName_' + data.LibrarySentenceId).text(data.LibraryName);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});