$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Vận chuyển LCK',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderStorecirculationtransports(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEdit = $('#urlEditStoreCirculationTransport').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].StoreCirculationTransportId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].StoreCirculationTransportId + '"></td>';
            html += '<td><a href="' + urlEdit + data[item].StoreCirculationTransportId + '">' + data[item].TransportCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td>' + (data[item].TransporterName != null ? data[item].TransporterName : '') + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
            html += '<td>' + (data[item].StoreSource != null ? data[item].StoreSource : '') + '</td>';
            html += '<td>' + (data[item].StoreDestination != null ? data[item].StoreDestination : '') + '</td>';
            html += '</tr>';
        }
        $('#tbodyStoreCirculationTransport').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}