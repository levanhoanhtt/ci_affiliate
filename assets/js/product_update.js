var app = app || {};

app.init = function (productId) {
    app.initLibrary(productId);
    app.configPrice(productId);
};

app.initLibrary = function(productId){
    CKEDITOR.replace('ProductDesc', {
        language: 'vi',
        height: 350
    });
    CKEDITOR.replace('ProductShortDesc', {
        language: 'vi',
        toolbar: 'ShortToolbar',
        height: 150
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.iCheckRadio').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.timepicker').timepicker({
        showInputs: false
    });
    $('input#cbWebsite1').on('ifToggled', function(e){
        if(e.currentTarget.checked){
            $('#divWebsite2').hide();
            $('input#cbWebsite2').iCheck('uncheck');
            $('input#productDisplayTypeId').val('1');
        }
        else{
            $('#divWebsite2').show();
            $('input#productDisplayTypeId').val('3');
        }
    });
    $('input#cbWebsite2').on('ifToggled', function(e){
        if(e.currentTarget.checked) $('input#productDisplayTypeId').val('2');
        else $('input#productDisplayTypeId').val('3');
    });
    var productDisplayTypeId = parseInt($('input#productDisplayTypeId').val());
    if(productDisplayTypeId == 2){
        $('input#cbWebsite1').iCheck('uncheck');
        $('input#cbWebsite2').iCheck('check');
        $('#divWebsite2').show();
    }
    else if(productDisplayTypeId == 3){
        $('input#cbWebsite1').iCheck('uncheck');
        $('input#cbWebsite2').iCheck('uncheck');
    }
    $('body').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.cost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('keyup', 'input#titleSEO', function(){
        $('span#count1').text($(this).val().trim().length);
    }).on('keyup', '#metaDesc', function(){
        $('span#count2').text($(this).val().trim().length);
    });
    $('#btnSEO').click(function(){
        $('#divSEO').toggle();
    });
    $('#btnDate').click(function(){
        $('#divDateTime').toggle();
    });
    Sortable.create(ulVideos, {});
    Sortable.create(ulImages, {});
    $('#btnUpImage1').click(function(){
        chooseFile('Products', function(fileUrl) {
            $('#ulImages').append('<li><a href="' + fileUrl + '" target="_blank"><img src="' + fileUrl + '"></a><i class="fa fa-times"></i></li>');
        });
    });
    $('#ulImages').on('click', 'i', function(){
        $(this).parent().remove();
    });
    $('#btnUpImage2').click(function(){
        chooseFile('Products', function(fileUrl) {
            $('img#productImage').attr('src', fileUrl).show();
        });
    });
    $('#btnAddVideo').click(function(){
        var videoUrl = $('input#videoUrl').val().trim();
        if(videoUrl != ''){
            var flag = false;
            $('#ulVideos li a').each(function(){
                if($(this).attr('href') == videoUrl){
                    flag = true;
                    return false;
                }
            });
            if(flag) showNotification('Video đã tồn tại', 0);
            else {
                $('#ulVideos').append('<li><a href="' + videoUrl + '" target="_blank">' + videoUrl + '</a><i class="fa fa-times"></i></li>');
                $('input#videoUrl').val('');
            }
        }
        else showNotification('Vui lòng nhập link video', 0);
    });

    $('#ulVideos').on('click', 'i', function(){
        $(this).parent().remove();
    });
    if(productId == 0){
        $('#productForm').on('focusout', 'input#productName', function(){
            $('input#titleSEO').val($(this).val());
            $('input#productSlug').val(makeSlug($(this).val()));
        });
    }
};

app.configPrice = function(productId){
    $('#aConfigPrice').click(function(){
        $('#divPrice').show();
        return false;
    });
    $('#tbodyPrice').on('click', 'a#link_update', function(){
        var quantity = replaceCost($('input#quantity1').val(), true);
        var price = replaceCost($('input#price1').val(), true);
        if(quantity > 0 && price > 0){
            var quantities = [{
                Quantity: quantity,
                Price: price
            }];
            var quantityTmp = 0;
            var priceTmp = 0;
            var flag = true;
            $('#tbodyPrice .trPrice').each(function(){
                quantityTmp = replaceCost($(this).find('.tdQuantity1').text());
                if(quantityTmp == quantity){
                    showNotification('Số lượng đã có, vui lòng sửa', 0);
                    flag = false;
                    return false;
                }
                priceTmp = replaceCost($(this).find('.tdPrice1').text());
                quantities.push({
                    Quantity: quantityTmp,
                    Price: priceTmp
                });
            });
            if(flag){
                if(productId > 0){
                    var id = parseInt($(this).attr('data-id'));
                    $.ajax({
                        type: "POST",
                        url: $('input#updateProductPriceUrl').val(),
                        data: {
                            ProductPriceId: id,
                            ProductId: productId,
                            ProductChildId: 0,
                            Quantity: quantity,
                            Price: price
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if(json.code == 1){
                                $('#tbodyPrice .trPrice').remove();
                                quantities.sort(function(a, b){
                                    return ((a.Quantity < b.Quantity) ? -1 : ((a.Quantity > b.Quantity) ? 1 : 0));
                                });
                                var html = '';
                                for(var  i = 0; i < quantities.length; i++){
                                    html += '<tr class="trPrice"><td class="tdQuantity1">' + formatDecimal(quantities[i].Quantity.toString()) + '</td><td class="tdPrice1">' + formatDecimal(quantities[i].Price.toString()) + '</td>';
                                    html += '<td><a href="javascript:void(0)" class="link_edit" data-id="' + json.data + '"><i class="fa fa-pencil" title="Sửa"></i></a><a href="javascript:void(0)" class="link_delete" data-id="' + json.data + '"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                                }
                                $('#tbodyPrice').prepend(html);
                                $('input#quantity1').val('0');
                                $('input#price1').val('0');
                            }
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        }
                    });
                }
                else{
                    $('#tbodyPrice .trPrice').remove();
                    quantities.sort(function(a, b){
                        return ((a.Quantity < b.Quantity) ? -1 : ((a.Quantity > b.Quantity) ? 1 : 0));
                    });
                    var html = '';
                    for(var  i = 0; i < quantities.length; i++){
                        html += '<tr class="trPrice"><td class="tdQuantity1">' + formatDecimal(quantities[i].Quantity.toString()) + '</td><td class="tdPrice1">' + formatDecimal(quantities[i].Price.toString()) + '</td>';
                        html += '<td><a href="javascript:void(0)" class="link_edit" data-id="0"><i class="fa fa-pencil" title="Sửa"></i></a><a href="javascript:void(0)" class="link_delete" data-id="0"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                    }
                    $('#tbodyPrice').prepend(html);
                    $('input#quantity1').val('0');
                    $('input#price1').val('0');
                }
            }
        }
        else showNotification('Số lượng và giá phải lơn hơn 0', 0);
        return false;
    }).on('click', 'a.link_delete', function(){
        var id = parseInt($(this).attr('data-id'));
        if(id > 0){
            $.ajax({
                type: "POST",
                url: $('input#deleteProductPriceUrl').val(),
                data: {
                    ProductPriceId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $(this).parent().parent().remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else $(this).parent().parent().remove();
        return false;
    }).on('click', 'a.link_edit', function(){
        $('a#link_update').attr('data-id', $(this).attr('data-id'));
        var tr = $(this).parent().parent();
        $('input#quantity1').val(tr.find('.tdQuantity1').text());
        $('input#price1').val(tr.find('.tdPrice1').text());
        tr.remove();
        return false;
    });
};

$(document).ready(function () {
    var productId = parseInt($('input#productId').val());
    app.init(productId);
    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        /*'onChange': function(tag){
            console.log('change ' + JSON.stringify(tag));
        },*/
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('input.tagName').each(function(){
        inputTag.addTag($(this).val());
    });
    $('#ulTagExist').on('click', 'a', function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
    });
    //product Combo=================================================
    if($('input#productKindId').val() == '3'){
        chooseProduct(function(tr){
            var id = tr.attr('data-id');
            var childId = tr.attr('data-child');
            var flag = false;
            $('#tbodyProduct tr').each(function(){
                if($(this).attr('data-id') == id && $(this).attr('data-child') == childId){
                    flag = true;
                    var quantity = replaceCost($(this).find('input.quantity').val(), true) + 1;
                    $(this).find('input.quantity').val(formatDecimal(quantity.toString()));
                    return false;
                }
            });
            if(!flag) {
                $.ajax({
                    type: "POST",
                    url: $('input#getProductDetailUrl').val(),
                    data: {
                        ProductId: id,
                        ProductChildId: childId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var products = json.data.Products;
                            var productPath = $('input#productPath').val();
                            var html = '';
                            var totalWeight = 0;
                            for(var i = 0; i < products.length; i++){
                                html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '" data-weight="' + products[i].Weight + '">';
                                html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + products[i].ProductName + '</a></td>';
                                html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                                html += '<td><input class="form-control quantity" value="1"></td>';
                                html += '<td class="tdPrice text-right"><span class="spanPrice">' + formatDecimal(products[i].Price.toString()) + '</span></td>';
                                html += '<td><input type="checkbox" class="iCheck iCheckVAT" disabled' + (products[i].VATStatusId == 2 ? ' checked' : '') + '></td>';
                                html += '<td><input class="form-control guaranteeMonth" value="' + products[i].GuaranteeMonth + '"></td>';
                                html += '<td><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                                totalWeight += parseInt(products[i].Weight);
                            }
                            $('#tbodyProduct').append(html);
                            var inputWeight = $('input#weight');
                            totalWeight += replaceCost(inputWeight.val(), true);
                            inputWeight.val(formatDecimal(totalWeight.toString()));
                            $('input.iCheckVAT').iCheck({
                                checkboxClass: 'icheckbox_square-blue',
                                radioClass: 'iradio_square-blue',
                                increaseArea: '20%'
                            });
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        });
        $('#tbodyProduct').on('keydown', 'input.cost', function (e) {
            if(checkKeyCodeNumber(e)) e.preventDefault();
        }).on('keyup', 'input.quantity', function() {
            var value = $(this).val();
            $(this).val(formatDecimal(value));
            var totalWeight = 0;
            $('#tbodyProduct tr').each(function(){
                totalWeight += parseInt($(this).attr('data-weight')) * replaceCost($(this).find('input.quantity').val(), true);
            });
            $('input#weight').val(formatDecimal(totalWeight.toString()));
        }).on('click', '.link_delete', function () {
            $(this).parent().parent().remove();
            var totalWeight = 0;
            $('#tbodyProduct tr').each(function(){
                totalWeight += parseInt($(this).attr('data-weight')) * replaceCost($(this).find('input.quantity').val(), true);
            });
            $('input#weight').val(formatDecimal(totalWeight.toString()));
            return false;
        });
    }
    else {
        //product Config=================================================
        $('#btnConfig').click(function () {
            if ($('input#productKindId').val() == '1') {
                $(this).html('<i class="fa fa-minus"></i> Hủy');
                $('#divProductConfig').show();
                $('input#productKindId').val('2');
            }
            else {
                $(this).html('<i class="fa fa-plus"></i> Thêm phiên bản');
                $('#divProductConfig').hide();
                $('input#productKindId').val('1');

            }
        });
        var variantValues = [];
        variantValues[1] = [];
        $('input#variantValue_1').tagsInput({
            'width': '100%',
            'height': '50px',
            'interactive': true,
            'defaultText': '',
            'onAddTag': function (tag) {
                variantValues[1].push(tag);
            },
            'onRemoveTag': function (tag) {
                var index = variantValues[1].indexOf(tag);
                if (index >= 0) variantValues[1].splice(index, 1);
            },
            'delimiter': [',', ';'],
            'removeWithBackspace': true,
            'minChars': 0,
            'maxChars': 0
        });
        if ($('input#variantValues_1').length > 0) {
            var variantValueStrs = $('input#variantValues_1').val().split(',');
            for (var i = 0; i < variantValueStrs.length; i++) $('input#variantValue_1').addTag(variantValueStrs[i]);
        }
        if ($('input#variantValue_2').length > 0) {
            variantValues[2] = [];
            $('input#variantValue_2').tagsInput({
                'width': '100%',
                'height': '50px',
                'interactive': true,
                'defaultText': '',
                'onAddTag': function (tag) {
                    variantValues[2].push(tag);
                },
                'onRemoveTag': function (tag) {
                    var index = variantValues[2].indexOf(tag);
                    if (index >= 0) variantValues[2].splice(index, 1);
                },
                'delimiter': [',', ';'],
                'removeWithBackspace': true,
                'minChars': 0,
                'maxChars': 0
            });
            if ($('input#variantValues_2').length > 0) {
                var variantValueStrs = $('input#variantValues_2').val().split(',');
                for (var i = 0; i < variantValueStrs.length; i++) $('input#variantValue_2').addTag(variantValueStrs[i]);
            }
        }
        if ($('input#variantValue_3').length > 0) {
            variantValues[3] = [];
            $('input#variantValue_3').tagsInput({
                'width': '100%',
                'height': '50px',
                'interactive': true,
                'defaultText': '',
                'onAddTag': function (tag) {
                    variantValues[3].push(tag);
                },
                'onRemoveTag': function (tag) {
                    var index = variantValues[3].indexOf(tag);
                    if (index >= 0) variantValues[3].splice(index, 1);
                },
                'delimiter': [',', ';'],
                'removeWithBackspace': true,
                'minChars': 0,
                'maxChars': 0
            });
            if ($('input#variantValues_3').length > 0) {
                var variantValueStrs = $('input#variantValues_3').val().split(',');
                for (var i = 0; i < variantValueStrs.length; i++) $('input#variantValue_3').addTag(variantValueStrs[i]);
            }
        }

        var variants = [];
        $('input.variant').each(function () {
            variants.push({
                VariantId: parseInt($(this).attr('data-id')),
                VariantName: $(this).val()
            });
        });

        $('#btnAddVariant').click(function () {
            var variantIds = [];
            $('#tbodyVariant select.variantId').each(function () {
                variantIds.push(parseInt($(this).val()));
            });
            if (variantIds.length < variants.length) {
                var variantLefts = [];
                var flag = false;
                var i = 0;
                for (i = 0; i < variants.length; i++) {
                    flag = false;
                    for (var j = 0; j < variantIds.length; j++) {
                        if (variants[i].VariantId == variantIds[j]) {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) variantLefts.push({
                        VariantId: variants[i].VariantId,
                        VariantName: variants[i].VariantName
                    });
                }
                if (variantLefts.length > 0) {
                    var variantNo = parseInt($('input#variantNo').val()) + 1;
                    var html = '<tr><td><select class="form-control variantId" id="variantId_' + variantNo + '">';
                    for (i = 0; i < variantLefts.length; i++) html += '<option value="' + variantLefts[i].VariantId + '">' + variantLefts[i].VariantName + '</option>';
                    html += '</select></td><td><input type="text" class="form-control variantValue" id="variantValue_' + variantNo + '"></td><td><a href="javascript:void(0)" class="link_delete" data-id="' + variantNo + '"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                    $('#tbodyVariant').append(html);
                    variantValues[variantNo] = [];
                    $('input#variantValue_' + variantNo).tagsInput({
                        'width': '100%',
                        'height': '50px',
                        'interactive': true,
                        'defaultText': '',
                        'onAddTag': function (tag) {
                            variantValues[variantNo].push(tag);
                        },
                        'onRemoveTag': function (tag) {
                            var index = variantValues[variantNo].indexOf(tag);
                            if (index >= 0) variantValues[variantNo].splice(index, 1);
                        },
                        'delimiter': [',', ';'],
                        'removeWithBackspace': true,
                        'minChars': 0,
                        'maxChars': 0
                    });
                    $('input#variantNo').val(variantNo);
                }
            }
        });
        $('#tbodyVariant').on('click', '.link_delete', function () {
            var variantNo = parseInt($(this).attr('data-id'));
            variantValues.splice(variantNo, 1);
            $(this).parent().parent().remove();
            return false;
        });
        $('#btnInitVariant').click(function () {
            if (variantValues.length > 0) {
                var variantValues1 = [], variantValues2 = [], variantValues3 = [];
                var variantId = 0;
                var i = 0;
                var flag = true;
                for (var variantNo in variantValues) {
                    variantId = $('select#variantId_' + variantNo).val();
                    if (variantValues[variantNo].length == 0) {
                        flag = false;
                        break;
                    }
                    else {
                        i++;
                        if (i == 1) variantValues1[variantId] = variantValues[variantNo];
                        else if (i == 2) variantValues2[variantId] = variantValues[variantNo];
                        else if (i == 3) variantValues3[variantId] = variantValues[variantNo];
                    }
                }
                if (flag) {
                    var variantId1, variantId2, variantId3, j, k;
                    var variantOptions = [];
                    for (variantId1 in variantValues1) {
                        for (i = 0; i < variantValues1[variantId1].length; i++) {
                            if (variantValues2.length > 0) {
                                for (variantId2 in variantValues2) {
                                    for (j = 0; j < variantValues2[variantId2].length; j++) {
                                        if (variantValues3.length > 0) {
                                            for (variantId3 in variantValues3) {
                                                for (k = 0; k < variantValues3[variantId3].length; k++) variantOptions.push(variantValues1[variantId1][i] + ' - ' + variantValues2[variantId2][j] + ' - ' + variantValues3[variantId3][k]);
                                            }
                                        }
                                        else variantOptions.push(variantValues1[variantId1][i] + ' - ' + variantValues2[variantId2][j]);
                                    }
                                }
                            }
                            else variantOptions.push(variantValues1[variantId1][i]);
                        }
                    }
                    var html = '';
                    var price = $('input#price').val().trim();
                    var oldPrice = $('input#oldPrice').val().trim();
                    var weight = $('input#weight').val().trim();
                    var guaranteeMonth = $('input#guaranteeMonth').val().trim();
                    var VATStatusId = $('input[name="VATStatusId"]:checked').val();
                    var variantImg = $('img#productImage').attr('src');
                    if (variantImg == '') variantImg = $('input#noImageProduct').val();
                    for (i = 0; i < variantOptions.length; i++) {
                        html += '<tr data-id="0" data-index="' + i + '" id="trPrice_0_' + i + '"><td><img class="variantImg" src="' + variantImg + '"/></td><td class="variantValue">' + variantOptions[i] + '</td>';
                        html += '<td><input type="text" class="form-control hmdrequired sku" value="" data-field="Sku"></td>';
                        html += '<td><input type="text" class="form-control hmdrequired barCode" value="" data-field="BarCode"></td>';
                        html += '<td><input type="text" class="form-control cost hmdrequiredNumber price" value="' + price + '" data-field="Giá bán lẻ"></td>';
                        html += '<td><input type="text" class="form-control cost hmdrequiredNumber oldPrice" value="' + oldPrice + '" data-field="Giá so sánh"></td>';
                        html += '<td><a href="javascript:void(0)" class="aPrice">Cấu hình</a></td>';
                        html += '<td><input type="text" class="form-control cost hmdrequiredNumber weight" value="' + weight + '" data-field="Khối lượng"></td>';
                        html += '<td><input type="text" class="form-control cost hmdrequiredNumber guaranteeMonth" value="' + guaranteeMonth + '" data-field="Bảo hành"></td>';
                        html += '<td><input type="checkbox" class="iCheck iCheckVAT"' + (VATStatusId == 2 ? ' checked' : '') + '></td>';
                        html += '<td><input type="text" class="form-control hmdrequiredNumber discountPercent" value="" data-field="DiscountPercent"></td>';
                         html += '<td><input type="text" class="form-control hmdrequiredNumber cookieTimeout" value="" data-field="CookieTimeout"></td>';
                        html += '<td><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a>';
                        html += '<span class="json" style="display: none"></span></td>';
                        html += '</tr>';
                    }
                    $('#tbodyVariantValue').html(html);
                    $('input.iCheckVAT').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%'
                    });
                }
                else showNotification('Chưa điền giá trị thuộc tính', 0);
            }
        });
        $('#tbodyVariantValue').on('click', '.link_delete', function () {
            $(this).parent().parent().remove();
            return false;
        })/*.on('keyup', 'input.cost', function () {
            var value = $(this).val();
            $(this).val(formatDecimal(value));
        })*/.on('click', 'img.variantImg', function () {
            var src = $(this).attr('src');
            $('input#productImageUrl').val(src);
            $('#imgProduct').attr('src', src);
            var imageIndex = $(this).index('img.variantImg');
            $('input#imageIndex').val($(this).index('img.variantImg'));
            $('#modalProductImage').modal('show');
        }).on('click', '.aPrice', function(){
            $('#tbodyMultiPrice .trPrice').remove();
            var tr = $(this).parent().parent();
            $('input#productChildId').val(tr.attr('data-id'));
            $('input#productChildIndex').val(tr.attr('data-index'));
            var json = tr.find('span.json').text();
            var html = '';
            if(json != ''){
                json = $.parseJSON(json);
                for(i = 0; i < json.length; i++){
                    html += '<tr class="trPrice" data-id="' + json[i].ProductPriceId + '"><td class="tdQuantity1">' + formatDecimal(json[i].Quantity.toString()) + '</td><td class="tdPrice1">' + formatDecimal(json[i].Price.toString()) + '</td>';
                    html += '<td><a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a>';
                    html += '<a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                }
            }
            $('#tbodyMultiPrice').prepend(html);
            $('#modalProductPrice').modal('show');
            return false;
        });
        $('#tbodyMultiPrice').on('click', 'a#link_update2', function(){
            var quantity = replaceCost($('input#quantity2').val(), true);
            var price = replaceCost($('input#price2').val(), true);
            if(quantity > 0 && price > 0){
                var productPriceId = parseInt($(this).attr('data-id'));
                var productChildId = parseInt($('input#productChildId').val());
                var quantities = [];
                /*var quantities = [{
                    ProductPriceId: productPriceId,
                    ProductId: productId,
                    ProductChildId: productChildId,
                    Quantity: quantity,
                    Price: price
                }];*/
                var quantityTmp = 0;
                var priceTmp = 0;
                var flag = true;
                $('#tbodyMultiPrice .trPrice').each(function(){
                    quantityTmp = replaceCost($(this).find('.tdQuantity1').text());
                    if(quantityTmp == quantity){
                        showNotification('Số lượng đã có, vui lòng sửa', 0);
                        flag = false;
                        return false;
                    }
                    priceTmp = replaceCost($(this).find('.tdPrice1').text());
                    quantities.push({
                        ProductPriceId: $(this).attr('data-id'),
                        ProductId: productId,
                        ProductChildId: productChildId,
                        Quantity: quantityTmp,
                        Price: priceTmp
                    });
                });
                if(flag){
                    var data = {
                        ProductPriceId: productPriceId,
                        ProductId: productId,
                        ProductChildId: productChildId,
                        Quantity: quantity,
                        Price: price
                    };
                    if(productId > 0 && productChildId > 0){
                        $.ajax({
                            type: "POST",
                            url: $('input#updateProductPriceUrl').val(),
                            data: data,
                            success: function (response) {
                                var json = $.parseJSON(response);
                                showNotification(json.message, json.code);
                                if(json.code == 1){
                                    $('#tbodyMultiPrice .trPrice').remove();
                                    data.ProductPriceId = json.data;
                                    quantities.push(data);
                                    quantities.sort(function(a, b){
                                        return ((a.Quantity < b.Quantity) ? -1 : ((a.Quantity > b.Quantity) ? 1 : 0));
                                    });
                                    var html = '';
                                    for(var  i = 0; i < quantities.length; i++){
                                        html += '<tr class="trPrice" data-id="' + json.data + '"><td class="tdQuantity1">' + formatDecimal(quantities[i].Quantity.toString()) + '</td><td class="tdPrice1">' + formatDecimal(quantities[i].Price.toString()) + '</td>';
                                        html += '<td><a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                                    }
                                    $('#tbodyMultiPrice').prepend(html);
                                    $('input#quantity2').val('0');
                                    $('input#price2').val('0');
                                    $('#trPrice_' + productChildId + '_' + $('input#productChildIndex').val()).find('span.json').text(JSON.stringify(quantities));
                                }
                            },
                            error: function (response) {
                                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                            }
                        });
                    }
                    else{
                        $('#tbodyMultiPrice .trPrice').remove();
                        quantities.push(data);
                        quantities.sort(function(a, b){
                            return ((a.Quantity < b.Quantity) ? -1 : ((a.Quantity > b.Quantity) ? 1 : 0));
                        });
                        var html = '';
                        for(var  i = 0; i < quantities.length; i++){
                            html += '<tr class="trPrice" data-id="0"><td class="tdQuantity1">' + formatDecimal(quantities[i].Quantity.toString()) + '</td><td class="tdPrice1">' + formatDecimal(quantities[i].Price.toString()) + '</td>';
                            html += '<td><a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td></tr>';
                        }
                        $('#tbodyMultiPrice').prepend(html);
                        $('input#quantity2').val('0');
                        $('input#price2').val('0');
                        $('#trPrice_' + productChildId + '_' + $('input#productChildIndex').val()).find('span.json').text(JSON.stringify(quantities));
                    }
                }
            }
            else showNotification('Số lượng và giá phải lơn hơn 0', 0);
            return false;
        }).on('click', 'a.link_edit', function(){
            var tr = $(this).parent().parent();
            $('a#link_update2').attr('data-id', tr.attr('data-id'));
            $('input#quantity2').val(tr.find('.tdQuantity1').text());
            $('input#price2').val(tr.find('.tdPrice1').text());
            tr.remove();
        }).on('click', 'a.link_delete', function(){
            var tr = $(this).parent().parent();
            var id = parseInt(tr.attr('data-id'));
            if(id > 0){
                $.ajax({
                    type: "POST",
                    url: $('input#deleteProductPriceUrl').val(),
                    data: {
                        ProductPriceId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) tr.remove();
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            else tr.remove();
        });
        $('#btnUpdatePrice').click(function(){
            var quantities = [];
            var min = 0, max = 0, price = 0;
            $('#tbodyMultiPrice .trPrice').each(function(){
                price = replaceCost($(this).find('.tdPrice1').text());
                if(price < min || min == 0) min = price;
                if(price > max) max = price;
                quantities.push({
                    ProductPriceId: $(this).attr('data-id'),
                    ProductId: productId,
                    ProductChildId: $('input#productChildId').val(),
                    Quantity: replaceCost($(this).find('.tdQuantity1').text()),
                    Price: price
                });
            });
            var priceText = '';
            if(min == 0 && max == 0) priceText = 'Cấu hình';
            else if(min == max) priceText = formatDecimal(min.toString()) + ' VNĐ';
            else priceText = formatDecimal(min.toString()) + ' - ' + formatDecimal(max.toString()) +  ' VNĐ';
            var tr = $('#trPrice_' + $('input#productChildId').val() + '_' + $('input#productChildIndex').val());
            tr.find('.aPrice').text(priceText);
            tr.find('span.json').text(JSON.stringify(quantities));
            $('#modalProductPrice').modal('hide');
        });

        $('#btnUploadImage').click(function(){
            chooseFile('Products', function(fileUrl) {
                $('input#productImageUrl').val(fileUrl);
                $('#imgProduct').attr('src', fileUrl);
            });
        });
        $('#modalProductImage').on('blur', 'input#productImageUrl', function(){
            $('#imgProduct').attr('src', $(this).val());
        });
        $('#btnUpdateImage').click(function(){
            var img = $('img.variantImg').get($('input#imageIndex').val());
            $(img).attr('src', $('input#productImageUrl').val());
            $('#modalProductImage').modal('hide');
        });
    }
    //=================================================
    $('.submit').click(function(){
        if(validateEmpty('#productForm') && validateNumber('#divProductAffiliate', true, ' không được bỏ trống')){
            var btn = $(this);
            var cateIds1 = JSON.stringify($('select#cate1').val());
            if(cateIds1 == null || cateIds1 == 'null'){
                cateIds1 = '[]';
                /*showNotification('Chưa chọn Nhóm sản phẩm', 0);
                $('select#cate1').focus();
                return false;*/
            }
            var cateIds2 = JSON.stringify($('select#cate2').val());
            if(cateIds2 == null || cateIds2 == 'null'){
                cateIds2 = '[]';
                /*showNotification('Chưa chọn Loại sản phẩm', 0);
                $('select#cate2').focus();
                return false;*/
            }
            var images = [];
            $('#ulImages li a').each(function(){
                images.push($(this).attr('href'));
            });
            var videoUrls = [];
            $('#ulVideos li a').each(function(){
                videoUrls.push($(this).attr('href'));
            });
            var variantNewValues = [];
            variantNewValues[0] = [];
            var variantOptions = [];
            var productKindId = parseInt($('input#productKindId').val());
            if(productKindId == 2) {
                var variantId = 0;
                for (var variantNo in variantValues) {
                    variantId = $('select#variantId_' + variantNo).val();
                    variantNewValues[variantId] = variantValues[variantNo];
                }
                variantOptions = getVariantValues();
                if(variantOptions.length == 0) return false;

            }
            else if(productKindId == 3){
                variantOptions = getProductChilds();
                if(variantOptions.length == 0) return false;
            }
            var isContactPrice = 1;
            if($('input#cbPrice').parent('div.icheckbox_square-blue').hasClass('checked')) isContactPrice = 2;
            var isManageExtraWarehouse = 1;
            //if($('input#cbIsManageExtraWarehouse').parent('div.icheckbox_square-blue').hasClass('checked')) isManageExtraWarehouse = 2;
            var productSlug = $('input#productSlug').val().trim();
            var prices = [];
            if(productId == 0) {
                $('#tbodyPrice .trPrice').each(function () {
                    prices.push({
                        Quantity: replaceCost($(this).find('.tdQuantity1').text()),
                        Price: replaceCost($(this).find('.tdPrice1').text())
                    });
                });
            }
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('#productForm').attr('action'),
                data: {
                    ProductId: productId,
                    ProductName: $('input#productName').val().trim(),
                    ProductSlug: productSlug,
                    ProductShortDesc: CKEDITOR.instances['ProductShortDesc'].getData().trim(),
                    ProductDesc: CKEDITOR.instances['ProductDesc'].getData().trim(),
                    ProductTypeId: $('select#productTypeId').val(),
                    ProductStatusId: $('input[name="ProductStatusId"]:checked').val(),
                    ProductKindId: productKindId,
                    VATStatusId: $('input[name="VATStatusId"]:checked').val(),
                    ProductDisplayTypeId: $('input#productDisplayTypeId').val(),
                    ProductLevelId: $('input[name="ProductLevelId"]:checked').val(),
                    ParentProductId: $('input#parentProductId').val(),
                    SupplierId: 0, //productKindId == 3 ? 0 : $('select#supplierId').val(),
                    ManufacturerId: productKindId == 3 ? 0 : $('select#manufacturerId').val(),
                    IsContactPrice: isContactPrice,
                    Price: replaceCost($('input#price').val().trim(), true),
                    OldPrice: replaceCost($('input#oldPrice').val().trim(), true),
                    ProductImage: $('img#productImage').attr('src'),
                    BarCode: $('input#barCode').val().trim(),
                    Sku: $('input#sku').val().trim(),
                    Weight: replaceCost($('input#weight').val().trim()),
                    PublishDateTime: getPublishDateTime(),
                    IsManageExtraWarehouse: isManageExtraWarehouse,
                    FormalStatus: $('input#formalStatus').val().trim(),
                    UsageStatus: $('input#usageStatus').val().trim(),
                    AccessoryStatus: $('input#accessoryStatus').val().trim(),
                    GuaranteeMonth: $('input#guaranteeMonth').val().trim(),
                    ProductUnitId: $('select#productUnitId').val(),
                    DiscountPercent: $('input#discountPercent').val(),
                    CookieTimeout: $('input#cookieTimeout').val(),
                    ProductPartnerId: $('input#productPartnerId').val(),
                    PartnerId: $('input#partnerId').val(),
                    VideoUrls: JSON.stringify(videoUrls),

                    TitleSEO: $('input#titleSEO').val().trim(),
                    MetaDesc: $('#metaDesc').val().trim(),
                    Canonical: productSlug,
                    IsRobotIndex: 1,
                    IsRobotFollow: 1,
                    IsOnSitemap: 1,

                    Images: JSON.stringify(images),
                    CateIds1: cateIds1,
                    CateIds2: cateIds2,
                    TagNames: JSON.stringify(tags),

                    Variants: JSON.stringify(variantNewValues),
                    VariantOptions: JSON.stringify(variantOptions),

                    Prices: JSON.stringify(prices)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(productId == 0) redirect(false, $('a#productListUrl').attr('href'));
                        else{
                            if(productSlug == '') productSlug = $('input#productName').val().trim();
                            productSlug = makeSlug(productSlug);
                            $('#aViewProduct').attr('href', $('#baseUrl').attr('href') + 'products/' + productSlug);
                            btn.prop('disabled', false);
                        }
                    }
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
});

function getPublishDateTime(){
    var retVal = '';
    var productDates = $('input#productDate').val().trim().split('/');
    if(productDates.length == 3){
        retVal = productDates[2] + '-' + productDates[1] + '-' + productDates[0];
        var productTimes = $('input#productTime').val().trim().split(' ');
        if(productTimes.length == 2){
            var hour = 0;
            var minute = '00';
            var times = productTimes[0].split(':');
            if(times.length == 2){
                hour = parseInt(times[0]);
                minute = times[1];
            }
            if(productTimes[1] == 'PM') hour += 12;
            if(hour < 10) hour = '0' + hour;
            retVal += ' ' + hour + ':' + minute + ':00';
        }
        else retVal += ' 00:00:00';
    }
    return retVal;
}

function getVariantValues(){
    var retVal = [];
    var productChildId = 0;
    var sku = '';
    var barCode = '';
    var quantity = 0;
    var price = 0;
    var oldPrice = 0;
    var weight = 0;
    var guaranteeMonth = 0;
    var discountPercent = 0;
    var cookieTimeout = 0;
    var flag = false;
    $('#tbodyVariantValue tr').each(function(){
        productChildId = parseInt($(this).attr('data-id'));
        sku = $(this).find('input.sku').val().trim();
        barCode = $(this).find('input.barCode').val().trim();
        //quantity = replaceCost($(this).find('input.quantity').val().trim(), true);
        price = replaceCost($(this).find('input.price').val().trim(), true);
        oldPrice = replaceCost($(this).find('input.oldPrice').val().trim(), true);
        weight = replaceCost($(this).find('input.weight').val().trim(), true);
        guaranteeMonth = replaceCost($(this).find('input.guaranteeMonth').val().trim(), true);
        discountPercent = replaceCost($(this).find('input.discountPercent').val().trim(), true);
        cookieTimeout = replaceCost($(this).find('input.cookieTimeout').val().trim(), true);
        if(sku == ''){
            flag = true;
            showNotification('Sku sản phẩm con không được bỏ trống', 0);
            $(this).find('input.sku').focus();
            return false;
        }
        else if(barCode == ''){
            flag = true;
            showNotification('BarCode sản phẩm con không được bỏ trống', 0);
            $(this).find('input.barCode').focus();
            return false;
        }
        else{
            retVal.push({
                ProductChildId: productChildId,
                Sku: sku,
                BarCode: barCode,
                Quantity: quantity,
                Price: price,
                OldPrice: oldPrice,
                Weight: weight,
                VariantValue: $(this).find('td.variantValue').text(),
                ProductImage: $(this).find('img.variantImg').attr('src'),
                VATStatusId: $(this).find('input.iCheckVAT').parent('div').hasClass('checked') ? 2 : 1,
                GuaranteeMonth: guaranteeMonth,
                ProductPrices: $(this).find('span.json').text(),
                DiscountPercent: discountPercent,
                CookieTimeout: cookieTimeout
            });
        }
    });
    if(flag) retVal = [];
    else if(retVal.length == 0) showNotification('Bạn chưa tạo sản phẩm con', 0);
    return retVal;
}

function getProductChilds(){
    var retVal = [];
    var quantity = 0;
    //var guaranteeMonth = 0;
    var flag = false;
    $('#tbodyProduct tr').each(function(){
        quantity = replaceCost($(this).find('input.quantity').val().trim(), true);
        //guaranteeMonth = replaceCost($(this).find('input.guaranteeMonth').val().trim(), true);
        if(quantity <= 0){
            flag = true;
            showNotification('Số lượng sản phẩm con phải lớn hơn 0', 0);
            $(this).find('input.quantity').focus();
            return false;
        }
        else{
            retVal.push({
                ProductChildId: 0,
                ProductName: '',
                ProductImage: '',
                Sku: '',
                StatusId: 2,
                BarCode: '',
                Quantity: quantity,
                Price: 0,
                OldPrice: 0,
                Weight: 0,
                ProductPartId: $(this).attr('data-id'),
                ProductPartChildId: $(this).attr('data-child'),
                DiscountPercent:0,
                CookieTimeout:0,
                //VATStatusId: $(this).find('input.iCheckVAT').parent('div').hasClass('checked') ? 2 : 1,
                //GuaranteeMonth: guaranteeMonth
            });
        }
    });
    if(flag) retVal = [];
    else if(retVal.length == 0) showNotification('Bạn chưa tạo sản phẩm con', 0);
    return retVal;
}