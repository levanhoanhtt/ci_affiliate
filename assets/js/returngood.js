$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Đơn hoàn hàng về',
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentReturnGoods(data) {
	var html = '';
    if(data!=null) {
        var transportStatusCss    = [];
        if(data.length > 0) transportStatusCss = data[0].transportStatusCss;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var urlEditReturnGood = $('#urlEditReturnGood').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].ReturnGoodId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].ReturnGoodId + '"></td>';
            html += '<td><a href="' + urlEditReturnGood + data[item].ReturnGoodId + '">' + data[item].ReturnGoodCode + '</a></td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td>' + data[item].StoreName + '</td>';
            html += '<td class="text-center"><span class="' + transportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
            html += '<td class="text-right"><span class="' + transportStatusCss[data[item].ReturnGoodTypeId] + '">' + data[item].ReturnGoodType + '</span></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table"></td></tr>';
        $('#tbodyReturnGoods').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}