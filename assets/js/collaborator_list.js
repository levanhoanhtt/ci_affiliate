$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'CTV',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
        }
    });


    
});

function renderContentCustomers(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var totalOrder = 0;
        var totalPrice = 0;
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].CustomerId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].CustomerId + '"></td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td>' + data[item].CustomerCode + '</td>';
            html += '<td class="text-right">';
            if(data[item].TotalOrder != null){
                html += formatDecimal(data[item].TotalOrder);
                totalOrder += parseInt(data[item].TotalOrder);
            }
            html += '</td><td class="text-right">0</td>';
            html += '<td class="text-right">0</td>';
            html += '<td class="text-right">0</td>';
            html += '<td class="text-right">0</td>';
            html += '<td></td>';
        }
        // if(html != '') html += '<tr><td colspan="5"></td><td class="text-right">' + formatDecimal(totalOrder.toString()) + '</td><td class="text-right">' + formatDecimal(totalPrice.toString()) + '</td><td></td></tr>';
        html += '<tr><td colspan="8" class="paginate_table"></td></tr>';
        $('#tbodyCustomer').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}