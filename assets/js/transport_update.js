var app = app || {};

app.init = function(transportId) {
    app.initLibrary();
    app.transportComment(transportId);
    app.updateCODStatus(transportId);
    app.updateTracking(transportId);
    app.updateShipCost(transportId);
    app.updateTransporter(transportId);
    app.updateTransportStatus(transportId);
    app.updatePendingStatus(transportId);
    app.chooseStore(transportId);
    app.customerAddress(transportId);
    app.updateTag(transportId);
    app.remind();
};

app.initLibrary = function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('body').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.cost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    var minNumber = 4;
    var maxNumber = 4;
    if ($(window).width() < 1300) {
        minNumber = 3;
        maxNumber = 3;
    }
    $('.bxslider').bxSlider({
        minSlides: minNumber,
        maxSlides: maxNumber,
        moveSlides: 1,
        slideWidth: 500,
        pager: false,
        infiniteLoop: false,
        nextText: ">",
        prevText: "<"
    });
};

app.transportComment = function(transportId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#insertTransportCommentUrl').val(),
                data: {
                    TransportId: transportId,
                    Comment: comment
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1) {
                        $('div#listComment').prepend(genItemComment(comment));
                        $('input#comment').val('');
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    $('#aShowComment').click(function(){
        $('#modalItemComment').modal('show');
        return false;
    });
};

app.updateCODStatus = function(transportId){
    $('#aCODStatus').click(function(){
        if(!$(this).hasClass('actived')){
            var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
            if(transportStatusId != 4 && transportStatusId != 5 || transportStatusId != 7) $('#modalCODStatus').modal('show');
        }
        return false;
    });
    $('#btnUpdateCODStatus').click(function(){
        var cODStatusId = $('select#cODStatusId').val();
        if(cODStatusId != $('input#cODStatusIdOriginal').val()) {
            var btn = $(this);
            btn.prop('disabled', true);
            updateTransportField(transportId, 'CODStatusId', cODStatusId, function(data){
                $('#tdCODStatus').html(data.StatusName);
                $('input#cODStatusIdOriginal').val(cODStatusId);
                $('#modalCODStatus').modal('hide');
                if(cODStatusId == '2' ) $('#aCODStatus').addClass('actived');
                btn.prop('disabled', false);
            }, function(){
                btn.prop('disabled', false);
            });
        }
        else showNotification('Vui lòng chọn trạng thái khác', 0);
    });
};

app.updateTracking = function(transportId){
    $('#aTracking').click(function(){
        var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var tracking = $(this).text();
            if (tracking == 'Cập nhật') tracking = '';
            $('input#tracking').val(tracking);
            $('#modalTracking').modal('show');
        }
        return false;
    });
    $('#btnUpdateTracking').click(function(){
        var tracking = $('input#tracking').val().trim();
        if(tracking != ''){
            var tracking1 = $('#aTracking').text();
            if(tracking1 == 'Cập nhật') tracking1 = '';
            if(tracking != tracking1) {
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(transportId, 'Tracking', tracking, function(data){
                    $('#aTracking').text(data.Tracking);
                    $('#modalTracking').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn mã vận đơn khác', 0);
        }
        else{
            showNotification('Mã vận đơn không được bỏ trống', 0);
            $('input#tracking').focus();
        }
    });
};

app.updateShipCost = function(transportId){
    $('#aShipCost').click(function(){
        var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var shipCost = $(this).find('span').text();
            if (shipCost == 'Cập nhật') shipCost = '0';
            $('input#shipCost').val(shipCost);
            $('#modalShipCost').modal('show');
        }
        return false;
    });
    $('#btnUpdateShipCost').click(function(){
        var shipCost = replaceCost($('input#shipCost').val(), true);
        if(shipCost > 0){
            var shipCost1 = $('#aShipCost').find('span').text();
            if(shipCost1 == 'Cập nhật') shipCost1 = '0';
            if(shipCost != replaceCost(shipCost1, true)){
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(transportId, 'ShipCost', shipCost, function (data) {
                    $('.spanShipCost').text(formatDecimal(shipCost.toString()));
                    $('#modalShipCost').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn phí ship khác', 0);
        }
        else{
            showNotification('Phí ship thực tế phải lớn hơn 0', 0);
            $('input#shipCost').focus();
        }
    });
};

app.updateTransporter = function(transportId){
    $('#aTransporter').click(function(){
        var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) $('#modalTransporter').modal('show');
        return false;
    });
    $('#btnUpdateTransporter').click(function(){
        var transporterId = $('select#transporterId').val();
        if(transporterId != $('input#transporterIdOriginal').val()) {
            var btn = $(this);
            btn.prop('disabled', true);
            updateTransportField(transportId, 'TransporterId', transporterId, function (data) {
                $('#aTransporter').html(data.TransporterName);
                $('input#transporterIdOriginal').val(transporterId);
                $('#modalTransporter').modal('hide');
                btn.prop('disabled', false);
            }, function(){
                btn.prop('disabled', false);
            });
        }
        else showNotification('Vui lòng chọn nhà vận chuyển khác', 0);
    });
};

app.updateTransportStatus = function(transportId){
    $('select#transportStatusId').change(function(){
        if($(this).val() == '5') $('#divCancelReason').fadeIn();
        else{
            $('#divCancelReason').fadeOut();
            $('select#cancelReasonId').val('0');
            $('input#cancelComment').val('');
        }
    });
    $('#aTransportStatus').click(function(){
        $('#modalTransportStatus').modal('show');
        return false;
    });
    $('#btnUpdateTransportStatus').click(function(){
        var transportStatusId = $('select#transportStatusId').val();
        if(transportStatusId != $('input#transportStatusIdOriginal').val()) {
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateFieldUrl').val(),
                data: {
                    TransportId: transportId,
                    FieldName: 'TransportStatusId',
                    FieldValue: transportStatusId,
                    OrderId: $('input#orderId').val(),
                    CancelReasonId: $('select#cancelReasonId').val(),
                    CancelComment: $('input#cancelComment').val().trim(),
                    StoreId: $('input#storeIdOriginal').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                        $('input#transportStatusIdOriginal').val(transportStatusId);
                        $('#tdTransportStatus').html(json.data.StatusName);
                        var transportStatusIconPath = $('input#transportStatusIconPath').val();
                        var li;
                        for(var i = 1; i < 10; i++){
                            li = $('#liTransportStatus_' + i);
                            if(li.hasClass('active')){
                                li.removeClass('active');
                                li.find('img').attr('src', transportStatusIconPath + i + '.1.png');
                                break;
                            }
                        }
                        li = $('#liTransportStatus_' + transportStatusId);
                        li.addClass('active').show();
                        li.find('img').attr('src', transportStatusIconPath + transportStatusId + '.2.png');
                        if(transportStatusId == 5 || transportStatusId == 7) $('#aTransportStatus').parent('p').remove();
                        $('#modalTransportStatus').modal('hide');
                        $('#divCancelReason').fadeOut();
                        $('select#cancelReasonId').val('0');
                        $('input#cancelComment').val('');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái khác', 0);
    });
};

app.updatePendingStatus = function(transportId){
    $('#aUpdatePending').click(function(){
        if($('input#transportStatusIdOriginal').val() == '1') $('#modalUpdatePending').modal('show');
        return false;
    });
    $('#btnUpdatePending').click(function(){
        var pendingStatusId = parseInt($('input[name="PendingStatusId"]:checked').val());
        if(pendingStatusId > 0 && pendingStatusId != parseInt($('input#pendingStatusIdOriginal').val())){
            var btn = $(this);
            btn.prop('disabled', true);
            updateTransportField(transportId, 'PendingStatusId', pendingStatusId, function(data){
                $('input#pendingStatusIdOriginal').val(pendingStatusId);
                $('#tdTransportStatus').html(data.StatusName);
                $('#modalUpdatePending').modal('hide');
                btn.prop('disabled', false);
            }, function(){
                btn.prop('disabled', false);
            });
        }
        else showNotification('Vui lòng chọn trạng thái Chờ xử lý khác', 0);
    });
};

app.chooseStore = function(transportId){
    $('#aChooseStore').click(function(){
        if($('input#transportStatusIdOriginal').val() == '1') $('#modalChooseStore').modal('show');
        return false;
    });
    $('#btnUpdateStore').click(function(){
        var storeId = parseInt($('input[name="StoreId"]:checked').val());
        if(storeId > 0 && storeId != parseInt($('input#storeIdOriginal').val())){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateFieldUrl').val(),
                data: {
                    TransportId: transportId,
                    FieldName: 'StoreId',
                    FieldValue: storeId,
                    OrderId: $('input#orderId').val(),
                    StoreId: $('input#storeIdOriginal').val(),
                    TransportCode: $('#aTransportCode').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        //$('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                        redirect(false, $('#aTransportList').attr('href'));
                    }
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn Cơ sở xử lý khác', 0);
    });
};

app.customerAddress = function(transportId){
    province('customerProvinceId', 'customerDistrictId', 'customerWardId');
    $('select#customerCountryId').change(function () {
        if ($(this).val() == '232' || $(this).val() == '0') {
            $('.VNoff').css('display', 'none');
            $('.VNon').fadeIn();
        }
        else {
            $('.VNon').css('display', 'none');
            $('.VNoff').fadeIn();
        }
    });
    $('#aCustomerAddress').click(function(){
        if($('input#transportStatusIdOriginal').val() == '1') {
            $('input#customerName').val($('.i-name').first().text());
            $('input#customerEmail').val($('.i-email').first().text());
            $('input#customerPhone').val($('.i-phone').first().text());
            $('input#customerAddress').val($('.spanAddress').first().text());
            var iCountry = $('.i-country').first();
            var countryId = iCountry.attr('data-id');
            if (countryId == '0' || countryId == '') countryId = '232';
            $('select#customerCountryId').val(countryId);
            $('select#customerProvinceId').val(iCountry.attr('data-province'));
            $('select#customerDistrictId').val(iCountry.attr('data-district'));
            $('select#customerWardId').val(iCountry.attr('data-ward'));
            $('input#customerZipCode').val(iCountry.attr('data-zip'));
            $('#modalCustomerAddress').modal('show');
        }
        return false;
    });
    $('#btnUpdateCustomerAddress').click(function(){
        if(validateEmpty('#modalCustomerAddress')) {
            var btn = $(this);
            btn.prop('disabled', true);
            var data = {
                CustomerId: $('input#customerId').val(),
                CustomerName: $('input#customerName').val().trim(),
                Email: $('input#customerEmail').val().trim(),
                PhoneNumber: $('input#customerPhone').val().trim(),
                Address: $('input#customerAddress').val().trim(),
                ProvinceId: $('select#customerProvinceId').val(),
                DistrictId: $('select#customerDistrictId').val(),
                WardId: $('select#customerWardId').val(),
                CountryId: $('select#customerCountryId').val(),
                ZipCode: $('input#customerZipCode').val().trim(),

                ItemId: transportId,
                OrderId: $('input#orderId').val(),
                ItemTypeId: 9
            };
            $.ajax({
                type: "POST",
                url: $('input#insertCustomerAddressUrl').val(),
                data: data,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        $('input#customerAddressId').val(json.data);
                        $('.i-name').text(data.CustomerName);
                        $('.i-phone').text(data.PhoneNumber);
                        $('.i-email').text(data.Email);
                        $('.i-country').text($('select#customerCountryId option[value="' + data.CountryId + '"]').text()).attr('data-id', data.CountryId).attr('data-province', data.ProvinceId).attr('data-district', data.DistrictId).attr('data-ward', data.WardId).attr('data-zip', data.ZipCode);
                        var address = '';
                        if (data.CountryId == '232') {
                            address += '<i class="fa fa-map-marker" aria-hidden="true"></i>';
                            var itemName  = '';
                            if(data.WardId != '0') itemName = $('select#customerWardId option[value="' + data.WardId + '"]').text();
                            address += '<span class="i-ward"><spam class="spanAddress">' + data.Address + '</spam> ' + itemName + '</span>';
                            itemName = data.DistrictId != '0' ? $('select#customerDistrictId option[value="' + data.DistrictId + '"]').text() : '';
                            if(itemName != '') address += '<span class="br-line i-district">' + itemName + '</span>';
                            itemName = data.ProvinceId != '0' ? $('select#customerProvinceId option[value="' + data.ProvinceId + '"]').text() : '';
                            if(itemName != '') address += '<span class="br-line i-province">' + itemName + '</span>';
                        }
                        else {
                            address += '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                            address += '<span class="i-province">ZipCode: ' + data.ZipCode + '</span>';
                        }
                        $('.i-address').html(address);
                        $('#modalCustomerAddress').modal('hide');
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

app.updateTag = function(transportId){
    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '50px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('.ntags').click(function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
        return false;
    });
    $('input.tagName').each(function () {
        inputTag.addTag($(this).val());
    });
    $('#btnUpdateTag').click(function(){
        if(tags.length > 0){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateItemTagUrl').val(),
                data: {
                    ItemIds: JSON.stringify([transportId]),
                    TagNames: JSON.stringify(tags),
                    ItemTypeId: 9,
                    ChangeTagTypeId: 3
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn nhãn', 0);
    });
};

app.remind = function(){
    $('.aRemind').click(function(){
        $('#modalRemind').modal('show');
        return false;
    });
    $('select#userId').change(function(){
        if($(this).val() != '0') $('select#partId').val('0').trigger('change');
    });
    $('select#partId').change(function(){
        if($(this).val() != '0') $('select#userId').val('0').trigger('change');
    });
    $('#btnAddRemind').click(function(){
        if(validateEmpty('#modalRemind')){
            var btn = $(this);
            btn.prop('disabled', true);
            var comments = [];
            var remindComment = $('input#remindComment').val().trim();
            if(remindComment != '') comments.push(remindComment);
            $.ajax({
                type: "POST",
                url: $('input#insertRemindUrl').val(),
                data: {
                    RemindId: 0,
                    RemindTitle: $('input#remindTitle').val().trim(),
                    RemindDate: $('input#remindDate1').val().trim(),
                    IsRepeat: 1,
                    RepeatDay: 0,
                    RepeatHour: 0,
                    RemindStatusId: 1,
                    UserId: $('select#userId').val(),
                    PartId: $('select#partId').val(),

                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        $('#modalRemind input').val('');
                        $('select#userId').val($('input#userLoginId').val());
                        $('select#partId').val('0');
                        $('#modalRemind').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

$(document).ready(function(){
    var transportId = parseInt($('input#transportId').val());
    app.init(transportId);
});

function updateTransportField(transportId, fieldName, fieldValue, fnSuccess, fnError){
    $.ajax({
        type: "POST",
        url: $('input#updateFieldUrl').val(),
        data: {
            TransportId: transportId,
            FieldName: fieldName,
            FieldValue: fieldValue,
            OrderId: $('input#orderId').val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                fnSuccess(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            fnError();
        }
    });
}