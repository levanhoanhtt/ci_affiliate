$(document).ready(function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('#btnAddSentence').click(function(){
        $('input#sentenceTitle, #sentenceContent').val('');
        $('input#sentenceId').val('0');
        $('input#iCheckFileTypeId_3').iCheck('check');
        $('#divFileTypeId_1').hide();
        $('#divFileTypeId_3').show();
        $('img#sentenceImage').attr('src', '').hide();
        $('#modalUpdateSentence').modal('show');
    });
    $("#tbodySentence").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#sentenceId').val(id);
        $("#sentenceGroupId").val($('input#sentenceGroupId_' + id)).trigger('change');
        $('input#sentenceTitle').val($('td#sentenceTitle_' + id).text());
        var sentenceContent = $('td#sentenceContent_' + id).text().trim();
        var fileTypeId = $('input#fileTypeId_' + id).val();
        $('input#iCheckFileTypeId_' + fileTypeId).iCheck('check');
        if(fileTypeId == '1'){
            $('#divFileTypeId_3').hide();
            $('#sentenceContent').val('');
            $('img#sentenceImage').attr('src', $('input#imagePath').val() +  sentenceContent).show();
            $('#divFileTypeId_1').show();
        }
        else{
            $('#sentenceContent').val(sentenceContent);
            $('#divFileTypeId_3').show();
            $('#divFileTypeId_1').hide();
            $('img#sentenceImage').attr('src', '').hide();
        }
        $('#modalUpdateSentence').modal('show');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteSentenceUrl').val(),
                data: {
                    SentenceId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#sentence_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('input.iCheckFileTypeId').on('ifToggled', function (e) {
        if (e.currentTarget.checked) {
            if (e.currentTarget.value == '1'){
                $('#divFileTypeId_3').hide();
                $('#sentenceContent').val('');
                $('img#sentenceImage').attr('src', '').hide();
                $('#divFileTypeId_1').show();
            }
            else{
                $('#sentenceContent').val('');
                $('#divFileTypeId_3').show();
                $('#divFileTypeId_1').hide();
                $('img#sentenceImage').attr('src', '').hide();
            }
        }
    });
    $('#btnUpImage').click(function(){
        chooseFile('Images', function(fileUrl) {
            $('img#sentenceImage').attr('src', fileUrl).show();
        });
    });
    $('#btnUpdateSentence').click(function(){
        var sentenceTitle = $("input#sentenceTitle").val().trim();
        if(sentenceTitle == ''){
            showNotification('Tiêu đề câu không được bỏ trống', 0);
            return false;
        }
        var sentenceContent = '';
        var fileTypeId = $('input.iCheckFileTypeId:checked').val();
        if(fileTypeId == '1') sentenceContent = $('img#sentenceImage').attr('src');
        else sentenceContent = $('#sentenceContent').val().trim();
        if(sentenceContent == ''){
            showNotification('Nội dung câu không được bỏ trống', 0);
            return false;
        }
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('#sentenceForm').attr('action'),
            data: {
                SentenceId: $("input#sentenceId").val(),
                SentenceTitle: sentenceTitle,
                SentenceContent: sentenceContent,
                SentenceGroupId: $("select#sentenceGroupId").val(),
                LibrarySentenceId: $("input#librarySentenceId").val(),
                FileTypeId: fileTypeId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });

        return false;
    });
});