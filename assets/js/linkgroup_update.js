var app = app || {};

app.init = function (canEdit) {
    app.product(canEdit);
};


app.product = function(canEdit){
    $('#tbodyProduct').on('click', '.link_delete', function () {
        if(canEdit == 1) {
            var tr = $(this).parent().parent();
            tr.remove();
        }
        return false;
    });
    if(canEdit == 1){
        chooseProductLink(function (tr) {
            chooseProductSearch(tr);
        });
        jwerty.key('f3', function (){
            var panelProduct = $('#panelProduct');
            if(panelProduct.hasClass('active')){
                panelProduct.removeClass('active');
                panelProduct.find('.panel-body').css("width", "99%");
            }
            else{
                panelProduct.addClass('active');
                setTimeout(function (){
                    panelProduct.find('.panel-body').css("width", "100%");
                    $('.wrapper').addClass('open-search');
                }, 100);
                $('input#pageIdProduct').val('1');
                getListProducts();
            }
        });
    }
};

$(document).ready(function () {
    var canEdit = parseInt($('input#canEdit').val());
    app.init(canEdit);
    var tags = [];
    $('.submit').click(function () {
        var products = getListOrderProducts();
        saveLinkGroup(products);
        return false;
    });
});

//=====================================================================

function saveLinkGroup(products){
    $.ajax({
        type: "POST",
        url: $('#linkGroupForm').attr('action'),
        data: {
            products: JSON.stringify(products),
            linkGroupName: $('#txtLinkGroupName').val(),
            linkGroupCommnet: $('#txtLinkGroupComment').val(),
            approachTypeId: $('#approachTypeId').val(),
            linkGroupId: $('#linkGroupId').val(),
        },
        success: function (response) {
            console.log(response)
            var json = $.parseJSON(response);

            showNotification(json.message, json.code);
            if (json.code == 1) {
                redirect(false, $('input#linkGroupEditUrl').val() + '/' + json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function chooseProductSearch(tr){
    var id = tr.attr('data-id');
    var childId = tr.attr('data-child');
    var flag = false;
    $('#tbodyProduct tr').each(function () {
        if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId) {
            flag = true;
            return false;
        }
    });
    if (!flag) {
        $.ajax({
            type: "POST",
            url: $('input#getProductDetailUrl').val(),
            data: {
                ProductId: id,
                ProductChildId: childId,
                IsGetPrice: 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var data = json.data;
                    var products = data.Products;
                    var productPath = $('input#productPath').val();
                    var html = '';
                    var totalPrice = replaceCost($('span#totalPrice').text(), true);
                    var totalWeight = 0;
                    for (var i = 0; i < products.length; i++) {
                        html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '" data-weight="' + products[i].Weight + '" data-kind="' + products[i].ProductKindId + '">';
                        html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank">' + products[i].ProductName + '</a></td>';
                        html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                        html += '<td class="text-center">' + products[i].BarCode + '</td>';
                        html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                        html += '<td class="tdPrice text-right"><span class="spanPrice">' + formatDecimal(products[i].Price.toString()) + '</span> ₫</td>';
                        html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a>';
                        html += '<span class="productPrices" style="display: none;">' + JSON.stringify(data.ProductPrices) + '</span><span class="originalPrice" style="display: none;">' + products[i].Price + '</span><span class="priceCapital" style="display: none;">' + products[i].PriceCapital + '</span></td></tr>';
                        totalPrice += parseInt(products[i].Price.toString());
                        totalWeight += parseInt(products[i].Weight);
                    }
                    $('#tbodyProduct').append(html);
                    $('span#totalPrice').text(formatDecimal(totalPrice.toString()));
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}


function getListOrderProducts(){
    var products = [];
    $('#tbodyProduct tr').each(function () {
        products.push({
            ProductId: parseInt($(this).attr('data-id')),
            ProductChildId: parseInt($(this).attr('data-child')),
        });
    });
    return products;
}
