$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentProducts(data) {
    var html = '';
    if(data!=null) {
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        var importIds = [];
        if(data.length > 0) importIds = data[0].ImportIds;
        var i = 0, j = 0, k = 0, n = importIds.length, m = 0, percent = 0, flag = false;
        html = '<tr><th>Sản phẩm</th><th class="text-center">Số lần nhập</th>';
        for(i = 1; i <= n; i++) html += '<th class="text-center">Lần ' + i + '</th>';
        html += '</tr>';
        $('#table-data').find('thead').html(html);
        html = '';
        for(i = 0; i < data.length; i++) {
            html += '<tr' + (data[i].ProductChildId > 0 ? ' class="cProduct"' : '') + '>';
            html += '<td class="tdHeight"><a href="' + ((data[i].ProductKindId == 3) ? urlEditProductCombo + data[i].ProductId : urlEditProduct + data[i].ProductId)  + '">' + data[i].ProductName + '</a></td>';
            m = data[i].ListPrices.length;
            html += '<td class="text-center tdHeight">' + m + '</td>';
            for(j = 0; j < n; j++){
                flag = false;
                for(k = 0; k < m; k++){
                    if(data[i].ListPrices[k].ImportId == importIds[j]){
                        html += '<td class="text-center"><span class="price">' + formatDecimal(data[i].ListPrices[k].Price.toString()) + '</span> ';
                        if(data[i].ListPrices[k].Price > data[i].ListPrices[k].OldPrice){
                            html += '<i class="fa fa-arrow-up"></i>';
                            if(data[i].ListPrices[k].OldPrice > 0) {
                                percent = Math.ceil((data[i].ListPrices[k].Price - data[i].ListPrices[k].OldPrice) * 100 / data[i].ListPrices[k].OldPrice);
                                html += ' <span class="percent-up">' + percent + '%</span>';
                            }
                        }
                        else if(data[i].ListPrices[k].Price < data[i].ListPrices[k].OldPrice){
                            html += '<i class="fa fa-arrow-down"></i>';
                            if(data[i].ListPrices[k].Price > 0) {
                                percent = Math.ceil((data[i].ListPrices[k].OldPrice - data[i].ListPrices[k].Price) * 100 / data[i].ListPrices[k].Price);
                                html += ' <span class="percent-down">' + percent + '%</span>';
                            }
                        }
                        html += '<br/>' + data[i].ListPrices[k].CrDateTime + '</td>';
                        flag = true;
                    }
                }
                if(!flag) html += '<td class="text-center">-</td>';
            }
            html += '</tr>';
        }
        html += '<tr><td colspan="' + (n + 2) + '" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
}