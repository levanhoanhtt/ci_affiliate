$(document).ready(function(){
    $("#tbodyManufacturer").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#manufacturerId').val(id);
        $('input#manufacturerName').val($('td#manufacturerName_' + id).text());
        scrollTo('input#manufacturerName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteManufacturerUrl').val(),
                data: {
                    ManufacturerId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#manufacturer_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#manufacturerForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#manufacturerForm')) {
            var form = $('#manufacturerForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="manufacturer_' + data.ManufacturerId + '">';
                            html += '<td id="manufacturerName_' + data.ManufacturerId + '">' + data.ManufacturerName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ManufacturerId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ManufacturerId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyManufacturer').prepend(html);
                        }
                        else $('td#manufacturerName_' + data.ManufacturerId).text(data.ManufacturerName);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});