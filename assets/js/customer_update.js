var app = app || {};

var paginateOrderObject = null;
var paginateTransactionType1Object = null;
var paginateTransactionType2Object = null;

app.init = function (customerId) {
    app.initLibrary(customerId);
    if(customerId > 0){
        app.collaborator(customerId);
        app.customerbank(customerId);
        app.updatePassword(customerId);
        // getListOrder(customerId);
        // getListTransaction(customerId, 1);
        // getListTransaction(customerId, 2);
        // app.customerComment(customerId);
    }
};

app.initLibrary = function(customerId){
    province();
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.iCheckCustomerType').on('ifToggled', function(e){
        if(e.currentTarget.checked) customerType(e.currentTarget.value);
    });
    $('select#countryId').change(function(){
        changeCountry($(this).val());
    });

    // $('#customerForm').on('keydown', 'input#debtCost', function (e) {
    //     if(checkKeyCodeNumber(e)) e.preventDefault();
    // }).on('keyup', 'input#debtCost', function () {
    //     var value = $(this).val();
    //     $(this).val(formatDecimal(value));
    // }).submit(function(e){
    //     e.preventDefault();
    // });

    $('.advance-btn').click(function(e) {
        $(this).remove();
        $('.advance-tab').slideDown();
    });
    if(customerId > 0){
        customerKind($('input#customerKindId').val());
        customerType($('input[name="CustomerTypeId"]:checked').val());
        changeCountry($('select#countryId').val());
    }
    $('#ulCustomerKindId').on('click', 'a', function(){
        var id = $(this).attr('data-id');
        $('input#customerKindId').val(id);
        customerKind(id);
    })
};

app.customerComment = function(customerId){
    $('#btnInsertComment').click(function(){
        var btn = $(this);
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#insertCustomerCommentUrl').val(),
                data: {
                    CustomerId: customerId,
                    Comment: comment
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1) {
                        $('div#listComment').prepend(genItemComment(comment));
                        $('input#comment').val('');
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    $('#aShowComment').click(function(){
        $('#modalItemComment').modal('show');
        return false;
    });
};

app.collaborator = function(customerId){
    $('#btnUpImage2').click(function(){
        chooseFile('Products', function(fileUrl) {
            $('img#productImage1').attr('src', fileUrl).show();
        });
    });
    $('#btnUpImage3').click(function(){
        chooseFile('Products', function(fileUrl) {
            $('img#productImage2').attr('src', fileUrl).show();
        });
    });

    $(".update_collaborator").click(function(){
        if(validateEmpty('#collaboratorForm')){
            var imgBefore = $("img#productImage1").attr('src');
            var imgAfter  = $("img#productImage2").attr('src');
            if(imgBefore == '' && imgAfter == ''){
                showNotification('Vui lòng chọn hình', 0);
                return false;
            }
            var images = [];
            images.push(imgBefore);
            images.push(imgAfter);
            $.ajax({
                type: "POST",
                url: $('#collaboratorForm').attr('action'),
                data: {
                    CustomerId : customerId,
                    IDCardNumber : $('input#iDCardNumber').val(),
                    FullName :  $('input#fullName').val(),
                    IDCardDate : $('input#iDCardDate').val(),
                    IDCardAddress : $('input#iDCardAddress').val(),
                    BirthDay: $('input#birthDay').val(),
                    IDCardImages : JSON.stringify(images),
                },
                success: function (response) {

                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        // if(customerId == 0) redirect(false, $('a#customerListUrl').attr('href'));
                        // else redirect(true, '');
                    }
                    // else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    // $('.submit').prop('disabled', false);
                }
            })
        }
        return false;
    })
};

app.customerbank = function(customerId){
    $('.update_customerbank').click(function(){
        if(validateEmpty('#customerbankForm')){
            $.ajax({
                type: "POST",
                url: $('#customerbankForm').attr('action'),
                data: {
                    CustomerBankId: $('input#customerBankId').val(),
                    BankName: $('input#bankName').val().trim(),
                    BranchName: $('input#branchName').val().trim(),
                    BankNumber: $('input#bankNumber').val(),
                    BankHolder: $('input#bankHolder').val(),
                    CustomerId: customerId,
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        // if($('input#customerBankId').val() == '0') redirect(false, $('.btn-default').attr('href'));
                        // else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });

    $(".radio_check").click(function(){
        if($(this).val() == '1'){
            $(".account_bank").show();
            $(".not_account_bank").hide();
        }else{
            $(".account_bank").hide();
            $(".not_account_bank").show();
        }
    })
}

app.updatePassword = function(customerId){
    $(".updatePassword").click(function(){
        if(validateEmpty('#updatePasswordForm')){
            var passwordNew = $('input#passwordNew').val().trim();
            var repeatPassword = $('input#repeatPassword').val().trim();
            if(passwordNew != repeatPassword){
                showNotification('Xác nhận mật khẩu chưa đúng',0);
                return false;
            }
            $.ajax({
                type: "POST",
                url: $('#updatePasswordForm').attr('action'),
                data: {
                    CustomerId: $('input#customerId').val().trim(),
                    PasswordOld : $('input#passwordOld').val().trim(),
                    Password : passwordNew,
                    RepeatPassword : repeatPassword,
                },
                success: function (response) {
                    console.log(response)
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        // if(customerId == 0) redirect(false, $('a#customerListUrl').attr('href'));
                        // else redirect(true, '');
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            })
        }
        return false;
    })
}


$(document).ready(function () {
    $('#btnUpImage1').click(function(){
        chooseFile('Images', function(fileUrl) {
            $('#ulImages').append('<li><a href="' + fileUrl + '" target="_blank"><img src="' + fileUrl + '"></a><i class="fa fa-times"></i></li>');
        });
    });
    $('#ulImages').on('click', 'i', function(){
        $(this).parent().remove();
    });
    var customerId = parseInt($('input#customerId').val());
    app.init(customerId);

    var tags = [];
    //https://github.com/xoxco/jQuery-Tags-Input
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '50px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('input.tagName').each(function(){
        inputTag.addTag($(this).val());
    });
    $('#ulTagExist').on('click', 'a', function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
    });

    $('.submit').click(function(){
        if(validateEmpty('#customerForm')){
            var password = $('input#password').val().trim();
            if(password != ''){
                if(password != $('input#rePass').val().trim()){
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            var images = [];
            $('#ulImages li a').each(function(){
                images.push($(this).attr('href'));
            });
            $('.submit').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('#customerForm').attr('action'),
                data: {
                    CustomerId: customerId,
                    FirstName: $('input#firstName').val().trim(),
                    LastName: $('input#lastName').val().trim(),
                    FullName: '',
                    Email: $('input#email').val().trim(),
                    PhoneNumber: $('input#phoneNumber').val().trim(),
                    //PhoneNumber2: $('input#phoneNumber2').val().trim(),
                    GenderId: $('input[name="GenderId"]:checked').val(),
                    StatusId: $('input#statusId').val(),
                    BirthDay: $('input#birthDay').val(),
                    CustomerTypeId: $('input[name="CustomerTypeId"]:checked').val(),
                    CustomerKindId: $('input#customerKindId').val(),
                    CountryId: $('select#countryId').val(),
                    ProvinceId: $('select#provinceId').val(),
                    DistrictId: $('select#districtId').val(),
                    WardId: $('select#wardId').val(),
                    ZipCode: $('input#zipCode').val(),
                    Address: $('input#address').val().trim(),
                    CustomerGroupId: $('select#customerGroupId').val(),  
                    FaceBook: $('input#facebook').val().trim(),
                    Comment: $('#customerComment').val().trim(),
                    // CareStaffId: $('select#careStaffId').val(),
                    DiscountTypeId: $('select#discountTypeId').val(),
                    PaymentTimeId: $('select#paymentTimeId').val(),
                    PositionName: $('input#positionName').val().trim(),
                    CompanyName: $('input#companyName').val().trim(),
                    IDCardDate: $('input#iDCardDate').val(),
                    IDCardNumber: $('input#iDCardNumber').val().trim(),
                    IDCardAddress: $('input#iDCardAddress').val().trim(),
                    IDCardImages: JSON.stringify(images),
                    // TaxCode: $('input#taxCode').val().trim(),
                    // DebtCost: replaceCost($('input#debtCost').val().trim(), true),
                    Password: password,
                    TagNames: JSON.stringify(tags)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(customerId == 0) redirect(false, $('a#customerListUrl').attr('href'));
                        else redirect(true, '');
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });


    
});

function customerKind(kind){
    if(kind == '2') $('#divWholesale').show();
    else{
        $('select#paymentTimeId').val('0');
        $('#divWholesale').hide();
    }
    $('select#customerGroupId .op').hide();
    $('select#customerGroupId .op_' + kind).show();
    $('select#customerGroupId').val('0');
}

function customerType(typeId){
    if(typeId == '2'){
        $('.privateCus').removeClass('in');
        $('.corporateCus').addClass('in');
    }
    else{
        $('.corporateCus').removeClass('in');
        $('.privateCus').addClass('in');
    }
}

function changeCountry(countryId){
    if(countryId != '232'){
        $('.VNon').css('display', 'none');
        $('.VNoff').fadeIn();
    }
    else{
        $('.VNoff').css('display', 'none');
        $('.VNon').fadeIn();
    }
}

function getListOrder(customerId) {
    var url = $('input#getListOrderUrl').val();
    $.ajax({
        type: "POST",
        url: url,
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            renderContentOrders(json.dataTables);
            var dataOrderPaging = {
                page: json.page,
                pageSize: json.pageSize,
                totalRow: json.totalRow,
                itemName: "Đơn hàng",
                urlPaging: url,
                classPaging: 'paginate_table_order',
                TransactionTypeId: 0
            };
            if(paginateOrderObject == null) paginateOrderObject = $('#table-data-order').PaginateCustom(dataOrderPaging);
            else paginateOrderObject.PaginateCustom(dataOrderPaging);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function getListTransaction(customerId, transactionTypeId) {
    var url = $('input#getListTransactionUrl').val() + '/' + transactionTypeId;
    $.ajax({
        type: "POST",
        url: url,
        data: {
            CustomerId: customerId,
            TransactionTypeId: transactionTypeId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            renderContentTransactions(json.dataTables);
            var dataTransactionPaging = {
                page: json.page,
                pageSize: json.pageSize,
                totalRow: json.totalRow,
                itemName: "Phiếu",
                urlPaging: url,
                classPaging: 'paginate_table_transaction_' + transactionTypeId,
                TransactionTypeId : transactionTypeId
            };
            if(transactionTypeId == 1){
                if(paginateTransactionType1Object == null) paginateTransactionType1Object = $('#table-data-transaction-type1').PaginateCustom(dataTransactionPaging);
                else paginateTransactionType1Object.PaginateCustom(dataTransactionPaging);
            }
            else {
                if(paginateTransactionType2Object == null) paginateTransactionType2Object = $('#table-data-transaction-type2').PaginateCustom(dataTransactionPaging);
                else paginateTransactionType2Object.PaginateCustom(dataTransactionPaging);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function renderContentOrders(data){
    var html = '';
    if(data!=null) {
        var orderLabelCss = [];
        var transportStatusCss = [];
        if(data.length > 0){
            orderLabelCss = data[0].orderLabelCss;
            transportStatusCss = data[0].transportStatusCss;
        }
        var urlEditOrder = $('#urlEditOrder').val() + '/';
        for(var item in data) {
            html += '<tr>';
            html += '<td><a href="' + urlEditOrder + data[item].OrderId + '" target="_blank">' + data[item].OrderCode + '</a></td>';
            html += '<td>' + getDayText(data[item].DayDiff) +  data[item].CrDateTime + '</td>';
            html += '<td class="text-center"><span class="' + orderLabelCss.OrderStatusCss[data[item].OrderStatusId] + '">' + data[item].OrderStatus + '</span></td>';
            html += '<td class="text-center"><span class="' + orderLabelCss.PaymentStatusCss[data[item].PaymentStatusId] + '">' + data[item].PaymentStatus + '</span></td>';
            html += '<td class="text-center"><span class="' + transportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
            html += '<td class="text-right">' + formatDecimal(data[item].TotalCost) + '</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="6" class="paginate_table_order paginate_table"></td></tr>';
        $('#tbodyOrderCustomer').html(html);
    }
}

function renderContentTransactions(data) {
    if(data!=null) {
        var html = '';
        var labelCss = [];
        var transactionTypeId = 0;
        if(data.length > 0){
            labelCss = data[0].labelCss;
            transactionTypeId = data[0].TransactionTypeId;
        }
        var urlEditTransaction = $('#urlEditTransaction').val() + '/';
        for(var item in data) {
            html += '<tr>';
            html += '<td><a href="' + urlEditTransaction + data[item].TransactionId + '" target="_blank">' + data[item].TransactionCode + '</a></td>';
            html += '<td>' + getDayText(data[item].DayDiff) + data[item].CrDateTime + '</td>';
            html += '<td>' + (data[item].TransactionReasonName != null ? data[item].TransactionReasonName : '') + '</td>';
            html += '<td>' + data[item].MoneySourceName + '</td>';
            html += '<td class="text-right">' + formatDecimal(data[item].PaidCost) + '</td>';
            html += '<td class="text-center"><span class="' + labelCss.TransactionStatusCss[data[item].TransactionStatusId] + '">' + data[item].TransactionStatusName + '</span></td>';
            html += '<td>' + data[item].Comment + '</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table paginate_table_transaction_'+transactionTypeId+'"></td></tr>';
        $('#tbodyTransactionType'+transactionTypeId).html(html);
    }
}

$.fn.PaginateCustom = function (opt) {
  var root = this;
  var conf = $.extend({pageShow: 11, page: 1, pageSize: 1, totalRow: 11,registerEvent: true}, opt);
  var actions = {
      init: function () {},
      render: function () {},
      event: function () {}
  };

  actions.init = function () {
      conf.page = parseInt(conf.page);
      conf.pageShow = parseInt(conf.pageShow);
      conf.pageSize = parseInt(conf.pageSize);
      conf.totalRow = parseInt(conf.totalRow);
  };

  actions.render = function () {
      if (conf.pageSize == 1) {
          html = '<div class="up-n-pager"><div class="total-row" style="padding-left:0 !important;">' + conf.totalRow + ' ' + conf.itemName + '</div></div>';
          $(root).find('.'+conf.classPaging).html(html);
          return false;
      }
      var html = "{total_rows}<ul>{first_page}{prev_page}{pages}{next_page}{last_page}</ul>";
      var total_rows = '<div class="total-row">' + conf.totalRow + ' ' + conf.itemName + '</div>';
      var first_page = '<li><a class="{option} first-page" data-page="1" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">Trang đầu</a></li>';
      var last_page = '<li><a class="{option} last-page" data-page="' + conf.pageSize + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">Trang Cuối</a></li>';
      var prev_page = '<li><a class="{option}" data-page="' + (conf.page - 1 > 0 ? conf.page - 1 : 1 ) + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;"><<</a></li>';
      var next_page = '<li><a class="{option}" data-page="' + (conf.page + 1 < conf.pageSize ? conf.page + 1 : conf.pageSize ) + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">>></a></li>';
      var start_page = 1;
      var end_page = 1;
      var offset = Math.floor(conf.pageShow / 2);

      if (conf.pageSize <= conf.pageShow) {
          start_page = 1;
          end_page = conf.pageSize;
          html = html.replace(/\{[a-z_]{6,}\}|/img, '');
      }
      else {
          // page ở khoảng giữa
          if (conf.page > offset && conf.pageSize > conf.page + offset) {
              start_page = conf.page - offset;
              end_page = conf.page + offset;
              // page ở cuối
          }
          else if (conf.page == conf.pageSize) {
              start_page = conf.pageSize - conf.pageShow + 1;
              end_page = conf.pageSize;
              next_page = next_page.replace('{option}', ' disable');
              last_page = last_page.replace('{option}', ' disable');
              //page ở đầu
          }
          else if (conf.page === 1) {
              start_page = 1;
              end_page = conf.pageShow;
              first_page = first_page.replace('{option}', ' disable');
              prev_page = prev_page.replace('{option}', ' disable');

              //page nhỏ hơn số hiển thị
          }
          else if (conf.page <= conf.pageShow) {
              start_page = 1;
              end_page = conf.pageShow;

              //page lớn hơn số hiển thị conf.page > conf.pageShow
          }
          else {
              start_page = conf.pageSize - conf.pageShow + 1;
              end_page = conf.pageSize;
          }
          next_page = next_page.replace('{option}', '');
          last_page = last_page.replace('{option}', '');
          prev_page = prev_page.replace('{option}', '');
          first_page = first_page.replace('{option}', '');
      }
      var pages = "";
      for (var page = start_page; page <= end_page; page++) {
          var row = '<li>';
          row += '<a class="{option}" data-page="' + page + '" data-transaction="'+conf.TransactionTypeId+'" href="javascript:;">' + page + '</a>';
          row += '</li>';
          if (page == conf.page) row = row.replace('{option}', 'active disable none-event');
          else row = row.replace('{option}', '');
          pages += row;
      }
      html = '<div class="up-n-pager">' + html;
      html = html.replace('{total_rows}', total_rows).replace('{pages}', pages).replace('{first_page}', first_page).replace('{prev_page}', prev_page).replace('{next_page}', next_page).replace('{last_page}', last_page);
      html += '</div>';
      $(root).find('.'+conf.classPaging).html(html);
  };
  actions.event = function () {
      $(root).on('click', '.'+conf.classPaging+' a', function (e) {
          if(!$(this).hasClass('disable')){
              var customerId = parseInt($('input#customerId').val());
              var data = {
                  page : $(this).attr('data-page'),
                  CustomerId : customerId,
                  TransactionTypeId : $(this).attr('data-transaction')
              };
              $.ajax({
                  type: "POST",
                  url: conf.urlPaging,
                  data: data,
                  success: function (response) {
                      var dataPaging = $.parseJSON(response);
                      render(dataPaging.callBackTable, dataPaging.dataTables);
                      conf.page = parseInt(dataPaging.page);
                      conf.pageSize = parseInt(dataPaging.pageSize);
                      conf.totalRow = parseInt(dataPaging.totalRow);
                      conf.CustomerId = customerId;
                      actions.render();
                  },
                  error: function (response) {
                      showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                  }
              });
          }
          return false;
      });

  };
  actions.init();
  actions.render();
  if(conf.registerEvent) actions.event();
  return root;
};
function render(call_back, data) {
    window[call_back](data);
}