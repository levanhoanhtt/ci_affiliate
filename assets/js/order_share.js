$(document).ready(function(){
    $('#tbodyOrder').on('click', '.aShare', function(){
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: $('input#updateFieldUrl').val(),
            data: {
                OrderId: id,
                FieldName: 'IsShare',
                FieldValue: 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) $('#trItem_' + id).remove();
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
});
