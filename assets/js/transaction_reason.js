$(document).ready(function(){
    $("#tbodyTransactionReason").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transactionReasonId').val(id);
        $('input#transactionReasonName').val($('td#transactionReasonName_' + id).text());
        $('select#transactionTypeId').val($('input#transactionTypeId_' + id).val());
        scrollTo('input#transactionReasonName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteTransactionReasonUrl').val(),
                data: {
                    TransactionReasonId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#transactionReason_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transactionReasonForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#transactionReasonForm')) {
            var form = $('#transactionReasonForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="transactionReason_' + data.TransactionReasonId + '">';
                            html += '<td id="transactionReasonName_' + data.TransactionReasonId + '">' + data.TransactionReasonName + '</td>';
                            html += '<td id="transactionType_' + data.TransactionReasonId + '">' + data.TransactionTypeHtml +'</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransactionReasonId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransactionReasonId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="transactionTypeId_' + data.TransactionReasonId + '" value="' + data.TransactionTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyTransactionReason').prepend(html);
                        }
                        else{
                            $('td#transactionReasonName_' + data.TransactionReasonId).text(data.TransactionReasonName);
                            $('td#transactionType_' + data.TransactionReasonId).text(data.TransactionTypeHtml);
                            $('input#transactionTypeId_' + data.TransactionReasonId).val(data.TransactionTypeId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});