$(document).ready(function(){
	$(".submit").click(function(){
		if(validateEmpty('#updatePasswordForm')){
			var passwordNew = $('input#passwordNew').val().trim();
			var repeatPassword = $('input#repeatPassword').val().trim();
			if(passwordNew != repeatPassword){
				showNotification('Xác nhận mật khẩu chưa đúng',0);
				return false;
			}
			$.ajax({
                type: "POST",
                url: $('#updatePasswordForm').attr('action'),
                data: {
                	CustomerId: $('input#customerId').val().trim(),
                	PasswordOld : $('input#passwordOld').val().trim(),
                	Password : passwordNew,
                	RepeatPassword : repeatPassword,
                },
                success: function (response) {
                	console.log(response)
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        // if(customerId == 0) redirect(false, $('a#customerListUrl').attr('href'));
                        // else redirect(true, '');
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            })
		}
		return false;
	})
})