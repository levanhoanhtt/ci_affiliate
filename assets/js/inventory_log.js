$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Lịch sử thay đổi tồn kho',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});


function renderContentProducts(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        var quantity = 0;
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].InventoryId+'">';
            html += '<td>' + (data[item].StatusId == 2 ? (getDayText(data[item].DayDiff) + data[item].UpdateDateTime) : '') + '</td>';
            html += '<td><a href="' + ((data[item].ProductKindId == 3) ? urlEditProductCombo + data[item].ProductId : urlEditProduct + data[item].ProductId)  + '">' + data[item].ProductName + '</a></td>';
            html += '<td>'+ data[item].BarCode +'</td>';
            html += '<td class="text-center">'+ formatDecimal(data[item].OldQuantity) +'</td>';
            quantity = parseInt(data[item].OldQuantity);
            if(data[item].InventoryTypeId == 1){
                html += '<td class="text-center">+ ' + data[item].Quantity + ' <i class="fa fa-arrow-up"></i></td>';
                quantity += parseInt(data[item].Quantity);
            }
            else{
                html += '<td class="text-center">- ' + data[item].Quantity + ' <i class="fa fa-arrow-down"></i></td>';
                quantity -= parseInt(data[item].Quantity);
            }
            html += '<td class="text-center">'+ formatDecimal(quantity.toString()) +'</td>';
            html += '<td>'+ data[item].Comment + '</td>';
            html += '<td>'+ data[item].StoreName + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].StatusId] + '">' + data[item].Status + '</span></td>';
            html += '<td>' + (data[item].StatusId == 2 ? data[item].FullName : '') + '</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="9" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('#tbodyProduct i.fa-info').tooltip();
}
