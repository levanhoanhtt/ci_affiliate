$(document).ready(function(){
    $("#tbodyPendingStatus").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#pendingStatusId').val(id);
        $('input#pendingStatusName').val($('td#pendingStatusName_' + id).text());
        $('select#itemTypeId').val($('input#itemTypeId_' + id).val());
        scrollTo('input#pendingStatusName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deletePendingStatusUrl').val(),
                data: {
                    PendingStatusId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#pendingStatus_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#pendingStatusForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#pendingStatusForm')) {
            var form = $('#pendingStatusForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="pendingStatus_' + data.PendingStatusId + '">';
                            html += '<td id="pendingStatusName_' + data.PendingStatusId + '">' + data.PendingStatusName + '</td>';
                            html += '<td id="itemTypeName_' + data.PendingStatusId + '">' + data.ItemTypeName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.PendingStatusId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.PendingStatusId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="itemTypeId_' + data.PendingStatusId + '" value="' + data.ItemTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyPendingStatus').prepend(html);
                        }
                        else{
                            $('td#pendingStatusName_' + data.PendingStatusId).text(data.PendingStatusName);
                            $('td#itemTypeName_' + data.PendingStatusId).html(data.ItemTypeName);
                            $('input#itemTypeId_' + data.PendingStatusId).val(data.ItemTypeId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});