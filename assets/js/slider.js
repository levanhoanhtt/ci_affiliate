$(document).ready(function(){
    $('#btnChooseImage').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Images';
        finder.selectActionFunction = function (fileUrl) {
            $('input#sliderImage').val(fileUrl);
            $('#imgSlider').attr('src', fileUrl).show();
        };
        finder.popup();
    });
    $('a#link_cancel').click(function(){
        $('#sliderForm').trigger("reset");
        $('#imgSlider').hide();
        return false;
    });
    $("#tbodySlider").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#sliderId').val(id);
        $('select#displayOrder').val($('select#displayOrder_' + id).val());
        var fileUrl = $('#imgSlider_' + id).attr('src');
        $('input#sliderImage').val(fileUrl);
        $('#imgSlider').attr('src', fileUrl).show();
        $('input#sliderLink').val($('td#sliderLink_' + id).text());
        $('input#sliderName').val($('td#sliderName_' + id).text());
        scrollTo('select#displayOrder');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteSliderUrl').val(),
                data: {
                    SliderId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#slider_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#sliderImage').val() == ''){
            showNotification('Xin mời chọn ảnh', 0);
            return false;
        }
        var form = $('#sliderForm');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');

            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
});
function changeDisplayOrder(select, sliderId){
    $.ajax({
        type: "POST",
        url: $('input#changeDisplayOrderUrl').val(),
        data: {
            SliderId: sliderId,
            DisplayOrder: select.value,
            SliderTypeId: $('input#sliderTypeId').val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1) redirect(true, '');
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}