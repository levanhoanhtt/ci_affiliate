$(document).ready(function(){
    $('#tbodyLibraryPage').on('click', 'a.link_edit', function(){
        var fbPageId = $(this).attr('data-id');
        var librarySentenceIds = JSON.stringify($('select#librarySentence_'+fbPageId).val());
        if(librarySentenceIds == null || librarySentenceIds == 'null'){
            librarySentenceIds = '[]';
            showNotification('Chưa chọn thư viện', 0);
            return false;
        }
        $.ajax({
            type: "POST",
            url: $('input#updateLibraryPageUrl').val().trim(),
            data: {
                FbPageId: fbPageId,
                LibrarySentenceIds: librarySentenceIds
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});