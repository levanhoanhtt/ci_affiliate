$(document).ready(function(){
	$('.submit').click(function(){
        if(validateEmpty('#customerbankForm')){
            $.ajax({
                type: "POST",
                url: $('#customerbankForm').attr('action'),
                data: {
                    CustomerBankId: $('input#customerBankId').val(),
                    BankName: $('input#bankName').val().trim(),
                    BranchName: $('input#branchName').val().trim(),
                    BankNumber: $('input#bankNumber').val(),
                    BankHolder: $('input#bankHolder').val(),
                    CustomerId: $('input#customerId').val(),
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if($('input#customerBankId').val() == '0') redirect(false, $('.btn-default').attr('href'));
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });

    $(".radio_check").click(function(){
        if($(this).val() == '1'){
            $(".account_bank").show();
            $(".not_account_bank").hide();
        }else{
            $(".account_bank").hide();
            $(".not_account_bank").show();
        }
    })
})