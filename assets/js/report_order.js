$(document).ready(function() {
	dateRangePicker();
	var reportTypeId = parseInt($('input#reportTypeId').val());
	var beginDate = '';
	var flag = false;
	var dates = $("#dateRangePicker").val().trim().split('-');
	if(dates.length == 2){
		beginDate =  dates[0].trim();
		if(beginDate != '') getReport(beginDate, dates[1].trim(), 0, 0, 0, reportTypeId);
		else flag = true;
	}
	else if(dates.length == 1){
		beginDate =  dates[0].trim();
		if(beginDate != '') getReport(beginDate, '', 0, 0, 0, reportTypeId);
		else flag = true;
	}
	else flag = true;
	if(flag) showNotification("Vui lòng nhập ngày bắt đầu", 0);

	$('#btnGetReport').click(function(){
		flag = false;
		dates = $("#dateRangePicker").val().trim().split('-');
		if(dates.length == 2){
			beginDate =  dates[0].trim();
			if(beginDate != '') getReport(beginDate, dates[1].trim(), $("select#storeId").val(), $("select#productTypeId").val(), $("select#deliveryTypeId").val(), reportTypeId);
			else flag = true;
		}
		else if(dates.length == 1){
			beginDate =  dates[0].trim();
			if(beginDate != '') getReport(beginDate, '', $("select#storeId").val(), $("select#productTypeId").val(), $("select#deliveryTypeId").val(), reportTypeId);
			else flag = true;
		}
		else flag = true;
		if(flag) showNotification("Vui lòng nhập ngày bắt đầu", 0);
	});

    $("#tbodyByDate").on('click', 'a.list_order', function () {
        var date = $(this).parent().parent().find('td:eq(0)').text();
        var orderStatusId = $(this).attr('order-status-id');
        $.ajax({
            type: "POST",
            url: $('input#searchByOrderStatusUrl').val(),
            data: {
                BeginDate: date,
                EndDate: date,
                OrderStatusId: orderStatusId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                var data = json.data;
                var html = '';
                if(data!=null) {
                    var labelCss    = [];
                    var orderLabelCss   = [];
                    var transportStatusCss = [];
                    if(data.length > 0){
                        labelCss = data[0].labelCss;
                        orderLabelCss = data[0].orderLabelCss;
                        transportStatusCss = data[0].transportStatusCss;
                    }
                    var urlEditOrder = $('#urlEditOrder').val() + '/';
                    var urlEditCustomer = $('#urlEditCustomer').val() + '/';
                    var sumTotalCost = 0;
                    for (var item = 0; item < data.length; item++) {
                        sumTotalCost += parseInt(data[item].TotalCost);
                        html += '<tr id="trItem_'+data[item].OrderId+'" class="orderStatus_'+data[item].OrderStatusId+'">';
                        if(data[item].VerifyStatusId == 2) html += '<td id="orderCode_'+data[item].OrderId+'"><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a><i class="fa fa-check tooltip1 active" title="Đã xác thực"></i></td>';
                        else html += '<td id="orderCode_'+data[item].OrderId+'"><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a></td>';
                        html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
                        html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
                        html += '<td class="text-center"><span class="' + orderLabelCss.OrderStatusCss[data[item].OrderStatusId] + '">' + data[item].OrderStatus + '</span></td>';
                        if(data[item].DeliveryTypeId == 1){
                            if(data[item].OrderStatusId == 6){
                                html += '<td class="text-center"><span class="' + transportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
                                html += '<td class="text-center"><span class="' + orderLabelCss.PaymentStatusCss[data[item].PaymentStatusId] + '">' + data[item].PaymentStatus + '</span></td>';
                            }
                            else html += '<td></td><td></td>';
                        }
                        else{
                            if(data[item].OrderStatusId == 1) html += '<td></td><td></td>';
                            else {
                                html += '<td class="text-center"><span class="' + transportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
                                html += '<td class="text-center"><span class="' + orderLabelCss.PaymentStatusCss[data[item].PaymentStatusId] + '">' + data[item].PaymentStatus + '</span></td>';
                            }
                        }
                        html += '<td class="text-right">' + formatDecimal(data[item].TotalCost) + '</td>';
                        html += '<td class="text-right"><span class="' + labelCss[data[item].OrderChanelId] + '">' + data[item].OrderChanel + '</span></td>';
                        html += '</tr>';
                    }
                    if(html != '') html += '<tr><td colspan="6"></td><td class="text-right">' + formatDecimal(sumTotalCost.toString()) + '</td><td></td></tr>';
                    html += '<tr><td colspan="9" class="paginate_table"></td></tr>';
                    $('#tbodyOrder').html(html);
                }
                $("#modalListOrder").modal('show');
            }
        });
    });
});

function getReport(beginDate, endDate, storeId, productTypeId, deliveryTypeId, reportTypeId){
	$('.text-null').text('');
	$('#tbodyByDate').html('');
	$.ajax({
        type: "POST",
        url: $('input#getReportUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate,
            StoreId: storeId,
            ProductTypeId: productTypeId,
            DeliveryTypeId: deliveryTypeId
        },
        success: function (response) {
        	var json = $.parseJSON(response);
        	var data = json.data;
        	
			var totalRevence = 0, totaldhSuccess = 0, totaldhFailure = 0, totaldhCOD = 0,  totaldhRevence = 0, totalShip = 0, totalPos = 0, total = 0;
			var html = '';
			if(reportTypeId == 1){ //doanh thu
				totalRevence 	= parseInt(data['Success'].TotalCost - data['Success'].DiscountCost) + parseInt(data['Failure'].TotalCost - data['Failure'].DiscountCost) + parseInt(data['COD'].TotalCost - data['COD'].DiscountCost);
				totaldhSuccess 	= parseInt(data['Success'].ShipCount) + parseInt(data['Success'].POSCount);
				totaldhFailure 	= parseInt(data['Failure'].ShipCount) + parseInt(data['Failure'].POSCount);
				totaldhCOD 		= parseInt(data['COD'].ShipCount) + parseInt(data['COD'].POSCount);
				totaldhRevence 	= totaldhSuccess + totaldhFailure + totaldhCOD;
				totalShip 		= parseInt(data['Success'].ShipCount) + parseInt(data['Failure'].ShipCount) + parseInt(data['COD'].ShipCount);
				totalPos 		= parseInt(data['Success'].POSCount) + parseInt(data['Failure'].POSCount) + parseInt(data['COD'].POSCount);

				$(".total-revence").text(formatDecimal(totalRevence.toString())+' đ');
				$(".total-dh-revence").text('ĐH: '+formatDecimal(totaldhRevence.toString()));
				$(".ship-post-revence").text(totalShip+ ' ship + '+ totalPos + ' pos');

				$(".total-success").text(formatDecimal((parseInt(data['Success'].TotalCost - data['Success'].DiscountCost)).toString())+ ' đ');
				$(".total-dh-success").text('ĐH: '+formatDecimal(totaldhSuccess.toString()));
				$(".ship-post-success").text(data['Success'].ShipCount+ ' ship + '+ data['Success'].POSCount + ' pos');

				$(".total-cod").text(formatDecimal((parseInt(data['COD'].TotalCost - data['COD'].DiscountCost)).toString())+ ' đ');
				$(".total-dh-cod").text('ĐH: '+formatDecimal(totaldhCOD.toString()));
				$(".ship-post-cod").text(data['COD'].ShipCount+ ' ship + '+ data['COD'].POSCount + ' pos');

				$(".total-failure").text(formatDecimal((parseInt(data['Failure'].TotalCost - data['Failure'].DiscountCost)).toString())+ ' đ');
				$(".total-dh-failure").text('ĐH: '+formatDecimal(totaldhFailure.toString()));
				$(".ship-post-failure").text(data['Failure'].ShipCount+ ' ship + '+ data['Failure'].POSCount + ' pos');

				html = '';
				$.each(data.ByDate,function(key, value){
					total = parseInt(value['Success'].TotalCost - value['Success'].DiscountCost) + parseInt(value['Failure'].TotalCost - value['Failure'].DiscountCost) + parseInt(value['COD'].TotalCost - value['COD'].DiscountCost);
					html += '<tr><td>' + key + '</td><td><a href="javascript:void(0);" order-status-id = "(6)" class="list_order"> ' + formatDecimal((parseInt(value['Success'].TotalCost - value['Success'].DiscountCost)).toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0);" order-status-id = "(2)" class="list_order"> ' + formatDecimal((parseInt(value['COD'].TotalCost - value['COD'].DiscountCost)).toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0);" order-status-id = "(5)" class="list_order"> ' + formatDecimal((parseInt(value['Failure'].TotalCost - value['Failure'].DiscountCost)).toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0);" order-status-id = "(2,5,6)" class="list_order"> ' + formatDecimal(total.toString()) + '</a></td></tr>';
				});
				$("#tbodyByDate").html(html);
			}
			else if(reportTypeId == 2){ //doanh so
				totalRevence 	= parseInt(data['Success'].TotalCost) + parseInt(data['Failure'].TotalCost) + parseInt(data['COD'].TotalCost);
				totaldhSuccess 	= parseInt(data['Success'].ShipCount) + parseInt(data['Success'].POSCount);
				totaldhFailure 	= parseInt(data['Failure'].ShipCount) + parseInt(data['Failure'].POSCount);
				totaldhCOD 		= parseInt(data['COD'].ShipCount) + parseInt(data['COD'].POSCount);
				totaldhRevence 	= totaldhSuccess + totaldhFailure + totaldhCOD;
				totalShip 		= parseInt(data['Success'].ShipCount) + parseInt(data['Failure'].ShipCount) + parseInt(data['COD'].ShipCount);
				totalPos 		= parseInt(data['Success'].POSCount) + parseInt(data['Failure'].POSCount) + parseInt(data['COD'].POSCount);

				$(".total-revence").text(formatDecimal(totalRevence.toString())+' đ');
				$(".total-dh-revence").text('ĐH: '+formatDecimal(totaldhRevence.toString()));
				$(".ship-post-revence").text(totalShip+ ' ship + '+ totalPos + ' pos');

				$(".total-success").text(formatDecimal(data['Success'].TotalCost.toString())+ ' đ');
				$(".total-dh-success").text('ĐH: '+formatDecimal(totaldhSuccess.toString()));
				$(".ship-post-success").text(data['Success'].ShipCount+ ' ship + '+ data['Success'].POSCount + ' pos');

				$(".total-cod").text(formatDecimal(data['COD'].TotalCost.toString())+ ' đ');
				$(".total-dh-cod").text('ĐH: '+formatDecimal(totaldhCOD.toString()));
				$(".ship-post-cod").text(data['COD'].ShipCount+ ' ship + '+ data['COD'].POSCount + ' pos');

				$(".total-failure").text(formatDecimal(data['Failure'].TotalCost.toString())+ ' đ');
				$(".total-dh-failure").text('ĐH: '+formatDecimal(totaldhFailure.toString()));
				$(".ship-post-failure").text(data['Failure'].ShipCount+ ' ship + '+ data['Failure'].POSCount + ' pos');

				html = '';
				$.each(data.ByDate,function(key, value){
					total = parseInt(value['Success'].TotalCost) + parseInt(value['Failure'].TotalCost) + parseInt(value['COD'].TotalCost);
					html += '<tr><td>' + key + '</td><td><a href="javascript:void(0)" order-status-id = "(6)" class="list_order">' + formatDecimal(value['Success'].TotalCost.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(2)" class="list_order">' + formatDecimal(value['COD'].TotalCost.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(5)" class="list_order">' + formatDecimal(value['Failure'].TotalCost.toString()) + '</a></td>';
					html += '<td><a href="javascript:void(0)" order-status-id = "(2,5,6)" class="list_order">' + formatDecimal(total.toString()) + '</a></td></tr>';
				});
				$('#tbodyByDate').html(html);
			}
        },
		error: function (response) {
			showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		}
    });
}