$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    province();
    $('.chooseImage').click(function(){
        chooseFile('Users', function(fileUrl) {
            $('input#avatar').val(fileUrl);
            $('img#imgAvatar').attr('src', fileUrl);
        });
    });
    $('#userForm').on('keyup', 'input#maxCost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('input#cbUnlimit').on('ifToggled', function(e){
        if(e.currentTarget.checked) $('input#maxCost').val('0').prop('disabled', true);
        else $('input#maxCost').val('0').prop('disabled', false);
    });
    $('a#generatorPass').click(function(){
        var pass = randomPassword(10);
        $('input#newPass').val(pass);
        $('input#rePass').val(pass);
        return false;
    });
    var userId = parseInt($('input#userId').val());
    $('.submit').click(function (){
        if(validateEmpty('#userForm') && validateNumber('#userForm', true, ' không được bỏ trống')){
            if ($('input#newPass').val() != $('input#rePass').val()) {
                showNotification('Mật khẩu không trùng', 0);
                return false;
            }
            if($('input#userName').length > 0 && $('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('Tên đăng nhập không được có khoảng trằng', 0);
                $('input#userName').focus();
                return false;
            }
            $('.submit').prop('disabled', true);
            var form = $('#userForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(userId == 0) redirect(false, $('input#userEditUrl').val() + '/' + json.data);
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
    if(userId > 0) {
        $('a#link_cancel').click(function(){
            $('input#beginDate, input#endDate').val('');
            $('input#userPartId').val('0');
            return false;
        });
        $('#tbodyUserPart').on('click', 'a.link_edit', function(){
            var id = $(this).attr('data-id');
            var roleId = $(this).attr('data-role-id');
            $('input#userPartId').val(id);
            $('select#partId').val($('input#partId_' + id).val());
            // $('select#roleId').val($('input#roleId_' + id).val());
            $('select#roleId').val(roleId).trigger('change');
            $('input#beginDate').val($('td#beginDate_' + id).text());
            $('input#endDate').val($('td#endDate_' + id).text());
            scrollTo('select#partId');
            return false;
        }).on('click', 'a.link_delete', function(){
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deletePartUrl').val(),
                    data: {
                        UserPartId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) $('tr#userPart_' + id).remove();
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            return false;
        });
        $('a#link_update').click(function () {
            var beginDate = $('input#beginDate').val().trim();
            if(beginDate == ''){
                showNotification('Ngày bắt đầu không được bỏ trống', 0);
                return false;
            }
            $.ajax({
                type: "POST",
                url: $('input#updatePartUrl').val(),
                data: {
                    UserPartId: $('input#userPartId').val(),
                    UserId: userId,
                    PartId: $('select#partId').val(),
                    RoleId: $('select#roleId').val(),
                    BeginDate: beginDate,
                    EndDate: $('input#endDate').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        $('a#link_cancel').trigger("click");
                        var data = json.data;
                        if (data.IsAdd == 1) {
                            var html = '<tr id="userPart_' + data.UserPartId + '">';
                            html += '<td id="partName_' + data.UserPartId + '">' + data.PartName + '</td>';
                            html += '<td id="roleName_' + data.UserPartId + '">' + data.RoleName + '</td>';
                            html += '<td id="beginDate_' + data.UserPartId + '">' + data.BeginDate + '</td>';
                            html += '<td id="endDate_' + data.UserPartId + '">' + data.EndDate + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.UserPartId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.UserPartId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="partId_' + data.UserPartId + '" value="' + data.PartId + '">' +
                                '<input type="text" hidden="hidden" id="roleId_' + data.UserPartId + '" value="' + data.RoleId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyUserPart').prepend(html);
                        }
                        else {
                            $('td#partName_' + data.UserPartId).text(data.PartName);
                            $('td#roleName_' + data.UserPartId).text(data.RoleName);
                            $('td#beginDate_' + data.UserPartId).text(data.BeginDate);
                            $('td#endDate_' + data.UserPartId).text(data.EndDate);
                        }

                        $('select').val('0').trigger('change');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
            return false;
        });

        $("body").on('change', '#partId', function(){
            $.ajax({
                type: "POST",
                url: $('input#listRoleUrl').val(),
                data: {
                    PartId: $(this).val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code = 1){
                        var html = "<option>Chọn</option>";
                        $.each( json.data, function( key, value ) {
                            console.log(value.RoleName)
                           html += `<option value="${value.RoleId}">${value.RoleName}</option>`;
                        });
                        $("#roleId").html(html);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        })
    }
});

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    //var chars = "ABCDEFGHIJKLMNOPQRSTXYZ1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}