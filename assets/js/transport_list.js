$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Vận chuyển',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
            if(actionCode == 'print_order'){
                $('input#transportIds').val(JSON.stringify(itemIds));
                $('#printForm').attr('action', $('input#printOrderMultiple').val()).submit();
            }
            else if(actionCode == 'print_transport'){
                $('input#transportIds').val(JSON.stringify(itemIds));
                $('#printForm').attr('action', $('input#printTransportMultiple').val()).submit();
            }
            else if(actionCode == 'export_transport'){
                $('input#transportIds').val(JSON.stringify(itemIds)); 
                $('#printForm').attr('action', $('input#exportTransport').val()).submit();

            }
            else if(actionCode == 'change_status'){
                $('input#transportIds').val(JSON.stringify(itemIds)); 
                $('#modalTransportStatus').modal('show');
            }
        }
    });
    $('#btnUpdateTransportStatus').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        var transportIds = $('input#transportIds').val();
        $.ajax({
            type: "POST",
            url: $('input#changeStatusBatchUrl').val(),
            data: {
                TransportIds: transportIds,
                TransportStatusId: $('select#transportStatusId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    transportIds = $.parseJSON(transportIds);
                    for(var i = 0; i < transportIds.length; i++) $('#tdStatus_' + transportIds[i]).html(json.data.StatusName);
                    $('#modalTransportStatus').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });
});

function renderContentTransports(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var sumCODCost = 0;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var urlEditOrder = $('#urlEditOrder').val() + '/';
        var urlEditTransport = $('#urlEditTransport').val() + '/';
        for (var item = 0; item < data.length; item++) {
            sumCODCost += parseInt(data[item].CODCost);
            html += '<tr id="trItem_'+data[item].TransportId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].TransportId + '"></td>';
            html += '<td><a href="' + urlEditTransport + data[item].TransportId + '">' + data[item].TransportCode + '</a></td>';
            html += '<td><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td>' + (data[item].TransporterName != null ? data[item].TransporterName : '') + '</td>';
            html += '<td class="text-center" id="tdStatus_'+data[item].TransportId+'"><span class="' + labelCss.TransportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
            html += '<td class="text-center"><span class="' + labelCss.CODStatusCss[data[item].CODStatusId] + '">' + data[item].CODStatus + '</span></td>';
            html += '<td class="text-right">' + formatDecimal(data[item].CODCost) + '</td>';
            html += '<td>' + data[item].StoreName + '</td>';
            html += '</tr>';
        }
        if(html != '') html += '<tr><td colspan="8"></td><td class="text-right">' + formatDecimal(sumCODCost.toString()) + '</td><td></td></tr>';
        html += '<tr><td colspan="10" class="paginate_table"></td></tr>';
        $('#tbodyTransport').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}