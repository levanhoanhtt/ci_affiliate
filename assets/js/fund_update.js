$(document).ready(function() {
    $('.submit').click(function () {
        if (validateEmpty('#fundForm')){
            $('.submit').prop('disabled', true);
            var form = $('#fundForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(false, $('a#fundListUrl').attr('href'));
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
});