$(document).ready(function(){
    getListProduct(1, 0, 0);
    $('#ulFilter').on('click', 'li', function(){
        getListProduct($(this).attr('data-id'), $('select#storeId').val(), $('input[name="InventoryStatusId"]:checked').val());
    });
    $('select#storeId').change(function(){
        getListProduct($('#ulFilter li.active').attr('data-id'), $(this).val(), $('input[name="InventoryStatusId"]:checked').val());
    });
    $('input.history').click(function(){
        getListProduct($('#ulFilter li.active').attr('data-id'), $('select#storeId').val(), $(this).val());
    });
    var statusSearch = null;
    $('#itemSearchName').keydown(function () {
        if (statusSearch != null) {
            clearTimeout(statusSearch);
            statusSearch = null;
        }
    }).keyup(function () {
        if (statusSearch == null) {
            statusSearch = setTimeout(function () {
                getListProduct($('#ulFilter li.active').attr('data-id'), $('select#storeId').val(), $('input[name="InventoryStatusId"]:checked').val());
            }, 500);
        }
    });

    $('#tbodyProduct').on('keydown', 'input.quantity', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.quantity', function () {
        var tr = $(this).parent('td').parent('tr');
        var current = replaceCost(tr.find('.tdQuantityCurrent').text(), true);
        var quantity = $(this).val().trim();
        if(quantity != ''){
            quantity = parseInt(quantity);
            if(tr.find('select').val() == '1') current += quantity;
            else current -= quantity;
        }
        tr.find('span.current').text(formatDecimal(current.toString()));
    }).on('change', 'select', function(){
        var tr = $(this).parent('td').parent('tr');
        var current = replaceCost(tr.find('.tdQuantityCurrent').text(), true);
        var quantity = tr.find('input.quantity').val().trim();
        if(quantity != ''){
            quantity = parseInt(quantity);
            if($(this).val() == '1') current += quantity;
            else current -= quantity;
        }
        tr.find('span.current').text(formatDecimal(current.toString()));
    }).on('click', 'button', function(){
        var btn = $(this);
        var tr = btn.parent('td').parent('tr');
        var quantity = tr.find('input.quantity').val().trim();
        if(quantity != '') {
            quantity = parseInt(quantity);
            if(quantity > 0){
                var storeId = $('select#storeId').val();
                if(storeId != '0') {
                    btn.prop('disabled', true);
                    $.ajax({
                        type: "POST",
                        url: $('input#updateInventoryUrl').val(),
                        data: {
                            ProductId: tr.attr('data-id'),
                            ProductChildId: tr.attr('data-child'),
                            OldQuantity: replaceCost(tr.find('.tdQuantityCurrent').text(), true),
                            Quantity: quantity,
                            InventoryTypeId: tr.find('select').val(),
                            StoreId: storeId,
                            StatusId: 1,
                            Comment: tr.find('input.comment').val().trim()
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if(json.code == 1){
                                tr.find('input.quantity').val('');
                                tr.find('input.comment').val('');
                                tr.find('span.current').text(tr.find('.tdQuantityCurrent').text());
                            }
                            btn.prop('disabled', false);
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                            btn.prop('disabled', false);
                        }
                    });
                }
                else showNotification('Bạn chưa chọn Cơ sở', 0);
            }
            else{
                showNotification('Số lượng tăng giảm phải lớn hơn 0', 0);
                tr.find('input.quantity').focus();
            }
        }
        else{
            showNotification('Vui lòng chọn số lượng tăng giảm', 0);
            tr.find('input.quantity').focus();
        }
    });
});

function getListProduct(productKindId, storeId, inventoryStatusId){
    var itemFilters = [];
    if(productKindId > 0){
        itemFilters.push({
            field_name: 'product_kind',
            conds: ['=', productKindId]
        });
        if(storeId > 0){
            itemFilters.push({
                field_name: 'product_store',
                conds: ['=', storeId]
            });
        }
        if(inventoryStatusId > 0){
            itemFilters.push({
                field_name: 'product_inventory_status',
                conds: ['=', inventoryStatusId]
            });
        }
        $.ajax({
            type: "POST",
            url: $('input#searchProductUrl').val(),
            data: {
                itemFilters: itemFilters,
                searchText: $('input#itemSearchName').val().trim(),
                limit: 10000
            },
            success: function (response) {
                var json = $.parseJSON(response);
                renderContentProducts(json.dataTables, productKindId);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}
function renderContentProducts(data, productKindId) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        var quantity = 0;
        var flag = false;
        for (var item = 0; item < data.length; item++) {
            flag = false;
            if(productKindId == 2) {
                if(data[item].ProductKindId != 2) flag = true;
                else flag = false;
            }
            else flag = true;
            quantity = data[item].ProductQuantity != null ? formatDecimal(data[item].ProductQuantity.toString()) : 0;
            var tr = '<tr id="trItem_'+data[item].ProductId+'_'+data[item].ProductChildId+'" data-id="'+data[item].ProductId+'" data-child="'+data[item].ProductChildId+'"' + (data[item].ProductChildId > 0 ? ' class="cProduct"' : '') + '>';
            tr += '<td class="productName"><a href="' + ((data[item].ProductKindId == 3) ? urlEditProductCombo + data[item].ProductId : urlEditProduct + data[item].ProductId)  + '">' + data[item].ProductName + '</a></td>';
            if(flag) tr += '<td class="text-center tdQuantityCurrent">' + quantity + '</td>';
            else tr += '<td></td>';
            tr += '<td class="text-center"></td>';
            tr += '<td class="text-center"><span class="' + labelCss[data[item].ProductKindId] + '">' + data[item].ProductKind + '</span></td>';
            tr += '<td>'+ data[item].BarCode + '</td>';
            tr += '<td class="text-center"><span class="' + labelCss[data[item].ProductStatusId] + '">' + data[item].ProductStatus + '</span></td>';
            if(flag){
                tr += '<td class="text-center tdQuantity"><span class="spanQuantity"><span class="old">' + quantity + '</span> -> <span class="current">' + quantity + '</span></span>';
                tr += '<select><option value="1">Tăng</option><option value="2">Giảm</option></select><input type="text" class="quantity" value="">';
                tr += '<input type="text" class="comment" placeholder="Ghi chú thay đổi"><button type="button">Lưu</button></td>';
            }
            else tr += '<td></td>';
            tr += '</tr>';
            html += tr;
        }
        $('#table-data').find('tbody').html(html);
    }
}