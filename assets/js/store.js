$(document).ready(function(){
    province();
    $('.submit').click(function(){
        if(validateEmpty('#storeForm')) {
            if($('#headUserId option:selected').val() == 0){
                showNotification('Vui lòng chọn Người phụ trách', 0);
                $('select#headUserId').focus();
                return false;
            }
            var userIds = JSON.stringify($('select#userId').val());
            if(userIds == null || userIds == 'null'){
                showNotification('Chưa chọn Nhân viên', 0);
                $('select#userId').focus();
                return false;
            }
            $('input#userIds').val(userIds);
            $('.submit').prop('disabled', true);
            var form = $('#storeForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1 ) redirect(false, $('a#storeListUrl').attr('href'));
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });

    $("#tbodyStore").on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    StoreId: id,
                    StoreStatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#store_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    }).on("click", "a.link_status", function(){
        var id = $(this).attr('data-id');
        var statusId = $(this).attr('data-status');
        if(statusId != $('input#statusId_' + id).val()) {
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    StoreId: id,
                    StoreStatusId: statusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1){
                        $('td#statusName_' + id).html(json.data.StatusName);
                        $('input#statusId_' + id).val(statusId);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});