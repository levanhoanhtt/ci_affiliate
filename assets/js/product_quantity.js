$(document).ready(function () {
    /*actionItemAndSearch({
     ItemName: 'Sản phẩm',
     IsRenderFirst: true,
     extendFunction: function(itemIds, actionCode){}
     });*/
    var searchUrl = $('input#searchFilterUrl').val();
    getListProductQuantity(0, 0, searchUrl);
    $('input.history').click(function () {
        getListProductQuantity(parseInt($(this).val()), parseInt($('select#storeId').val()), searchUrl);
    });
    $('select#storeId').change(function () {
        getListProductQuantity(parseInt($('input[name="InventoryStatusId"]:checked').val()), parseInt($(this).val()), searchUrl);
    });
    $('#btnCloseQuantityInfo').click(function () {
        $('div#quantityInfo').removeClass('active');
    });
    $(document).on('click', 'body', function () {
        $('div#quantityInfo').removeClass('active');
    }).on('click', 'div#quantityInfo', function (e) {
        e.stopPropagation();
    });
});
function getListProductQuantity(inventoryStatusId, storeId, searchUrl) {
    if(inventoryStatusId > 0 && storeId > 0) $('#btn-filter').attr('data-href', searchUrl + '?InventoryStatusId=' + inventoryStatusId + '&StoreId=' + storeId);
    else if(inventoryStatusId > 0) $('#btn-filter').attr('data-href', searchUrl + '?InventoryStatusId=' + inventoryStatusId);
    else if(storeId > 0) $('#btn-filter').attr('data-href', searchUrl + '?StoreId=' + storeId);
    else $('#btn-filter').attr('data-href', searchUrl);
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: 1,
        extendFunction: function (itemIds, actionCode) {}
    });
    /*var i = 0;
    var index = -1;
    for (i = 0; i < data_filter.itemFilters.length; i++) {
        if (data_filter.itemFilters[i].field_name == "product_inventory_status") {
            index = i;
            break;
        }
    }
    if (inventoryStatusId > 0) {
        var inventoryStatusName = inventoryStatusId == 2 ? 'Còn hàng' : 'Hết hàng';
        if (index > -1) {
            data_filter.itemFilters[index].conds = ["=", inventoryStatusId];
            data_filter.itemFilters[index].tag = 'Tình trạng tồn kho là ' + inventoryStatusName;
            for (i = 0; i < data_filter.tagFilters.length; i++) {
                if (data_filter.tagFilters[i].indexOf('Tình trạng tồn kho') > -1) {
                    data_filter.tagFilters[i] = 'Tình trạng tồn kho là ' + inventoryStatusName;
                    break;
                }
            }
        }
        else {
            data_filter.itemFilters.push({
                conds: ["=", inventoryStatusId],
                field_name: "product_inventory_status",
                tag: 'Tình trạng tồn kho là ' + inventoryStatusName
            });
            data_filter.tagFilters.push('Tình trạng tồn kho là ' + inventoryStatusName);
        }
    }
    else {
        if (index > -1) {
            data_filter.itemFilters.splice(index, 1);
            data_filter.tagFilters.splice(index, 1);
        }
    }
    index = -1;
    for (i = 0; i < data_filter.itemFilters.length; i++) {
        if (data_filter.itemFilters[i].field_name == "product_store") {
            index = i;
            break;
        }
    }
    if (storeId > 0) {
        var storeName = $('select#storeId option[value="' + storeId + '"]').text();
        if (index > -1) {
            data_filter.itemFilters[index].conds = ["=", storeId];
            data_filter.itemFilters[index].tag = 'Cơ sở là ' + storeName;
            for (i = 0; i < data_filter.tagFilters.length; i++) {
                if (data_filter.tagFilters[i].indexOf('Cơ sở là') > -1) {
                    data_filter.tagFilters[i] = 'Cơ sở là ' + storeName;
                    break;
                }
            }
        }
        else {
            data_filter.itemFilters.push({
                conds: ["=", storeId],
                field_name: "product_store",
                tag: 'Cơ sở là ' + storeName
            });
            data_filter.tagFilters.push('Cơ sở là ' + storeName);
        }
    }
    else {
        if (index > -1) {
            data_filter.itemFilters.splice(index, 1);
            data_filter.tagFilters.splice(index, 1);
        }
    }
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: 1,
        extendFunction: function (itemIds, actionCode) {}
    });*/
}

function renderContentProducts(data) {
    var html = '';
    if (data != null) {
        var labelCss = [];
        if (data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        var canViewPrice = $('input#canViewPrice').val() == '1';
        for (var item = 0; item < data.length; item++) {
            html += '<tr' + (data[item].ProductChildId > 0 ? ' class="cProduct"' : '') + '>';
            html += '<td class="productName"><a href="' + ((data[item].ProductKindId == 3) ? urlEditProductCombo + data[item].ProductId : urlEditProduct + data[item].ProductId) + '">' + data[item].ProductName + '</a></td>';
            html += '<td class="text-center">';
            if (data[item].ProductKindId != 2) {
                html += '<span class="spanQuantity" data-id="' + data[item].ProductId + '" data-child="' + data[item].ProductChildId + '">';
                html += data[item].ProductQuantity != null ? formatDecimal(data[item].ProductQuantity.toString()) : '0';
                html += '</span>';
            }
            html += '</td>';
            html += '<td class="text-center"></td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductKindId] + '">' + data[item].ProductKind + '</span></td>';
            if (canViewPrice) html += '<td class="text-right">' + (data[item].PriceCapital != null ? formatDecimal(data[item].PriceCapital.toString()) : '0') + '</td>';
            html += '<td>' + data[item].BarCode + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductStatusId] + '">' + data[item].ProductStatus + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);

        $('.spanQuantity').hover(function () {
            var span = $(this);
            $('div#quantityInfo').css('top', ($(this).offset().top - 220) + 'px').addClass('active');
            $('div#quantityInfo .imgLoading').show();
            $('div#quantityInfo table').hide();
            var productId = span.attr('data-id');
            var productChildId = span.attr('data-child');
            var inputs = $('input.productQuantity_' + productId + '_' + productChildId);
            if (inputs.length > 0) {
                var html = '';
                inputs.each(function () {
                    html += '<tr><td>' + $(this).attr('data-store') + '</td>';
                    html += '<td>' + $(this).val() + '</td></tr>';
                });
                $("div#quantityInfo table tbody").html(html);
                $('div#quantityInfo .imgLoading').hide();
                $('div#quantityInfo table').show();
                $('#pProductName').text(span.parent().parent().find('td.productName').text());
            }
            else {
                $.ajax({
                    type: "POST",
                    url: $('input#getQuantityUrl').val(),
                    data: {
                        ProductId: productId,
                        ProductChildId: productChildId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var html = '';
                            var htmlInput = '';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) {
                                html += '<tr><td>' + data[i].StoreName + '</td>';
                                html += '<td>' + data[i].Quantity + '</td></tr>';
                                htmlInput += '<input type="text" hidden="hidden" class="productQuantity_' + productId + '_' + productChildId + '" data-store="' + data[i].StoreName + '" value="' + data[i].Quantity + '">';
                            }
                            $("div#quantityInfo table tbody").html(html);
                            $('div#quantityInfo .imgLoading').hide();
                            $('div#quantityInfo table').show();
                            $('#divProductQuantity').append(htmlInput);
                            $('#pProductName').text(span.parent().parent().find('td.productName').text());
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }, function () {
        });
    }
}