var app = app || {};

app.init = function (storeCirculationId, canEdit, canActive, transportId) {
    app.initLibrary();
    if(canEdit == 1){
        app.product();
        if(storeCirculationId > 0) app.cancelOrder(storeCirculationId);
    }
    app.submit(storeCirculationId, canEdit);
    //if(storeCirculationId > 0 && canActive == 1 && canEdit == 1) app.transport(storeCirculationId);
    if(storeCirculationId > 0){
        if (transportId == 0 || canActive == 1 && canEdit == 1) app.addTransport(storeCirculationId);
        else app.cancelTransport(transportId, storeCirculationId);
    }
    app.storeCirculationComment(storeCirculationId);
};

app.initLibrary = function(){
    if($('.bxslider').length > 0) {
        var minNumber = 3;
        var maxNumber = 3;
        if($('.bxslider.short').length > 0) {
            minNumber = 3;
            maxNumber = 3;
        }
        else{
            minNumber = 4;
            maxNumber = 4;
        }
        if ($(window).width() < 1300) {
            if($('.bxslider.short').length > 0) {
                minNumber = 2;
                maxNumber = 2;
            }
            else{
                minNumber = 3;
                maxNumber = 3;
            }
        }
        $('.bxslider').bxSlider({
            minSlides: minNumber,
            maxSlides: maxNumber,
            moveSlides: 1,
            slideWidth: 500,
            pager: false,
            infiniteLoop: false,
            nextText: ">",
            prevText: "<"
        });
    }
};

app.product = function(){
    var inputWeight = $('input#transportWeight');
    chooseProduct(function (tr) {
        var id = tr.attr('data-id');
        var childId = tr.attr('data-child');
        var flag = false;
        $('#tbodyProduct tr').each(function () {
            if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId) {
                flag = true;
                var quantity = replaceCost($(this).find('input.quantity').val(), true) + 1;
                $(this).find('input.quantity').val(formatDecimal(quantity.toString()));
                quantity = quantity * replaceCost($(this).find('.spanPrice').text(), true);
                $(this).find('input.sumPrice').val(formatDecimal(quantity.toString()));
                calcPrice(0);
                if(inputWeight.length > 0){
                    var totalWeight = parseInt($(this).attr('data-weight')) + replaceCost(inputWeight.val(), true);
                    inputWeight.val(formatDecimal(totalWeight.toString()));
                }
                return false;
            }
        });
        if (!flag) {
            $.ajax({
                type: "POST",
                url: $('input#getProductDetailUrl').val(),
                data: {
                    ProductId: id,
                    ProductChildId: childId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var data = json.data;
                        var products = data.Products;
                        var productPath = $('input#productPath').val();
                        var html = '';
                        var totalWeight = 0;
                        for (var i = 0; i < products.length; i++) {
                            html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '" data-weight="' + products[i].Weight + '">';
                            html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank">' + products[i].ProductName + '</a></td>';
                            html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                            html += '<td class="text-center">' + products[i].BarCode + '</td>';
                            html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                            html += '<td class="tdPrice text-right"><span class="spanPrice">' + formatDecimal(products[i].Price.toString()) + '</span> ₫</td>';
                            html += '<td><input class="form-control quantity sll" value="1"></td>';
                            html += '<td><input class="form-control sumPrice text-right" disabled value="' + formatDecimal(products[i].Price.toString()) + '"></td>';
                            html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td></tr>';
                            totalWeight += parseInt(products[i].Weight);
                        }
                        $('#tbodyProduct').append(html);
                        if(inputWeight.length > 0){
                            totalWeight += replaceCost(inputWeight.val(), true);
                            inputWeight.val(formatDecimal(totalWeight.toString()));
                        }
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    $('#tbodyProduct').on('keydown', 'input.quantity', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.quantity', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        var tr = $(this).parent().parent();
        var sumCost = replaceCost(value, true) * replaceCost(tr.find('.spanPrice').text(), true);
        tr.find('input.sumPrice').val(formatDecimal(sumCost.toString()));
        if(inputWeight.length > 0){
            var totalWeight = 0;
            $('#tbodyProduct tr').each(function(){
                totalWeight += parseInt($(this).attr('data-weight')) * replaceCost($(this).find('input.quantity').val(), true);
            });
            inputWeight.val(formatDecimal(totalWeight.toString()));
        }
    }).on('click', '.link_delete', function (){
        $(this).parent().parent().remove();
        if(inputWeight.length > 0){
            var totalWeight = 0;
            $('#tbodyProduct tr').each(function(){
                totalWeight += parseInt($(this).attr('data-weight')) * replaceCost($(this).find('input.quantity').val(), true);
            });
            inputWeight.val(formatDecimal(totalWeight.toString()));
        }
        return false;
    });
};

app.cancelOrder = function(storeCirculationId){
    $('#btnCancelOrder').click(function(){
        if(confirm('Bạn có thực sự muốn hủy ?')){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    StoreCirculationId: storeCirculationId,
                    StoreCirculationStatusId: 4
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('select#storeCirculationStatusId').val(4);
                        $('#divActive, .submit').remove();
                    }
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

app.submit = function(storeCirculationId, canEdit){
    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '54px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    if(storeCirculationId > 0) {
        $('input.tagName').each(function () {
            inputTag.addTag($(this).val());
        });
    }
    if(canEdit == 1) {
        $('.ntags').click(function () {
            var tag = $(this).text();
            if (!inputTag.tagExist(tag)) inputTag.addTag(tag);
            return false;
        });
        $('.submit').click(function () {
            var products = [];
            $('#tbodyProduct tr').each(function () {
                products.push({
                    ProductId: parseInt($(this).attr('data-id')),
                    ProductChildId: parseInt($(this).attr('data-child')),
                    Quantity: replaceCost($(this).find('input.quantity').val(), true)
                });
            });
            if (products.length > 0) {
                var storeSourceId = parseInt($('select#storeSourceId').val());
                var storeDestinationId = parseInt($('select#storeDestinationId').val());
                if (storeSourceId == 0) {
                    showNotification('Vui lòng chọn Cơ sở xuất hàng', 0);
                    return false;
                }
                if (storeDestinationId == 0) {
                    showNotification('Vui lòng chọn Cơ sở nhập hàng', 0);
                    return false;
                }
                if (storeSourceId == storeDestinationId) {
                    showNotification('Cơ sở nhập hàng phải khác Cơ sở xuất hàng', 0);
                    return false;
                }
                var comments = [];
                if(storeCirculationId == 0){
                    $('.listComment .pComment').each(function(){
                        comments.push($(this).text());
                    });
                }
                $('.submit').prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('#storeCirculationForm').attr('action'),
                    data: {
                        StoreCirculationId: storeCirculationId,
                        StoreSourceId: storeSourceId,
                        StoreDestinationId: storeDestinationId,
                        StoreCirculationStatusId: $('select#storeCirculationStatusId').val(),

                        TagNames: JSON.stringify(tags),
                        Products: JSON.stringify(products),
                        Comments: JSON.stringify(comments)
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1){
                            if(storeCirculationId == 0) redirect(false, $('a#storeCirculationListUrl').attr('href'));
                            $('.submit').prop('disabled', false);
                        }
                        else $('.submit').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('.submit').prop('disabled', false);
                    }
                });
            }
            else showNotification('Vui lòng chọn sản phẩm', 0);
            return false;
        });
    }
};

app.addTransport = function(storeCirculationId){
    $('#btnTransport').click(function(){
        $("#modalTransport").modal('show');
    });
    $('#btnUpdateTransport').click(function(){
        var btn = $(this);
        $.ajax({
            type: "POST",
            url: $('input#insertTransportUrl').val(),
            data: {
                StoreCirculationTransportId: 0,
                StoreCirculationId: storeCirculationId,
                TransportStatusId: 1,
                PendingStatusId: 0,
                TransportTypeId: $("select#transportTypeId").val(),
                TransporterId: $("select#transporterId").val(),
                Tracking: '',
                Weight: $('input#transportWeight').val().trim(),
                ShipCost: 0,
                Comment:$("#transportComment").val().trim(),
                CancelReasonId: 0,
                CancelComment: ''
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });



    /*$('.btnActive').click(function(){
        var btn = $(this);
        var statusId = parseInt(btn.attr('data-id'));
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#activeStoreCirculationUrl').val(),
            data: {
                StoreCirculationId: storeCirculationId,
                StoreSourceId: $('select#storeSourceId').val(),
                StoreDestinationId: $('select#storeDestinationId').val(),
                StoreCirculationStatusId: statusId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1){
                    $('select#storeCirculationStatusId').val(statusId);
                    if(statusId == 2){
                        $('#btnActive1, #btnCancelOrder').remove();
                        $('#btnActive2').fadeIn();
                    }
                    else if(statusId == 3) $('#divActive').remove();
                    $('.submit').remove();
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });*/
};

app.cancelTransport = function(transportId, storeCirculationId){
    $('.btn-cancle-cart').click(function(){
        $('#modalCancelTransport').modal('show');
        return false;
    });
    $('#btnCancelTransport').click(function(){
        var btn = $(this);
        var transportStatusId = parseInt($('input#transportStatusId').val());
        if(transportStatusId == 1){ //huy thang, ko can thong bao
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changeTransportStatusUrl').val(),
                data: {
                    StoreCirculationTransportId: transportId,
                    FieldName: 'TransportStatusId',
                    FieldValue: 5,
                    StoreCirculationId: storeCirculationId,
                    CancelReasonId: $('select#cancelReasonId1').val(),
                    CancelComment: $('input#cancelComment1').val().trim(),
                    StoreSourceId: $('select#storeSourceId').val(),
                    StoreDestinationId: $('select#storeDestinationId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Liên hệ với BPVC để có hướng xử lý');
        /*else if(transportStatusId != 5){ //gui thong bao den VC

         }*/
    });
};

app.storeCirculationComment = function(storeCirculationId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            if(storeCirculationId > 0) {
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertStoreCirculationCommentUrl').val(),
                    data: {
                        StoreCirculationId: storeCirculationId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            $('div#listComment').prepend(genItemComment(comment));
                            $('input#comment').val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment').prepend(genItemComment(comment));
                $('input#comment').val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    $('#aShowComment').click(function(){
        $('#modalItemComment').modal('show');
        return false;
    });
};

$(document).ready(function(){
    var storeCirculationId = parseInt($('input#storeCirculationId').val());
    var canEdit = parseInt($('input#canEdit').val());
    var canActive = parseInt($('input#canActive').val());
    var transportId = parseInt($('input#transportId').val());
    app.init(storeCirculationId, canEdit, canActive, transportId);
});