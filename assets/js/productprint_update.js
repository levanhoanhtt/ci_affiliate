var app = app || {};

app.init = function (productPrintId) {
    app.productPrintComment(productPrintId);
    app.product();
    if(productPrintId > 0){
        if ($('#tbodyProduct tr').length > 0) {
            keyUpInput();
            calcQuantity(0);
        }
    }
    app.submit(productPrintId);
};

app.product = function(){
    chooseProduct(function(tr){
        var id = tr.attr('data-id');
        var childId = tr.attr('data-child');
        var flag = false;
        $('#tbodyProduct tr').each(function () {
            if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId) {
                flag = true;
                var quantity = replaceCost($(this).find('input.quantity').val(), true) + 1;
                $(this).find('input.quantity').val(formatDecimal(quantity.toString()));
                calcQuantity(0);
                return false;
            }
        });
        if (!flag) {
            $.ajax({
                type: "POST",
                url: $('input#getProductDetailUrl').val(),
                data: {
                    ProductId: id,
                    ProductChildId: childId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var products = json.data.Products;
                        var productPath = $('input#productPath').val();
                        var html = '';
                        for(var i = 0; i < products.length; i++){
                            html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '">';
                            html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + products[i].ProductName + '</a></td>';
                            html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                            html += '<td class="text-center">' + products[i].BarCode + '</td>';
                            html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                            html += '<td class="tdPrice text-right"><span class="spanPrice">' + formatDecimal(products[i].Price.toString()) + '</span>₫</td>';
                            html += '<td><input class="form-control quantity sll" value="1"></td>';
                            html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td></tr>';
                        }
                        $('#tbodyProduct').append(html);
                        calcQuantity(0);
                        keyUpInput();
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    $('#tbodyProduct').on('click', '.link_delete', function () {
        $(this).parent().parent().remove();
        calcQuantity(0);
        return false;
    });
};

app.productPrintComment = function(productPrintId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            if(productPrintId > 0){
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertProductPrintCommentUrl').val(),
                    data: {
                        ProductPrintId: productPrintId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            $('div#listComment').prepend(genItemComment(comment));
                            $('input#comment').val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment').prepend(genItemComment(comment));
                $('input#comment').val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    if(productPrintId > 0){
        $('#aShowComment').click(function(){
            $('#modalItemComment').modal('show');
            return false;
        });
    }
};

app.submit = function(productPrintId){
    $('#productPrintForm').on('submit', function (e) {
        e.preventDefault();
    });
    $('.submit').click(function () {
        var products = [];
        $('#tbodyProduct tr').each(function () {
            products.push({
                ProductId: parseInt($(this).attr('data-id')),
                ProductChildId: parseInt($(this).attr('data-child')),
                Quantity: replaceCost($(this).find('input.quantity').val(), true)
            });
        });
        if (products.length > 0) {
            $('.submit').prop('disabled', true);
            var comments = [];
            if(productPrintId == 0){
                $('.listComment .pComment').each(function(){
                    comments.push($(this).text());
                });
            }
            $.ajax({
                type: "POST",
                url: $('#productPrintForm').attr('action'),
                data: {
                    ProductPrintId: productPrintId,
                    Products: JSON.stringify(products),
                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        if(productPrintId == 0) redirect(false, $('input#productPrintEditUrl').val() + '/' + json.data);
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn sản phẩm', 0);
        return false;
    });
};

$(document).ready(function () {
	var productPrintId = parseInt($('input#productPrintId').val());
    app.init(productPrintId);
});

function keyUpInput() {
    var totalQuantity = 0;
    $('#tbodyProduct').on('keydown', 'input.quantity', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.quantity', function () {
        var value = $(this).val();
        totalQuantity = 0;
        $('#tbodyProduct tr').each(function () {
            totalQuantity += replaceCost($(this).find('input.quantity').val(), true);
        });
        $('span#totalQuantity').text(formatDecimal(totalQuantity.toString()));
        calcQuantity(totalQuantity);
    });
}

function calcQuantity(totalQuantity) {
    if (totalQuantity == 0) {
        $('#tbodyProduct tr').each(function () {
            totalQuantity += replaceCost($(this).find('input.quantity').val(), true);
        });
        $('span#totalQuantity').text(formatDecimal(totalQuantity.toString()));
    }
}