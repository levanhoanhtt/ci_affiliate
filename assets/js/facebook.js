
var access_token= "EAActZC3KEBCwBAA5CgfxBNkXCUNLbEnsOOsIfZCrUjD08EZCUXDqyiVXKz47TyO2z8u2dR2cD01OZBbRfnNFWn7Qug6sJZCxnJriEOx9tTe4ychmrbhS8WJRIZBbpZChZC9dkrtxYumcsknpD1aEzoYZBtbER05FsJ7mP4NJ3hqhgdQZDZD";
var fbPageCode = $("#select_page_id option:selected").attr('data-page-code');


window.fbAsyncInit = function() {
      FB.init({
        appId      : '157454151622226',
        xfbml      : true,
        version    : 'v2.12'
      });


     

    $("body").on('click','.send-messenger', function(){
        chatMessage();
    }).on('keypress', 'input[name="message"]', function(event){
        if ( event.which == 13 ) {
            chatMessage();
            return false;
        }
    })

    $("body").on('click','.send-commnent', function(){
        var ctm = $("#txt-message").val();
        var fbPostId = $(this).attr('post-id');
        if (ctm == null || ctm == undefined || ctm == '') {
                return false;
        }else{
            FB.api(
                "/" + fbPostId + "/comments",
                "POST",
                {
                    message: ctm,// neu chỉ hình không thì xóa luôn " message"
                    access_token: access_token
                },
                function (response) {
                    if (response && !response.error) {
                        showNotification('Đã trả lời bình luận này',1);
                        $("#txt-message").val("");
                    } else showNotification('Trả lời gửi không thành công.',0);
                    return false;
                }
            );
        }
    })

    // cmt face book 
   
    $("body").on('keypress', 'input[name="comment"]', function(event){
        if ( event.which == 13 ) {
            // console.log(window.location.host);return false;
            event.preventDefault();
            var host = window.location.host;
            var $this = $(this);
            var messageReply = "";
            var message = $this.val().trim();
            var fbPageName = $this.attr('fb-page-name');
            var commentId = $this.attr('comment-id');
            var image = $this.parent().parent().find('ul li div a img').attr('src');
             
            if(image != undefined && message != "") messageReply = {'message': $this.val().trim(),'attachment_url':host+image,access_token: access_token};
            else if(image != undefined && message == "") messageReply = {'attachment_url':host+image,access_token: access_token};
            else messageReply = {'message': message,access_token: access_token};

            FB.api(
                "/" + commentId + "/comments",
                "POST",messageReply,
                function (response) {
                    if (response && !response.error) {
                        showNotification('Đã trả lời bình luận này',1);
                        $this.val("");
                        $this.parent().parent().find('ul li').remove();
                    } else showNotification('Trả lời gửi không thành công.',0);
                    return false;
                }
            );
        }
    })

    // remove comment
    $("body").on("click", ".delete-cmt", function(){
       
        var comment_id = $(this).attr('data-comment-id');
        var fbCommentId = "";
        if($(this).attr('id-fb-comment') != undefined) fbCommentId = $(this).attr('id-fb-comment');
        FB.api("/"+comment_id, 'delete',{access_token: access_token}, function(response) {
            if (response || !response.error) {
                $.ajax({
                    type: 'POST',
                    url: $('input#urlchangeCommentStatus').val().trim(),
                    data:{
                        type: 'delete',
                        comment_id : comment_id,
                        FbPageCode:fbPageCode,
                        FbCommentId: fbCommentId,
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1)$('#div_cmt_id_'+comment_id).remove();
                        showNotification(json.message, json.code);
                    }
                });
                
                
            } else showNotification('Xóa bình luận không thành công',0);
        });
    })

    // hide comment
    $("body").on('click', '.hide-cmt', function(){
        var comment_id = $(this).attr('data-comment-id');
        var idCheck = $(this).attr('id-check');
        var fbCommentId = "";
        if($(this).attr('id-fb-comment') != undefined) fbCommentId = $(this).attr('id-fb-comment');
        FB.api("/"+comment_id, 'POST',{access_token: access_token,is_hidden:true}, function(response) {
            if (response || !response.error){

                $.ajax({
                    type: 'POST',
                    url: $('input#urlchangeCommentStatus').val().trim(),
                    data:{
                        type: 'hide',
                        comment_id : comment_id,
                        FbPageCode:fbPageCode,
                        FbCommentId: fbCommentId,
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            if(idCheck != undefined) $("#div-hide_cmt_id_"+comment_id).html(`<div class="text-center"><span title="1 đã ẩn đi"><i class="fa fa-ellipsis-h"></i></span></div>`);
                            else $('#div_cmt_id_'+comment_id).html(`<div class="text-center"><span title="1 đã ẩn đi"><i class="fa fa-ellipsis-h"></i></span></div>`);
                        }
                        showNotification(json.message, json.code);
                    }
                });
            } 
            else showNotification('Ẩn bình luận không thành công',0);
        });
        
    })

    $("body").on('click','.list-chat', function(){
        $(".body_content_detail").html('');
        $("#check-tags-active").val('2');
        $("#search-inbox").val('all');
        //listChat(fbPageCode,'','',''); 
        loadUserMess();

    });

    
};

// get mess truc tiep tu api fb
function loadUserMess(){
    FB.api(
      fbPageCode + '?fields=conversations{id,snippet,updated_time,senders}',
      'GET',
      {'access_token': access_token},
      function(response) {
        if (response && !response.error) {
            var listConversationHtml = '';
            $.each( response.conversations.data, function( key, value ) {
                var FbId = value.id.substring(2);
                listConversationHtml += 
                   `<a href="javascript:void(0)" onclick="displayConversation(\'${value.id}\',\'${value.senders.data[0].name}\',\'${value.senders.data[0].id}\',\'${value.senders.data[0].email}\','',\'${fbPageCode}\')"><div class="box-comment-fix">
                        <table width="100%" class="direct-chat-name">
                            <tbody>
                            <tr>
                                <td rowspan="3" valign="center"><img class="avatar" src="https://graph.facebook.com/${FbId}/picture?type=square" alt=""></td>
                                <td  valign="top" class="names">${value.senders.data[0].name}</td>
                                <td  valign="top" class="times">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            ${formatTime(value.updated_time)}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" valign="center"><span class="pComment">${trimMess(value.snippet)}</span></td>
                                <td colspan="3">
                                    <i class="fa fa-comment-o"></i>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td colspan="3" class="dark-green'" id="ring_new_mess_${value.senders.data[0].id}" id-sender= "${value.senders.data[0].id}"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div></a>`;
            });
            $("#listChatCustomer").html(listConversationHtml);
            
        }
      }
    );
}


$(window).ready(function(){
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
});

function checkLoginState() {
    /*FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });*/
    // FB.login(function(response) {
    //     statusChangeCallback(response);
    // }, {scope: 'manage_pages'});
}

// function statusChangeCallback(response){
//     if (response.status === 'connected'){
//         FB.api('/me/accounts', function(response1) {
//             console.log(response1); return false;
//            response1 = dataJson;
//             if(response1.data.length > 0){
               
//             }else showNotification('Không có dữ liệu', 0);
//         });
//     }
// }
 var arrCheckActivePost = [];
$(document).ready(function(){
    var socket =  io('https://hoanmuada.cf:8443');
    var body = $("body");
    var pageAccessToken = '';
    var userLogin = "";
    var fbPageId = $("#select_page_id").val();

    body.on('change', '#select_page_id', function(){
        fbPageId = $(this).find(":selected").val();
        fbPageCode = $(this).find(":selected").attr('data-page-code');
        $("li.li_chat").removeClass('active');
        $("li.li_post").addClass('active');
        postComment(fbPageCode,'','');
        $("#listComment").html('');
        $(".title-name").html('');
        $(".tag_info_customer").hide();
        $(".body_content_detail").html('');
    })
    
    var ws;
    $('#listChatCustomer').slimScroll({
        height: ($(window).height() - 230) + 'px',
        alwaysVisible: true,
        wheelStep: 20,
        touchScrollStep: 500
    });

    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '34px',
         'interactive': true,
        'defaultText': '',
        'onAddTag': function (tag) {
            tags.push(tag);
        },
        'onRemoveTag': function (tag) {
            var index = tags.indexOf(tag);
            if (index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });

    body.on('click', '#listChatCustomer a div', function(){
        $('.actives').removeClass('actives');
        $(this).addClass('actives');
    })

    postComment(fbPageCode,'','');

    $("body").on('click','.list-post', function(){
        $(".body_content_detail").html('');
        $("#check-tags-active").val('1');
        postComment(fbPageCode,'','');
    })

    // tạm thời không dùng hiện tai lấy trực tiếp từ fb

    // $("body").on('click','.list-chat', function(){
    //     $(".body_content_detail").html('');
    //     $("#check-tags-active").val('2');
    //     $("#search-inbox").val('all');
    //     //listChat(fbPageCode,'','',''); 

    // });

    

    // bộ lọc, phan loai, post và inbox, chưa đọc, đã đọc và trả lời, dọc chưa trả lời
    body.on('click', '.tags-unread', function(){
        
        $(".body_content_detail").html('');
        if($("#check-tags-active").val().trim() == 1)postComment(fbPageCode,'2','');
        else if($("#check-tags-active").val().trim() == 2){
            $("#search-inbox").val('unread');
            listChat(fbPageCode,'2','','');
        }
    })
    body.on('click', '.tags-answered', function(){
        $(".body_content_detail").html('');
        if($("#check-tags-active").val().trim() == 1)postComment(fbPageCode,'1','1');
        else if($("#check-tags-active").val().trim() == 2){
            listChat(fbPageCode,'1','1','');
            $("#search-inbox").val('answered');
        }
        
    })
    body.on('click', '.tags-not-answered', function(){
        $(".body_content_detail").html('');
        if($("#check-tags-active").val().trim() == 1) postComment(fbPageCode,'1','2');
        else if($("#check-tags-active").val().trim() == 2){
            listChat(fbPageCode,'1','2','');
            $("#search-inbox").val('not-answered');
        }
        
    });

    var statusSearch = null;
    // search input name customer mess
    body.on('keydown','input.search-inbox', function(){
        if (statusSearch != null) {
            clearTimeout(statusSearch);
            statusSearch = null;
        }
    }).keyup(function (e) {
        if (statusSearch == null) {
            statusSearch = setTimeout(function () {
                var keySearch = $("input.search-inbox").val().trim();
                if($("#check-tags-active").val().trim() == 2 && $("#search-inbox").val().trim() == 'all'){
                    listChat(fbPageCode,'','',keySearch);
                }else if($("#check-tags-active").val().trim() == 2 && $("#search-inbox").val().trim() == 'unread'){
                    listChat(fbPageCode,'2','',keySearch);
                }else if($("#check-tags-active").val().trim() == 2 && $("#search-inbox").val().trim() == 'answered'){
                    listChat(fbPageCode,'1','1',keySearch);
                }else if($("#check-tags-active").val().trim() == 2 && $("#search-inbox").val().trim() == 'not-answered'){
                    listChat(fbPageCode,'1','2',keySearch);
                }
            }, 500);
        }
    });

    // show modal lấy mẫu câu co sẵn
    body.on('click', '.sample_sentences', function(){
        var keySearch = "";
        var sentenceGroupId = $("select#sentenceGroupId").val().trim();
        getContentSentences(keySearch,sentenceGroupId);
        $("#modalSampleSentences").modal('show');
    })

    // search-entences
    var statusSearch2 = null;
    body.on('keydown', 'input.search-entences', function(){
        if (statusSearch2 != null) {
            clearTimeout(statusSearch2);
            statusSearch2 = null;
        }
    }).keyup(function (e) {
        if (statusSearch2 == null) {
            statusSearch2 = setTimeout(function () {
                var keySearch = $("input.search-entences").val().trim();
                var sentenceGroupId = $("select#sentenceGroupId").val().trim();
                if(keySearch != ""){
                    getContentSentences(keySearch, sentenceGroupId);
                } 
            }, 500);
        }
    });

    body.on("click", 'select#sentenceGroupId', function(){
        var sentenceGroupId = $(this).val().trim();
        var keySearch = "";
        getContentSentences(keySearch, sentenceGroupId);
    })

    body.on('click', '#tbodyContent td', function(){
        var customerCare = $("select#customer_care").val();
        var customerNeedCare = $("select#customers_need_care").val();
        if(customerCare != "" && customerNeedCare != "" ){
            var mapObj = {
               '{u}': customerCare,
               '{i}': customerNeedCare
            };
            var content = $(this).find('p').text();
            var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
            content = content.replace(re, function(matched){
              return mapObj[matched];
            });
            $("input#txt-message").val(content);
            $("#modalSampleSentences").modal('hide');
        }else{
            showNotification("Vui lòng chọn ngôi thứ xưng hô", 0);
            return false;
        }

        
    })

    body.on('click','.choose-picture', function(){
        var id = $(this).attr('data-id');
        chooseFile('Products', function(fileUrl) {
            $('ul.ulImages_'+id).append('<li><div><a href="' + fileUrl + '" target="_blank"><img style="width:144px!important;" src="' + fileUrl + '"></a></div><i class="fa fa-times remove_img"></i></li>');
        });
   })

    body.on('click', 'i.remove_img', function(){
        $(this).parent().remove();
    });
    
    var arrCheck = [];
    socket.on("new message", function(datas){
        // console.log(datas)
        if(datas.code == '1' && datas.data.length > 0){
            if(datas.data[0].TypeId == 2){
                var cmt = datas.data;
                if(jQuery.inArray(cmt[0].Comment.FbPostId, arrCheckActivePost) == -1) {
                    arrCheckActivePost.push(cmt[0].Comment.FbPostId);
                    $.ajax({
                        type: 'POST',
                        url: $('input#urlSearchPost').val().trim(),
                        data:{
                            FbPostId : cmt[0].Comment.FbPostId,
                            FbPageId : cmt[0].Comment.FbPageId,
                        },
                        success: function (response) {
                            var listConversationHtml = '';
                            $.each(response.data, function( key, value ) {
                                var view = "";
                                if(value.ViewStatusId == 2){
                                    view = `<div class="dark-green"><i class="fa fa-circle"></i></div>`;
                                }else if(value.ViewStatusId == 1 && value.AnswerStatusId == 2){
                                    view = `<div class="grey"><i class="fa fa-circle"></i></div>`;
                                }
                                listConversationHtml += 
                                   `<a data-id="${cmt[0].Comment.FbPostId}" href="javascript:void(0)" onclick="displayDetailCommentPost(\'${value.FbPageCode}\',\'${value.FbUserName}\',\'${value.PostLink}\',\'${value.FbPostId}\',\'${value.FbPageName}\',\'${value.CommentId}\', \'${value.SenderId}\', \'${value.Prefix}\')"><div class="box-comment-fix">
                                        <table width="100%" class="direct-chat-name">
                                            <tbody>
                                            <tr>
                                                <td rowspan="3" valign="center"><img class="avatar" src="https://graph.facebook.com/${value.FbPageCode}/picture?type=square" alt=""></td>
                                                <td  valign="top" class="names">${value.FbPageName}</td>
                                                <td  valign="top" class="times">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            ${formatTime(value.CrDateTime)}
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" valign="center"><span class="pComment">${trimMess(value.PostContent)}</span></td>
                                                <td colspan="3" id="icon_${cmt[0].Comment.FbPostId}">
                                                    <i class="fa fa-comment-o"></i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                   
                                                </td>

                                                <td colspan="3">
                                                    ${view}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div></a>`;
                                $("#fb-page-id").val(value.FbPageId);
                                $("#fb-post-id").val(value.FbPostId);
                                $("#fb-fbPage-code").val(value.FbPageCode);
                            });

                            $("#listChatCustomer").prepend(listConversationHtml);
                        }
                    })
                } 
                var idRingStore = document.getElementById("ring_store_fb");
                $( "#listChatCustomer a" ).each(function() {
                    if($(this).find("div").hasClass('actives') == true && $("#fb-post-id").val().trim() == cmt[0].Comment.FbPostId){
                        var fbFbPageCode = $("#fb-fbPage-code").val().trim();
                        var message = cmt[0].Comment.Message;
                        if(cmt[0].Comment.Message != "" && cmt[0].Comment.Image != "") message += `</br><img style="width:144px!important;" src="${cmt[0].Comment.Image}">`;
                        else if(cmt[0].Comment.Message == "" && cmt[0].Comment.Image != "") message = `<img style="width:144px!important;" src="${cmt[0].Comment.Image}">`;
                        var btnAction = "";
                        if(fbFbPageCode == cmt[0].Comment.SenderId){
                            btnAction = `<a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt delete-cmt" data-comment-id="${cmt[0].Comment.CommentId}"  id-fb-comment="${cmt[0].Comment.FbCommentId}">Xóa</a>&nbsp;&nbsp;&nbsp;
                                        <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(cmt[0].Comment.CrDateTime)).format('DD/MM h:mm: A')} | Bởi ${cmt[0].Comment.SenderName}</span>`;
                        }else{
                            btnAction = ` <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt reply_cmt">Trả lời</a>
                                        <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt hide-cmt" data-comment-id="${cmt[0].Comment.CommentId}">Ẩn</a>
                                        <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt delete-cmt" data-comment-id="${cmt[0].Comment.CommentId}">Xóa</a>
                                        <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt">Nhắn tin</a>&nbsp;&nbsp;&nbsp;
                                        <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(cmt[0].Comment.CrDateTime)).format('DD/MM h:mm: A')}</span>`;
                        }
                        if(cmt[0].Comment.ParentCommentId == '0'){
                            // if(){
                                $('input[name="comment"]').parent().parent().parent().parent().parent().parent().parent().append(`<li comment-id="fbCommentId_${cmt[0].Comment.FbCommentId}" id="div_cmt_id_${cmt[0].Comment.CommentId}">    
                                    <div class="row" id="div-hide_cmt_id_${cmt[0].Comment.CommentId}">
                                        <div class="col-sm-1">
                                            <img class="direct-chat-img" src="https://graph.facebook.com/${cmt[0].Comment.SenderId}/picture?type=square" alt="Message User Image">
                                        </div>
                                        <div class="col-sm-11">
                                            <div class="direct-chat-msg">
                                                <span class="direct-chat-name pull-left">${message}
                                                <div class="clearfix"></div>
                                                ${btnAction}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <ul>
                                    <li id="input_chat_${cmt[0].Comment.FbCommentId}">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <img class="direct-chat-img" src="https://graph.facebook.com/${fbFbPageCode}/picture?type=square" alt="Message User Image">
                                            </div>
                                            <div class="col-sm-11">
                                                <div class="has-feedback inner-addon">
                                                    <input type="text" fb-page-name="" sender-id="${fbFbPageCode}" fb-page-id="${cmt[0].Comment.FbPageId}" fb-post-id="${cmt[0].Comment.FbPostId}" parent-cmt-id="${cmt[0].Comment.FbCommentId}" comment-id="${cmt[0].Comment.CommentId}" class="form-control txt-comment" name="comment"  placeholder="Type Message ..."/>
                                                    <a href="javascript:void(0)" class="send-smile">
                                                        <i class="fa fa-smile-o form-control-feedback-send" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" class="">
                                                        <i class="fa fa-camera form-control-feedback-smile pointer-events" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" class="choose-picture" data-id="${cmt[0].Comment.FbCommentId}">
                                                        <i class="fa fa-camera form-control-feedback-camera pointer-events" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" >
                                                        <i class="fa fa-file form-control-feedback-picture pointer-events" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <ul class="list-inline ulImages_${cmt[0].Comment.FbCommentId}" id="ulImages" ></ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                `);
                            // }
                        }else{
                            $( ".ulDetailCMT li" ).each(function( index ) {
                                var strId = $(this).attr('comment-id');
                                if(strId != undefined){
                                    var commentId = strId.replace(/fbCommentId_/g,'');
                                    if(commentId == cmt[0].Comment.ParentCommentId){
                                       
                                        $('#input_chat_'+commentId).prepend(`
                                            <li id="div_cmt_id_${cmt[0].Comment.CommentId}">    
                                                <div class="row">
                                                    <div class="col-sm-1">
                                                        <img class="direct-chat-img" src="https://graph.facebook.com/${cmt[0].Comment.SenderId}/picture?type=square" alt="Message User Image">
                                                    </div>
                                                    <div class="col-sm-11">
                                                        <div class="direct-chat-msg">
                                                            <span class="direct-chat-name pull-left">${message}</span>
                                                            <div class="clearfix"></div>
                                                            ${btnAction}
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            
                                        `);
                                    }
                                }
                                
                            });
                        }
                    }else{

                        if($(this).attr('data-id') == cmt[0].Comment.FbPostId){
                            $('#icon_'+cmt[0].Comment.FbPostId).html('<div class="dark-green"><i class="fa fa-circle"></i></div>');
                            localStorage.clear();
                            idRingStore.load();
                            idRingStore.play();
                        }
                    }
                });
            }else if(datas.data[0].TypeId == 1){
                var threadId = datas.data[0].Thread.ThreadId;
                FB.api('/'+threadId+'/messages?fields=message,attachments,created_time,to,from,shares{link}',
                    'GET',
                    {
                        'access_token': access_token
                    },
                    function (response) {
                        var htmlMessage = '';
                        var data = response.data[0]; 
                        var timeChat = data.created_time;
                        var idRingStores = document.getElementById("ring_store_fb");
                        if(jQuery.inArray(timeChat, arrCheck) == -1){

                            var img = "";
                            if(data.attachments != undefined) img = `<ul class="list-inline ulImages_" id="ulImages" ><li><img src="${data.attachments.data[0].image_data.url}" /></li></ul>`;
                            if (data.from.id != fbPageCode) {
                                htmlMessage += `
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">${data.from.name}</span>
                                        <span class="direct-chat-timestamp pull-right">${moment(new Date(data.created_time)).format('DD/MM h:mm: A')}</span>
                                    </div>
                                    <img class="direct-chat-img" src="https://graph.facebook.com/${data.from.id}/picture?type=square" alt="Message User Image">
                                    <div class="direct-chat-text">
                                        ${data.message}
                                        ${img}
                                    </div>
                                </div>`; 
                            }else{
                                htmlMessage += `
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">${data.from.name}</span>
                                        <span class="direct-chat-timestamp pull-left">${moment(new Date(data.created_time)).format('DD/MM h:mm: A')}</span>
                                    </div>
                                    <img class="direct-chat-img" src="https://graph.facebook.com/${data.from.id}/picture?type=square" alt="Message User Image">
                                    <div class="direct-chat-text">
                                        ${data.message}
                                        ${img}
                                    </div>
                                </div>`;
                            }
                            $("#listChat").append(htmlMessage);
                            arrCheck.push(timeChat);

                            var checRing = $("#ring_new_mess_"+data.from.id).attr("id-sender");
                            if(data.from.id == checRing){
                                $("#ring_new_mess_"+data.from.id).html('<i class="fa fa-circle"></i>');
                                localStorage.clear();
                                idRingStores.load();
                                idRingStores.play();
                            }
                        }
                    }
                );
            }
        }
        return false;
    });
});

// lẫy nội dung mẫu câu có sẵn ra
function getContentSentences(keySearch, sentenceGroupId){
    var fbPageId = parseInt($("#fb-page-id").val().trim());
    if(fbPageId > 0){
        $.ajax({
            type: "POST",
            url: $("#getContentSentences").val().trim(),
            data: {
                FbPageId : fbPageId,
                keySearch : keySearch,
                SentenceGroupId: sentenceGroupId
            },// now data come in this function
            success: function (response) {
                $("#tbodyContent").html('');
                var json =  $.parseJSON(response);
                if(json.code = 1){
                    
                    var html = "";
                    $.each(json.data, function(key, value){
                        html += `
                            <tr>
                                <td><label>${value.SentenceTitle}</label><p>${value.SentenceContent}</p></td>
                            </tr>
                        `;
                    })
                    $("#tbodyContent").html(html);
                    $("input.search-entences").val('');
                    // listPostComment(response.data)
                }else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }else showNotification('Có lỗi xảy ra, vui lòng thử lại', 0);

}

// post comment
function postComment(fbPageCode,viewStatusId,answerStatusId){
    $.ajax({
        type: "POST",
        url: $("#urlApiFbPost").val().trim(),
        data: {
            FbPageCode : fbPageCode,
            ViewStatusId : viewStatusId,
            AnswerStatusId : answerStatusId,
        },// now data come in this function
        success: function (response) {
            $("#listChatCustomer").html('');
            if(response.code = 1){
                listPostComment(response.data)
            }else showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function listPostComment(data){
    var listConversationHtml = '';
    $.each(data, function( key, value ) {
        arrCheckActivePost.push(value.FbPostId);
        var view = "";
        if(value.ViewStatusId == 2){
            view = `<div class="dark-green"><i class="fa fa-circle"></i></div>`;
        }else if(value.ViewStatusId == 1 && value.AnswerStatusId == 2){
            view = `<div class="grey"><i class="fa fa-circle"></i></div>`;
        }
        listConversationHtml += 
           `<a data-id="${value.FbPostId}" href="javascript:void(0)" onclick="displayDetailCommentPost(\'${value.FbPageCode}\',\'${value.FbUserName}\',\'${value.PostLink}\',\'${value.FbPostId}\',\'${value.FbPageName}\',\'${value.CommentId}\', \'${value.SenderId}\', \'${value.Prefix}\')"><div class="box-comment-fix">
                <table width="100%" class="direct-chat-name">
                    <tbody>
                    <tr>
                        <td rowspan="3" valign="center"><img class="avatar" src="https://graph.facebook.com/${value.FbPageCode}/picture?type=square" alt=""></td>
                        <td  valign="top" class="names">${value.FbPageName}</td>
                        <td  valign="top" class="times">
                            <div class="row">
                                <div class="col-sm-12">
                                    ${formatTime(value.CrDateTime)}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="center"><span class="pComment">${trimMess(value.PostContent)}</span></td>
                        <td colspan="3" id="icon_${value.FbPostId}">
                            <i class="fa fa-comment-o"></i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                           
                        </td>

                        <td colspan="3">
                            ${view}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div></a>`;
        $("#fb-page-id").val(value.FbPageId);
       
        $("#fb-fbPage-code").val(value.FbPageCode);

    });
    $("#listChatCustomer").html(listConversationHtml);
}

// detail comment post
function displayDetailCommentPost(FbPageCode, FbUserName, PostLink, FbPostId, FbPageName, CommentId, SenderId, Prefix){
    $.ajax({
        type: 'POST',
        url: $('input#urlDetailCommentPost').val().trim(),
        data:{
            FbPostId : FbPostId,
            Prefix : Prefix,
        },
        success: function (response) {
            // console.log(response.post[0].PostContent)
            $(".body_content_detail").html('');
            if(response.code = 1){
                $('.title-name').text(FbPageName);
                $("#fb-comment-id").val(CommentId);
                $("#fb-sender-id").val(SenderId);
                $(".send-data").removeClass('send-messenger');
                $(".send-data").addClass('send-commnent');
                var htmlComment = `<div class="direct-comment" id="listComment">
                        <div class="direct-post">
                            <div class="row border-bottom">
                                <div class="col-sm-3">
                                    <img width="120" src="assets/vendor/dist/img/no_img.jpg" alt="Message User Image">
                                </div>
                                <div class="col-sm-9 content-post">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">${response.post[0].PostContent}</span>
                                        <span class="direct-chat-timestamp pull-right"><a href="${response.post[0].PostLink}" target="_bank"><i class="fa fa-external-link"></i></a></span>
                                        <div class="clearfix"></div>
                                        <span class="direct-chat-timestamp pull-left">Đăng bởi [${FbPageName}], ${moment(new Date(response.post[0].CrDateTime)).format('DD/MM h:mm: A')}</span>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="tree-comment-post" id="listReplyCmt">`;
                htmlComment += `<ul class="ulDetailCMT" style="padding-left: 0px!important;">`;

                $.each( response.parentComment, function( k, c ) {
                    if(c.CommentStatusId == 2){
                        var message = c.Message;
                        if(c.Message != "" && c.Image != "") message += `</br><img style="width:144px!important;" src="${c.Image}">`;
                        else if(c.Message == "" && c.Image != "") message = `<img style="width:144px!important;" src="${c.Image}">`;
                        var btnActions = "";
                        if(FbPageCode == c.SenderId){
                            btnActions = `<a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt delete-cmt" data-comment-id="${c.CommentId}" id-fb-comment="${c.FbCommentId}">Xóa</a>&nbsp;&nbsp;&nbsp;
                            <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(c.CrDateTime)).format('DD/MM h:mm: A')} | Bởi ${FbPageName}</span>`;
                        }else{
                            btnActions = ` <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt reply_cmt">Trả lời</a>
                            <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt hide-cmt" data-comment-id="${c.CommentId}" id-check="1">Ẩn</a>
                            <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt delete-cmt" data-comment-id="${c.CommentId}">Xóa</a>
                            <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt">Nhắn tin</a>&nbsp;&nbsp;&nbsp;
                            <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(c.CrDateTime)).format('DD/MM h:mm: A')}</span>`;
                        }
                        htmlComment += `<li comment-id="fbCommentId_${c.FbCommentId}" id="div_cmt_id_${c.CommentId}" > `;
                        htmlComment += `
                                <div class="row" id="div-hide_cmt_id_${c.CommentId}">
                                    <div class="col-sm-1">
                                        <img class="direct-chat-img" src="https://graph.facebook.com/${c.SenderId}/picture?type=square" alt="Message User Image">
                                    </div>
                                    <div class="col-sm-11">
                                        <div class="direct-chat-msg">
                                            <span class="direct-chat-name pull-left">${message}</span>
                                            <div class="clearfix"></div>
                                            ${btnActions}
                                        </div>
                                    </div>
                                </div><ul>
                                `;
                    }else{
                        htmlComment +=`<li comment-id="fbCommentId_${c.FbCommentId}" id="div_cmt_id_${c.CommentId}" >
                            <div class="row text-center">
                                <span title="1 đã ẩn đi"><i class="fa fa-ellipsis-h"></i></span>
                            </div><ul>`;
                    }
                    var parentCommentId = 0;
                    $.each( response.childComment, function( k2, c2 ) {
                        if(c.FbCommentId == c2.ParentCommentId){
                            if(c2.CommentStatusId == 1){
                                // if(c.FbCommentId == c2.ParentCommentId){
                                    htmlComment += `
                                        <li id="div_cmt_id_${c2.CommentId}">
                                            <div class="row text-center" >
                                            <span" title="1 bị ẩn đi"><i class="fa fa-ellipsis-h"></i></span>
                                            </div>
                                        </li>
                                    `;
                                // }
                            }else if(c2.CommentStatusId == 2){
                                var message = c2.Message;
                                if(c2.Message != "" && c2.Image != "") message += `</br><img style="width:144px!important;" src="${c2.Image}">`;
                                else if(c2.Message == "" && c2.Image != "") message = `<img style="width:144px!important;" src="${c2.Image}">`;
                                var btnAction = "";
                                if(FbPageCode == c2.SenderId){
                                    btnAction = `<a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt delete-cmt" data-comment-id="${c2.CommentId}">Xóa</a>&nbsp;&nbsp;&nbsp;
                                                <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(c2.CrDateTime)).format('DD/MM h:mm: A')} | Bởi ${FbPageName}</span>`;
                                }else{
                                    btnAction = ` <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt reply_cmt">Trả lời</a>
                                                <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt hide-cmt" data-comment-id="${c2.CommentId}">Ẩn</a>
                                                <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt delete-cmt" data-comment-id="${c2.CommentId}">Xóa</a>
                                                <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt">Nhắn tin</a>&nbsp;&nbsp;&nbsp;
                                                <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(c2.CrDateTime)).format('DD/MM h:mm: A')}</span>`;
                                }
                                
                                parentComment = c2.ParentCommentId;
                                htmlComment += `<li id="div_cmt_id_${c2.CommentId}">    
                                    <div class="row" >
                                        <div class="col-sm-1">
                                            <img class="direct-chat-img" src="https://graph.facebook.com/${c2.SenderId}/picture?type=square" alt="Message User Image">
                                        </div>
                                        <div class="col-sm-11">
                                            <div class="direct-chat-msg">
                                                <span class="direct-chat-name pull-left">${message}</span>
                                                <div class="clearfix"></div>
                                                ${btnAction}
                                                
                                            </div>
                                        </div>
                                    </div>
                                </li>`;
                            }
                        }
                        
                    })
                    htmlComment += `
                    <li id="input_chat_${c.FbCommentId}">
                        <div class="row">
                            <div class="col-sm-1">
                                <img class="direct-chat-img" src="https://graph.facebook.com/${FbPageCode}/picture?type=square" alt="Message User Image">
                            </div>
                            <div class="col-sm-11">
                                <div class="has-feedback inner-addon">
                                    <input type="text" fb-page-name="${FbPageName}" sender-id="${FbPageCode}" fb-page-id="${c.FbPageId}" fb-post-id="${c.FbPostId}" parent-cmt-id="${c.FbCommentId}" comment-id="${c.CommentId}" class="form-control txt-comment" name="comment"  placeholder="Type Message ..."/>
                                    <a href="javascript:void(0)" class="send-smile">
                                        <i class="fa fa-smile-o form-control-feedback-send" aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="">
                                        <i class="fa fa-camera form-control-feedback-smile pointer-events" aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="choose-picture" data-id="${c.FbCommentId}">
                                        <i class="fa fa-camera form-control-feedback-camera pointer-events" aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void(0)" >
                                        <i class="fa fa-file form-control-feedback-picture pointer-events" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <ul class="list-inline ulImages_${c.FbCommentId}" id="ulImages" ></ul>
                            </div>
                        </div>
                    </li>`;
                    htmlComment +=` </ul></li>`;

                })
                htmlComment += '</ul></div></div></div>';
                $("input#prefix").val(Prefix);
                 $("#fb-post-id").val(FbPostId);

                $(".body_content_detail").html(htmlComment);

                $(".send-commnent").attr({'post-id':response.post[0].PostId});
                

               
            }else showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            listReplyComment();
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

// // replay cmt
// function replyCommnent(senderId, fbCommentId, fbPostId, parentCommentId, commentId, messageReply, fbPageId, $this, fbPageName, htmlImg) {
//     // var 
//     $.ajaxSetup({ cache: true });
//       $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
//         FB.init({
//           appId: '2020899998139436',
//           version: 'v2.10' // or v2.1, v2.2, v2.3, ...
//         });
//         FB.api(
//             "/" + commentId + "/comments",
//             "POST",
//             {
//                 message: messageReply,
//                 access_token: appToken
//             },
//             function (response) {
//                 // console.log(response);
//                 if (response && !response.error) { 
                    
//                     // alertify.success('Đã trả lời bình luận này');
//                 } else {// giả sử dưới local gửi thanh cong tin nhan fb
//                     saveReplyComment(senderId, fbCommentId, fbPostId, parentCommentId, commentId, messageReply, fbPageId, $this, fbPageName,htmlImg);
//                     //gui tin nhan ko thnah cong
//                     // alertify.error('Trả lời gửi không thành công.');

//                 }
//             }
//         );
//     });
// }

// save data replyCommnent
function saveReplyComment(senderId, fbPostId, parentCommentId, commentId, messageReply, fbPageId,$this, fbPageName,htmlImg){
    $.ajax({
        url: $('input#urlSaveReplyCommnent').val().trim(),
        type:'POST',
        data:{
            FbCommentId: 0,
            SenderId: senderId,
            FbPostId: fbPostId,
            ParentCommentId: parentCommentId,
            CommentId : commentId,
            Message: messageReply,
            FbPageId: fbPageId,
            Prefix : $("input#prefix").val().trim(),
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1){
                var htmlimg = "";
                if(htmlImg != undefined) htmlimg = '<br>'+htmlImg;

                // $this.parent().parent().parent().parent().prepend(`<li>    
                //     <div class="row">
                //         <div class="col-sm-1">
                //             <img class="direct-chat-img" src="https://graph.facebook.com/${json.data[0].SenderId}/picture?type=square" alt="Message User Image">
                //         </div>
                //         <div class="col-sm-11">
                //             <div class="direct-chat-msg">
                //                 <span class="direct-chat-name pull-left">${json.data[0].Message}
                //                 <div class="clearfix"></div>
                //                 <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt">Ẩn</a>
                //                     <a href="javascript:void(0)" class="direct-chat-timestamp pull-left action-cmt">Xóa</a>&nbsp;&nbsp;&nbsp;
                //                     <span class="direct-chat-timestamp pull-left times-cmt">${moment(new Date(json.data[0].CreatedDateTime)).format('DD/MM h:mm: A')} | Bởi ${fbPageName}</span>
                //             </div>

                //         </div>
                //     </div>
                // </li>`);
                $this.val("");
            }else showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            

        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function listChat(fbPageCode,viewStatusId,answerStatusId,keySearch){
    // $.ajax({
    //     type:"POST",
    //     url: $('input#urlFbUsers').val().trim(),
    //     data: {
    //         ViewStatusId : viewStatusId,
    //         AnswerStatusId : answerStatusId,
    //         keySearch: keySearch,
    //         FbPageCode: fbPageCode,
    //     },// now data come in this function
    //     success: function (response) {
    //         var json = $.parseJSON(response);
    //         $("#listChatCustomer").html('');
    //         console.log(json)
    //         if(json.code = 1){

    //            // showListChat(json); // khi nao luu data sẽ mở lại giờ tạm thơi lấy dữ liệu trực tiếp từ fb
    //         }else showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
    //     },
    //     error: function (response) {
    //         showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
    //     }
    // });
}

function showListChat(json){
    var listConversationHtml = '';
    $.each( json.datas, function( key, value ) {
        listConversationHtml += 
           `<a href="javascript:void(0)" onclick="displayConversation(\'${value.FbPageCode}\',\'${value.FbUserName}\',\'${value.FbId}\',\'${value.Email}\',\'${value.Address}\',\'${value.Prefix}\')"><div class="box-comment-fix">
                <table width="100%" class="direct-chat-name">
                    <tbody>
                    <tr>
                        <td rowspan="3" valign="center"><img class="avatar" src="https://graph.facebook.com/${value.FbId}/picture?type=square" alt=""></td>
                        <td  valign="top" class="names">${value.FbUserName}</td>
                        <td  valign="top" class="times">
                            <div class="row">
                                <div class="col-sm-12">
                                    ${formatTime(value.CrDateTime)}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="center"><span class="pComment">${value.IsCustomerSend == 1 ? 'Bạn: '+trimMess(value.Message): trimMess(value.Message)}</span></td>
                        <td colspan="3">
                            <i class="fa fa-comment-o"></i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                           
                        </td>
                        <td colspan="3" class="dark-green'">
                            <i class="fa fa-circle"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div></a>`;
    });
    $("#listChatCustomer").html(listConversationHtml);
}

function chatMessage(){
    
    var fbUserId = $('#fb-user-id').val().trim();
    var fbPageId = $("#fb-page-id").val().trim();
    if(fbUserId != '' && fbPageId != ""){
        var message = $('input[name="message"]').val().trim();
        if (message == null || message == undefined || message.trim() == '') {
            return false;
        }else{
            FB.api(
                "/"+fbUserId+"/messages",
                "POST",{
                    message: message,
                    access_token: access_token
                },
                function (response) {
                    if (response && !response.error) {
                        showNotification("Tin nhắm gửi thành công.", 1);
                        $("#txt-message").val('');
                    } else showNotification("Tin nhắm gửi không thành công.", 0);
                    return false;
                }
            );
        }
    }else console.log("Chưa chọn người chát"); return false;
}

function saveMessage(fbUserId, fbPageId, message) {
    
    $.ajax({
        type: 'POST',
        url: $('input#urlSaveMessage').val().trim(),
        data:{
            FbChatId: 0,
            FbUserId : fbUserId,
            FbPageId : fbPageId,
            Message : message,
            StaffId: 1,
            ChatStatusId: 1,
            IsCustomerSend: 1,
            CreatedDate: 0,
            Prefix: $("input#prefix").val().trim(),
        },
        success: function (response) { 
            //clear input text
            $('input[name="message"]').val('');
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        },
    });
}


function displayConversation(idConversation, name, id, email, address, Prefix){
    $("#ring_new_mess_"+id).html('');
    $(".mh-info-customer").html('');
    var detailCustommer = '';
    detailCustommer = `
    <table class="mh-wrap-customer" width="100%">
          <tr>
            <td valign="top" rowspan="3" style="width: 90px;">
                <div class="item">
                    <i class="fa fa-facebook-square"></i> ${name}
                    <img class="info-avatar"  src="https://graph.facebook.com/${id}/picture?type=square">
                </div>
            </td>
            <td valign="top"><a href="#">Thông tin khách hàng <i class="fa fa-pencil"></i></a></td>
            <td align="right"></td>
          </tr>
          <tr>
            <td valign="top">
                <div class="item">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span class="i-name">${name}</span>
                </div>
                <div class="item">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span class="i-phone">Chưa có thông tin</span>
                </div>
                <div class="item">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span class="i-email">${email}</span>
                </div>
                <div class="item i-address">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span class="i-ward">${address}</span>
                </div>
            </td>
            <td align="left" valign="top">
                <div class="item">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <span class="i-total-orders">ĐH: 5</span>
                </div>
                <div class="item">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <span class="i-total-orders">TK: 30.000 ₫</span>
                </div>
            </td>
          </tr>
        </table>`;
    $('.title-name').text(name);
    $(".mh-info-customer").html(detailCustommer);
    $(".tag_info_customer").show();


    // show data chat ở khung giữa
    // bindConversationData(idConversation, id, Prefix);
    // console.log(idConversation)

    FB.api(
        '/'+idConversation + '/messages?fields=message,attachments,created_time,to,from,shares{link}',
        'GET',
        {'access_token': access_token},
        function(response) {
            if (response && !response.error) {
                var htmlMessage = '';
                var fbUserId = "";
                var fbPageId = "";
                 $(".send-data").addClass('send-messenger');
                $(".send-data").removeClass('send-commnent');
                $(".body_content_detail").html('');
                htmlMessage += '<div class="direct-chat-messages" id="listChat">';
                var arrSort = response.data.reverse();
                $.each(arrSort, function( key, value ) {
                    var img = "";
                    var idRemoveNewMess = "";
                    if(value.attachments != undefined) img = `<ul class="list-inline ulImages_" id="ulImages" ><li><img src="${value.attachments.data[0].image_data.url}" /></li></ul>`;
                    
                    if (value.from.id != Prefix) {
                        htmlMessage += `
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">${value.from.name}</span>
                                <span class="direct-chat-timestamp pull-right">${moment(new Date(value.created_time)).format('DD/MM h:mm: A')}</span>
                            </div>
                            <img class="direct-chat-img" src="https://graph.facebook.com/${value.from.id}/picture?type=square" alt="Message User Image">
                            <div class="direct-chat-text">
                                ${value.message}
                                ${img}
                            </div>
                        </div>`;
                        idRemoveNewMess =  value.from.id;
                    }else{
                        htmlMessage += `
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">${value.from.name}</span>
                                <span class="direct-chat-timestamp pull-left">${moment(new Date(value.created_time)).format('DD/MM h:mm: A')}</span>
                            </div>
                            <img class="direct-chat-img" src="https://graph.facebook.com/${value.from.id}/picture?type=square" alt="Message User Image">
                            <div class="direct-chat-text">
                                ${value.message}
                                ${img}
                            </div>
                        </div>`;
                    }
                });
                htmlMessage += '</div>';
                // $('#private-id-message').val(idConversation);
                $("#fb-user-id").val(idConversation);
                $("#fb-page-id").val(Prefix);
                $('#message-type').val('message');
                $(".body_content_detail").html(htmlMessage);
                $("input#prefix").val(Prefix);
                // 
                listChatScroll();
            }
        }
    );
}

function bindConversationData(idConversation, id, Prefix){
    $.ajax({
        type: 'POST',
        url: $('input#urlFbMess').val().trim(),
        data:{
            FbPageCode : idConversation,
            FbId : id,
            Prefix: Prefix,
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1){
                var htmlMessage = '';
                var fbUserId = "";
                var fbPageId = "";
                $(".body_content_detail").html('');
                htmlMessage += '<div class="direct-chat-messages" id="listChat">';
                $.each( json.datas, function( key, value ) {
                    fbUserId = value.FbUserId;
                    fbPageId = value.FbPageId;
                    if (value.IsCustomerSend != 1) {
                        htmlMessage += `
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">${value.FbUserName}</span>
                                <span class="direct-chat-timestamp pull-right">${moment(new Date(value.CrDateTime)).format('DD/MM h:mm: A')}</span>
                            </div>
                            <img class="direct-chat-img" src="https://graph.facebook.com/${value.FbId}/picture?type=square" alt="Message User Image">
                            <div class="direct-chat-text">
                                ${value.Message}
                            </div>
                        </div>`; 
                    }else{
                        htmlMessage += `
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">${value.FbPageName}</span>
                                <span class="direct-chat-timestamp pull-left">${moment(new Date(value.CrDateTime)).format('DD/MM h:mm: A')}</span>
                            </div>
                            <img class="direct-chat-img" src="https://graph.facebook.com/${value.FbPageCode}/picture?type=square" alt="Message User Image">
                            <div class="direct-chat-text">
                                ${value.Message}
                            </div>
                        </div>`;
                    }
                });
                htmlMessage += '</div>';
                // $('#private-id-message').val(idConversation);
                $("#fb-user-id").val(fbUserId);
                $("#fb-page-id").val(fbPageId);
                $('#message-type').val('message');
                $(".body_content_detail").html(htmlMessage);
                $("input#prefix").val(Prefix);
                listChatScroll();
            // console.log(json)
            }else showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
   
}

function formatTime(time){
    return moment(new Date(time)).format('DD/MM/YY h:mm');
}



function trimMess (trimLenght) {
    return trimLenght.length > 15 ? trimLenght.substring(20, trimLenght - 3) + '...' : trimLenght;
};

function listChatScroll(){
    $('#listChat').slimScroll({
        height: ($(window).height() - 120) + 'px',
        alwaysVisible: true,
        wheelStep: 20,
        touchScrollStep: 500
    });
}

function listReplyComment(){
    $("#listReplyCmt").slimScroll({
        height: ($(window).height() - 220) + 'px',
        alwaysVisible: true,
        wheelStep: 20,
        touchScrollStep: 500
    });
}






