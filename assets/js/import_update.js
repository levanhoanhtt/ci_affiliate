var app = app || {};

app.init = function(importId, canEdit, canActive){
    app.product(canEdit, canActive);
    if(canEdit == 1){
        if(canActive == 1) app.configExpand();
        if(importId > 0) app.cancel(importId);
        //app.supplier();
    }
    app.submit(importId, canEdit, canActive);
};

app.product = function(canEdit, canActive){
    if(canActive == 1) {
        if ($('#tbodyProduct tr').length > 0) calcPrice(0, 0);
        $('#tbodyProduct').on('keydown', 'input.cost', function (e) {
            if (checkKeyCodeNumber(e)) e.preventDefault();
        }).on('keyup', 'input.cost', function () {
            var value = $(this).val();
            if ($(this).hasClass('percent') && replaceCost(value, true) > 100) $(this).val('0');
            else $(this).val(formatDecimal(value));
            var tr = $(this).parent().parent();
            var sumCost = replaceCost(tr.find('input.quantity').val()) * replaceCost(tr.find('input.price').val());
            tr.find('input.sumCost').val(sumCost);
            var sumTax = sumCost * replaceCost(tr.find('input.percent').val()) / 100;
            sumTax = Math.ceil(sumTax);
            tr.find('input.sumTax').val(sumTax);
            sumCost += sumTax;
            tr.find('input.sumPrice').val(formatDecimal(sumCost.toString()));
            calcPrice(0, 0);
        }).on('click', '.link_delete', function () {
            if (canEdit == 1) {
                var tr = $(this).parent().parent();
                tr.remove();
                calcPrice(0, 0);
            }
            return false;
        });
        $('#importForm').on('keydown', 'input.cost1', function (e) {
            if (checkKeyCodeNumber(e)) e.preventDefault();
        }).on('keyup', 'input#transportCost', function () {
            var value = $(this).val();
            $(this).val(formatDecimal(value));
            calcPrice(replaceCost($('span#totalPrice').text(), true), replaceCost($('span#totalTax').text(), true));
        }).on('keyup', 'input#discountCost', function () {
            var value = $(this).val();
            $(this).val(formatDecimal(value));
            calcPrice(replaceCost($('span#totalPrice').text(), true), replaceCost($('span#totalTax').text(), true));
        }).on('keyup', 'input#paymentCost', function () {
            var value = $(this).val();
            $(this).val(formatDecimal(value));
            calcPrice(replaceCost($('span#totalPrice').text(), true), replaceCost($('span#totalTax').text(), true));
        });
    }
    if(canEdit == 1){
        chooseProduct(function (tr) {
            var id = tr.attr('data-id');
            var childId = tr.attr('data-child');
            var flag = false;
            $('#tbodyProduct tr').each(function (){
                if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId) {
                    flag = true;
                    var tr = $(this);
                    var quantity = replaceCost(tr.find('input.quantity').val(), true) + 1;
                    tr.find('input.quantity').val(formatDecimal(quantity.toString()));
                    if(canActive == 1) {
                        var sumCost = quantity * replaceCost(tr.find('input.price').val());
                        tr.find('input.sumCost').val(sumCost);
                        var sumTax = sumCost * replaceCost(tr.find('input.percent').val()) / 100;
                        sumTax = Math.ceil(sumTax);
                        tr.find('input.sumTax').val(sumTax);
                        sumCost += sumTax;
                        tr.find('input.sumPrice').val(formatDecimal(sumCost.toString()));
                        calcPrice(0, 0);
                    }
                    return false;
                }
            });
            if (!flag) {
                $.ajax({
                    type: "POST",
                    url: $('input#getProductDetailUrl').val(),
                    data: {
                        ProductId: id,
                        ProductChildId: childId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var products = json.data.Products;
                            var productPath = $('input#productPath').val();
                            var html = '';
                            for (var i = 0; i < products.length; i++) {
                                html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '">';
                                html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark" target="_blank">' + products[i].ProductName + '</a></td>';
                                html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                                html += '<td class="text-center">' + products[i].BarCode + '</td>';
                                html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                                html += '<td><input class="form-control quantity cost" value="1"></td>';
                                if(canActive == 1) {
                                    html += '<td><input class="form-control price cost" value="0"></td>';
                                    html += '<td><input class="form-control percent cost" value="0"></td>';
                                    html += '<td><input class="form-control sumPrice text-right" disabled value="0"><input type="text" hidden="hidden" class="sumCost" value="0"><input type="text" hidden="hidden" class="sumTax" value="0"></td>';
                                }
                                html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td></tr>';
                            }
                            $('#tbodyProduct').append(html);
                            if(canActive == 1) calcPrice(0, 0);
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        });
    }
};

app.configExpand = function(){
    $('input#otherCost_' + $('input#debitOtherTypeId').val()).parent().parent().parent().remove();
    $('#importForm').on('click', '.aExpand', function(){
        $('#modalConfigExpand').modal('show');
        return false;
    });
    $('.btnApplyConfigExpand').click(function(){
        var id = $(this).attr('data-id');
        var cost = replaceCost($('input#otherCost_' + id).val().trim(), true);
        if(cost <= 0){
            showNotification('Số tiền phải lớn hơn 0', 0);
            return false;
        }
        var flag = true;
        $('#divService div.item').each(function(){
            if($(this).attr('data-id') == id){
                flag = false;
                return false;
            }
        });
        if(flag){
            var html = '<div class="item" data-id="' + id + '">';
            html += '<div class="col-md-6" style="clear: both">';
            html += '<a href="javascript:void(0)" class="aExpand">' + $('#pOtherServiceName_' + id).text() + '</a></div>';
            html += '<div class="col-md-6 text-right"><span id="spanOtherCost_' + id + '" class="spanOtherCost">' + formatDecimal(cost.toString()) + '</span> đ</div>';
            html += '</div>';
            $('#divService').append(html);
        }
        else $('span#spanOtherCost_' + id).text(formatDecimal(cost.toString()));
        $('#modalConfigExpand').modal('hide');
        calcPrice(replaceCost($('span#totalPrice').text(), true), replaceCost($('span#totalTax').text(), true));
    });
};

/*app.supplier = function(){
    $("#modal-supplier").on('click', function(){
        $("#modalSupplier").modal('show');
        return false;
    });

    $(".save-supplier").click(function(){
        if(validateEmpty('#supplierForm')){
            var contacts = [];
            var positionName = '';
            var contactName = '';
            var contactPhone = '';
            var flag = true;
            $('#tbodyContact tr').each(function(){
                positionName = $(this).find('input.positionName').val().trim();
                contactName = $(this).find('input.contactName').val().trim();
                contactPhone = $(this).find('input.contactPhone').val().trim();
                if(contactName != '' && contactPhone != '' && positionName != ''){
                    contacts.push({
                        PositionName: positionName,
                        ContactName: contactName,
                        ContactPhone: contactPhone
                    });
                }
                else{
                    flag = false;
                    return false;
                }
            });
            if(flag){
                $('.submit').prop('disabled', true);
                var sType = $('input#sType').val();
                var taxCode = '';
                var hasBill = '';
                if (sType == 1) {
                    taxCode = $('input#taxCode').val().trim();
                    hasBill = $('input#hasBill').val();
                }
                $.ajax({
                    type: "POST",
                    url: $('#supplierForm').attr('action'),
                    data: {
                        SupplierId: $('input#supplierId').val(),
                        SupplierCode: $('input#supplierCode').val().trim(),
                        SupplierName: $('input#supplierName').val().trim(),
                        SupplierTypeId: sType,
                        ItemStatusId: $('select#itemStatusId').val(),
                        TaxCode: taxCode,
                        HasBill: hasBill,
                        Comment: $('input#comment').val().trim(),
                        Contacts: JSON.stringify(contacts)
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1){
                            console.log(json.data)
                            $('#supplierId option:selected').removeAttr('selected');
                            $('#supplierId').append($("<option selected='selected'></option>").attr("value",json.data).text($('input#supplierName').val().trim())); 
                            $("#modalSupplier").modal('hide');
                            $('#supplierForm').closest('form').find("input[type=text], textarea").val("");
                            // if($('input#supplierId').val() == '0')
                             // redirect(false, $('a#supplierListUrl').attr('href'));
                            // else $('.submit').prop('disabled', false);
                        }
                        // else $('.submit').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('.submit').prop('disabled', false);
                    }
                });
            }
            else showNotification('Điền đầy đủ Chức vụ, Tên và SĐT người liên hệ', 0);
        }
        return false;
    })
};*/

app.submit = function(importId, canEdit, canActive){
    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '54px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('.ntags').click(function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
        return false;
    });
    if(importId > 0) {
        $('input.tagName').each(function () {
            inputTag.addTag($(this).val());
        });
    }
    if(canEdit == 1) {
        $('.submit').click(function () {
            if (validateNumber('#importForm', true, ' không được bỏ trống')) {
                var products = [];
                var quantity = 0;
                var price = 0;
                var totalQuantity = 0;
                $('#tbodyProduct tr').each(function () {
                    quantity = replaceCost($(this).find('input.quantity').val(), true);
                    if(canActive == 1){
                        price = replaceCost($(this).find('input.price').val(), true);
                        if(quantity > 0 && price > 0){
                            products.push({
                                ProductId: parseInt($(this).attr('data-id')),
                                ProductChildId: parseInt($(this).attr('data-child')),
                                Quantity: quantity,
                                Price: price,
                                VAT: replaceCost($(this).find('input.percent').val(), true),
                                AveragePrice: price
                            });
                            totalQuantity += quantity;
                        }
                        else {
                            totalQuantity = 0;
                            return false;
                        }
                    }
                    else{
                        if(quantity > 0){
                            products.push({
                                ProductId: parseInt($(this).attr('data-id')),
                                ProductChildId: parseInt($(this).attr('data-child')),
                                Quantity: quantity,
                                Price: 0,
                                VAT: 0,
                                AveragePrice: 0
                            });
                            totalQuantity += quantity;
                        }
                        else {
                            totalQuantity = 0;
                            return false;
                        }
                    }
                });
                if (totalQuantity == 0) {
                    showNotification(canActive == 1 ? 'Số lượng và đơn giá không được nhỏ hơn 0' : 'Số lượng không được nhỏ hơn 0', 0);
                    return false;
                }
                var totalProduct = products.length;
                if (totalProduct > 0) {
                    $('.submit').prop('disabled', true);
                    var importServices = [];
                    var otherServiceId = 0;
                    var serviceCost = 0;
                    var totalServiceCost = 0;
                    var transportCost = 0;
                    var discountCost = 0;
                    if(canActive == 1) {
                        $('#divService div.item').each(function () {
                            otherServiceId = $(this).attr('data-id');
                            serviceCost = replaceCost($(this).find('.spanOtherCost').text(), true);
                            if (otherServiceId != '0' && serviceCost > 0) {
                                importServices.push({
                                    OtherServiceId: otherServiceId,
                                    ServiceCost: serviceCost
                                });
                                totalServiceCost += serviceCost;
                            }
                        });
                        transportCost = replaceCost($('input#transportCost').val(), true);
                        discountCost = replaceCost($('input#discountCost').val(), true);
                        var transportCostProduct = Math.ceil(transportCost / totalQuantity);
                        var discountCostProduct = Math.ceil(discountCost / totalQuantity);
                        totalServiceCost = Math.ceil(totalServiceCost / totalQuantity);
                        for (var i = 0; i < totalProduct; i++) products[i].AveragePrice = products[i].Price + Math.ceil(products[i].Price * products[i].VAT / 100) + transportCostProduct - discountCostProduct + totalServiceCost;
                    }
                    $.ajax({
                        type: "POST",
                        url: $('#importForm').attr('action'),
                        data: {
                            ImportId: importId,
                            ImportStatusId: $('select#importStatusId').val(),
                            SupplierId: $('select#supplierId').val(),
                            DeliverName: $('input#deliverName').val().trim(),
                            DeliverPhone: $('input#deliverPhone').val().trim(),
                            StoreId: $('select#storeId').val(),

                            TransportCost: transportCost,
                            DiscountCost: discountCost,
                            PaymentCost: replaceCost($('input#paymentCost').val(), true),

                            Comment: $('#comment').val().trim(),
                            FileExcel: '',
                            ScanBarCodeId: 0,

                            TagNames: JSON.stringify(tags),
                            Products: JSON.stringify(products),
                            ImportServices: JSON.stringify(importServices)
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if (json.code == 1) {
                                if (importId == 0) redirect(false, $('#aImportList').attr('href'));
                                else redirect(true, '');
                            }
                            else $('.submit').prop('disabled', false);
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                            $('.submit').prop('disabled', false);
                        }
                    });

                }
                else showNotification('Vui lòng chọn sản phẩm', 0);
            }
            return false;
        });
    }
};

app.cancel = function(importId){
    $('.btnCancel').click(function(){
        if(confirm('Bạn thật sự muốn hủy phiếu?')){
            $('.btnCancel').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    ImportId: importId,
                    ImportStatusId: 3
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('.submit, .btnCancel').remove();
                        $('select#importStatusId').val(3);
                    }
                    else $('.btnCancel').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.btnCancel').prop('disabled', false);
                }
            });
        }
    });
};

$(document).ready(function () {
    var importId = parseInt($('input#importId').val());
    var canEdit = parseInt($('input#canEdit').val());
    var canActive = parseInt($('input#canActive').val());
    app.init(importId, canEdit, canActive);
});

function calcPrice(totalPrice, totalTax){
    if(totalPrice == 0 && totalTax == 0) {
        $('#tbodyProduct tr').each(function () {
            totalPrice += replaceCost($(this).find('input.sumCost').val(), true);
            totalTax += replaceCost($(this).find('input.sumTax').val(), true);
        });
        $('span#totalPrice').text(formatDecimal(totalPrice.toString()));
        $('span#totalTax').text(formatDecimal(totalTax.toString()));
    }
    var orderCost = totalPrice + totalTax + replaceCost($('input#transportCost').val(), true);
    $('#divService div.item').each(function(){
        orderCost += replaceCost($(this).find('.spanOtherCost').text(), true);
    });
    $('span#orderCost').text(formatDecimal(orderCost.toString()));
    var totalCost = orderCost - replaceCost($('input#discountCost').val(), true);
    $('span#totalCost').text(formatDecimal(totalCost.toString()));
    totalCost -= replaceCost($('input#paymentCost').val(), true);
    $('span#ownCost').text(formatDecimal(totalCost.toString()));
}