$(document).ready(function () {
    $('#aAddLink').click(function () {
        $('#modalAddLink input.form-control').val('');
        $('#modalAddLink select.form-control').val(0);
        $('#divLinkPreview, #btnOtherLink').hide();
        $('#modalAddLink').modal('show');
    });
    $('#btnCreateLink').click(function() {
        var originalLink = $('input#originalLink').val().trim();
        if(originalLink != ''){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateLinkAff').val(),
                data: {
                    LinkAffId: 0,
                    OriginalLink: originalLink,
                    LinkGroupId: $('select#linkGroupId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('input#shortLink').val(json.data.ShortLink);
                        $('input#oldLink').val(originalLink);
                        $('#divLinkPreview').show();
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Link sản phẩm không được bỏ trống', 0);
    });
    /*$('.modal-create-link-new').click(function() {
        $('.created-link').hide();
        $('.modal-create-link').show();
        $('.modal-create-link-new').hide();
        $('#nameId').val("");
        $('#note').val("");
        $('#approachId').val(0);
        $('.bs-deselect-all').click();
        $('#url').val("");
        $.ajax({
            type: "POST",
            url: $('#urlGetListGroup').val(),
            data: {},
            success: function (response) {
                var json = $.parseJSON(response);
                var html = '<label class="control-label">Chiến dịch </label>';
                html += '<select class="form-control selectpicker" data-actions-box="true" ';
                html += 'data-deselect-all-text="Bỏ chọn" data-none-selected-text="--Chọn chiến dịch--" data-select-all-text="Tất cả"';
                html += 'name="LinkGroupId[]" id="LinkGroupId" multiple>';
                $.each( json.listGroups, function( key, value ) {
                    html += '<option value="'+key+'">'+value+'</option>';
                });
                html += '</select>';
                $('.select-box-group').html(html);
                $('.selectpicker').selectpicker();
            },
            error: function (response) {
            }
        });
    });*/
});

function copyClipboard(id) {
    var copyText = document.getElementById(id);
    copyText.select();
    document.execCommand("Copy");
}