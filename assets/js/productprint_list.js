$(document).ready(function () {
    actionItemAndSearch({
        ItemName: 'Phiếu in barcode',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentProductPrints(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditProductPrint = $('#urlEditProductPrint').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].ProductPrintId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].ProductPrintId + '"></td>';
            html += '<td><a href="' + urlEditProductPrint + data[item].ProductPrintId + '">' + data[item].ProductPrintCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td class="text-center">' + formatDecimal(data[item].Quantity) + '</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="4" class="paginate_table"></td></tr>';
        $('#tbodyProductPrint').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}