var app = app || {};
app.init = function(storeCirculationTransportId) {
    app.initLibrary();
    app.transportComment(storeCirculationTransportId);
    app.updateTracking(storeCirculationTransportId);
    app.updateShipCost(storeCirculationTransportId);
    app.updateTransporter(storeCirculationTransportId);
    app.updateTransportStatus(storeCirculationTransportId);
    app.updatePendingStatus(storeCirculationTransportId);
    app.updateTag(storeCirculationTransportId);
    app.remind();
};

app.initLibrary = function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('body').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.cost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    var minNumber = 4;
    var maxNumber = 4;
    if ($(window).width() < 1300) {
        minNumber = 3;
        maxNumber = 3;
    }
    $('.bxslider').bxSlider({
        minSlides: minNumber,
        maxSlides: maxNumber,
        moveSlides: 1,
        slideWidth: 500,
        pager: false,
        infiniteLoop: false,
        nextText: ">",
        prevText: "<"
    });
};

app.updateTracking = function(storeCirculationTransportId){
    $('#aTracking').click(function(){
        var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var tracking = $(this).text();
            if (tracking == 'Cập nhật') tracking = '';
            $('input#tracking').val(tracking);
            $('#modalTracking').modal('show');
        }
        return false;
    });
    $('#btnUpdateTracking').click(function(){
        var tracking = $('input#tracking').val().trim();
        if(tracking != ''){
            var tracking1 = $('#aTracking').text();
            if(tracking1 == 'Cập nhật') tracking1 = '';
            if(tracking != tracking1) {
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(storeCirculationTransportId, 'Tracking', tracking, function(data){
                    $('#aTracking').text(data.Tracking);
                    $('#modalTracking').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn mã vận đơn khác', 0);
        }
        else{
            showNotification('Mã vận đơn không được bỏ trống', 0);
            $('input#tracking').focus();
        }
    });
};

app.updateShipCost = function(storeCirculationTransportId){
    $('#aShipCost').click(function(){
        var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var shipCost = $(this).find('span').text();
            if (shipCost == 'Cập nhật') shipCost = '0';
            $('input#shipCost').val(shipCost);
            $('#modalShipCost').modal('show');
        }
        return false;
    });
    $('#btnUpdateShipCost').click(function(){
        var shipCost = replaceCost($('input#shipCost').val(), true);
        if(shipCost > 0){
            var shipCost1 = $('#aShipCost').find('span').text();
            if(shipCost1 == 'Cập nhật') shipCost1 = '0';
            if(shipCost != replaceCost(shipCost1, true)){
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(storeCirculationTransportId, 'ShipCost', shipCost, function (data) {
                    $('.spanShipCost').text(formatDecimal(shipCost.toString()));
                    $('#modalShipCost').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn phí ship khác', 0);
        }
        else{
            showNotification('Phí ship thực tế phải lớn hơn 0', 0);
            $('input#shipCost').focus();
        }
    });
};

app.updateTransporter = function(storeCirculationTransportId){
    $('#aTransporter').click(function(){
        var transportStatusId = parseInt($('input#transportStatusIdOriginal').val());
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) $('#modalTransporter').modal('show');
        return false;
    });
    $('#btnUpdateTransporter').click(function(){
        var transporterId = $('select#transporterId').val();
        if(transporterId != $('input#transporterIdOriginal').val()) {
            var btn = $(this);
            btn.prop('disabled', true);
            updateTransportField(storeCirculationTransportId, 'TransporterId', transporterId, function (data) {
                $('#aTransporter').html(data.TransporterName);
                $('input#transporterIdOriginal').val(transporterId);
                $('#modalTransporter').modal('hide');
                btn.prop('disabled', false);
            }, function(){
                btn.prop('disabled', false);
            });
        }
        else showNotification('Vui lòng chọn nhà vận chuyển khác', 0);
    });
};

app.remind = function(){
    $('.aRemind').click(function(){
        $('#modalRemind').modal('show');
        return false;
    });
    $('select#userId').change(function(){
        if($(this).val() != '0') $('select#partId').val('0').trigger('change');
    });
    $('select#partId').change(function(){
        if($(this).val() != '0') $('select#userId').val('0').trigger('change');
    });
    $('#btnAddRemind').click(function(){
        if(validateEmpty('#modalRemind')){
            var btn = $(this);
            btn.prop('disabled', true);
            var comments = [];
            var remindComment = $('input#remindComment').val().trim();
            if(remindComment != '') comments.push(remindComment);
            $.ajax({
                type: "POST",
                url: $('input#insertRemindUrl').val(),
                data: {
                    RemindId: 0,
                    RemindTitle: $('input#remindTitle').val().trim(),
                    RemindDate: $('input#remindDate1').val().trim(),
                    IsRepeat: 1,
                    RepeatDay: 0,
                    RepeatHour: 0,
                    RemindStatusId: 1,
                    UserId: $('select#userId').val(),
                    PartId: $('select#partId').val(),

                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        $('#modalRemind input').val('');
                        $('select#userId').val($('input#userLoginId').val());
                        $('select#partId').val('0');
                        $('#modalRemind').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

app.updateTag = function(storeCirculationTransportId){
    var tags = [];
    var inputTag = $('input#tags');
    inputTag.tagsInput({
        'width': '100%',
        'height': '50px',
        'interactive': true,
        'defaultText': '',
        'onAddTag': function(tag){
            tags.push(tag);
        },
        'onRemoveTag': function(tag){
            var index = tags.indexOf(tag);
            if(index >= 0) tags.splice(index, 1);
        },
        'delimiter': [',', ';'],
        'removeWithBackspace': true,
        'minChars': 0,
        'maxChars': 0
    });
    $('.ntags').click(function(){
        var tag = $(this).text();
        if(!inputTag.tagExist(tag)) inputTag.addTag(tag);
        return false;
    });
    $('input.tagName').each(function () {
        inputTag.addTag($(this).val());
    });
    $('#btnUpdateTag').click(function(){
        if(tags.length > 0){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateItemTagUrl').val(),
                data: {
                    ItemIds: JSON.stringify([storeCirculationTransportId]),
                    TagNames: JSON.stringify(tags),
                    ItemTypeId: 25,
                    ChangeTagTypeId: 3
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn nhãn', 0);
    });
};

app.transportComment = function(storeCirculationTransportId){
    $('#btnInsertComment').click(function(){
        var comment = $('input#comment').val().trim();
        if(comment != ''){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#insertTransportCommentUrl').val(),
                data: {
                    StoreCirculationTransportId: storeCirculationTransportId,
                    Comment: comment
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1) {
                        $('div#listComment').prepend(genItemComment(comment));
                        $('input#comment').val('');
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment').focus();
        }
    });
    $('#aShowComment').click(function(){
        $('#modalItemComment').modal('show');
        return false;
    });
};

app.updatePendingStatus = function(storeCirculationTransportId){
    $('#aUpdatePending').click(function(){
        if($('input#transportStatusIdOriginal').val() == '1') $('#modalUpdatePending').modal('show');
        return false;
    });
    $('#btnUpdatePending').click(function(){
        var pendingStatusId = parseInt($('input[name="PendingStatusId"]:checked').val());
        if(pendingStatusId > 0 && pendingStatusId != parseInt($('input#pendingStatusIdOriginal').val())){
            var btn = $(this);
            btn.prop('disabled', true);
            updateTransportField(storeCirculationTransportId, 'PendingStatusId', pendingStatusId, function(data){
                $('input#pendingStatusIdOriginal').val(pendingStatusId);
                $('#tdTransportStatus').html(data.StatusName);
                $('#modalUpdatePending').modal('hide');
                btn.prop('disabled', false);
            }, function(){
                btn.prop('disabled', false);
            });
        }
        else showNotification('Vui lòng chọn trạng thái Chờ xử lý khác', 0);
    });
};

app.updateTransportStatus = function(storeCirculationTransportId){
    $('select#transportStatusId').change(function(){
        if($(this).val() == '5') $('#divCancelReason').fadeIn();
        else{
            $('#divCancelReason').fadeOut();
            $('select#cancelReasonId').val('0');
            $('input#cancelComment').val('');
        }
    });
    $('#aTransportStatus').click(function(){
        $('#modalTransportStatus').modal('show');
        return false;
    });
    $('#btnUpdateTransportStatus').click(function(){
        var transportStatusId = $('select#transportStatusId').val();
        if(transportStatusId != $('input#transportStatusIdOriginal').val()) {
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateFieldUrl').val(),
                data: {
                    StoreCirculationTransportId: storeCirculationTransportId,
                    FieldName: 'TransportStatusId',
                    FieldValue: transportStatusId,
                    StoreCirculationId: $('input#storeCirculationId').val(),
                    CancelReasonId: $('select#cancelReasonId').val(),
                    CancelComment: $('input#cancelComment').val().trim(),
                    StoreSourceId: $('input#storeSourceId').val(),
                    StoreDestinationId: $('input#storeDestinationId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                        $('input#transportStatusIdOriginal').val(transportStatusId);
                        $('#tdTransportStatus').html(json.data.StatusName);
                        var transportStatusIconPath = $('input#transportStatusIconPath').val();
                        var li;
                        for(var i = 1; i < 10; i++){
                            li = $('#liTransportStatus_' + i);
                            if(li.hasClass('active')){
                                li.removeClass('active');
                                li.find('img').attr('src', transportStatusIconPath + i + '.1.png');
                                break;
                            }
                        }
                        li = $('#liTransportStatus_' + transportStatusId);
                        li.addClass('active');
                        li.find('img').attr('src', transportStatusIconPath + transportStatusId + '.2.png');
                        if(transportStatusId == 5 || transportStatusId == 7) $('#aTransportStatus').parent('p').remove();
                        $('#modalTransportStatus').modal('hide');
                        $('#divCancelReason').fadeOut();
                        $('select#cancelReasonId').val('0');
                        $('input#cancelComment').val('');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái khác', 0);
    });
};

$(document).ready(function(){
    var storeCirculationTransportId = parseInt($('input#storeCirculationTransportId').val());
    app.init(storeCirculationTransportId);
});

function updateTransportField(storeCirculationTransportId, fieldName, fieldValue, fnSuccess, fnError){
    $.ajax({
        type: "POST",
        url: $('input#updateFieldUrl').val(),
        data: {
            StoreCirculationTransportId: storeCirculationTransportId,
            FieldName: fieldName,
            FieldValue: fieldValue
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                fnSuccess(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            fnError();
        }
    });
}