$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'CTV',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
            if(actionCode == 'create_group'){
                $('#cbCustomerGroup1').prop('checked', true);
                $("input#customerIds").val(JSON.stringify(itemIds));
                $("input#customerGroupName").hide();
                $("#divSelectGroup").show();
                $('#customerGroupId').val('0').trigger("change");
                $("#customerGroupName, input#comment").val('');
                $('#ulCondition li.condition').remove();
                var html = '';
                $('ul#container-filters li.item .btn-field').each(function(){
                    html += '<li class="condition">' + $(this).text() + '</li>';
                });
                $('#ulCondition').append(html);
                $("#modalCustomerGroup").modal('show');
            }
        }
    });
    $('input.history').click(function(){
        $('#btn-filter').attr('data-href', $('input#searchProductUrl').val() + $(this).val());
        actionItemAndSearch({
            ItemName: 'CTV',
            IsRenderFirst: true,
            extendFunction: function(itemIds, actionCode){}
        });
        $("input#customerKindId").val($(this).val());
        if($(this).val() == 1) $("#spanCustomerKindName").text('Khách lẻ');
        else if($(this).val() == 2) $("#spanCustomerKindName").text("Khách buôn");
        else $("#spanCustomerKindName").text("CTV");
    });
    $('input.cbCustomerGroup').click(function(){
        if($(this).val() == 'old'){
            $("input#customerGroupName").hide();
            $("#divSelectGroup").show();
            $('#customerGroupId').val('0').trigger("change");
        }
        else{
            $("input#customerGroupName").show();
            $("#divSelectGroup").hide();
            $("#customerGroupName").val('');
        }
    });
    $('#btnUpdateCustomerGroup').click(function(){
        var customerGroupId = $("#customerGroupId").val();
        var customerGroupName = $("#customerGroupName").val().trim();
        if($('input[name="cbCustomerGroup"]:checked').val() == 'old'){
            if(customerGroupId == '0'){
                showNotification("Vui lòng chọn nhóm CTV", 0);
                return false;
            }
            else customerGroupName = $('#customerGroupId option[value="' + customerGroupId + '"]').text();
        }
        else{
            if(customerGroupName == ''){
                showNotification("Vui lòng điền nhóm CTV", 0);
                return false;
            }
        }
        var conditions = [];
        $('#ulCondition li').each(function(){
            conditions.push($(this).text().trim());
        });
        $.ajax({
            type: "POST",
            url: $('input#updateCustomerGroupUrl').val(),
            data: {
                CustomerGroupId : customerGroupId,
                CustomerGroupName : customerGroupName,
                CustomerKindId : $("input#customerKindId").val().trim(),
                //Comment : $("input#comment").val().trim(),
                Conditions: JSON.stringify(conditions),
                CustomerIds: $("input#customerIds").val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) $("#modalCustomerGroup").modal('hide');
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});

function renderContentCustomers(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var totalOrder = 0;
        var totalPrice = 0;
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].CustomerId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].CustomerId + '"></td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].CustomerCode + '</a></td>';
            html += '<td>' + data[item].FullName + '</td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td>' + data[item].PhoneNumber + '</td>';
            html += '<td class="text-right">';
            if(data[item].TotalOrder != null){
                html += formatDecimal(data[item].TotalOrder);
                totalOrder += parseInt(data[item].TotalOrder);
            }
            html += '</td><td class="text-right">0</td>';
            html += '<td class="text-right">0</td>';
            html += '<td class="text-right">0</td>';
            html += '<td class="text-right">0</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].StatusId] + '">' + data[item].StatusName + '</span></td>';
        }
        html += '<tr><td colspan="11" class="paginate_table"></td></tr>';
        $('#tbodyCustomer').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}