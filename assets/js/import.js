$(document).ready(function () {
    actionItemAndSearch({
        ItemName: 'Nhập kho',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentImports(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditImport = $('#urlEditImport').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].ImportId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].ImportId + '"></td>';
            html += '<td><a href="' +  urlEditImport + data[item].ImportId  + '">' + data[item].ImportCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td>'+ data[item].StoreName +'</td>';
            html += '<td>'+ (data[item].SupplierName != null ? data[item].SupplierName : '') +'</td>';
            if(data[item].DeliverName != '') {
                if(data[item].DeliverPhone != '') html += '<td>' + data[item].DeliverName + ' (' + data[item].DeliverPhone + ')</td>';
                else html += '<td>' + data[item].DeliverName + '</td>';
            }
            else html += '<td>' + data[item].DeliverPhone + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ImportStatusId] + '">' + data[item].ImportStatus + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}